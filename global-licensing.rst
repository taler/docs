===========================
Taler licensing information
===========================

This file gives an overview of all Taler component's licensing and of runtime
dependencies thereof. A "component" in this context is to be read as a set of
source files which can be retrieved from a single Git repository.  If
components consist of sources under different licensing regimes, i.e.  because
we want to enable third party developments to easily integrate with Taler,
those are described as well.

All components are generally released under Lesser GPL, GPL or Affero GPL.
The main strategy is for libraries that third parties may need to integrate
with Taler to be under LGPL, standalone binaries and testcases to be under
GPL, and Web servers implementing Web services and REST APIs to be under AGPL.
Trivial code (such as build logic) is occasionally also simply put into the
public domain. Legislations that do not have a public domain should consider
that code to be under LGPL.

The project was initially started based on code and documentation from GNUnet
e.V. and INRIA, and is continued to be developed by the GNUnet community and
Taler Systems SA.  INRIA and GNUnet e.V. have both assigned or shared their
copyrights with Taler Systems SA.  Thus, it should generally be said that the
code is copyright Taler Systems SA.

We are also working closely with the GNU Anastasis package. Here, the same
copyright sharing is in place between the original developers from BFH and
GNUnet e.V., except that the copyright was assigned or shared with Anastasis
SARL. Agreements are in place that allows developers to freely move code
between GNU Anastasis, GNUnet and GNU Taler if this is technically warranted
(note that all developers must sign the GNUnet e.V. copyright contributor
agreement).

Note that this file *may* be outdated. If in doubt, please contact the
involved GNU maintainers (currently Florian Dold, Christian Grothoff and
Martin Schanzenbach) as they generally have the responsibility and right to
determine the appropriate license for individual components. (That right is
technically granted and constrained by the copyright agreemnts signed by
individual contributors and possibly subject to decision processes within
Anastasis SARL, GNUnet e.V. and Taler Systems SA.  However, in practice this
has so far never been an issue as so far everyone agrees on the licensing
strategy and the maintainers will just make minor adjustments to consistently
implement that strategy when issues arise, such as code moving between
LGPL, GPL and AGPL licensed components or developers accidentally using the
wrong license in a header.)


++++++++++++++++++
Following the AGPL
++++++++++++++++++

The main goal of the AGPL is to require a service provider to make the source
code of the service easily accessible to all users of that service.

For GNUnet, GNU Taler and GNU Anastasis, we are realizing this requirement by
implementing an ``/agpl/`` endpoint that directs users of the API to the
source code (either a TGZ or a redirect to the Git repository) of the
component and its dependencies (we believe it is acceptable to not include
unmodified dependencies that are easily available and findable elsewhere
already).  The source code (as defined in the GPL) must then be downloadable
gratis and without access control from that address in a form suitable for
development (that is, including build system and instructions).  All of the
usual constraints of distributing code under GPL apply as well.

When deploying sources that have been modified from the upstream versions
released by GNU, you *must* thus update the ``/agpl/`` endpoint to point to
the modified source code.  Note that you *must* always make the code of the
actually deployed version available, which includes customizations,
extensions, integrations or security enhancements or bug fixes.

.. note:

  We provide this text as a clarification, the ultimate
  license text that applies is that of the AGPLv3+.

+++++++++++++++++
Following the GPL
+++++++++++++++++

The GPL requires you to make the source code of the respective program
available to all users that may be running the program. This includes again
any customizations (branding, theming, extensions, integrations or security
enhancements or bug fixes) you may have made.  The source code for the
modified program must be again released under the GPL and must be reasonably
easy to find gratis for all users of your program.

When shipping binaries of *unmodified* versions of the GNU releases, this is
satisfied as GNU makes the sources available via both the Git repositories of
the project and the FTP mirrors of the GNU project.  You are still required to
make any packaging logic you may have written to create the binaries available
to others.

.. note:

  We provide this text as a clarification, the ultimate
  license text that applies is that of the GPLv3+.


++++++++++++++++++
Following the LGPL
++++++++++++++++++

To follow the LGPL, you basically do everything you need to do
under the GPL when it comes to the sources you were provided.

However, you are allowed to integrate the code provided under
the LGPL with other code that is not under the LGPLv3+.
Nevertheless, modifications to our existing code must
remain under the LGPLv3+, and we strongly encourage you
to release all of your code under an appropriate FLOSS
license.

.. note:

  We provide this text as a clarification, the ultimate
  license text that applies is that of the LGPLv3+.



++++++++++++++++++++++++++++++
API (git://git.taler.net/docs)
++++++++++++++++++++++++++++++

The specification has been originally developed by INRIA and by individuals
being under the juridical subject called 'GNUnet e.V.'. For each source
file, the header indicated whose is holding the copyright, since some
parts have been taken "verbatim" from the GNUnet e.V. foundation, and
some other have been developed at INRIA or Taler Systems SA "ex novo".

Generally, GNU GPLv3 license is used for them; see COPYING.GPL.


--------------------
Runtime dependencies
--------------------
This component has no runtime dependencies as it is supposed to generate
HTML.


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Firefox/Android/Python Wallet (git://git.taler.net/wallet-core)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

This project includes contributions from INRIA and GNUnet
e.V. developers.  Please refer to each source file to obtain
information about the copyright holder. The GNU GPLv3 is used as the
license for Wallets.  Some components may be under the LGPL.

--------------------
Runtime dependencies
--------------------

The following list encompasses all the runtime dependencies for this
project, and gives the copyright holder for each of them:

* libgnunetutil: GPLv3+, GNUnet e.V.
* libgnunetjson: GPLv3+, GNUnet e.V.
* libgcrypt: LGPL, Free Software Foundation
* libunistring: LGPL, Free Software Foundation
* Python:   Python Software Foundation License, LGPL-Compatible, Python Software Foundation
* Mozilla Firefox:   Mozilla Public License, LGPL-Compatible, Mozilla Foundation


+++++++++++++++++++++++++++++++++++++++++++++++++++++++
WebExtensions Wallet (git://git.taler.net/wallet-webex)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++

The TypeScript code was originally developed at INRIA, but the project
involves compiling libgnunetutil and libtalerutil to JavaScript, and
thus depends on software from GNUnet e.V.

Each source carries its own copyright holder(s), but it is generally
licensed under GPLv3+.

--------------------
Runtime dependencies
--------------------

The following list encompasses all the runtime dependencies for this
project, and gives the copyright holder for each of them:

* libgnunetutil: GPLv3+, GNUnet e.V.
* libgnunetjson: GPLv3+, GNUnet e.V.
* libgcrypt: LGPL, Free Software Foundation
* libunistring: LGPL, Free Software Foundation

Note that these dependencies are compiled into the extension and do
not appear as separate binary files.


+++++++++++++++++++++++++++++++++++++++
Merchant (git://git.taler.net/merchant)
+++++++++++++++++++++++++++++++++++++++

This project contains code under two different licenses, and whose
copyright is held by INRIA and/or GNUnet e.V..  Please refer to each
source file to know which party holds the original copyright.

Source files are located in the following directories:

* src/lib/
* src/backend/
* src/backenddb/
* src/include/
* src/tests/
* examples/blog/
* examples/shop/
* copylib/

The merchant's backend (i.e. all the code in src/backend/) is under the GNU
Affero GPL.  Note that the use of the Affero GPL has little impact as the
backend is not supposed to be directly accessible to the Internet).  The
license for this code is in COPYING.GPL and COPYING.AGPL.

The merchant's frontend logic (i.e. JavaScript interacting with
the wallet, sample code for a shop) is under the GNU GPL.

The merchant library (src/lib/) is under the Lesser GNU GPL as it can be
linked with more diverse licensed software.  The license text for this code is
in COPYING.LGPL.

In taler-merchant-demos.git/talermerchantdemos/blog/articles/ we included a book
by Richard Stallman.  It comes with its own permissive license (see COPYING in
the directory).



--------------------
Runtime dependencies
--------------------

The following list encompasses all the runtime dependencies for this
project, and gives the copyright holder for each of them:

* libjansson: MIT License, AGPL- and LGPL-Compatible, owned by Petri Lehtinen and other individuals
* libgcrypt: LGPL, owned by Free Software Foundation
* postgresql: PostgreSQL License, AGPL- and LGPL-Compatible, owned by The PostgreSQL Global Development Group
* libgnunetutil (in all of its variants): GPLv3+, owned by GNUnet e.V.
* PHP:  PHP License, AGPL- and LGPL-Compatible, owned by The PHP Group

+++++++++++++++++++++++++++++++++++++++
Libeufin (git://git.taler.net/libeufin)
+++++++++++++++++++++++++++++++++++++++

---------
Licensing
---------

This project has been developed by Stanisci and Dold who are
sharing their copyright with Taler Systems SA.  For each source file, the
header indicated whose is holding the copyright.  The license is
the Affero GPLv3+.

--------------------
Runtime dependencies
--------------------

The following list encompasses all the runtime dependencies for this
project, and gives the copyright holder for each of them:

* Django:   BSD License, AGPL-Compatible, owned by Django Software Foundation
* validictory:   BSD License, AGPL-Compatible, owned by James Turk
* django-simple-math-captcha:   Apache Software License, LGPL-Compatible (FIXME), Brandon Taylor
* requests:   Apache2 License, AGPL-Compatible, owned by Kenneth Reitz
* Python:   Python Software Foundation License, AGPL-Compatible, Python Software Foundation
* PHP:   PHP License, AGPL-Compatible, owned by The PHP Group


.. _exchange-repo:

+++++++++++++++++++++++++++++++++++++++
Exchange (git://git.taler.net/exchange)
+++++++++++++++++++++++++++++++++++++++

This component is based on code initially developed in Munich for GNUnet
e.V. but was significantly rewritten by INRIA. Most recent improvements and
maintenance has been done at Taler Systems SA and BFH (under a copyright
sharing agreement with GNUnet e.V.).  The copyright is with Taler Systems SA.

The licensing for exported libraries to access the exchange is LGPL,
the exchange itself is under AGPL, and testcases and standalone
binaries are under GPL.


--------------------
Runtime dependencies
--------------------

The following list encompasses all the runtime dependencies for this
project, and gives the copyright holder for each of them:

* libjansson: MIT License, AGPL- and LGPL-Compatible, owned by Petri Lehtinen and other individuals
* libgcrypt: LGPL, owned by Free Software Foundation
* libsodium: LGPL
* postgresql: PostgreSQL License, AGPL- and LGPL-Compatible, owned by The PostgreSQL Global Development Group
* libgnunetutil (in all of its variants): GPLv3+, owned by GNUnet e.V.
* libgnunetjson: LGPLv3+, GNUnet e.V.
* libgnunetpq: AGPLv3+, GNUnet e.V.
* libgnunetcurl: LGPLv3+, GNUnet e.V.


+++++++++++++++++++++++++++++++++++++++++++++
Web includes (git://git.taler.net/web-common)
+++++++++++++++++++++++++++++++++++++++++++++

All copyright owned by INRIA (but questionable whether creativity
threshold for copyright is even met).

Sources are licensed under the GNU LGPL.

+++++++++++++
Documentation
+++++++++++++

The documentation is licensed under the GNU Free Documentation License Version 1.3 or later.
