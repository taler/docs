################################
GNU Taler Demo Upgrade Checklist
################################

.. |check| raw:: html

    <input type="checkbox">

Domains
-------

The checklist uses the ``demo.taler.net`` domains.  However,
the same sandcastle demo can also be hosted at other domains.
The same instructions should apply.

Post-upgrade checks
-------------------

- |check| Run the headless wallet to check that services are actually working:

  .. code-block:: console

    taler-wallet-cli api 'runIntegrationTestV2' '{"exchangeBaseUrl":"https://exchange.demo.taler.net", "corebankApiBaseUrl": "https://bank.demo.taler.net", "merchantBaseUrl": "https://backend.demo.taler.net", "merchantAuthToken":"secret-token:sandbox"}'


Wallets
-------

We consider the following published wallets to be "production wallets":

* Browser: Firefox Add-On Store
* Browser: Chrome Web Store
* Android: Google Play
* Android: F-Droid
* iOS: Apple Store / Testflight

Basics
------

- |check| Visit https://demo.taler.net/ to see if the landing page is displayed correctly
- |check| landing language switcher
- |check| Visit the wallet installation page, install the wallet
- |check| see if the wallet presence indicator is updated correctly (in browsers).
- |check| Visit https://bank.demo.taler.net/, register a new user
- |check| bank language switcher
- |check| bank logout
- |check| bank login
- |check| bank-integrated withdraw process, abort in bank
- |check| transaction history: delete pending withdraw
- |check| do bank-integrated withdraw process (5 KUDOS)
- |check| do wallet-initiated withdraw process (5 KUDOS)
- |check| withdraw process of large amount (20 KUDOS) runs into KYC check
- |check| fail KYC check (if possible for the given setup)
- |check| pass KYC check (tests that 2nd attempt is possible)
- |check| withdraw process of very large amount (50 KUDOS) runs into AML check
- |check| visit exchange SPA, create AML officer key
- |check| register AML officer key with offline tool (if possible)
- |check| allow withdraw process blocked on AML to proceed (if possible)


Exchange AML SPA
----------------

- |check| enter non-trivial form, change status to frozen
- |check| check account status in history is now frozen and shows in that category
- |check| enter another form, change status to normal, increase AML threshold
- |check| view forms in history, view previously submitted form
- |check| check account status in history is now normal and shows in that category
- |check| log out
- |check| check log in succeeds with correct password
- |check| check log in fails from different browser with same password


Blog demo
---------

- |check| Visit https://shop.demo.taler.net/
- |check| blog page article list renders
- |check| payment for blog article
- |check| Verify that the balance in the wallet was updated correctly.
- |check| Go back to https://shop.demo.taler.net/ and click on the same article
  link.  Verify that the article is shown and **no** repeated payment is
  requested.
- |check| Open the fulfillment page from the previous step in an anonymous browsing session
  (without the wallet installed) and verify that it requests a payment again.
- |check| Delete cookies on https://shop.demo.taler.net/ and click on the same article again.
  Verify that the wallet detects that the article has already purchased and successfully
  redirects to the article without spending more money.
- |check| payment for other blog article
- |check| refund of 2nd blog article (button at the end)
- |check| wallet transaction history rendering
- |check| delete refund history entry; check original purchase entry was also deleted
- |check| payment for other blog article
- |check| refund of 3rd blog article (button at the end)
- |check| wallet transaction history rendering
- |check| delete 3rd block purchase history entry; check refund entry was also deleted


Donation demo
-------------

- |check| Reset wallet
- |check| Withdraw age-restricted coins (< 14)
- |check| Try to make a donation on https://donations.demo.taler.net/, fail due to age-restriction
- |check| Withdraw age-restricted coins (>= 14)
- |check| Make a donation on https://donations.demo.taler.net/
- |check| Make another donation with the same parameters and verify
  that the payment is requested again, instead of showing the previous
  fulfillment page.


Merchant SPA
------------

- |check| test SPA loads
- |check| try to login with wrong password
- |check| try to login with correct password
- |check| create instance, check default is set to cover (STEFAN) fees
- |check| modify instance
- |check| add bank account
- |check| edit bank account
- |check| remove bank account
- |check| check order creation fails without bank account
- |check| add bank account again
- |check| add product with 1 in stock and preview image
- |check| add "advanced" order with inventory product and a 2 minute wire delay
- |check| claim order, check available stock goes down in inventory
- |check| create 2nd order, check this fails due to missing inventory
- |check| pay for 1st order with wallet
- |check| check transaction history for preview image
- |check| trigger partial refund
- |check| accept refund with wallet
- |check| create template with fixed summary, default editable price
- |check| scan template QR code, edit price and pay
- |check| add TOTP device (using some TOTP app to share secret with)
- |check| edit template to add TOTP device, set price to fixed, summary to be entered
- |check| scan template QR code, edit summary and pay
- |check| check displayed TOTP code matches TOTP app
- |check| create reserve for rewards
- |check| do manual wire transfer in bank to establish reserve funding
- |check| check that partially refunded order is marked as awaiting wire transfer
- |check| check bank wired funds to merchant (if needed, wait)
- |check| add bank wire transfer manually to backend
- |check| change settings for merchant to not pay for (STEFAN) fees
- |check| create and pay for another order with 1 minute wire transfer delay
- |check| edit bank account details, adding revenue facade with credentials
- |check| wait and check if wire transfer is automatically imported
- |check| check that orders are marked as completed


Survey/Rewards
--------------

- |check| Visit https://survey.demo.taler.net/ and receive a reward.
- |check| Verify that the survey stats page (https://survey.demo.taler.net/survey-stats) is working,
  and that the survey reserve has sufficient funds.


P2P payments
------------

- |check| generating push payment (to self is OK)
- |check| accepting push payment (from self is OK)
- |check| generating pull payment (to self is OK)
- |check| accepting pull payment (from self is OK)
- |check| sending money back from wallet to bank account
- |check| wallet transaction history rendering
- |check| delete history entry


Shutdown
--------

- |check| create two full wallets, fill one only via (a large) P2P transfer
- |check| revoke highest-value denomination
- |check| spend money in a wallet such that the balance falls below highest denomination value
- |check| revoke all remaining denominations
- |check| fail to spend any more money
- |check| if wallet was filled via p2p payments, wallet asks for target deposit account (exchange going out of business)
- |check| enter bank account (if possible)
- |check| wallet balance goes to zero
- |check| specified bank account receives remaining balance

