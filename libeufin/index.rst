LibEuFin
########

LibEuFin is a project providing free software tooling for European FinTech.

.. toctree::
  :maxdepth: 1
  :glob:

  nexus-manual
  bank-manual
  regional-manual
  setup-ebics-at-postfinance
