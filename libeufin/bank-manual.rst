.. target audience: operator, developer

Bank Setup Manual
#################

.. contents:: Table of Contents

LibEuFin Bank implements a simple core banking system with
account and REST APIs, including REST APIs for a Web interface
and REST APIs to interact with GNU Taler components.

In this manual, we explain how to setup a bank.

Installing LibEuFin Bank
========================

The following section was tested on an *OpenJDK 17* environment.

Building from source
--------------------

Bank belongs to the LibEuFin project, and can be downloaded via Git:

.. code-block:: console

  $ git clone git://git.taler.net/libeufin

Note that Kotlin and Gradle should already work on the host system.

Navigate into the *libeufin* local repository, and from top-level run:

.. code-block:: console

  $ ./bootstrap
  $ ./configure --prefix=$PREFIX
  $ make install-bank

If the previous steps succeeded, the ``libeufin-bank`` command should
be found in the $PATH.

Setting up the LibEuFin Bank
============================

The following snippet shows the mandatory configuration values :

.. code-block:: console

  [libeufin-bank]
  CURRENCY = KUDOS

  # Debt limit
  DEFAULT_ADMIN_DEBT_LIMIT = KUDOS:200000000

  # Server
  SERVE = tcp
  PORT = 8080

  [libeufin-bankdb-postgres]
  CONFIG = postgresql:///libeufinbank

.. note::
  Refer to the manpage ``libeufin-man.conf(5)``
  for the full array of configuration values.

Assuming that the configuration file exists at ``$config_file``, the following
command would define the database schema and create an admin account.

.. code-block:: console

  libeufin-bank dbinit -c $config_file

If you wish to authenticate as admin, you must change the account password with the following command.

.. code-block:: console

  libeufin-bank passwd -c $config_file admin $PASSWORD

Then you can start the HTTP server.

.. code-block:: console

  libeufin-bank serve -c $config_file

Enable regional currency conversion
===================================

Prerequisites
-------------

This step assumes that you already have a working regional currency bank
and have successfully connected to a backing fiat bank account. 
Follow the instructions from :ref:`EBICS subscriber setup <ebics-setup>`.

Configuration
-------------

Then you have to enable conversion and at least one TAN channel for cashout in the configuration file.

.. code-block:: console

  [libeufin-bank]
  ALLOW_CONVERSION = yes
  FIAT_CURRENCY = EUR

  TAN_SMS = libeufin-tan-sms.sh
  # And/Or
  TAN_EMAIL = libeufin-tan-email.sh


You must have an exchange account with username ``exchange`` for conversion to work. 
Assuming that the configuration file exists at ``$config_file``, the following
command would create one: 

.. code-block:: console
  
  libeufin-bank create-account '{"username":"exchange","password":"password","name":"Cashout Exchange","is_taler_exchange":true}' -c $config_file

Then the following command would start the server with conversion API enabled :

.. code-block:: console

  libeufin-bank serve -c $config_file

Web-based Configuration
-----------------------

Now you should be able to setup conversion rates though the Web
interface of the bank as the ``admin`` user.

Conversion ON!
--------------

The last step is to run Nexus to import incoming bank
transactions (cash in) and to trigger outgoing bank transactions (cash out).
Refers to ``nexus-manual.conf`` for more details.