.. target audience: operator, developer

Nexus Manual
############

.. contents:: Table of Contents

LibEuFin Nexus is an EBICS facilitator.  It offers a command line
interface to setup EBICS access, download banking records, and submit payments.
Future versions will offer a Web API to allow Taler Exchanges to talk to their
banks.

In this manual, we explain how to setup an EBICS subscriber.  We assume that
the bank had already granted EBICS access to the subscriber.

Installing Nexus
================

The following section was tested on an *OpenJDK 17* environment.

Building from source
--------------------

Nexus belongs to the LibEuFin project, and can be downloaded via Git:

.. code-block:: console

  $ git clone git://git.taler.net/libeufin

Note that Kotlin and Gradle should already work on the host system.

Navigate into the *libeufin* local repository, and from top-level run:

.. code-block:: console

  $ ./bootstrap
  $ ./configure --prefix=$PREFIX
  $ make install-nexus 

If the previous steps succeeded, the ``libeufin-nexus`` command should
be found in the $PATH.

.. _ebics-setup:

Setting up the EBICS subscriber
===============================

When you sign up for an EBICS-enabled bank account, the bank will provide you
with various credentials. Those must be provided in the
``/etc/libeufin/libeufin-nexus.conf`` configuration file together with the
name of the *fiat* currency.

The following snippet shows the mandatory configuration values:

.. _core-config:

.. code-block:: console

  [nexus-ebics]
  CURRENCY = CHF

  # Bank
  HOST_BASE_URL = http://bank.example.com/
  BANK_DIALECT = postfinance

  # EBICS IDs
  HOST_ID = mybank
  USER_ID = myuser
  PARTNER_ID = myorg

  # Key files
  BANK_PUBLIC_KEYS_FILE = ${LIBEUFIN_HOME}/bank-keys.json
  CLIENT_PRIVATE_KEYS_FILE = ${LIBEUFIN_HOME}/subscriber-keys.json

  # Account information
  IBAN = myiban
  BIC = mybic
  NAME = myname

.. note::
  Refer to the manpage ``libeufin-nexus.conf(5)``
  for the full array of configuration values.

Assuming that the configuration file exists at ``$config_file``, the following
command would start the EBICS setup process.  The files CLIENT_PRIVATE_KEYS_FILE
and BANK_PUBLIC_KEYS_FILE would be created at the CWD.  Adjust their path to your
setup ('$HOME' is currently not supported along paths).

.. code-block:: console

  libeufin-nexus ebics-setup -c $config_file

If the previous command succeeded, the subscriber keys reached the bank, but the setup
**should** fail with an ``EBICS_INVALID_USER_STATE`` error code.  That happens because
the client tries to download the bank keys *before* having confirmed the subscriber keys
via the traditional post service.

To that purpose, the previous run should have left a PDF document that the subscriber can
print, sign, and send to the bank to confirm their subscriber keys.  Look for the message
looking like ``PDF file with keys hex encoding created at: /tmp/libeufin-nexus-keys-$timestamp.pdf``.

Once the bank received and approved such printed document, run the same command again, in
order to download the bank keys and let the user accept them.

.. code-block:: console

  libeufin-nexus ebics-setup -c $config_file

The setup is considered finished once the user accepts the bank keys.

Sending payments
================

Sending payments must follow a successful `EBICS subscriber setup <ebics-setup>`_,
where the bank obtained the subscriber keys, and the subscriber accepted the
bank keys.  The responsible subcommand for sending payments is ``ebics-submit``,
and its configuration is a **superset** of core-config_.  On top of that, it
expects the database connection string, and *optionally* a frequency to check
for submittable payments in the database.

The connection string is specified as

.. code-block:: console
  
  [nexus-postgres]

  config = postgres:///nexus

The frequency **may** be specified as

.. code-block:: console
  
  [nexus-submit]
  frequency = 5m
  # Optional, but useful to check what Nexus sends to the bank
  submissions_log_directory = $LIBEUFIN_DATA_HOME/uploads

The supported time units are ``s`` (seconds), ``m`` (minutes), ``h`` (hours).

Before delving into the following sections, the database schema must be
created, therefore run the following command:

.. code-block:: console

  libeufin-nexus dbinit -c $config_file

Where ``$config_file`` is the path to a configuration path that contains at
least the ``[nexus-postgres]`` section.

For testing
-----------

The ``ebics-submit`` subcommand is **not** suitable to send arbitrary
payments, but rather to submit initiated payments that may be found
in the database.

Such initiated payments may be refunds of incoming payments with a subject
that would be invalid for a Taler withdrawal, or from a Taler exchange in
the attempt to pay a merchant.

For testing purposes, however, it is possible to artificially initiate
a payment into the database with the ``contrib/payment_initiation_debug.sh``
script, found in the libeufin repository.  The script pays always one Swiss
Franc to an IBAN and subject pair of choice.

The following invocation pays 1 CHF to the CH987654321 with an "Hello, Nexus!"
subject.

.. code-block:: console

  ./payment_initiation_debug.sh $config_file CH987654321 "Hello, Nexus!"

``$config_file`` is the path to Nexus' configuration file and it does not need
the FREQUENCY value.  If the previous command worked, now the database should
contain a new row in the ``initiated_outgoing_transactions`` table, with an
``unsubmitted`` submission state.

The following command would now pick such an unsubmitted initiated payment and
send it to the bank.


.. code-block:: console

  libeufin-nexus ebics-submit -c $config_file --transient

The ``--transient`` flag instructs the software to perform only one pass on
the database, submit what is submittable, and return.

Alternatively, the ``--debug`` flag makes ebics-submit accept data from STDIN,
and submit it to the bank.

For production
--------------

The ``ebics-submit`` subcommand can run in fixed frequency, or transient mode.

The fixed frequency mode causes the command to check the database every time the
frequency period expires.  To activate this mode, and -- for example -- set a frequency
of five minutes, set the configuration value ``FREQUENCY`` to ``5m``.  Assuming that
``$config_fixed_frequency`` is set as described above, the following invocation would
make ``ebics-submit`` check the database every five minutes, and submit any initiated
payment according to their submission state.

.. code-block:: console

  libeufin-nexus ebics-submit -c $config_fixed_frequency


Transient mode causes ``ebics-submit`` to check the database only once, submit any
initiated payment according to their submission state, and return.

.. code-block:: console

  libeufin-nexus ebics-submit -c $config

Downloading records
===================

Downloading records must follow a successful `EBICS subscriber setup <ebics-setup>`_,
where the bank obtained the subscriber keys, and the subscriber accepted the bank keys.

The configuration file must contain all the ``[ebics-setup]`` sections, plus the following
values to set the database connection and a log directory where any downloaded record would
be stored.

.. code-block:: console
  
  [nexus-postgres]
  config = postgres:///nexus
  [nexus-fetch]
  # Optional, but usfeful against data loss.
  statement_log_directory = $LIBEUFIN_DATA_HOME/downloads

Assuming that ``$config_file`` contains any required option, the following command
would download any unseen notifications (as camt.054 files).

.. code-block:: console

  libeufin-nexus ebics-fetch -c $config_file --transient

The ``--transient`` flag makes the command to download only once and return.  If the
bank returned any records, look in ``$LIBEUFIN_DATA_HOME/camt/YYYY-MM-DD`` to find them.
YYYY-MM-DD is the date when the download took place.

To activate periodic downloads, add the following setting to the ``nexus-fetch``
configuration section in ``$config_file``:

.. code-block:: console

   frequency = 5m

The following invocation would then download records every five minutes, asking the bank
to only return documents that are timestamped *from* (inclusive) the last incoming transaction.

.. code-block:: console

  libeufin-nexus ebics-fetch -c $config_file

Testing
-------

If the bank account history has transactions that are useful for testing, it is
possible to redownload them via the ``--pinned-start`` argument.  Note: if the Nexus
database contains already the bank account history, you might need to **reset** it,
in order to have the redownloaded transactions ingested.

.. note::

   double-check the configuration: resetting the wrong database might cause DATA LOSS!

For example, say that three back on the 2023-06-01 an handful of incoming transactions
got booked at the bank account watched by Nexus.  After such day, the following command
would redownload them (and ingest them, if the database doesn't have them):

.. code-block:: console

  libeufin-nexus ebics-fetch -c $config_file --pinned-start 2023-06-01

Note: the command redownloads transactions *from* the pinned start until the present.
