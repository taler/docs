DD XY: Template
###############

Summary
=======

Motivation
==========

Requirements
============

Proposed Solution
=================

Definition of Done
==================

(Only applicable to design documents that describe a new feature.  While the
DoD is not satisfied yet, a user-facing feature **must** be behind a feature
flag or dev-mode flag.)

Alternatives
============

Drawbacks
=========

Discussion / Q&A
================

(This should be filled in with results from discussions on mailing lists / personal communication.)
