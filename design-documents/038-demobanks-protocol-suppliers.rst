XX 38: Demobanks protocol suppliers
###################################

Summary
=======

This document models the association between financial data
held in a LibEuFin *demobank* and the interface to let users
access such financial data.

Motivation
==========

LibEuFin Sandbox offers multitenency banking by the means of
'demobanks'.  Each demobank offers access to financial data via
*several* APIs.  The objective is to model such APIs so that each
operation impacts one and only one demobank.

Definitions
===========

Each API to access financial data at one demobank is offered by
a **resource** called *protocol supplier*.  Therefore protocol
suppliers MAY be subject to all the CRUD operations

For each request that a protocol supplier serves, the demobank
being impacted can be found in the following ways:

.. _demobank-mutually-exclusive:

1.  In a value that belongs to the request.
2.  In a value that belongs to the protocol supplier's state.
3.  Relying on the default demobank.

Note: the three elements are mutually exclusive, so as to reduce
ambiguity and simplify the implementation.

Suppliers creation
==================

Suppliers can be static or dynamic.

Static
^^^^^^

This supplier never changes its state.  Whether this type
of supplier is associated or not with a particular demobank
MUST be stated in the documentation.

Examples
--------

1.  A JSON-based protocol that lets users access their bank accounts
always under the 'default' demobank belongs to this category.  It
is therefore a 'static protocol supplier' with static demobank.

2.  A XML-based protocol that lets users access their bank accounts
in a demobank whose name appear in the URI is as well a 'static protocol
supplier' with dynamic demobank.

Note: the upcoming (in version 0.9.3) JSON-based supplier that will
let Nexus reach Sandbox accounts is planned as a 'dynamic protocol
supplier' with dynamic demobank.  That allows Taler demos to only
speak JSON.

Dynamic
^^^^^^^

This supplier has a name and its state CAN refer to one
particular demobank.  These suppliers need to be created
first, in order to be used.

Examples
--------

1.  A JSON-based protocol that lets users access their bank
accounts under the demobank whose name is held in the supplier
state belongs to this category.  It is therefore a dynamic
supplier with semi-dynamic demobank.

2.  A XML-based protocol that lets user access their bank
accounts under the demobank whose name is held both in the
supplier state *and* in the URI is **wrong**.  This supplier
doesn't respect this `mutual exclusivity <demobank-mutually-exclusive_>`_.

3.  A XML-based protocol that lets user access their bank accounts
always under the 'default' demobank belongs to this category.  It
is a dynamic supplier with static demobank.

Supplier reachability
=====================

Each supplier must be available under its own URI.


Current protocol suppliers design
=================================

Static X-LIBEUFIN-BANK with dynamic demobank
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``x-libeufin-bank`` protocol supplier is reachable under
``/demobanks/{demobankName}/access-api/``.  As the path suggests,
it offers banking operations via the :doc:`Core Bank API </core/api-corebank>`.
It is static in the sense that it's not possible to assign a name
to one particular x-libeufin-bank protocol supplier.  On the other
hand, the demobank is dynamic because can be specified along the path
in the ``demobankName`` placeholder.

Dynamic EBICS supplier with dynamic demobank
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Every protocol supplier of this type is reachable under ``POST /ebicsweb``
and by specifying its EBICS host name inside the EBICS message.

As of the internal representation, Sandbox keeps a database table called
``EbicsHostsTable`` that does **not** point at any demobank.  Such table
is the one that provides the bank's EBICS private keys and has **no** business
implications.

CCT (Payment initiations)
-------------------------

This handler gets the bank account directly from the IBAN that was
carried along the pain.001, therefore -- as long as every IBAN is
unique -- this works with **any** demobank that hosts such IBAN.  The
EBICS subscriber public keys are extracted differently: they come from
the tuple [userID, partnerID, systemID?] held in the request.  Hence as
long as such tuple is unique for each subscriber (Sandbox checks that),
even the subscriber public keys are found regardless of the demobank name.

.. note::

   The 'context' object found via the [userID, partnerID, systemID?] tuple
   has **also** a reference to the bank account.  The consistency with the
   other bank account reference returned by the IBAN is currently NOT checked.

C52 (transactions report)
-------------------------

This handler gets the reference to the subscriber public keys and bank
account via the [userID, partnerID, systemID?] tuple.  It then uses
this bank account label to find the transactions that belong to the
subscriber that made the request.

.. note::

   The current implementation does NOT uses any demobank name along
   the transactions research: only the bank account label.  This can
   lead to **ambiguity**, in case two different demobanks host respectively
   one bank account under the same label.  This is not possible however
   in the current version, as Sandbox only admits one ``default`` demobank.


Alternatives
============

Drop support for multitenancy banking. What is the benefit of this anyway?
