DD 29: Mobile P2P UI
####################

Summary
=======

Design the ui and interaction of p2p payments using mobile

Motivation
==========

The wallet user should be able to easily send and receive money to and from other wallet.

Requirements
============

 * It should work even if the user does not have other communication channel
 * Use the swipe up (to send) and swipe down (to request) gesture as other apps do
 * It should be able to request payment before any withdraw (currency still unknown)
 * It should support multi-device
 * The user may opt-in to be findable through a registry service

Proposed Solution
=================

.. image:: ../images/wallet-mobile-overview.svg
  :width: 800


Alternatives
============

Drawbacks
=========



Discussion / Q&A
================

(This should be filled in with results from discussions on mailing lists / personal communication.)
