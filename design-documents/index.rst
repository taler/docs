Design Documents
################

This is a collection of design documents related to GNU Taler.
The goal of these documents is to discuss facilitate discussion around
new features while keeping track of the evolution of the whole system
and protocol.

Design documents that start with "XX" are considered deprecated.

.. toctree::
  :maxdepth: 1
  :glob:

  001-new-browser-integration
  002-wallet-exchange-management
  003-tos-rendering
  004-wallet-withdrawal-flow
  005-wallet-backup-sync
  006-extensions
  007-payment
  008-fees
  009-backup
  010-exchange-helpers
  011-auditor-db-sync
  012-fee-schedule-metrics
  013-peer-to-peer-payments
  014-merchant-backoffice-ui
  015-merchant-backoffice-routing
  016-backoffice-order-management
  017-backoffice-inventory-management
  018-contract-json
  019-wallet-backup-merge
  020-backoffice-rewards-management
  021-exchange-key-continuity
  022-wallet-auditor-reports
  023-taler-kyc
  024-age-restriction
  025-withdraw-from-wallet
  026-refund-fees
  027-sandboxing-taler.rst
  028-deposit-policies
  029-mobile-ui
  030-offline-payments
  031-invoicing
  032-brandt-vickrey-auctions
  033-database
  034-wallet-db-migration
  035-regional-currencies
  036-currency-conversion-service
  037-wallet-transactions-lifecycle.rst
  038-demobanks-protocol-suppliers.rst
  039-taler-browser-integration.rst
  040-distro-packaging.rst
  041-wallet-balance-amount-definitions.rst
  042-synthetic-wallet-errors.rst
  043-managing-prebuilt-artifacts.rst
  044-ci-system.rst
  045-kyc-inheritance.rst
  046-mumimo-contracts.rst
  047-stefan.rst
  048-wallet-exchange-lifecycle.rst
  049-auth.rst
  050-libeufin-nexus.rst
  051-fractional-digits.rst
  999-template
