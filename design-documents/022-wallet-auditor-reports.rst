DD 22: Wallet Proofs to Auditor
###############################

.. note::

   Status (2021-05-25): Writing in progress.


Summary
=======

This design document defines the structure and contents of proofs
of misbehavior that the wallet sends to auditors.

Motivation
==========

There are some situations where the wallet learns that some entity did
something against the protocol specification.  When the wallet has
cryptographic proof for this, this proof should be stored in the database and
eventually be exportable to auditors, courts, etc.

Requirements
============

* Users should be able to review all the information that
  a misbehavior proof would reveal.

Proposed Solution
=================

Types of Misbehavior
--------------------

This section collects all types of misbehavior for which the wallet
can export cryptographic proof.

* ``exchange-denomination-spec-inconsistent``

  An exchange has announced a denomination with the same
  denomination public key, but different metadata (value, expiration)

* ``exchange-denomination-gone``

  The exchange is not accepting/listing a denomination
  anymore that it previously listed.


Discussion / Q&A
================

* What about complaints to the auditor that do not contain
  cryptographic proof?  (e.g. "exchange XYZ has not been responding
  for the last 14 days")
