DD 12: Exchange Fee Configuration
#################################

.. note::

  This document is a draft.

Summary
=======

This design document discusses considerations for configuring the fee
structure of an Exchange from different points of view (Exchange operators,
buyers/users, and sellers/merchants).


Background
==========

Fees are necessary for covering costs that non-governmental Exchange operators
bear for offering their services established in-house or outsourced to a data
center: variable costs (e.g. electricity and wire fees for every wired
transfer to bank accounts) and fixed-cost expenditures for hardware, company
assets, marketing and staff, and so forth. Fees enable operators to pass on
these costs to users.

The Taler protocol offers different types of fees for the different operations
performed by the exchange. This enables the Exchange operator to adapt fees to
closely relate to operational expenses.  Fee types and their underlying
metrics not only to cover operational expenses, but also to provide incentives
to users to adopt economic behaviours.  In particular, they can be used to
ensure that attackers which try to overwhelm the Exchange infrastructure with
unusually large numbers of transactions proportionally contribute to the
Exchange's infrastructure budget.

There are six fee types available for configuration by the Exchange operator:

1. **Withdraw**: For each successful withdrawal from the checking account, **per coin**
2. **Deposit**: For spending, **per coin**
3. **Refresh**: **Per coin** for
    a. Refresh transactions for receiving change
    b. Refresh of coins at the end of their validity
    c. Abort of transactions due to network failure
    d. Refund
4. **Refund**: For refunds or in case of contract cancellation by seller, **per coin**
5. **Wire**: For aggregated amounts wired by the Exchange to the merchant's checking account, **per wire transfer**
6. **Closing**: In case a withdraw process did not complete (the user's wallet did not withdraw the value from the reserve) or could not complete because the user specified an invalid wire transfer subject, this fee is charged **per wire transfer** from the Exchange's escrow account to the account of origin

When withdrawing coins, **withdraw** fees are deducted from the respective
reserve.  When a coin is **deposited**, **refreshed** or **refunded**, the
respective fees are deducted from the coin's value depending on the type of
operation that was performed.  The **wire** and **closing** fees are deducted
from the total amount that is being wired.

Exchange operators must configure a combination of fee amounts for each
denomination type and each supported wire method.  For each denomination type,
the operator must configure the four fee amounts for the per coin operations.
These denomination fees are valid for the lifetime of the denomination type:
The protocol does not allow retroactive changes for denomination keys that
have already been announced.  This protects buyers against fee hikes for
coins they already withdrew.  Additionally, the **wire** and **closing** fees
that apply per wire transfer must be configured for each wire method. These
fees are typically defined per calendar year.

For deposit, withdraw, refresh and refund operations, fees are charged per
coin. The number of coins per operation increases logarithmically with the
amount transacted. The fees set for a denomination may differ depending on the
time of issuance of a coin (that is, whenever the public key changes) and may
also depend on the specific value of a coin.

Once an Exchange operator has configured specific fees, the Taler
implementation will communicate the fees to new users upon withdrawal and
henceforth charge fees on those newly withdrawn coins automatically.

Most fees are covered directly by the buyers, with two exceptions.
For **deposit** fees, sellers may offer to cover deposit fees.
For this, the seller can configure (per sale) a maximum amount of
deposit fees the seller is willing to cover. Those deposit fees are
then deducted from the seller's income from the sale by the exchange.
Additionally, sellers cover the full **wire** fee, as the wire fee
is subtracted from the aggregated amounts wired to a seller.

For the **wire** fee, we note that deposits are aggregated by the exchange and
wired to the receiving checking account of the seller in larger bulk wire
transfers. Sellers can set the frequency by which these aggregated amounts are
wired. Every wire transfer imposes costs on the Exchange operator collected by
the operator's bank for having the amount wired. Therefore, the Exchange
operator will tend to charge the **Wire** fee to the sellers for this
transaction type, as the sellers are the ones causing the aggregated transfer
and not the buyers.

If from a seller's point of view an Exchange operator has set the **wire** fee
too high, the seller again impose a limit and ask buyers to cover the
difference. Given that typically multiple purchases will be aggregated into
one wire transfer, the seller can specify a so-called amortization factor to
divide the (excessive) wire fee amongst a number of buyers using the same
(expensive) Exchange.

During the withdraw process, the wallet shows to the buyer the complete fee
schedule and indicates the **Wire** and **closing** fees. However, if a seller
takes over the wire fee charge instead of the buyers, the buyers'
wallets will no longer show a wire fee for that seller. These sellers thus
render the fee schedule clearer for their buyers, but certainly will have
the wire fee calculated with their sales prices.

.. note::

   The 'Recoup' operation does not allow Exchange operators to set any fee
   amount, because reimbursing funds from an Exchange that is about to cease its
   activity is not an action initiated or controlled by the user, and thus the
   Taler designers decided that it must always be at zero cost to the user.


Motivation
==========

Choosing a fee structure for an Exchange needs to satisfy several
conflicting criteria:

* Fees chosen by Exchange operators have to be explained to the users in
  the terms of service of the Exchange.  Thus, any proposed solution
  should consider its impact on usability and comprehension.
* The refresh transaction is automatically triggered by the wallet software
  3 months before the end of the validity of a coin. Especially if Exchange
  operators charge **refresh** fees, the fact that a fee may automatically
  be charged in the background without user interaction is likely particularly
  difficult to explain.
* Fees should deter abusive behavior by malicious parties that simply
  run transactions to increase transaction costs at the Exchange or
  to impact availability for normal users.  The most effective operation
  for such attacks are refresh operations, especially if those do not have a fee.
* From a marketing perspective, it is unwise that buyers see fees.
  In particular, **withdraw** fees are likely to be the biggest deterrent
  as they appear before the buyer actually uses the system to make a payment.
  Only **deposit** and **wire** fees can be made "invisible" to buyers
  as the protocol allows sellers to fully or partially cover
  those fees.
* The protocol does not allow an Exchange operator to retroactively
  change fees.  Thus, adaptations to changing conditions cannot always
  be done in a timely fashion.

The potential for abuse for the different operations involving the
exchange differs:

* Abuse due to ``withdraw`` operations is unlikely as the costs of wire
  transfers are borne by the bank account holders and not the Exchange
  operators or sellers.

* Abuse due to ``deposit`` operations is unlikely we basically always
  recommend that an Exchange operator should charge deposit fees on
  every denomination to generate income to cover costs.

* Abuse due to ``refresh`` operations is likely and requires a differentiated
  treatment: The normal case for ``refresh`` operations is given anytime when
  wallets obtain fresh coins as change for a spent coin of higher denomination
  than the amount to be paid.  ``refresh`` operations can also happen if
  coins are about to expire or if transactions failed, say due to network
  outages between buyer, seller and exchange.  Because ``refresh`` operations
  happen completely anonymously, and because ``refresh`` operations without
  a fee basically output a coin that can serve as input into a subsequent
  ``refresh`` operation, they can easily be abused by any adversary to increase
  the load on the exchange.
  Thus, when an exchange suffers from an excessive number of refresh operations,
  the Exchange operator may need to charge **refresh**  fees to cover its costs.

* Abuse due to ``refund transactions`` involves sellers that refund an
  excessive fraction of purchases. This can be limited by introducing or
  increasing the **refund** fees.  However, refund fees are charged
  to consumers, who may then no longer receive a ``full`` refund as a result.

* Abuse due to ``wire transfers`` can theoretically affect an Exchange operator when
  sellers increase the frequency of aggregated wire transfers from his
  exchange to their banking accounts. A reason for frequently actuated wire
  transfers may be a seller's urgent need for immediate liquidity from sales
  revenues. Some sellers might also want to generate profit from interest
  rates for their sales revenues before they pay for their merchandise already
  sold. In any of these cases, IBAN wire transfers can be costly. Thus, we
  recommend for Exchange operators to always charge a **wire** fee.

* Abuse due to ``closing transactions`` and the accompanying wire transfer of
  remittances back to the originating accounts burdens the Exchange operator
  with costs for wire transfers.  This abuse is again unlikely as the costs of
  wire transfers to the exchange are borne by the bank account holders and not
  the Exchange operators or sellers.  Nevertheless, the Exchange operator
  could introduce a **closing** fee to cover such costs.

We now consider each of the fee types, viewed from the perspective of the
buyer, the Exchange operator, and the seller, in detail.

**Withdraw** fee for buyers
-----------------------------

Anyone who wants to load Taler wallets with coins must initiate a wire
transfer from their own checking account to the Exchange operator's escrow
account to let the Exchange fund a reserve which can be subsequently withdrawn
by the wallet. Costs for the wire transfer may be incurred according to the
user's contract with the bank. In addition to these potentially incurred
costs, the withdraw fee could be charged for each coin withdrawn into the
wallet. Even though many bank customers are already accustomed to wire
transfer charges, the withdraw fee acts like a loss of purchasing power even
before intended transactions take place. Buyers are made aware of this loss
when being shown all fee types at withdrawal. Once buyers become aware that
they will have to pay the cost for each coin generated, they might prefer to
have as few high-denomination coins as possible withdrawn into their wallets.

We note that there are other reasons why rational wallets already always
withdraw high-denomination coins, such as reducing computational, storage and
bandwidth demands as well as **refresh** fees. Thus, there does not seem to be
a need to provide an additional incentive in the form of **withdraw** fees
here.

**Withdraw** fee for the Exchange operator
--------------------------------------------

A fee on each coin generated would indeed affect all electronic coins
withdrawn from an Exchange operator and allocate costs necessary for their
generation over all coins signed for the first time.  **withdraw** fees
thus have the advantage that the Exchange operator does not have to wait
until the consumer spends the coin.  In case there are no **refresh**
fees, consumers may choose to hoard digital cash, which may create a
legal and (negative) interest liability for the operator.  Introducing
a **withdraw** fee may help an Exchange operator collect revenue up-front.

**Withdraw** fee for sellers
------------------------------

While **withdraw** fees do not burden sellers, withdraw fees are imposing
a psychological barrier for their buyers to use Taler. Sellers may thus
prefer to include the costs of generating coins in their selling prices and
hide this cost from buyers.

**Deposit** fee for buyers
--------------------------

**Deposit** fees are split between buyer and seller, the seller offering to
cover a certain total amount in fees (as part of the commercial offer) and
the buyer having to cover the remaining amount in full.  It is expected that
merchants will offer to cover the full typical range of deposit fees for
competitive Exchange operators.  Thus, **deposit** fees are only relevant
for buyers if they choose an expensive Exchange operator.

Deposit fees are thus a good candidate to cover all or most expenses that
Exchange operators have to bear.

**Deposit** fee for Exchange operators
--------------------------------------

All reasonable uses of the Taler payment system regularly involve deposit
operations.  Other fees, such as **refund**, **wire** and **refresh** fees
could be almost entirely avoided by certain groups of users, such as those
withdrawing coins that match precisely the amounts they will spend (no
refresh), sellers that never grant refunds and that configure their aggregated
wire transfers to happen rarely (like once per year).  Thus, an Exchange
operator that wants regular income from regular users must charge either
**deposit** or **withdraw** fees.

**Deposit** fee for sellers
---------------------------

As an Exchange operator could charge high **deposit** fees, sellers can
protect themselves against excessive fees by refusing to cover fees.  Sellers
determine the default maximum amount they want to bear by setting the variable
``default_max_deposit_fee``.  This default can be overridden on a per-purchase
basis.  **Deposit** fees exceeding this maximum are borne by the buyer.

Sellers are expected to cover **deposit** fees to a similar degree that they
cover such expenses with other payment systems.

**Refresh** fee for buyers
--------------------------

**Refresh** fees are mostly caused by the generation of fresh coins as change for
a coin of higher denomination that was redeemed for a smaller price that had
to be paid: The payment amount was paid with a coin of a higher denomination,
subsequently the wallet receives coins with denominations that add up to the
difference. The **refresh** fee for the change booking is therefore only ever
charged for one coin used and should be marginal from the buyer's point of
view.

Refresh also occurs together with refund transactions (a refresh transaction
will always be triggered subsequently to discounting or a cancellation of
purchase contracts). Less common should be refresh transactions due to the
expiration of coins or because of transaction aborts after network or
equipment failures. The **refresh** fee is charged to buyers per coin.

Buyers are expected to consider this fee as an unexpected nuisance. They may
complain about it, just like they are more particularly inclined to complain
about negative interest rates.  We expect that they will often not understand
when or why it is charged, especially since fees for getting change are very
uncommon in other payment systems.

**Refresh** fee for Exchange operators
--------------------------------------

As long as there is no abuse with refresh transactions, the Exchange operator
has to consider whether to pass on the costs for refreshes directly to buyers
or to cover these costs with another type of fee. Using the **refresh** fee to
cover costs means that the originators of excessive refreshes requests also
bear their excessive cost.

**Refresh** fee for sellers
---------------------------

Refresh operations do not directly affect sellers.

**Refund** fee for buyers
-------------------------

In contrast to the **refresh** fees, the sellers -- and not the buyers --
trigger refunds. If an Exchange charges **refund** fees, the already deposited
coins of the buyers would be charged with this fee in case of a partial or
full refund.  If a **refund** fee is charged for a coin, the respective
**deposit** fee is waived.

From the buyers' point of view, therefore, the sellers should legitimately
bear this fee, alas this is not possible given that sellers do not inherently
have any money to pay with, and also allowing sellers to give coins to buyers
would violate our income transparency principle.

Given that buyers would likely perceive it as unfair if they have to pay the
**refund** fee, we generally recommend that Exchange operators should simply
avoid using **refund** fees.

**Refund** fee for Exchange operators
-------------------------------------

Exchange operators should not disable refunds, as this is a frequently
legally required operation for sellers.

Sellers who excessively trigger refunds can be identified. So instead of
charging a **refund** fee, an Exchange operator may have a clause in its Terms
of Service that allows it to take special measures against sellers that abuse
the refund feature.  We note that the Taler protocol does not allow the
Exchange to automatically communicate such a clause to the sellers, and that
the sellers do not have to explicitly agree to the Exchange's terms of
service.  Thus, such a clause needs to be worded to simply specify what the
Exchange operator's may do in the case of criminal behavior.  For this, refund
abuse would have to happen to a degree that can basically be categorized as a
denial-of-service attack, giving the exchange operator a legal argument for
refusing to continue to do business with the abusive seller.

**Refund** fee for sellers
--------------------------

In the event of a seller refunding a purchase, the buyer bears the cost of the
**refund** and **refresh** fees.  While **deposit** fees are waved in case of
refunds, these other fees may apply for the buyer. Furthermore, the seller
cannot cover any of those fees.

Thus, sellers cannot guarantee a 100% refund (including fees) should an
Exchange charge **refund** or **refresh** fees.  **refresh** fees are slightly
less problematic, as they can happen in the background to coin owners anyway,
and are typically expected to be very low.  An exchange should be cautious
when charging **refund** fees, as this may create probems for retailers that
are legally obliged to refund 100% of the buyer's expenses (including banking
costs).

**Wire** fee for buyers
-----------------------

This fee is to be paid by the sellers (i.e. sellers or generally all
recipients of coins). The **wire** fee directly affects buyers only in the
following case: The protocol allows sellers to partially pass on the cost of
the **wire** fee to buyers if the Exchange operator that signed buyers' coins
sets the **wire** fee above the value that each seller can define in the
seller backend via ``max_wire_fee``.

Given that sellers can specify the wire transfer frequency, **wire** fees are
unlikely to be a driver for Exchange profits. Thus, Exchanges are likely to
charge competitive rates, and sellers are likely to be happy to cover the
entire **wire** fee.  Thus, **wire** fees should in practice rarely matter
for buyers.

**Wire** fee for Exchange operators
-----------------------------------

Exchange operators may charge **wire** fees in order to cover their expenses
for wiring the value of coins to the beneficiaries. The **wire** fee passes on
the cost of wire transfers from the Exchange's escrow account to the receiving
banking accounts, and for this usually banks charge handling fees. Buyers are
only shown the **wire** fee upon withdrawal and if the seller does not bear
them to the full extent.

For Exchange operators, opting out of the **wire** fee would be tantamount to
giving sellers carte blanche to trigger an aggregated booking of their sales
revenue as often as possible. (Except that refunds are not possible after the
wire transfer has been initiated by the exchange, but some sellers may never
make use of refunds.)

If, on the other hand, the Exchange operator charges the **wire** fee, this
will cause the sellers to adjust the frequency of the aggregated wire transfer
as they need it for their business and want to afford the fee for it.  Thus,
setting **wire** fees slightly above operational costs for wire transfers
should result in an optimal wire transfer frequency.

**Wire** fee for sellers
------------------------

Sellers want to register their sales as quickly and often as possible. Timely
revenue recognition improves their liquidity and generates interest income if
sales revenues are received earlier than payments to suppliers. They are
therefore forced to decide whether they would rather bear higher absolute
costs due to the **wire** fee or forego liquidity.  However, as **wire** fees
are expected to be relatively low, sellers are likely to primarily set their
aggregation periods based on the needs for refunds.

**Closing** fee for buyers
--------------------------

The **closing** fee is triggered by users of the payment system if, after a
successful wire transfer to an Exchange's escrow account, they do not drain
the reserve in a timely fashion. This could be the case when for
example the wallet could not connect to the Taler exchange within 14 days.

Costs incur to the Exchange for the wire transfer back to the originating
account. This is done by remitting the original amount minus the **closing**
fee. The **closing** fee is expected to be rarely charged and should be meet
with understanding from most users.  Still, an Exchange operator is unlikely
to depend on income from such a fee, and not having a **closing** fee will
simplify the terms of service and could be a cheap way to produce or maintain
consumer goodwill.

**Closing** fee for Exchange operators
--------------------------------------

Costs for the closing of a reserve are incurred by the Exchange operator due
to irregular user behavior (withdrawing to the wallet failed within the given
time frame, the Exchange has to wire the funds back to the originating
account). Thus, Exchange operators may charge a **closing** fee to cover these
costs.

The **closing** fee is likely dispensable, especially as abuse is expected to
not be a problem: malicious parties wiring funds to the Exchange to trigger
**closing** operations would need to have spare liquidiy, would still have to
cover their own banking costs, and would also be easily identified.

**Closing** fee for sellers
---------------------------

The **closing** fee does not affect sellers in any way.


Proposed Solution
=================

Fee levels if abuse is low
--------------------------

By default, we suggest that the Exchange should amortize its costs and create
a profit using only **deposit** and **wire** fees.  This minimizes the
psychological barriers during the critical withdraw phase, and allows all
regular fees to be fully covered by merchants.

Specifically, an exchange should pick a smallest currency unit it is willing
to transact in, say 0.005 EUR.  For coins of that denomination, the
**deposit** fee should be 100%, that is the entire value of the coin.  Further
denominations should be created at powers of two from this currency unit, so
in our example 0.01 EUR, 0.02 EUR, 0.04 EUR, 0.08 EUR, 0.16 EUR, etc.  For the
next 4 powers of two, we suggest that the fee remains at the unit currency.
Then, the **deposit** fee should double at 2^4 times the unit currency, so at
0.08 EUR the **deposit** fee (per coin) should be 0.01 EUR.  At 2^8 times the
unit currency (1.28 EUR), the **deposit** fee should triple, rising to 0.015
EUR.  At 2^12 times the unit currency, the **deposit** fee would quadruple,
so for a 20.48 EUR coin, the **deposit** fee would rise to 0.02 EUR.

Note that for a typical transaction, the number of coins is logarithmic to the
amount. So with the above fee structure, paying amounts around 10 EUR would on
average involve about 6 coins with 1/3rd fees at 0.005, 1/3rd fees at 0.01 and
1/3rd fees at 0.015, resulting in an expected total transaction cost in
**deposit** fees of 0.03 EUR.  In contrast, paying 0.50 cents would require
on average 4 coins cost less than 0.02 EUR in **deposit** fees.  As a result
of this fee structure, microtransactions with Taler have a higher fee in terms
of percentage, while larger transactions are still highly competitive.

This fee structure becomes problematic if attackers begin to abuse it, say by
excessively refreshing coins or constantly depositing and refunding the same
coin.

Fee levels in case of abuse
---------------------------

If such transactions are triggered anonymously in excessive ways by malicious
parties, the Exchange operators may need to configure **refresh**, **refund**
or **closing** fees.  We believe nominal **refresh** fees are most likely
needed in case of malicious activity.  Excessive **refunds** are easily
attributed to merchants and thus less likely to be a problem.  **closing**
fees imply that the attacker performs wire transfers at an equal cost to the
attacker. Thus, we believe **closing** fees will most likely never be needed.

In all cases, these fees should be set to deter, not to make a profit.  Hence,
likely the smallest configured currency unit should suffice for **refresh**
and **refund** fees, and the actual wire transfer cost would be an appropriate
**closing** fee.


Alternatives
============

Another way for Exchange operators to cover costs or generate income would be
to set all of the above fees to zero and use income from the forfeiture of
users' funds on the escrow account. Some voucher distributors even already use
this income source as a normal business model. This solution might possibly be
a "best case", since without confusing the users with a complex fee schedule
and/or a range of fees. However, these revenues are discontinuous and
unpredictable and therefore not really suitable for sustainable financing.


Drawbacks
=========

* Not charging **refresh** and **refund** fees all the time exposes
  an exchange operator to potential losses for a period of time until
  such fees can be introduced, which may be weeks or months depending
  on the denomination key rotation frequency.  Still, it would seem that
  a simplified, more understandable fee structure will help the system
  grow, which is likely more important than this risk.
* The fee structure does not result in a flat percentage being charged
  on all transactions. Such a flat percentage may be easier to
  comprehend than the logarithmic structure prescribed in this document.
  However, a flat percentage structure cannot technically be achieved
  for the unit denomination (where the fee basically has to be zero
  or 100%), and would not reflect the market position of Taler, where
  low fees are more important for larger transactions that are well
  supported by competing systems on offer today.
* The proposed fee structure tries to balance income with system growth,
  setting fees below current market rates to drive adoption. Given the
  privacy features, one could justify charging above market rate fees.
  However, our goal is to grow and become the dominant payment system,
  and given low per-transaction operational costs, long-term survival
  depends more on growth than on early income. Given the actual
  per-transaction costs, it would also be conceivable to charge lower
  fees to supercharge growth. However, we do have limited funding and
  a need to show investors that the market is willing to pay for our
  payment system product. Thus, using fees that might even with
  significant growth not be able to cover operational costs for many
  years is also not a good option.  Thus, the unit denomination
  should likely be in the 0.25 to 1 cent range, with the linear
  growth at factors between 2^2 and 2^6.


Discussion / Q&A
================

Other documents regarding fee specifications:

* Fee schedule and metrics from the users' point of view :doc:`008-fees`

* Wire fee for different wiring methods (``iban`` or ``x-taler-wire``) <https://docs.taler.net/taler-exchange-manual.html#wire-fee-structure>
