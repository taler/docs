DD 15: Merchant backoffice Routing
##################################


Motivation
==========

A well defined routing will allow users to share backoffice links pointing
directly into instance pages (settings, orders, products, etc...)

The backoffice should load from the instance URL and then allow an internal
routing of the views with the possibility to accessing them directly when
sharing a link.

This 3 definitions are going to be use in this document:

* BACKOFFICE_URL as the url where the app is loaded.

* BACKEND_URL as the url where the merchant backend is.

* INSTANCE the name of the instance being manage


Application Ready definition
============================

The application is considered ready after

* the user tried to login.

* the application checked that the backend url points to a merchant backend

* the merchant backend response successfully

The backoffice test for ``$BACKEND_URL/config`` to define if the $BACKEND_URL is ok.
The application can potentially test if the protocol or version matched.

While the application is not ready, just the top navigation bar will be shown
with a message in the left and the lang selection option.

.. _routing:

Application routing
====================

There are 2 type of routing: instance and internal

  **instance**: define how the URL will be interpreted to infer which instance is
  being accessed. Also the full URL is used as default when no BACKEND_URL exist
  yet in localStorage. After login, the BACKEND_URL is saved in localStorage and
  the pathname is parsed to infer the instance id.

  **internal**: define which view will be rendered. It is implemented using a hash
  routing since 1) the SPA is not server side rendered and 2) the http servers
  doest not need to care about matching all applications URL

Knowing that the $BACKEND_URL points to a correct merchant backend the SPA will
check for ``$BACKEND_URL/management/instances``:

* if Unauthorized ask for credentials

* if error check with the user

* if not found, then url should end with ``/instances/$INSTANCE``. otherwise is
  an error. app will continue as admin = false

* if ok then then $INSTANCE == 'default', app will continue as admin = true

When a user access the SPA there are 3 scenarios possible:

* **standard**: admin is false so BACKEND_URL points to a non-default instance.
  standard features and links are shown

* **admin**: admin is true so BACKEND_URL point to default instance. same as
  before and user can create and list instances with some additional links in
  the sidebar.

* **mimic**: admin is true and the request parameter "instance" is set $INSTANCE
  instance. BACKEND_URL point to default instance but the user is managing
  $INSTANCE

Normally all communication with the backend will be done to $BACKOFFICE_URL and
``backend-token`` will be used used for authentication.

For **mimic** scenario then ``backend-token-$INSTANCE`` is used and the queries
to the merchant are being sent to ``$BACKEND_URL/instances/$INSTANCE``

Application entry points
========================

When the application is ready to start, it queries the backend to test if the
user can access to the list of instance and decide if the admin panel should be
accessible. See :ref:`routing`

All of this entry points are corresponds to the internal routing and is
implemented using the hash path. For any entry point that do not require a
parameter (like order id or product id) it should be accessible from the Sidebar.

If the user has admin access, this entry points are available:

 - /instances: Show the list of instances currently created
 - /instance/new: Show an instance creation form

Where admin or not, there is also this entry points:

 - / : redirects to the instance settings page
 - /update: instance setting page, renders a form and allow modifying it
 - /products: list of instance products
 - /product/$ID/update: product modification page
 - /product/new : product creation page
 - /orders : list of instance orders
 - /transfers : list of transfers

As an example:

* ``$BACKOFFICE_URL/?instance=foo#/orders`` will show the orders of the foo
  instance, assuming that BACKEND_URL points to the default instance.

* ``$BACKOFFICE_URL/#/product/foo/update`` will show the update page for the
  ``foo`` product of the instance pointed by $BACKEND_URL


Special cases
=============

First time
----------

If the application is loaded for the first time a welcoming message is shown
with the login form (even if the backend doest not need authentication)

Log out
-------

On logout, the application go to the initial state. Removing all backend-token-*
from the localStorage

Unauthorized
------------

For any case that the backend respond 401 the application will render the
login view showing a notification error that the user must check the login
credentials or the backend url

Not found
---------

For any case that the backend respond 404 the application will render a
custom not found page

Default instance is missing
---------------------------

If the **user is admin** AND is loading the setting page (/update), product list
(/products), order list (/orders) or transfer list (/transfers) AND **gets a
404** it will tell the user that it need to create a default instance before
proceeding.
