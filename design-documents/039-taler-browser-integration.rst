DD 39: Taler Wallet Browser Integration Considerations
######################################################

Summary
=======

This design document discusses considerations for integrating the GNU Taler
wallet with browsers and highlights difficulties with the implementation of a
GNU Taler wallet as a cross-browser WebExtension.

Motivation
==========

GNU Taler is a payment system based on open standards with a free and open
source reference implementation.  The GNU Taler wallet is the main component
used by end users to manage their electronic cash balance and payments.

Payments with GNU Taler are typically initiated via a QR code or link that
contains a ``taler://pay/{merchant}/{order_id}`` URI.  Navigating to such a
link should result in a context switch to the wallet, where the payment can can
be approved/declined, and the user is subsequently redirected to the merchant's
website again.

Other ``taler://`` URIs (for withdrawals, refunds, etc.) are also commonly
used, but not explicitly discussed in this document, as very similar
considerations apply.

There are multiple reference implementations available for multiple
platforms (command line, Android, iOS, WebExtension).

While native applications can register themselves as a handler for the
``taler`` URI scheme, the story is different for WebExtensions: There is
currently no reasonable, cross-platform mechanism that allows a WebExtension to
register itself as the handler for the ``taler`` URI scheme.

This is unfortunate, as a WebExtension could otherwise easily provide a Taler
wallet implementation without requiring the user to install a native App,
providing a better and safer user experience.

The problems with individual browsers are:

* Firefox allows specifying ``protocol_handlers`` in the extension manifest.
  However, this list only allows schemes with the prefix ``ext+`` and
  schemes that are included in an allowlist.  The ``taler`` URI scheme
  is not part of this list yet.
* Chromium / Google Chrome allows extensions to use the
  ``registerProtocolHandler`` API.  However, the same allowlist restrictions
  apply.  Furthermore, the registered protocol scheme is not listed as the
  extension's required/optional permissions.  Instead, a different permission
  prompt is dynamically shown to the user.
* Safari currently neither supports ``registerProtocolHandler`` nor the
  ``protocol_handlers`` mechanism.
* Opera does not seem to have any support for WebExtension protocol handlers
  either.

Another issue is that Websites can't easily find out whether a browser
extension handling the ``taler://`` protocol is installed.  To avoid using this
information for fingerprinting, anchors could provide an alternative ``href``
in case the main one is not handled, such as:

.. code:: html

  <a href="taler://pay/..." handler-unavailable-href="https://wallet.taler.net/">...</a>

Requirements
============

* No vendor lock-in: The integration should not require merchant
  Websites to rely on a particular list of extension IDs but instead
  any WebExtension to potentially handle ``taler://`` URIs
  or other mechanisms that Websites can use to interact with Taler
  wallets.
* Security: The integration mechanism should require as few
  permissions as possible.
* Ergonomic user experience: As few clicks and permission
  prompts as possible should be shown to the user.
* Ergonomic developer experience: The code size and
  effort to trigger a Taler payment on a merchant's Website
  should be minimized.
* Forward compatibility: The integration mechanism
  should work smoothly with future browsers that
  have native, built-in support for Taler payments.


Alternatives
============

* JavaScript API: The WebExtension could inject a JavaScript API into Websites
  that allow interacting with the Taler wallet.  This is the approach taken by
  the MetaMask crypto wallet.  It requires excessive permissions, may break
  some Websites (https://github.com/brave/browser-laptop/issues/13711) and
  requires merchants to include extra JavaScript.

  * This type of interaction is useful for Single Page Apps and
    might be provided by the GNU Taler wallet reference implementation,
    at least when the user grants additional permissions.
  * Unfortunately, browsers currently do not provide a safe way
    for the communication between a WebExtension and the page
    without excessive permissions.  This especially applies
    if the Website does not know the extension's ID. Hard-coding
    the extension IDs would violate the "no vendor lock-in requirement".

* Handling ``taler://`` URIs by overriding the onclick handler of ``a`` HTML elements.
  This requires excessive permissions but would be a viable work-around,
  at least on pages that opt in with a special ``<meta name="taler-support" content="uri">`` tag.
  It does not work in all use-cases, for example when a navigation
  to a ``taler://`` URI is initiated programmatically or by pasting
  the URI in the browser's address bar.

* Triggering interactions with the ``taler://`` URI in a ``Taler:`` HTTP
  header.  This approach would allow browsers with native Taler support
  (or a WebExtension) to handle payment/withdrawal initiations directly,
  without rendering a page that shows the QR code or link.
  However, the WebExtension APIs do not allow extensions to
  read particular headers without excessive permissions.  Furthermore,
  more recent versions of Chrome/Chromium do not allow blocking
  processing of headers, leading to flickering when the extension
  redirects based on the presence of the ``Taler:`` header.

* Browser and wallet presence detection. Merchants' Websites could include custom
  code to detect the browser and/or presence of a Taler WebExtension and show
  different instructions to guide the user towards processing the payment or to
  show ``ext+taler`` URIs instead of ``taler`` URIs. This is not a viable
  solution, as it requires a lot of extra and brittle logic on merchants'
  Websites.

* Always use ``ext+taler`` URIs.  This would help with Firefox.
  Bad for forward compatibility, as we have already provisionally registered the
  ``taler`` URI scheme.

* Web Payments API: Using the Web Payments API is not possible, because current
  browsers do not allow specifying a WebExtension as a handler. Furthermore,
  the Web Payments API would not support the withdrawal flow
  (``taler://withdraw`` URIs).


Related Work and References
===========================

* **[1]** https://github.com/whatwg/html/issues/8596
* **[2]** https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/protocol_handlers
* **[3]** https://github.com/ipfs/devgrants/blob/master/targeted-grants/protocol-handler-api-for-browser-extensions.md
