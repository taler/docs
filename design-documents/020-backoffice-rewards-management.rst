DD 20: Backoffice Rewards Management
####################################

Summary
=======

This document describe the complete list features for rewards and reserve
management and how will be shown.

Motivation
==========

User will use the backoffice to manage reserve and authorize

Requirements
============

User should use the backoffice to:

* creating new reserves
* listing active reserves
* authorize rewards for a reserve
* list all rewards for an active reserve
* check rewards status

Proposed Solution
=================

Listing reserves
----------------

.. image:: ../images/backoffice-reserve-list.svg
  :width: 400


Can be filtered with optional arguments:

* after: if present will brings reserve created after specified date

* active: if present will bring reserve active (or inactive), otherwise all

* failures: if present will bring all reserves that have different initial
  balance reported by the exchange (or equal), otherwise all

columns:

* initial: if the exchange and merchant-backend disagree in the initial balance
  (failure) the cell will be red and have a tooltip with more information

* actions: delete button will be disabled on failure or committed > 0, new_reward
  button will be disabled on picked_up == initial or failure


Create new reserve
------------------

.. image:: ../images/backoffice-reserve-create.svg
  :width: 400

fields:

* initial balance must be >0 and the current currency

* exchange should be a known exchange of the merchant backend

* wire method should be one of the current supported of the instance

If there is an error in the creation a Notification message will be shown

Authorize Reward
----------------

The merchant can authorize rewards clicking in the plus (+) button that will bring
the next popup

.. image:: ../images/backoffice-reward-create.svg
  :width: 400

after confirm it will continue with a success page:

.. image:: ../images/backoffice-reward-create.confirmation.svg
  :width: 400

Details of reserve
------------------

.. image:: ../images/backoffice-reserve-details.svg
  :width: 400

Rewards sorted from newer to older

When the reserve has not yet funded

.. image:: ../images/backoffice-reserve-details.unfunded.svg
  :width: 400
