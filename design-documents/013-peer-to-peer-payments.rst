DD 13: Wallet-to-Wallet Payments
################################

Summary
=======

This design document proposes an extension of the Taler protocol that allows
payments from wallet-to-wallet without a merchant.


Motivation
==========

To be usable as an electronic payment system with cash-like properties,
customers should be able to transfer money between themselves without
needing to setup anything beyond their wallet(s).

This will be used for payments via e-mail and other messaging apps, as well as
possibly for transfers via NFC/QR code between mobile phones.

Invoice Flow User Experience
----------------------------

.. graphviz::

   digraph invoice {
     ranksep="0.5"
     { rank = same; "inbox"; "begin"; }
     { rank = same; "sending"; "receiving2"; }
     { rank = same; "receiving"; "paying"; }
     { rank = same; "mid"; "midbox"; }
     { rank = same; "body"; "amount"; }
     begin [label="Payer Inbox",shape=box];
     body [label="compose\nE-mail message"];
     amount [label="specify\ninvoice details"];
     receiving [label="receiving...",shape=diamond];
     sending [label="transmitting...",shape=diamond];
     mid [label="Payee Inbox",shape=box];
     notified [label="Notification:\npayment received"];
     end [label="Payee Inbox",shape=box];
     begin -> body [label="(1) new"];
     body -> amount [label="(2) attach invoice"];
     amount -> body [label="(3) Ok"];
     body -> sending [label="(4) send"];
     sending -> mid [style=dashed];
     mid -> receiving [style=dashed];
     receiving -> notified [style=dashed];
     notified -> end [label="(9) Acknowledge"];
     inbox [label="Payer Inbox",shape=box];
     receiving2 [label="receiving...",shape=diamond];
     midbox [label="Payer Inbox",shape=box];
     open [label="message with\nattached invoice"];
     confirm [label="review invoice"];
     paying [label="paying...", shape=diamond];
     paid [label="message with\npaid invoice"];
     finbox [label="Payer Inbox",shape=box];
     inbox -> receiving2 [style=dashed];
     receiving2 -> sending [label="Internet\n(pEp)",style=dashed,dir=back];
     receiving2 -> midbox [style=dashed];
     midbox -> open [label="(5) select message"];
     open -> confirm [label="(6) view invoice"];
     confirm -> paying [label="(7) pay"];
     paying -> paid [style=dashed];
     paid -> finbox [label="(8) back"];
     paying -> receiving [style=dashed, label="Internet\n(Taler)"];
   }

Donation Flow User Experience
-----------------------------

.. graphviz::

   digraph donation {
     ranksep="0.5"
     { rank = same; "inbox"; "begin"; }
     { rank = same; "sending"; "receiving2"; }
     { rank = same; "body"; "amount"; }
     { rank = same; "mid"; "midbox"; }
     { rank = same; "accepting"; "timeout"; "receiving"; }
     begin [label="Donor Inbox",shape=box];
     body [label="compose\nE-mail message"];
     amount [label="specify\npayment details"];
     receiving [label="receiving...",shape=diamond];
     timeout [label="timeout...",shape=diamond];
     sending [label="transmitting...",shape=diamond];
     mid [label="Donor Inbox",shape=box];
     notified [label="Notification:\npayment confirmed"];
     notified2 [label="Notification:\npayment refunded"];
     end [label="Donor Inbox",shape=box];
     begin -> body [label="(1) new"];
     body -> amount [label="(2) attach payment"];
     amount -> body [label="(3) Ok"];
     body -> sending [label="(4) send"];
     sending -> mid [style=dashed];
     mid -> receiving [style=dashed];
     receiving -> notified [style=dashed];
     mid -> timeout [style=dashed];
     timeout -> notified2 [style=dashed];
     notified -> end [label="(9a) Acknowledge"];
     notified2 -> end [label="(9b) Acknowledge"];
     inbox [label="Recipient Inbox",shape=box];
     receiving2 [label="receiving...",shape=diamond];
     midbox [label="Recipient Inbox",shape=box];
     open [label="message with\nattached payment"];
     confirm [label="accept payment?"];
     accepting [label="accepting...", shape=diamond];
     paid [label="message with\naccepted payment"];
     finbox [label="Recipient Inbox",shape=box];
     inbox -> receiving2 [style=dashed];
     receiving2 -> sending [label="Internet\n(pEp)",style=dashed,dir=back];
     receiving2 -> midbox [style=dashed];
     midbox -> open [label="(5) select message"];
     open -> confirm [label="(6) review payment details"];
     confirm -> accepting [label="(7) yes"];
     accepting -> paid [style=dashed];
     paid -> finbox [label="(8) back"];
     accepting -> receiving [style=dashed, label="Internet\n(Taler)"];
   }

Requirements
============

* The protocol must permit transacting arbitrary amounts in any currency,
  as long as both parties trust the exchange involved.
* The control data for wallet-to-wallet payments should be small
  enough to fit into a QR code or short message (so ideally less than 64 bytes).
* No other direct communication channel between payer and payee should
  be required.
* The wallet-to-wallet payment must be possible without trusting the other
  party beyond the point where the money has been received by the payee.  Thus,
  sharing of coin private keys is not sufficient, we need transactional semantics
  resulting in exclusive control over the funds by the recipient.
* The wallet-to-wallet payment protocol must not allow users to circumvent income
  transparency.  That is, each wallet-to-wallet transaction must be visible
  on a KYCed transaction ledger (such as a bank account).
* The money received via a wallet-to-wallet payment must be usable for
  further Taler payments with minimal delay (after KYC).
* It must still be possible to associate payments with a contract that is
  effectively (alas not necessarily directly) signed by both parties:
  the payer with the coin private keys, and the payee with their KYC'ed
  account private key.
* The contract must be able to satisfy laws like the German TSE law,
  which implies that the payer must be able to obtain a payment receipt.
* Two payment scenarios must be possible: (1) one where the payee first
  transmits a proposal to the payer (request-to-pay) that the payer
  accepts by making the payment, and (2) completely uni-directional
  payments where the payer includes a proposal with the payment and the
  payee accepts the proposal by taking the offered payment.
* If the payment fails (i.e. the receiver refuses to accept the money
  or the message is lost), the payer must automatically recover the
  funds (minus applicable fees) without the need for further communication.
* If funds flow back to the payer due to an aborted payment, it must be
  provable for the payer that these funds were not income but merely an aborted
  transaction.  Furthermore, in this case, no KYC should be required from the
  payer.
* If a payment would partially succeed, i.e. because the payer inadvertedly
  used some double-spent coins and some valid coins, this must fail before the
  uni-directional communication and be correctable payer-side.  In other words,
  the actual payment must be atomic.
* The usual properties of Taler (everything auditable, unlinkability,
  high-performance in terms of CPU, bandwidth, latency, storage
  requirements, and the ability to levy fees on every operation that
  is costly for the exchange) need to be preserved.
* The system must handle the case where a customer no longer intends to
  use the KYCed account (due to disuse, death, or key compromise).



New Terminology
===============

* An ``account`` is a non-expiring reserve for which entity knowing the
  reserve private key has completed a KYC procedure sufficient to enable
  receiving income under that address.
* A ``purse`` is a public-private key pair where the public key is
  an exchange address from which any owners of an account
  can ``merge`` the amount left at a ``purse`` into their
  account balance assuming they know the purse private key.
* A ``wad`` is an exchange-to-exchange wire transfer that wires money
  into a group of accounts at the target exchange.


Proposed Solution
=================

Principles
----------

* Purses are ephemeral and only serve for one transaction.
* The purse's transaction amount is fixed when the purse is created, and
  specified together with the maximum deposit fee acceptable to the payee.
  Deposit fees exceeding this limit must be paid by the payer.
* Each purse is associated with a contract terms hash and an expiration date.
* The contract is optionally stored encrypted at the exchange.
  The contract must be encrypted to the purse private key.
  An additional ephemeral public key (for DH encryption) should be
  part of the POSTed payload.
* The exchange deletes the encrypted contract at this expiration date.
* The exchange may limit the encrypted contract size and storage duration.
* Either payer or payee can create the purse and associate it with the contract.
* By merging the purse into the account, the payee accepts the contract.
* By paying the purse to the designated amount, the payer accepts the contract.
* Until the purse is fully paid, the payer can abort the payment.
* The exchange may charge a **purse fee** for managing the purse, but we
  want most scenarios to not require it to be effectively charged.
* By associating a purse with an account upon creation, the purse fee can be
  made optional for account holders as long as the number of purses created
  per account is below a configurable threshold.
* By charging the purse fee only in case the payee did **not** merge the
  purse into their account, the purse fee can be limited for payers to
  the case where they are receiving a refund --- and here it could be
  then entirely avoided if the refund fee is non-zero.


W2W Payment Metadata
--------------------

The standard Taler Customer-to-Merchant payments always use a contract terms
JSON object to record the modalities of the payment and the resulting
obligations of a successful payment.

The contract terms concept does not directly carry over to W2W payments,
because:

* Either party may initiate the payment.
* The payee does not have a merchant public key, and
  at the time of payment initiation, the payee account might not yet
  be known.
* There is no nonce that the customer generates and uses to prove that they
  uniquely "own" the contract terms.
* There is no negotiation of trusted auditors / exchanges possible.

As a result, some of the existing fields of the contract terms no longer apply
to wallet-to-wallet payments.

Contract metadata for W2W payments can be exchanged in three ways:

1. Inline, as part of the payment request / payment offer.  In this case,
   both parties are already aware of the contract's contents and
   the exchange's contract exchange facility is simply not used.
2. The payee can create a **purse** and immediately associate it with
   an **account** by sending a signed **merge** request together with
   the (encrypted) contract.

   a. The purse creation request created by the payee must include a
      signature with the account private key of the payee signing the purse
      public key and the hash of the contract, thereby affirming that
      the contract was pre-approved by the account owner.
   b. The exchange may wave the purse fee for a certain number of
      active purses per account. Additional purses can be purchased
      by paying the **purse fee**.
3. The payer can store a contract with the exchange by POSTing
   an encrypted contract to the exchange as part of creating a **purse**.

   a. The exchange charges the **purse fee** to payers for purses that
      are refunded after not being **merged**.
   b. When paying into a purse, the coin signature includes the purse
      public key, the contract hash and the desired expiration date
      (how long a merge is allowed).
   c. Payment offers are not allowed if the amount transacted is below
      the purse fee.
   d. The exchange auto-refunds coins in **purses** with deposits
      matching expired contracts.

      .. note::

         While the **refund fee** amount can be reused, these types of refunds
         are not approved by a merchant's signature. Thus, we will need
         a new message type in the coin history to represent these events.


Account creation
----------------

An account is simply a reserve that has been subjected to
KYC. A reserve that has seen a purse merged into it must
be upgraded to an account before further withdraw (or close)
operations are allowed.  The usual closure deadline for a
reserve is extended to the KYC deadline.

1. The payee generates a reserve key, which also yields a
   ``payto://taler/$EXCHANGE_BASE_URL/$RESERVE_PUB``
   target address (for which the payee knows the corresponding
   reserve private key).
2. When withdrawing from a reserve that has experienced
   merge operations and thus must be an account, the exchange
   first checks if the customer has satisfied the KYC requirements.
   If not, the customer is redirected to a Web page where they
   can perform the necessary KYC operation.
3. For this, the exchange wire gateway is extended with a request to
   check the KYC status of a customer based on an ``RESERVE_PUB``.
   Possible replies are ``in-progress`` and ``succeeded``.
   An ``in-progress`` status should be accompanied with
   information how the customer may complete the KYC check.
4. A new exchange endpoint ``/reserves/$RESERVE_PUB/kyc``
   allows wallets to request a KYC for a
   ``$RESERVE_PUB``.  Such a request may include the requirement to pay
   a **KYC fee**.
   The KYC fee may be charged to the reserve (a sufficient
   balance can be provided by the wallet by creating a purse
   and merging the purse with the reserve, if needed),
   or could be waved if the reserve was established via a wire transfer
   from a partner bank where KYC is free.  For this, the Wire
   gateway API is extended with a flag that informs the exchange
   that the incoming wire transfer implies a free KYC check.
5. If the account owner fails to perform the KYC check, all funds
   in an reserve remain inaccessible.  After a configurable duration,
   the funds may be considered forfeit and become the property of
   the exchange where the reserve is located.
6. The exchange may charge an annual **account fee**, and can
   close accounts where the account balance is insufficient to
   cover the account fee.


Withdrawing from accounts
-------------------------

1. When requesting an account's history (which can get quite long),
   the exchange only returns the last 3 months of data.  Requesting
   the full history requires paying an **account history fee**
   (which is not done via a 402, but simply charged to the account
   when requested; full account histories for accounts with an
   insufficient balance cannot be requested -- except of course
   the wallet could simply top up the account balance first, see below).
2. If the exchange has **merged** a **purse** into an account, or
   received an inbound wire transfer from a **wad** matching the
   account (see below), it adds the respective amount(s) to the
   account's balance, allowing the KYC'ed customer to withdraw the funds,
   similar to withdrawals from a reserve.
3. The account history endpoint should also allow long-polling.
   Note that long-polling should be limited to short durations,
   as inbound transfers via ``taler-exchange-wirewatch`` cannot cause the long
   polling to be resumed, only transfers within the same exchange can benefit
   from long-polling acceleration.


Account deletion
----------------

1. A reserve owner can delete a reserve by signing a deletion message
   with the reserve private key.
2. This basically resets the KYC data at the exchange, preventing
   further use of the account. This is helpful in case a user is
   concerned about having
   accidentally disclosed the reserve private key to a third party.
3. If funds remain in the reserve, the exchange will close the
   reserve and wire the funds to the associated bank account.
   If no bank account is associated with the reserve,
   an error message is generated instead.  The
   user can pass an extra override parameter to delete reserves
   even if they still contain funds.
4. A related endpoint should exist for the exchange operator, possibly
   using messages signed with a new exchange management key.
   This could be useful in case customers die or are otherwise
   in need for manual intervention that requires an account to
   be deleted. In this case,
   remaining funds in the account should be wired to a bank account
   designated in the message with the management signature. The audit
   report should contain a special note for all of these types of
   account deletions.



Payment offers
--------------

In this protocol variant, the payer is initiating the process.

1. The payer creates a **purse** by computing a public-private key pair.
2. The payer POSTs to the ``/purse/$PURSE_PUB/depost`` endpoint to
   deposit coins into the purse and optionally upload the encrypted contract terms.
   The deposit signatures should use ``payto://taler/$PURSE_PUB``
   as the target address and signing over the ``$CONTRACT_HASH`` as
   usual in deposit operations. Note that the lack of a hostname
   indicates that the target address is a local purse.
3. The payer shares the purse's private key and the base URL
   of the exchange where the purse was created with the payee.
   This can be done using a ``taler://purse/$BASE_URL/$PURSE_PRIV`` URL.
   The chapter on ``Refinements`` below clarifies why this
   step is not quite OK and was modified when implementing the   design.
4. The payee uses the new ``/purse/$PURSE_PUB`` endpoint to retrieve
   the encrypted contract (if available) and purse balance, which includes all
   (coin) deposits and **merges** involving the purse.
5. The payee's wallet must ensure that either:

   a. The purse has an attached encrypted contract terms, the contract
      terms can be decrypted and are valid, and their hash matches the contract
      terms hash of the purse.
   b. The wallet received detached contract terms, and their hash matches
      contract terms of the purse.

   If neither case applies, the payee's wallet must reject the payment.
6. The payee can then POST to ``/purse/$PURSE_PUB/merge`` a
   request signed by the purse's private key to **merge** the
   funds into an account.  A second signature must be provided
   by the account private key, signing the ``$CONTRACT_HASH`` thereby
   affirming that the payee accepted the contract.
   The account is of the form ``payto://taler/$EXCHANGE_BASE_URL/$ACCOUNT_PUB``.
7. Processing continues depending on the location of the account:

   a. If the ``$EXCHANGE_BASE_URL`` matches the local exchange, then
      the exchange processes the **merge** request akin to the logic
      for payments into known accounts, as detailed above.
   b. If the ``$EXCHANGE_BASE_URL`` does not match the local exchange,
      a **wad fee** is charged, and the remaining amount are placed into a **wad**
      to inform the target exchange, as detailed below.  Wad fees may be covered
      by the merchant, just like deposit fees, depending on the contract.
8. The exchange confirms the merge (per response to the **merge** request).
   This allows the payee software to instantly
   affirm to the users that the transaction is final (even if it may not
   be instantly available to the payee if the payee did not complete the
   KYC process for the account).
9. The payer uses the GET ``/purse/$PURSE_PUB`` endpoint
   to obtain the receipt from the payee (in the form of the
   **merge** signature).  Query parameters are used to avoid
   downloading the (already known) encrypted contract and the
   deposit operations.  Long-polling must also be possible for
   this request.



Payment requests
----------------

1. The payee creates a **purse** by computing a public-private key pair.
2. The payee POSTs to the ``/purse/$PURSE_PUB/merge`` endpoint to
   both upload the encrypted contract, associate it with the payee's
   account and signal its agreement to the contract.  The
   **merge** request must be signed by the purse's private key.
   A second signature must be provided by the account private key,
   signing the ``$CONTRACT_HASH`` thereby affirming that the payee
   accepted the contract.
3. The payee provides the ``$PURSE_PRIV`` to the payer.
4. The payer computes the corresponding public key and uses the
   new ``/purse/$PURSE_PUB`` endpoint to retrieve
   the encrypted contract and the merge request, which signifies that
   the payee would agree to the contract.
5. The payer software decrypts the encrypted contract using the purse private
   key and the payer accepts the contract in the user interface.
6. Processing continues depending on the source of the coins:

   a. If the payer's coins originate from the same exchange, the
      payer software POSTs to the ``/purse/$PURSE_PUB/depost`` endpoint to
      deposit coins into the purse. The deposit signatures should use
      ``payto://taler/$PURSE_PUB``
      as the target address and signing over the ``$CONTRACT_HASH`` as
      usual in deposit operations. Note that the lack of a hostname
      indicates that the target address is a local purse.
   b. If the payer's coins originate from another exchange, the
      payer software deposits the coins at the originating exchange
      using the traditional ``/deposit`` endpoint and a target account of the form
      ``payto://taler/$EXCHANGE_BASE_URL/$ACCOUNT_PUB``.  In this case,
      the remote exchange charges a **wad fee** and places the remaining
      amount into a **wad** to inform the target exchange, as detailed below.
7. The exchange confirms the deposit. This allows the payer software to instantly
   affirm to the users that the transaction is final, or to abort or try again
   in case of errors.
8. The payee uses the GET ``/purse/$PURSE_PUB`` endpoint (possibly with long-polling)
   to be notified about the successful deposit and subsequent completion of the
   **merge** request.


Payment into accounts at remote exchanges
-----------------------------------------

In case the coins and the accounts in the transaction flows above are at
different exchanges, an aggregated exchange-to-exchange payment (short
**wad**) is used.

1. Exchanges specify a new **wad fee** that they charge for exchange-to-exchange
   payments.  They also specify their wad policy, that is how often they
   perform exchange-to-exchange transfers.

   .. note::

      We may want to consider allowing for different wad-speed levels, where
      express payments (without aggregation) are allowed in return for higher
      wad fees.

2. The payer's exchange creates a **wad** by grouping all wad requests
   to the same target exchange. It executes
   the transaction when either the **wad threshold** (maximum number
   of transactons aggregated per wad) or the **wad delay** (maximum
   delay for transfers) has been reached.
3. If the (aggregated) wire transfer fails (say the
   ``/wire`` endpoint of the payee exchange does not
   resolve to a valid bank account), the
   originating exchange automatically creates a full refund for
   all involved coins (**refund fees** apply).

   .. note::

      While the **refund fee** amount can be reused, these types of refunds
      are not approved by a merchant's signature. Thus, we will need
      a new message type in the coin history to represent these events.

4. The payee's exchange observes the wire transfer with a wire transfer
   subject with the originating exchange base URL and a ``$WATID``,
   and uses a GET ``/wad/$WATID`` request to obtain
   details about the target accounts.
5. When the payer's exchange is requested to provide information about
   aggregated transfers under the ``$WATID``, it provides a signed list of
   account public keys and associated amounts that must add up to an
   amount below the total amount transferred. If they do not, the
   payee's exchange does not credit any of the accounts and instead
   preserves the bogus reply (to justify its inaction with its own
   auditor) and reports the issue to the auditor of the payer's exchange
   (keeping the received funds for future manual resolution).
6. ``taler-exchange-wirewatch`` and the Taler wire gateway API will
   need to be extended to allow passing inbound wire transfers with ``$WATID``
   and exchange base URL to the exchange. Furthermore, another tool
   is needed to lookup the **wad** data at remote exchanges.
7. If the payee trusts the originating exchange, it may consider the
   transaction ``final`` once the originating exchange has affirmed the
   deposit (assuming the payer has a way to submit the evidence of that
   payment, which may not apply in uni-directional scenarios).
   Otherwise, the payee may simply only trust its own exchange,
   resulting in the transfer only being considered final after the
   receiving exchange has confirmed that the **wad** has arrived.


Examples
---------

Cross-exchange W2W payment request:

* Bob borrowed 15 EUR from Alice to buy a train ticket.  A few days later,
  Alice wants her money back.  She creates a request for payment in her wallet.
  The wallet creates a purse for 15 EUR at the only exchange that Alice is currently
  using.  The wallet shows her a
  ``taler://purse/{EXCHANGE_URL}/{PURSE_PRIV}``
  link that she can share with Bob.  Bob receives the link and opens it with
  his Taler wallet.  Bob is using a different EUR exchange than Alice.  Bob's
  wallet makes a ``/deposit`` request to his own exchange.  Shortly after, Alice's
  exchange receives the wad from Bob's exchange, and credits the money into
  Alice's purse.

  * Q: How does Bob find out if Alice's exchange supports a wad transfer from
    Bob's exchange? A: This needs to be part of the wad policy.
  * Q: How does Bob get a "receipt" to prove that he paid Alice?
    A: He has Alice's account public key and the associated signature
    chain leading to her payment request. If he paid someone else by accident,
    the KYC of Alice's exchange could be used to find out who received the funds.

Cross-exchange W2W payment offer:

* Carol wants to send some money to Dave as a birthday gift.  Carol knows that
  Dave is using Taler, but she does not know which exchange he is using.  She
  opens her Taler wallet and initiates a P2P payment.  She sends the resulting
  ``taler://purse/{EXCHANGE_URL}/{PURSE_PRIV}`` in an e-mail to Dave.
  Dave opens they link in the e-mail with his Taler wallet.
  Since Dave is using a different exchange than Carol, Dave's wallet
  issues a **merge** request to Carol's exchange pointing Carol's exchange
  to Dave's account at his exchange.  Shortly after,
  Dave's exchange receives a **wad** from Carol's exchange,
  and credits Dave's account with the money.


State machine for Purses
------------------------

.. code-block:: none

   // The "OPEN-ACCOUNT" start state implies that the purse is associated
   // with an account and a merge request for that account.
   -> OPEN-ACCOUNT

   // "Partial" means that it is filled with a fraction of the coins
   // indicated in the creation request.
   OPEN-ACCOUNT -> PARTIAL

   // The purse was filled with as many coins
   // as indicated in the creation request, resulting in the transaction to complete.
   OPEN-ACCOUNT -> ACCEPTED

   // The offer expired before any payment was received.
   OPEN-ACCOUNT -> CLOSED

   // The purse was filled with as many coins
   // as indicated in the creation request, resulting in the transaction to complete.
   PARTIAL -> ACCEPTED

   // During an abort, already deposited coins are being taken out of the purse.
   PARTIAL -> OPEN-ACCOUNT

   // All coins put into the purse are refunded because the
   // payer never completed the purchase before the timeout.
   PARTIAL -> CLOSED

   // The "OPEN-DEPOSIT" start state implies that the purse is filled with
   // deposited coins.
   -> OPEN-DEPOSIT

   // Paid and merged with an account (locally or via a wad)
   OPEN-DEPOSIT -> ACCEPTED

   // The offer expired without a merge request.
   OPEN-DEPOSIT -> CLOSED


Additional considerations
-------------------------

* Creation of additional accounts per customer can
  be discouraged by asking for higher fees.
* The global transaction volume of one customer can be easily
  determined by authorities, which can then trigger further audits
  of the customer
* As a technically expensive but more water-tight measure, normal
  withdrawals from reserves could be disallowed.  Instead,
  a modified refresh protocol could ensure that whoever has knowledge
  of the account private key can also learn the private keys
  of coins withdrawn from that account, thereby removing
  Taler's "one-hop withdrawal loohole".


Exchange database schema changes
--------------------------------

We need to exchange the existing reserves table to include bits for KYC-needed
and KYC-passed. Also, we need to store the payto://-URI of the bank account.

Finally, we may need to keep some link to the KYC data, even though the
exchange technically does not need it, but likely there might be regulatory
reasons to have that association for legal inquiries. (However, it would
also be possible to keep that link only in the external KYC service's
database.)

.. sourcecode:: sql

  -- Everything in one big transaction
  BEGIN;
  -- Check patch versioning is in place.
  SELECT _v.register_patch('exchange-TBD', NULL, NULL);
  --
  CREATE TABLE IF NOT EXISTS partners
  (partner_serial_id BIGSERIAL UNIQUE
  ,partner_master_pub BYTEA NOT NULL CHECK(LENGTH(reserve_pub)=32)
  ,start_date INT8 NOT NULL
  ,end_date INT8 NOT NULL
  ,wad_frequency INT8 NOT NULL
  ,wad_fee_val INT8 NOT NULL
  ,wad_fee_frac INT4 NOT NULL
  ,master_sig BYTEA NOT NULL CHECK (LENGTH(master_sig)=64))
  ,partner_base_url TEXT NOT NULL
  );
  COMMENT ON TABLE partners
    IS 'exchanges we do wad transfers to';
  COMMENT ON COLUMN partners.partner_master_pub
    IS 'offline master public key of the partner';
  COMMENT ON COLUMN partners.start_date
    IS 'starting date of the partnership';
  COMMENT ON COLUMN partners.end_date
    IS 'end date of the partnership';
  COMMENT ON COLUMN partners.wad_frequency
    IS 'how often do we promise to do wad transfers';
  COMMENT ON COLUMN partners.wad_fee_val
    IS 'how high is the fee for a wallet to be added to a wad to this partner';
  COMMENT ON COLUMN partners.partner_base_url
    IS 'base URL of the REST API for this partner';
  COMMENT ON COLUMN partners.master_sig
    IS 'signature of our master public key affirming the partnership, of purpose TALER_SIGNATURE_MASTER_PARTNER_DETAILS';
  --
  ALTER TABLE reserves
    ADD COLUMN kyc_needed BOOLEAN NOT NULL DEFAULT (false)
    ADD COLUMN kyc_passed BOOLEAN NOT NULL DEFAULT (false)
    ADD COLUMN payto_uri TEXT DEFAULT (NULL)
    ADD COLUMN kyc_link TEXT DEFAULT (NULL);
  COMMENT ON COLUMN reserves.kyc_needed
    IS 'set to true once a reserve was merged with a purse';
  COMMENT ON COLUMN reserves.kyc_passed
    IS 'set to true once the user performed the KYC check';
  COMMENT ON COLUMN reserves.payto_uri
    IS 'bank account details to use in case reserve is closed';
  COMMENT ON COLUMN reserves.kyc_link
    IS 'optional link to KYC data';
  --
  CREATE TABLE IF NOT EXISTS kyc_requests
  (kyc_request_serial_id BIGSERIAL UNIQUE
  ,reserve_uuid INT8 NOT NULL REFERENCES reserves (reserve_uuid) ON DELETE CASCADE
  ,kyc_date INT8 NOT NULL
  ,kyc_retry INT8 NOT NULL
  ,kyc_fee_val INT8 NOT NULL
  ,kyc_fee_frac INT4 NOT NULL
  ,reserve_sig BYTEA NOT NULL CHECK (LENGTH(reserve_sig)=64))
  ,kyc_id TEXT NOT NULL
  ,PRIMARY KEY (reserve_uuid, kyc_date)
  );
  COMMENT ON TABLE kyc_requests
    IS 'KYC processes initiated by the owner of a reserve';
  COMMENT ON COLUMN kyc_requests.reserve_uuid
    IS 'Reserve for which the KYC request was triggered.';
  COMMENT ON COLUMN kyc_requests.reserve_sig
    IS 'Signature affirming the KYC request';
  COMMENT ON COLUMN kyc_requests.kyc_fee_val
    IS 'Amount paid by the reserve for the KYC process.';
  COMMENT ON COLUMN kyc_requests.kyc_date
    IS 'When was the KYC process originally initiated.';
  COMMENT ON COLUMN kyc_requests.kyc_retry
    IS 'Timestamp when we should next query the KYC backend for the KYC status. The maximum possible numeric value indicates that we do not need to ever check the status of this KYC process again.';
  COMMENT ON COLUMN kyc_requests.kyc_id
    IS 'ID of the KYC process, used to compute the URL returned to the client as well as for the exchange to check if the KYC has completed. Format depends on the KYC process of the bank.';
  --
  CREATE TABLE IF NOT EXISTS mergers
  (merge_request_serial_id BIGSERIAL UNIQUE
  ,reserve_uuid BYTEA NOT NULL REFERENCES reserves (reserve_uuid) ON DELETE CASCADE
  ,partner_serial_id INT8 REFERENCES partners(partner_serial_id) ON DELETE CASCADE,
  ,reserve_pub BYTEA NOT NULL CHECK (LENGTH(reserve_pub)=32),
  ,purse_pub BYTEA NOT NULL CHECK (LENGTH(purse_pub)=32),
  ,reserve_sig BYTEA NOT NULL CHECK (LENGTH(reserve_sig)=64))
  ,purse_sig BYTEA NOT NULL CHECK (LENGTH(purse_sig)=64))
  ,merge_timestamp INT8 NOT NULL
  ,purse_expiration INT8 NOT NULL
  ,h_contract_terms BYTEA NOT NULL CHECK (LENGTH(h_contract_terms)=64))
  ,purse_val INT8 NOT NULL
  ,purse_frac INT4 NOT NULL
  ,PRIMARY KEY (purse_pub)
  );
  COMMENT ON TABLE mergers
    IS 'Merge requests where a purse- and account-owner requested merging the purse into the account';
  COMMENT ON COLUMN mergers.reserve_uuid
    IS 'identifies the reserve';
  COMMENT ON COLUMN mergers.partner_serial_id
    IS 'identifies the partner exchange, NULL in case the target reserve lives at this exchange';
  COMMENT ON COLUMN mergers.reserve_pub
    IS 'public key of the target reserve';
  COMMENT ON COLUMN mergers.purse_pub
    IS 'public key of the purse';
  COMMENT ON COLUMN mergers.reserve_sig
    IS 'signature by the reserve private key affirming the merge';
  COMMENT ON COLUMN mergers.purse_sig
    IS 'signature by the purse private key affirming the merge';
  COMMENT ON COLUMN mergers.merge_timestamp
    IS 'when was the merge message signed';
  COMMENT ON COLUMN mergers.purse_expiration
    IS 'when is the purse set to expire';
  COMMENT ON COLUMN mergers.h_contract_terms
    IS 'hash of the contract terms both sides are to agree upon';
  COMMENT ON COLUMN mergers.purse_val
    IS 'amount to be transferred from the purse to the reserve (excludes deposit fees)';
  CREATE INDEX IF NOT EXISTS mergers_reserve_uuid
    ON mergers (reserve_uuid);
  COMMENT ON INDEX mergers_reserve_uuid
    IS 'needed in reserve history computation';
  --
  CREATE TABLE IF NOT EXISTS contracts
  (contract_serial_id BIGSERIAL UNIQUE
  ,purse_pub BYTEA NOT NULL CHECK (LENGTH(purse_pub)=32),
  ,pub_ckey BYTEA NOT NULL CHECK (LENGTH(pub_ckey)=32)),
  ,e_contract BYTEA NOT NULL,
  ,PRIMARY KEY (purse_pub)
  );
  COMMENT ON TABLE contracts
    IS 'encrypted contracts associated with purses';
  COMMENT ON COLUMN contracts.purse_pub
    IS 'public key of the purse that the contract is associated with';
  COMMENT ON COLUMN contracts.pub_ckey
    IS 'Public ECDH key used to encrypt the contract, to be used with the purse private key for decryption';
  COMMENT ON COLUMN contracts.e_contract
    IS 'AES-GCM encrypted contract terms (contains gzip compressed JSON after decryption)';
  --
  CREATE TABLE IF NOT EXISTS history_requests
  (reserve_uuid INT8 NOT NULL REFERENCES reserves(reserve_uuid) ON DELETE CASCADE,
  ,request_timestamp INT8 NOT NULL
  ,reserve_sig BYTEA NOT NULL CHECK (LENGTH(reserve_sig)=64))
  ,history_fee_val INT8 NOT NULL
  ,history_fee_frac INT4 NOT NULL
  ,PRIMARY KEY (reserve_uuid,request_timestamp)
  );
  COMMENT ON TABLE history_requests
    IS 'Paid history requests issued by a client against a reserve';
  COMMENT ON COLUMN history_requests.request_timestamp
    IS 'When was the history request made';
  COMMENT ON COLUMN history_requests.reserve_sig
    IS 'Signature approving payment for the history request';
  COMMENT ON COLUMN history_requests.history_fee_val
    IS 'History fee approved by the signature';
  --
  CREATE TABLE IF NOT EXISTS close_requests
  (reserve_uuid INT8 NOT NULL REFERENCES reserves(reserve_uuid) ON DELETE CASCADE,
  ,close_timestamp INT8 NOT NULL
  ,reserve_sig BYTEA NOT NULL CHECK (LENGTH(reserve_sig)=64))
  ,close_val INT8 NOT NULL
  ,close_frac INT4 NOT NULL
  ,PRIMARY KEY (reserve_uuid,close_timestamp)
  );
  COMMENT ON TABLE close_requests
    IS 'Explicit requests by a reserve owner to close a reserve immediately';
  COMMENT ON COLUMN close_requests.close_timestamp
    IS 'When the request was created by the client';
  COMMENT ON COLUMN close_requests.reserve_sig
    IS 'Signature affirming that the reserve is to be closed';
  COMMENT ON COLUMN close_requests.close_val
    IS 'Balance of the reserve at the time of closing, to be wired to the associated bank account (minus the closing fee)';

  --
  CREATE TABLE IF NOT EXISTS purse_requests
  (purse_deposit_serial_id BIGSERIAL UNIQUE
  ,purse_pub BYTEA NOT NULL CHECK (LENGTH(purse_pub)=32),
  ,purse_expiration INT8 NOT NULL
  ,h_contract_terms BYTEA NOT NULL CHECK (LENGTH(h_contract_terms)=64)
  ,amount_with_fee_val INT8 NOT NULL
  ,amount_with_fee_frac INT4 NOT NULL
  ,purse_sig BYTEA NOT NULL CHECK(LENGTH(purse_sig)=64)
  ,PRIMARY KEY (purse_pub,coin_pub)
  );
  COMMENT ON TABLE purse_requests
    IS 'Requests establishing purses, associating them with a contract but without a target reserve';
  COMMENT ON COLUMN purse_requests.purse_pub
    IS 'Public key of the purse';
  COMMENT ON COLUMN purse_requests.purse_expiration
    IS 'When the purse is set to expire';
  COMMENT ON COLUMN purse_requests.h_contract_terms
    IS 'Hash of the contract the parties are to agree to';
  COMMENT ON COLUMN purse_requests.amount_with_fee_val
    IS 'Total amount expected to be in the purse';
  COMMENT ON COLUMN purse_requests.purse_sig
    IS 'Signature of the purse affirming the purse parameters, of type TALER_SIGNATURE_PURSE_REQUEST';
  --
  CREATE TABLE IF NOT EXISTS purse_deposits
  (purse_deposit_serial_id BIGSERIAL UNIQUE
  ,purse_pub BYTEA NOT NULL CHECK (LENGTH(purse_pub)=32),
  ,coin_pub BYTEA NOT NULL REFERENCES known_coins (coin_pub) ON DELETE CASCADE
  ,amount_with_fee_val INT8 NOT NULL
  ,amount_with_fee_frac INT4 NOT NULL
  ,coin_sig BYTEA NOT NULL CHECK(LENGTH(coin_sig)=64)
  ,PRIMARY KEY (purse_pub,coin_pub)
  );
  COMMENT ON TABLE purse_deposits
    IS 'Requests depositing coins into a purse';
  COMMENT ON COLUMN purse_deposits.purse_pub
    IS 'Public key of the purse';
  COMMENT ON COLUMN purse_deposits.coin_pub
    IS 'Public key of the coin being deposited';
  COMMENT ON COLUMN purse_deposits.amount_with_fee_val
    IS 'Total amount being deposited';
  COMMENT ON COLUMN purse_deposits.coin_sig
    IS 'Signature of the coin affirming the deposit into the purse, of type TALER_SIGNATURE_PURSE_DEPOSIT';
  --
  CREATE TABLE IF NOT EXISTS wads_out
  (wad_out_serial_id BIGSERIAL UNIQUE
  ,wad_id BYTEA PRIMARY KEY CHECK (LENGTH(wad_id)=24)
  ,partner_serial_id INT8 NOT NULL REFERENCES partners(partner_serial_id) ON DELETE CASCADE,
  ,amount_val INT8 NOT NULL
  ,amount_frac INT4 NOT NULL
  ,execution_time INT8 NOT NULL
  ,UNIQUE (exchange_url, execution_time)
  );
  COMMENT ON TABLE wads_out
    IS 'Wire transfers made to another exchange to transfer purse funds';
  COMMENT ON COLUMN wads_out.wad_id
    IS 'Unique identifier of the wad, part of the wire transfer subject';
  COMMENT ON COLUMN wads_out.partner_serial_id
    IS 'target exchange of the wad';
  COMMENT ON COLUMN wads_out.amount_val
    IS 'Amount that was wired';
  COMMENT ON COLUMN wads_out.execution_time
    IS 'Time when the wire transfer was scheduled';
  --
  CREATE TABLE IF NOT EXISTS wad_out_entries
  (wad_out_entry_serial_id BIGSERIAL UNIQUE
  ,wad_out_serial_id INT8 REFERENCES wads_out (wad_out_serial_id) ON DELETE CASCADE
  ,reserve_pub BYTEA NOT NULL CHECK(LENGTH(reserve_pub)=32)
  ,purse_pub BYTEA PRIMARY KEY CHECK(LENGTH(purse_pub)=32)
  ,h_contract BYTEA NOT NULL CHECK(LENGTH(h_contract)=64)
  ,purse_expiration INT8 NOT NULL
  ,merge_timestamp INT8 NOT NULL
  ,amount_with_fee_val INT8 NOT NULL
  ,amount_with_fee_frac INT4 NOT NULL
  ,wad_fee_val INT8 NOT NULL
  ,wad_fee_frac INT4 NOT NULL
  ,deposit_fees_val INT8 NOT NULL
  ,deposit_fees_frac INT4 NOT NULL
  ,reserve_sig BYTEA NOT NULL CHECK (LENGTH(reserve_sig)=64))
  ,purse_sig BYTEA NOT NULL CHECK (LENGTH(purse_sig)=64))
  );
  CREATE INDEX IF NOT EXISTS wad_out_entries_index_by_wad
    ON wad_out_entries (wad_out_serial_id);
  COMMENT ON TABLE wad_out_entries
    IS 'Purses combined into a wad';
  COMMENT ON COLUMN wad_out_entries.wad_out_serial_id
    IS 'Wad the purse was part of';
  COMMENT ON COLUMN wad_out_entries.reserve_pub
    IS 'Target reserve for the purse';
  COMMENT ON COLUMN wad_out_entries.purse_pub
    IS 'Public key of the purse';
  COMMENT ON COLUMN wad_out_entries.h_contract
    IS 'Hash of the contract associated with the purse';
  COMMENT ON COLUMN wad_out_entries.purse_expiration
    IS 'Time when the purse expires';
  COMMENT ON COLUMN wad_out_entries.merge_timestamp
    IS 'Time when the merge was approved';
  COMMENT ON COLUMN wad_out_entries.amount_with_fee_val
    IS 'Total amount in the purse';
  COMMENT ON COLUMN wad_out_entries.wad_fee_val
    IS 'Wat fee charged to the purse';
  COMMENT ON COLUMN wad_out_entries.deposit_fees_val
    IS 'Total deposit fees charged to the purse';
  COMMENT ON COLUMN wad_out_entries.reserve_sig
    IS 'Signature by the receiving reserve, of purpose TALER_SIGNATURE_ACCOUNT_MERGE';
  COMMENT ON COLUMN wad_out_entries.purse_sig
    IS 'Signature by the purse of purpose TALER_SIGNATURE_PURSE_MERGE';
  --
  CREATE TABLE IF NOT EXISTS wads_in
  (wad_in_serial_id BIGSERIAL UNIQUE
  ,wad_id BYTEA PRIMARY KEY CHECK (LENGTH(wad_id)=24)
  ,origin_exchange_url TEXT NOT NULL
  ,amount_val INT8 NOT NULL
  ,amount_frac INT4 NOT NULL
  ,arrival_time INT8 NOT NULL
  ,UNIQUE (wad_id, origin_exchange_url)
  );
  COMMENT ON TABLE wads_in_entries
    IS 'Incoming exchange-to-exchange wad wire transfers';
  COMMENT ON COLUMN wads_in.wad_id
    IS 'Unique identifier of the wad, part of the wire transfer subject';
  COMMENT ON COLUMN wads_in.origin_exchange_url
    IS 'Base URL of the originating URL, also part of the wire transfer subject';
  COMMENT ON COLUMN wads_in.amount_val
    IS 'Actual amount that was received by our exchange';
  COMMENT ON COLUMN wads_in.arrival_time
    IS 'Time when the wad was received';
  --
  CREATE TABLE IF NOT EXISTS wad_in_entries
  (wad_in_entry_serial_id BIGSERIAL UNIQUE
  ,wad_in_serial_id INT8 REFERENCES wads_in (wad_serial_id) ON DELETE CASCADE
  ,reserve_pub BYTEA NOT NULL CHECK(LENGTH(reserve_pub)=32)
  ,purse_pub BYTEA PRIMARY KEY CHECK(LENGTH(purse_pub)=32)
  ,h_contract BYTEA NOT NULL CHECK(LENGTH(h_contract)=64)
  ,purse_expiration INT8 NOT NULL
  ,merge_timestamp INT8 NOT NULL
  ,amount_with_fee_val INT8 NOT NULL
  ,amount_with_fee_frac INT4 NOT NULL
  ,wad_fee_val INT8 NOT NULL
  ,wad_fee_frac INT4 NOT NULL
  ,deposit_fees_val INT8 NOT NULL
  ,deposit_fees_frac INT4 NOT NULL
  ,reserve_sig BYTEA NOT NULL CHECK (LENGTH(reserve_sig)=64))
  ,purse_sig BYTEA NOT NULL CHECK (LENGTH(purse_sig)=64))
  );
  COMMENT ON TABLE wad_in_entries
    IS 'list of purses aggregated in a wad according to the sending exchange';
  COMMENT ON COLUMN wad_in_entries.wad_in_serial_id
    IS 'wad for which the given purse was included in the aggregation';
  COMMENT ON COLUMN wad_in_entries.reserve_pub
    IS 'target account of the purse (must be at the local exchange)';
  COMMENT ON COLUMN wad_in_entries.purse_pub
    IS 'public key of the purse that was merged';
  COMMENT ON COLUMN wad_in_entries.h_contract
    IS 'hash of the contract terms of the purse';
  COMMENT ON COLUMN wad_in_entries.purse_expiration
    IS 'Time when the purse was set to expire';
  COMMENT ON COLUMN wad_in_entries.merge_timestamp
    IS 'Time when the merge was approved';
  COMMENT ON COLUMN wad_in_entries.amount_with_fee_val
    IS 'Total amount in the purse';
  COMMENT ON COLUMN wad_in_entries.wad_fee_val
    IS 'Total wad fees paid by the purse';
  COMMENT ON COLUMN wad_in_entries.deposit_fees_val
    IS 'Total deposit fees paid when depositing coins into the purse';
  COMMENT ON COLUMN wad_in_entries.reserve_sig
    IS 'Signature by the receiving reserve, of purpose TALER_SIGNATURE_ACCOUNT_MERGE';
  COMMENT ON COLUMN wad_in_entries.purse_sig
    IS 'Signature by the purse of purpose TALER_SIGNATURE_PURSE_MERGE';
  CREATE INDEX IF NOT EXISTS wad_in_entries_wad_in_serial
    ON wad_in_entries (wad_in_serial_id);
  CREATE INDEX IF NOT EXISTS wad_in_entries_reserve_pub
    ON wad_in_entries (reserve_pub);
  COMMENT ON INDEX wad_in_entries_wad_in_serial
    IS 'needed to lookup all transfers associated with a wad';
  COMMENT ON INDEX wad_in_entries_reserve_pub
    IS 'needed to compute reserve history';
  --
  CREATE TABLE IF NOT EXISTS p2pfees
  (p2pfees_serial_id BIGSERIAL UNIQUE
  ,start_date INT8 NOT NULL
  ,end_date INT8 NOT NULL
  ,kyc_timeout INT8 NOT NULL
  ,purse_timeout INT8 NOT NULL
  ,history_retention INT8 NOT NULL
  ,purse_account_limit INT NOT NULL
  ,kyc_fee_val INT8 NOT NULL
  ,kyc_fee_frac INT4 NOT NULL
  ,history_fee_val INT8 NOT NULL
  ,history_fee_frac INT4 NOT NULL
  ,account_fee_val INT8 NOT NULL
  ,account_fee_frac INT4 NOT NULL
  ,purse_fee_val INT8 NOT NULL
  ,purse_fee_frac INT4 NOT NULL
  ,master_sig BYTEA NOT NULL CHECK (LENGTH(master_sig)=64))
  );
  --
  CREATE TABLE IF NOT EXISTS partner_accounts
  (payto_uri VARCHAR PRIMARY KEY
  ,partner_serial_id INT8 REFERENCES partners(partner_serial_id) ON DELETE CASCADE,
  ,partner_master_sig BYTEA CHECK (LENGTH(partner_master_sig)=64)
  ,last_seen INT8 NOT NULL
  );
  CREATE INDEX IF NOT EXISTS partner_accounts_index_by_partner_and_time
    ON partner_accounts (partner_serial_id,last_seen);
  COMMENT ON TABLE partner_accounts
    IS 'Table with bank accounts of the partner exchange. Entries never expire as we need to remember the signature for the auditor.';
  COMMENT ON COLUMN wire_accounts.payto_uri
    IS 'payto URI (RFC 8905) with the bank account of the partner exchange.';
  COMMENT ON COLUMN wire_accounts.partner_master_sig
    IS 'Signature of purpose TALER_SIGNATURE_MASTER_WIRE_DETAILS by the partner master public key';
  COMMENT ON COLUMN wire_accounts.last_seen
    IS 'Last time we saw this account as being active at the partner exchange. Used to select the most recent entry, and to detect when we should check again.';
  -- Complete transaction
  COMMIT;



Alternatives
============

* The payer could directly give deposit permissions to the payee.
  This has two problems:

  1. The payer does not know the wire details of the payee.
     Thus we would need to introduce some "wildcard deposit permission",
     where the exchange allows any wire details on ``/deposit``.
  2. The payment information would be rather large, making it difficult
     to transfer via a QR code or short text message.

* Account history exceeding a configurable time limit (like 6 years)
  could be subject to garbage collection. However, doing so may be difficult to
  square with onboarding new auditors in the presence of existing
  accounts, as the auditors could then not reconstruct the account
  balances from cryptographic proofs.

* Accounts without KYC check could be eventually closed. However,
  even if the coins used to fill the account are refunded, it
  would be difficult to inform the originating wallet that the
  coins have received a refund. This applies even more strongly
  in case of accounts filled via wads, where in theory the
  originating exchange may not even be in business anymore.
  Thus, it is cleaner and simpler to declare such funds forfeit.


Drawbacks
=========

The overall changes required are not small:

* New **KYC fee**, **wad fee** and **account history fee**
  required in ``/keys`` endpoint (similar to closing and wire fees),
  requires some work across toolchain (offline signature, etc.)
* New ``taler`` wire method needs special case to possibly bypass
  (same exchange scenario, with long-poll trigger) the usual aggregation logic.
* New exchange table(s) required to store inbound amounts by account.
  Likely two tables, one for local exchange p2p and one for remote exchange p2p
  payments.
* New exchange table for purses required (for remote p2p payments).
* New exchange logic required to make ``transfers`` requests for purses
  (another separate process).
* New ``/account/$ACCOUNT_PUB/kyc`` endpoint required.
* New ``/purse/$PURSE_PUB/merge`` endpoint required.
* Additional tables to be verified by the auditor.
* ``taler-exchange-wirewatch`` needs to support receiving purses closures
  and exchange-to-exchange wire transfers with WTIDs.

Aside from implementation complexity, the solution has the following drawbacks:

* If a W2W payment failed (say the receiver lost the account private key),
  the customer's money can be forfeit.  Alas, this should be very, very rare
  as the wallet software can trivially ensure that a backup was made of the
  account private key before initiating the KYC process.


Refinements
===========

In the original design, a payer making a payment offer sends the purse private
key to the payee, so that the payee can sign the merge request with it.  This
creates a security issue, as theoretically the payee could sign a different
contract with the purse private key, and conspire with the exchange to replace
the original contract.  In this case, the payer would be making a payment to
the "wrong" contract, and have no proof of the exchange an payee conspiring
against it.

A simple fix seems possible: instead of having simply one public-private key
pair for a purse, we have a PayerContractKey and a PurseMergeKey pair. The payer
would pay into a purse identified by the PayerContractKey and associate a
PurseMergeKey with the purse. The payer can then safely share the
PayeeMergeKey with the payee, as it is ONLY useful for the merge and not to
sign the contract.  Payments would be made into a purse identified by the
PurseContractKey.

When payments flow in the other direction, the split of the keys seems
unnecessary (as only a public key is transmitted anyway. However, schema-wise,
signing the contract with the PurseContractKey and the merge with the
PurseMergeKey would still work.  Only the public PurseContractKey would need
to be sent to the payer.



Q / A
=====

* Q: Why are direct payments into accounts allowed?

  * A: Direct payments into accounts may be used by the customer
    to fund the expenses for using the account.  They should not
    be used for payments between customers, as contract terms for
    the ``/deposit`` of coins cannot be negotiated.  Furthermore,
    the sender of the payment cannot be sure that the account of
    the sender is still valid.

* Q: Who "owns" a purse?  The payer of payee?

  * Both.  Ownership is shared.  Either the payer issues
    a refund on the purse, or the payee claims it by merging
    it with one of their accounts.

* Q: Are purses created with a pre-determined "capacity"?

  * A: Yes.  Otherwise there would be weird failure modes when the payee
    merges the purse before the payer fully deposited into it.

* Q: Are account public keys considered private or public data?

  * A: Public.  The payer needs a signature from the payee affirming
    that they accepted the contract, and this requires a key that
    is linked to the KYC process to be meaningful.  However, the
    software should NOT permit direct payments into foreign accounts
    because it would be too easy to accidentally send payments that
    nobody can receive, because the account public key is wrong/lost.

* Q: Why do traditional merchant payments not use purses?

  * Refunds are not possible with purses after they are closed.
  * The customer cannot prove that they own the contract terms
    (Contract terms claiming requires interactivity that is not
    possible in all W2W scenarios.) Thus, while payers can prove
    that they paid, the payee may claim someone else also
    bought the same product. A secure channel must thus be used to
    exchange the purchase offer.

* Q: What determines when a wad transfer can happen between two exchanges?

  * Exchanges explicitly state which other exchanges they are willing
    to do wad transfers with (and how often, at what cost). This may involve
    abstract policies like sharing an auditor, using the same currency and the
    same (banking) protocol, or other constraints (like a specific list of
    exchanges).

* Q: What happens if the owner of a reserve never drains it?

  * Reserves are eventually closed. If the reserve is associated
    with a bank account, the remaining funds are sent to that bank
    account. If the reserve was created via a merge, and the owner
    failed to associate a bank account with it (say because the
    KYC step never happened), then the reserve balance is forfeit
    to the exchange upon expiration.
