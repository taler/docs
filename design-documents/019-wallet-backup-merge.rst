DD 19: Wallet Backup Merging
############################

Summary
=======

This design doc discusses considerations for merging wallet backups.


Motivation
==========

The wallet backup functionality is meant to be used primarily with one device
per backup account.  Multiple devices sharing one backup account is heavily
discouraged, as it can lead to unexpected and unwanted user experiences, such
as money suddenly vanishing when it has been spent by another device
that shares the same backup account.

However, there are some situations where more than one device
accesses the same backup account.  This happens when:

1. A wallet backup is restored on a new device, but the
   old device is still active.  In this scenario, the devices
   have different device IDs, but share the same wallet root
   public key.
2. An old wallet backup is taken over by an existing wallet.
   In this scenario, the devices have different devices IDs and
   different wallet root public keys.
   ### CG: This is not exactly more than one device accessing the same backup account!
   ### CG: Maybe formulate intro differently, to talk about key scenarios that deserve consideration / need to be distinguished?
3. A wallet device is copied, for example by restoring the whole
   device from a device-level backup (not a wallet backup!).
   In this scenario, the devices have the same device ID
   and the same wallet root public key.


Requirements
============

The backup merging must ensure that:

* No data that the user wants to keep is lost.
* No data resurfaces that the user has previously intentionally deleted.
* Conflicts should be resolved automatically wherever possible.
* The solution tolerates system clocks not being monotonic.


Proposed Solution
=================

Stored Information
------------------

* Every wallet keeps track of the following data:

  * The current version number (positive integer)
  * The current wallet root public key (Ed25519 public key)
    ### CG: public key? Not the private key? What is the private information the wallet usually keeps?
  * The current device ID (human-readable string)
  * The status of every backup service account (not defined further here)
  * The last system time observed on the current device (by device ID).

* A backup blob stores the following information relevant for backup merging:

  * The backup's version number, equal to the version number of
    the wallet when the backup was uploaded.
  * The wallet root public key of the wallet that **owns** the backup account
  * The device ID of the wallet that **owns** the backup account.

* Every record and tombstone in the wallet's database and the backup blob keeps
  track of:

  * The version number at which the entry was created.
  * A timestamp for the entry, based on enforced monotonic time (per device ID).

The version number is incremented with every operation that adds an record or
tombstone to the wallet's database.
### CG: operation or transaction? I would prefer transaction here.


Resolving Conflicts
-------------------

This section describes how conflicts are resolved when a wallet (with ``wallet_version``, ``wallet_device_id``
and ``wallet_root_pub``) is merged with a backup (with ``backup_version``, ``backup_device_id``, and
``backup_root_pub``).
### CG: The term 'merged' is something I do not like. Is this during 'backup', 'restore', or 'sync'?
###     I suspect these cases need to be distinguished, because the user asking for a 'restore' is
###     not creating the same situation than a wallet 'sync'ing during an automated backup, and
###     that may again differ from an _initial_ backup (where I guess there are no conflicts, but
###     to improve understanding

* If ``wallet_root_pub != backup_root_pub``:  The user is shown a warning "the backup
  account was written to by another wallet and can't be read by this wallet", and offered a dialog to either:

  a. "Take over" the backup account and migrate it to the existing wallet root public key.
     A clear warning must be shown that this will kick out the other device currently connected
     to this account **and** will cause all data from the backup account to be lost.
     ### CG: Do we even want to allow this? How _can_ this happen exactly? What is the relationship between backup account and root key?
     ### CG: The private account key is derived from the root public key;
     ###     I do not see us saying anywhere that we would even support
     ###     extracting/exchanging account keys. Hence, I think this basically
     ###     cannot happen: to access the backup, I already must know the root private key.
  b. Remove the backup account from the list.

  Note that when first adding the backup account via a recovery code, there is a third option:
  Migrate wallet to the account's wallet root public key.  This is **only** possible when
  scanning the recovery code, as the wallet needs the wallet root secret key to migrate
  to the account.
  ### CG: I think this should be the only thing that can possibly happen, by UI/UX.
  ###     Of course _theroretically_ someone could extract ONLY an account-priv and
  ###     use that to download the backup, but then they should just not be able to
  ###     decrypt it. End of story.

* If ``wallet_root_pub == backup_root_pub`` and ``wallet_device_id != backup_device_id``:  The
  user is shown a warning "two wallet devices are using the same backup account", and given
  the option of:

  a. Taking over the backup account from the existing device.  This will not cause data loss,
     but the other device (if it still exists!) will stop syncing.
  b. To "abandon" the current wallet.  This (optional, but recommended) will sync the current wallet state
     with a special marker in the backup blob (so the other wallet continues syncing without
     having to ask the user), and then delete the database contends and create a new ``wallet_root_pub``.
     ### CG: I do not think we can 'recommend' option b, because we do not know if the other
     ### device still exists. So the UI should be neutral here between the two equally valid choices.

* If ``wallet_root_pub == backup_root_pub`` and ``wallet_device_id == backup_device_id``:

  * If ``wallet_version > backup_version``, do a normal backup
    cycle (merge backup blob into wallet and upload a new backup).
    ### CG: We should note that the motivation for a merge arises
    ###     from the 3rd scenario under Motivation: full device backup&recovery.

  * If ``wallet_version <= backup_version``, another wallet with the same
    root public key must have "tampered"
    with the wallet's state.  Do a normal backup cycle, but consider displaying
    a warning/notification to the user.
    ### CG: I think there is no point in distinguishing these two cases;
    ###     in both cases, if the merge is non-trivial, something odd happened.
    ###     Still, I am not sure that a warning/notification is helpful, as
    ###     it is hardly actionable for the user.


Garbage-collecting Tombstones
-----------------------------

Tombstones should be automatically garbage-collected when the following criteria
are both fulfilled:

* The versions of active backup accounts are all larger than
  the tombstone's version, and
* the tombstone exceeded a threshold age (say, 3 days).

### CG: I backup at providers A and B. Make transaction T. Then I remove
###     provider A from my provider list. I then delete T. Eventually, I backup
###     again at provider B without the expired tombstone.  Finally,
###     I restore from provider A, and then merge with provider B.
###     Here, the 'merge' has to be somehow smart enough to drop
###     the deleted data from provider A's backup without the tombstone.
###     I think we can safely decide that this is the case because
###     backup from A says that it was---at the time---synced with provider B.
###     However, this means that we do need to additionally retain the
###     historic chain of backup providers and their last merge points/versions!


Q / A
=====

* Q: Why are version numbers and tombstones necessary in backups?

  * A: When syncing with a backup server that still has an old version
    (but same device ID and wallet root pub), the tombstones ensure
    that no old data is re-surfaced that has been deleted in later
    versions.  This can happen in practice even with only one device,
    namely when a backup provider is unavailable for a long time,
    but then becomes available again.

* Q: Why are tombstones only GCed after exceeding an age threshold?

  * A:  If we deleted them immediately, this might cause data to resurface
    if a user temporarily removes and adds a backup account (say by accident)
    that hasn't been synced in a while.
    ### CG: See above: the timeout does IMO not help here.
    ###     I think we need to track removed backup accounts
    ###     and the last version that was synced there,
    ###     and then basically determine if a sync/merge-chain exists
    ###     from the (possibly resurfaced) transaction version to
    ###     the current wallet version!

* Q: Why doesn't the wallet root public key get rotated every time
  that a wallet backup is restored on a new device?

  * A: Because that would mean that old "paper backups" and Anastasis
    backups stop working, because they are based on the wallet root key.

* Q: Why can't the wallet obtain some unique devices identifier to exclude
  case 3 (same device ID, same wallet root pub)?

  * A: Because we don't have a reliable API for this on many platforms.
    Even if we had one, we shouldn't rely on it.
