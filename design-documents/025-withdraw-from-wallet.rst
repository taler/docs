DD 25: Withdraw coins manually starting from the wallet
#######################################################

Summary
=======

This document presents the design discussion about the manual withdraw screens

Motivation
==========

To have a way to initiate a withdrawal process and complete with a bank that is
not aware of GNU Taler

Proposed Solution
=================

Access to the feature
^^^^^^^^^^^^^^^^^^^^^

Adding a withdraw button in the main balance page to initiate the process.

This feature can be use in the Pay call-to-action when there is not enough coins.

Start
^^^^^

This screen the user will be able to select the currency from a list of known
currencies, switch the exchange, go to a page to add an exchange and define an
amount to be withdraw.

.. image:: ../images/wallet-start-manual-withdraw.svg
  :width: 800


Success
^^^^^^^

Here the user will see the account details where it needs to send money to
complete the withdrawal.

.. image:: ../images/wallet-confirm-withdraw.svg
  :width: 800

Transaction history
^^^^^^^^^^^^^^^^^^^

The account information should be added into the Withdrawal Transaction detail
screen if the withdrawal is still pending. This will also affect the use case
when the user started the transaction from a taler-aware bank (in which case the
user doesn't need to do an extra step to complete the process) so the text
should be consistent in both scenarios.

Alternatives
============

* removing the amount field, the exchange will send coins equal to the amount it received

* showing the fees, can we calculate the withdrawal fee?

* should we show the terms of service?

* exchange field first has been discussed, but the exchange list its only showing the
  current currency exchanges, the user need to switch the currency first. Adding a
  new exchange should be done in a different context that can be accessed using the
  ``add exchange`` link
