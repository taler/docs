DD 45: Single-Depth Inheritance of KYC for Reserves
###################################################

Summary
=======

This document presents and discusses a mechanism by which a reserve A can
provide KYC attestation for another reserve B, whenever A's KYC attestation is
the result of a proper KYC-process and not inherited itself. During the
transitive attestation process, A can change the birthday for reserve B become
younger, i.e. choose a date closer to the current date than the original
birthday.


Motivation
==========

There are two reasons that motivate our proposal:

#. KYC attestation of a reserve is a must for Peer-2-Peer payments.  However, 
   a KYC process is usually costly for the exchange and -ultimately- the
   customer. When a customer has multiple long-term reserves, it should be
   possible to inherit the KYC attestation from one to another.

#. A parent should be able to provide KYC attestation for the long-term reserve
   of his or her child and at the same time provide appropriate birthday
   information.  That way, the child can withdraw money from its reserve 

      - with appropriate and evolving age-restriction always in place,

      - no further age-commitment interaction with the parent.

With the ability to attest KYC transitively, we can reduce the cost of
ownership of long-term reserves and enable the principle of subsidiarity,
according to which parents are responsible for age-restriction settings.


Requirements
============

* none

Proposed Solution
=================

There are changes in the exchange and in the wallet necessary.

Changes in the Exchange
^^^^^^^^^^^^^^^^^^^^^^^

A new configuration option in the TALER-configuration defines the maximum
number of attestations that a (KYC'ed) reserve can provide for other reserves.

.. code:: none

   [kyc-legitimization-inheritance]
   MAXIMUM_ATTESTATIONS = [number]


The database schema needs to be adjusted to incorporate

* a boolean field ``inherited`` in the table ``kyc_attributes`` to indicate the
  inheritance status

* an integer field ``attestations`` in the table ``reserves`` to count the
  number of transitive attestations performed by a reserve.


On the exchange we propose the following new endpoint:


.. http:post:: /kyc-attest/$TARGET_RESERVE_PUB

**Request:**

The request body must be a `KYCAttestationRequest` object.

**Response:**

  :http:statuscode:`200 OK`:
    Both, the attesting and the target reserves were known to the exchange, and the 
    KYC data of the attesting reserve has been successfully inherited by the
    target reserve (with optionally adjusted birthday).
  :http:statuscode:`403 Forbidden`:
    The attesting reserve is not allowed to transitively attest another reserve.
    This is because the attesting reserve either

    #. lacks KYC attestation or 

    #. its KYC attestation was itself inherited or

    #. has reached the allowed maximum number of transitive attestations.

    The response comes with a standard `ErrorDetail`.
  :http:statuscode:`404 Not found`:
    One of the reserve keys belongs to a reserve which is unknown to the exchange.
    The response comes with a standard `ErrorDetail`, containing the unknown key.
  :http:statuscode:`409 Conflict`:
    The birthday in the request is not acceptable, as it points to an earlier
    point in time (more distant from current time) than the birthday of the
    attesting reserve.

**Details:**

.. ts:def:: KYCAttestationRequest

   interface KYCAttestationRequest {
      // An optional birthday for the target reserve. MUST be equal or younger
      // (more recent to current time) than the birthday of the attesting
      // reserve.
      birthday?: FuzzyDateString;

      // The public key of the attesting reserve
      attester_pub: EddsaPublicKey;

      // Signature of purpose
      // ``TALER_SIGNATURE_WALLET_RESERVE_TRANSITIVE_KYC`` over
      // a `TALER_ReserveKYCAttestationPS`.
      attester_sig: EddsaSignature;
   }


.. ts:def:: FuzzyDateString

   // A date of the form "YYYY-MM-DD","YYYY-MM-00" or "YYYY-00-00".
   // "YYYY-MM-00" will be evaluated as "YYYY-MM-01" and
   // "YYYY-00-00" will be evaluated as "YYYY-01-01".
   type FuzzyDateString = string;   


.. _TALER_ReserveKYCAttestationPS:
.. sourcecode:: c

   struct TALER_ReserveKYCAttestationPS {
    /**
     * purpose.purpose = TALER_SIGNATURE_WALLET_RESERVE_TRANSITIVE_KYC
     */
    struct GNUNET_CRYPTO_EccSignaturePurpose purpose;
    struct TALER_ReservePublicKeyP attester_reserve_pub;
    struct TALER_ReservePublicKeyP target_reserve_pub;
    /* If no birthday is set, must be all 0 */
    char birthday[sizeof("YYYY-MM-DD")];
   }


Changes in the Wallet
^^^^^^^^^^^^^^^^^^^^^

We need a workflow for the attestation of one wallet through another.

TODO.


Definition of Done
==================

For the exchange, the implementation of the configuration option and the
endpoint, and corresponding unit tests in ``src/testing`` are necessary.


For the wallet: TODO.

Alternatives
============

* KYC for a reserve can only be provided by a full KYC legitimization process.

Drawbacks
=========

Other than adding code to the exchange: unknown.

Discussion / Q&A
================

The proposed solution makes the principle of subsidiarity for age-restrictions
(i.e parents are responsible for setting the age-restriction) explicit in the
code.

It also simplifies the KYC process for many situations for customers: Families
members and partners benefit from it.

However, the proposed solution still allows for other ways to set
age-restriction in the wallet.  For example, parents who do **not** have a
Taler wallet, are still able to assist their children with the settings of
age-restriction during the withdraw process.
