DD 16: Backoffice Order Management
##################################

Summary
=======

This document describe the complete list features for orders management and how
will be shown.

Motivation
==========

User will use the backoffice to manage their orders and verify the status of the
current and past orders.

Requirements
============

Access all information provided from the Merchant Backend API in a
understandable and accessible way

User should use the backoffice to manage orders by:

* searching for orders
* showing details about the order and the activity history
* refund order that are refundable
* create orders ( if admin )

Proposed Solution
=================

Listing orders
--------------

.. image:: ../images/backoffice-order-list.svg
  :width: 800

4 tabs will be show for an easy access to common filter, click on any of this and
search will reset all filter except date

* paid (default)
* refunded
* not wired
* all (empty filter box)

Columns for the order listings will be:

* date in 'dd/MM/yyyy HH:mm:ss' format
* amount: value and currency
* summary

Actions per row:

* refund: pop up with amount and reason (select box and text)
* copy payment url (if not status==unpaid)
* view details

An export button will allow the user to download the result page in CSV format.

An infinity scroll is propose instead of a pagination, reasons:

* users usually does not jump from one page to another using random access
* if the user want to search, is going to limit the result using filters
* no need to go back after loading more result
* it could be triggered by scrolling, with buttons (or both)

Create orders
-------------

this form is divided into 4 sections

* ``product``: where the merchant can add inventory product and non-inventory
  product.

* ``price``: where the merchant can override the total price (if product was
  added) or set the order price and summary.

* ``payment``: where some default of the payment processing can be changed

* ``extra``: where the merchant can add extra information in JSON format


Create order: Product section
.............................

This section has two parts:

The first part will add/remove product from the current stock.

* ``name``: search box to select product by description field. if not found it
  will be a 'create new' option leading to the create product without losing
  context

* ``quantity``: mandatory

The second part will add non inventory product. To add a product a :ref:`create
product <backoffice-create-product>` form will be shown. The product in the list
can be edited or deleted from the list.

In both cases, the total unit and price of the products will be calculated and
shown in the bottom of the section. If the merchant collapse one of the product
list a line with a resume of the total price and units will be shown.

.. image:: ../images/backoffice-order-create.product-section.svg
  :width: 800

Create order: Price section
...........................

This section has 2 scenarios.

The fist one is without products being added: the ``order price`` and
``summary`` inputs will be shown.

If there is at least one product added, the ``total products price`` as the sum
of all products prices will be shown. The ``order price`` will default to
``total products price``. The ``products taxes`` and ``profit`` will be shown
since ``order price`` cannot be less that ``product taxes``.

.. image:: ../images/backoffice-order-create.price-section.svg
  :width: 800

Create order: Payment section
.............................

This section show optional values that can be overwritten by the merchant

* ``refund deadline``: calendar type of input. default from instance

* ``pay deadline``: calendar type of input. default from instance

* ``auto refund deadline``: calendar type of input. default empty, optional.

* ``delivery date``: calendar type of input. default empty, optional.

* ``delivery location``: location section similar to instance jurisdiction or
  instance address. optional

* ``max fee``: default value from the instance

* ``max wire fee``: default value from the instance

* ``wire_fee_amortization``: default value from the instance

.. image:: ../images/backoffice-order-create.payment-section.svg
  :width: 800

Create order: all section expanded
..................................

An example of how all section in a page will be shown.

.. image:: ../images/backoffice-order-create.all-expanded.svg
  :width: 800


Creation order success
......................

A success message showing the amount, summary and the order id. Additionally the
taler_pay_uri can be shown to be copied to send to the customer.

action buttons that allow the following:

* create another payment: go to the create payment page again
* view details: show details of the payment (see page)

Order details
-------------

Status text should be set with a background color for easier recognition when
indicated:

* paid: green
* claimed: light-green
* unpaid: grey
* refunded: red

Header
......

This is a resume of most important information

* big status with color
* date
* total

  - deposit_total (if not refunded)
  - refund_amount (if status is refunded)
  - deposit_total - refund amount (if refunded partially)

* order_status_url
* taler_pay_uri (if unpaid)
* actions: refund (if not refunded), add note, copy order_status_url

Timeline of events
..................

Event of status changed over time describe vertically.
Sorted from newest to oldest.
On line per status updated, with datetime and a short description.

Info taken from:

* refund_details: a row showing date an amount

* wire_details: a row date and id


    About wire details

    Normally one wire transfer will involve more than one payment from the same
    exchange. And an order can have multiple wire transfers if the payment
    involved more than one exchange.



Error status
............

This section is not going to be shown if there is no error

* Wire Error: info taking from wire_reports in contractTerm. for every element
  in the list show hint and error code

* Exchange Error: connection with exchange failed, info taking from exchange_ec
  and exchange_hc

Payment details
...............

If the order was claimed

* summary
* amount (contract.amount)
* fee (contract.amount - deposit_total)
* refund_amount (if status is refunded)
* net (deposit_total - refund_amount)
* current status


Contract Terms
..............

collapsed as default. show disabled if unpaid

* summary
* amount
* fulfillment_url, if present
* max fee
* max fire fee
* wire_fee_amortization
* list of (exchange | auditor) name and url
* products table: list of products, one row per product

  * description
  * quantity
  * total price
  * total tax
  * sum at the bottom

* horizontal timeline of important dates in the contract

  * start at contract creation time
  * show current date
  * pay_deadline
  * wire_transfer_deadline
  * refund_deadline
  * delivery_date (immediately if null)
  * auto_refund


refund popup
............

If there is any refund:

* show total refund from ``refund_amount``
* a (down arrow) to show more information
* if expanded show table with date, amount and reason

Warn if there is a pending refund when ``refund_pending`` is true

Ask for:

* amount: default 0, show max amount refundable (order amount - already
  refunded)

* reason: concatenation of the next values

  * drop down options: duplicated, requested by customer, other
  * after selecting, free text for additional information

.. image:: ../images/backoffice-order-refund.svg
  :width: 800

.. image:: ../images/backoffice-order-refund.already.svg
  :width: 800

Example of details by status
----------------------------



.. image:: ../images/backoffice-order-details.paid.svg
  :width: 800

.. image:: ../images/backoffice-order-details.unpaid.svg
  :width: 800

.. image:: ../images/backoffice-order-details.claimed.svg
  :width: 800

.. image:: ../images/backoffice-order-details.refunded.svg
  :width: 800


Alternatives
============


pagination
----------
order list was originally thought with pagination footer

.. image:: ../images/backoffice-order-list.pagination.svg
  :width: 800

ascending boolean flag cloud be eliminated using the load before and load after
and a design without random access to pages was proposed

search box
----------

A SearchBox was considered for managing filter.
Filter will allow paid, refunded, wired status and date.

* paid: yes/no
* refunded: yes/no
* wired: yes/no
* date: 'dd/MM/yyyy'

The UI of the search box is an input field with tags showing ``field=value`` for
any filter set. Additionally a custom criteria can be supported like
``date>last_week`` or ``amount>USD:10``



Discussion / Q&A
================

* Shall we show merchant information from the ContractTerm in the order detail?

* Do we have the date and time of when the order goes from these states?

  * null => unpaid (using contractTerm.timestamp from now but may not be correct)

  * unpaid => claimed (maybe contractTerm.timestamp)

  * claimed => paid (maybe last wire_details.execution_time with confirmed = true )

* Refund button is being show if "refund_amount < contract_terms.amount" and "time < refund deadline".
  Is there any other case? Is this taking into account auto_refund?

* Field left out in the order creation:

  * contractTerm.summary_i18n: it makes the UI complex
  * contractTerm.order_id: should be created by the backend
  * contractTerm.timestamp: defined by backend
  * contractTerm.merchant_pub: filled by the backend
  * contractTerm.merchant_base_url: filled by the backend
  * contractTerm.h_wire: defined by the backend
  * contractTerm.nonce: not used
