DD 26: Refunds and Fees
#######################

.. note::

   This is implemented (as of 2023-09-15).

Summary
=======

This document discusses what should happen with deposit fees when a
deposit is refunded.


Motivation
==========

When a user receives a refund, we have to decide what happens to the deposit
fee that was originally paid on the associated deposit.  Originally, we said
that the deposit fee is waived when any refund happens.  However, if the
refund fee is zero and the deposit fee is non-zero, this results in a
problematic scenario where merchants issue minuscule refunds that primarily
enable customers to effectively obtain the deposit fee.


Requirements
============

  * If the refund and refresh fees are zero, it should be possible for
    consumers to get 100% of their digital cash back on refunds.
  * This should not result in a problematic situation where merchants
    conspire with consumers and issue minuscule refunds to allow consumers
    to work around deposit fees.

Proposed Solution
=================

  * Only waive the deposit fee for full refunds where for the
    specific coin (!) the refunded amount is the total value of the
    refunded deposit.

Alternatives
============

  * Only waive the deposit fee for full refunds where for the
    specific coin the refunded amount is the total value of the
    denomination of the coin. This may slightly simplify the
    logic, but has the problem that it does not enable 100%
    refunds if the original payment already required a refresh
    because the coin's value exceeded the paid amount.
  * Waive the deposit fee on any (including partial) refund.
    This creates a bad incentive structure if combined refresh
    and refund fees are below deposit fees.

Drawbacks
=========

  * We need to update and test an already complex fee calculation
    logic.


Discussion / Q&A
================

(This should be filled in with results from discussions on mailing lists / personal communication.)
