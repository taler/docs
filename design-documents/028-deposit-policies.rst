DD 28: Deposit Policy Extensions
################################

.. note::

  This is Work-In-Progress.

Summary
*******

We will propose here a plugable mechanism in the exchange to support deposits
with associated policy.  An exchange can enable support for such policies via
configuration.

The inital set of policy extensions that an exchange might provide consists of

Merchant refunds
        Merchant can grant customers refundable payments.  In this case, the
        amount of the deposit is put into escrow by the exchange for a certain
        period until which the customer can claim a refund.

Escrowed payments
        A trustor puts coins into escrow with the exchange.  It can be claimed
        by a beneficiary until a certain deadline, when the claim is signed by
        both, the beneficiary's and the trustor's keys.

Brandt-Vickrey auctions
        A bidder puts coins into escrow with the exhange in order to
        participate in an Brandt-Vickrey auction.  The deposit confirmation is
        proof to the seller for the escrow and contains a hash of the auction
        meta-data and a deadline.  After successfull execution of the auction,
        the seller provides a valid transcript to the exchange from which the
        exchange learns which bidder(s) won the auction for which prices.   It
        then transfers the amounts from the winners' coins to the seller.  In
        case of a timeout and for all losing bidders, the coins can be
        refreshed.

The policies shall be implemented as *extensions* to the exchange (see
:doc:`006-extensions`).  

Motivation
**********

TODO

Background and Requirements
***************************

TODO

Proposed Solution
*****************

TODO, explain:

- C-structs for policy extensions (esp. the handlers)
- Naming conventions for policy extensions
- Deadlines and -handling
- API-endpoints (``/extensions/policy_...``)
- Typical choreography of a deposit with policy and its fulfillment

Database-schema
===============

TODO: Description

.. graphviz::

   digraph deposit_policies {
        rankdir = LR;
        splines = false;
        fontname="monospace"
        node [
                fontname="monospace"
                shape=record
        ]

        subgraph cluster_deposits {
                label=<<B>deposits</B>>
                margin=20
                deposits [
                  label="...|<ref>policy_details_id (null)\l|...|timestamp\l|..."
                ]
        }

        subgraph cluster_policy_details {
                label=<<B>policy_details</B>>
                margin=20
                policy_details [
                  label="<id>id\l|<hash>policy_hash_code (unique)\l|deadline\l|commitment (amount)\l|accumulated_total (amount)\l|fee (amount)\l|transferable (amount)\l|fulfillment_state\l|<fid>fulfillment_id (null)\l"
                ]
        }

        subgraph cluster_policy_fulfillments {
                label=<<B>policy_fulfillments</B>>
                margin=20
                rank=min;
                policy_fulfillments [
                  label="<id>id\l|proof\l|timestamp\l|<codes>policy_hash_codes (blob)\l"
                ]
        }

        deposits:ref->policy_details:id [ label="n:1"; fontname="monospace" ];
        policy_details:fid->policy_fulfillments:id [label="n:1"; fontname="monospace" ];
   }


The field ``policy_hash_codes`` in table ``policy_fulfillments`` is a binary
blob that consists of the concatenation of the sorted
``policy_details.policy_hash_code`` entries from all policies that are fulfilled by
this proof.


Policy Fulfillment States
=========================

The fulfillment of a policy can be in one of the following five states:

Ready
        The policy is funded and ready. The exchange is waiting for a proof of
        fulfillment to arrive before the deadline.

Insufficient
        The policy lacks funding, that is ``accumulated_total`` <
        ``commitment``, but has otherwise been accepted.  Funding can be
        continued by calling ``/deposit`` or ``/batch-deposit`` with more coins
        and the same policy details.

Success
        The policy is provably fulfilled.  The amounts for payout, fees and
        refresh are transfered/can be claimed.  Note that a policy fulfillment
        handler can change the values for the amounts for payout, fees and
        refresh.

Timeout
        The policy has timed out.  The amounts for payout and refresh are
        transfered/can be claimed.
       
Failure
        The policy is in an failure state.  Payouts and refreshes are
        blocked, timeouts are ignored.



Invariants
^^^^^^^^^^

The following invariants need to be fulfilled and be checked by the auditor:

- The fulfillment state of a policy is **Insufficient** IF AND ONLY IF the
  amount in ``policy_details.commitment`` is equal or larger than the amount in
  ``policy_details.accumulated_total``.

- The sum of amounts in ``policy_details.fee`` and
  ``policy_details.transferable`` MUST be equal or less than the amount in
  ``policy_details.accumulated_total``.

- The amount in ``policy_details.accumulated_total`` MUST be equal to the total
  sum of contributions of the individual coins of the deposits that reference
  this policy.

- Each hash code encoded in ``policy_fulfillments.policy_hash_codes`` MUST
  refer to an existing ``policy_details.hash_code`` AND its ``.fulfillment_id``
  MUST point to the same ``policy_fulfillments.id``.

- Conversely: If a ``policy_details.fulfillment_id`` points to an entry in
  ``policy_fulfillment``, the ``policy_details.policy_hash_code`` MUST be
  present in that entry's ``.policy_hash_codes``.

   

Alternatives
============

TODO

Drawbacks
=========

TODO


Discussion / Q&A
================

TODO

(This should be filled in with results from discussions on mailing lists / personal communication.)
