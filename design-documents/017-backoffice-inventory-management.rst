DD 17: Backoffice Inventory Management
######################################

Summary
=======

This document describe the complete list features for inventory management and
how will be shown.

Motivation
==========

User will use the backoffice to manage their inventory, prices and update stock.

Requirements
============

Access all information provided from the Merchant Backend API in a
understandable and accessible way

User should use the backoffice to manage inventory by:

* creating new products
* updating the products information
* inspecting inventory list
* deleting products from the inventory list

Proposed Solution
=================

Inspecting inventory
--------------------

.. image:: ../images/backoffice-product-list.svg
  :width: 800

Listing the product will shown this columns:

* image
* description
* sell price
* stock left (with next_restock in days if present)
* stock sold

Actions will be

* modify
* delete: with a confirm popup, it may fail if have some locked


.. _backoffice-create-product:

Create and Update Product form
------------------------------

.. image:: ../images/backoffice-product-create.svg
  :width: 800

Update product will use the same form except for the ``product_id``

* product_id: BACKOFFICE_URL + id
* description: split in two fields, concatenated with a line separator

  * name: required, one line
  * extra: optional, free text area

* description localized: list with

  * lang: dropdown list with supported lang + custom
  * description: text area

* unit: string
* price: amount
* image: image box that allows upload when clicked
* taxes: list with

  * name: string
  * value: amount

* Stock: button that opens more fields for stock control

  * stock remaining: number
  * address: first collapsed, then field for Location
  * next_restock: date
  * cancel: button to set the stock to infinity, closing the section

Stock management
----------------

* ``manage stock`` button will open the dialog below

* ``without stock`` will close the dialog and set stock props to not defined

* ``unknown`` button will set next restock value to undefined

* ``never`` button will set next restock to never

* when updating the product, the option ``without stock`` will no be available
  if the product already has stock

* if the product already exist then:

  * the option ``without stock`` will no be available, since the product cannot
    change from infinite stock to managed stock

  * the option ``manage stock`` will no be available, since the product cannot
    change from managed stock to infinite stock

  * ``current stock`` will be managed using ``incoming`` and ``notify lost``

  * a label at the end of the section will inform about the final result


.. image:: ../images/backoffice-product-create.stock.svg
  :width: 800


Alternatives
============

* price and stock columns in the list can be merged into a more complex column
  with the same information

* rows in the table can be expandable when clicked to get access to some common
  actions like increase stock or change price

.. image:: ../images/backoffice-product-list.actions.svg
  :width: 800

* detail page was intentionally left out since all information can be access
  from the update page

Q&A
===

* can we add the quantity locked in the product description? so we can add it
  to the inventory list to reflect the current activity.

* can we allow add extra data like order has in contractTerm?, this could be
  useful for frontend apps. example of usage: country/state to where the product
  is sold since taxes may vary
