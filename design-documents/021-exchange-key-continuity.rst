DD 21: Exchange Key Continuity
##############################

Summary
=======

This design document discusses what happens if
an exchange's master public key ever changes.

Motivation
==========

The exchange master public key is an offline signing key.  While this makes
compromise of this key less likely, it makes it more likely that this key is
lost.

The wallet (and merchants) must handle such a scenario where the
exchange deploys a new master public key.

Proposed Solution
=================

When wallets or merchants specify that they directly trust
an exchange, the trust record must always explicitly mention
a base URL and a master public key.

An exchange **should** have a single, canonical base URL.
However, an exchange that's reachable over different base URLs
should still be handled gracefully.

Wallet
------

We generally assume that the base URL of an exchange stays constant.
Wallets do not support changing the base URL of an exchange.

A ``/keys`` response with an unknown exchange master public key is only
accepted if the exchange is audited by a trusted auditor or the wallet
has an exchange trust record with the advertized master public key.

Denomination records explicitly store which master public key
signed them.

Merchant
--------

When coins are deposited, the wallet only specifies the base URL
for the respective exchange, but not the master public key of the exchange.
The merchant **must** always
resolve the URL to the current master public key,
and decide whether to accept the exchange based only
on the master public key.  That is, a merchant
may not reject an exchange because the wallet is not
specifying what the merchant believes to be the
canonical base URL.


Alternatives
============

* The wallet could treat each (base URL, master pub) pair as a
  separate exchange.  This, however, does not work in practice,
  since the exchange with the new public key should still accept
  deposits and refreshes of coins with denominations signed by the
  old key.


Discussion / Q&A
================

* Does the current merchant backend have support
  for changing master public keys of exchanges trusted
  via the auditor?  Or does the code base make the assumption
  that one merchant base URL corresponds to only one master
  public key?
