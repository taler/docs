DD 35: Regional currencies
##########################

Summary
=======

This design document discusses how the GNU Taler wallet can support both
regional currency deployments and official fiat currencies.

Motivation
==========

Digital cash in a Taler wallet always requires some kind of trust anchor that
backs its value, be it either an exchange directly or an auditor that vouches
for one or more exchanges.

The currency code or symbol (EUR, USD, $, ...) is thus not enough to know what
a particular wallet balance really means.  It also matters what exchange or
auditor is behind it.  Thus the wallet needs some mechanism to allow users to
distinguish between an official deployment of a currency (say EUR in Europe) or
deployments of regional currencies.  Regional currencies might have coinciding
currency names for different incompatible deployments (say, MANA to buy Club
Mate drinks at different hacker events with completely separate and independent
Taler deployments).

Requirements
============

* Official deployments for fiat currencies should be supported without clutter
* Regional currencies should be easy to use as well
* It must be easy to integrate regional/official currencies with the existing
  Taler auditor/exchange structure

Proposed Solution
=================

Users usually do not want to see and verify the auditor/exchange URL for their
digital cash.  The wallet thus needs to support some form of "scope" for
currencies that indicates the trust anchor for particular amounts and balances.

The scope of a balance/amount gives the user additional information about the
meaning and trust anchor of a balance/amount. The scope is always local and
contextual to the wallet.  Depending on the configuration, the wallet can show
different scope information for the exact same coins.

A balance is in exactly one of three scopes:

1. Global. An amount in the global scope is by default displayed without
   any additional qualification on the currency.
2. Exchange-scoped.  An exchange-scoped amount is always displayed with the
   prettified base URL of the exchange.
3. Auditor-scoped. An auditor-scoped amount is always displayed with the
   prettified base URL of the auditor.  When multiple auditors are applicable,
   either the one with the lexically smallest base URL is chosen, or the
   one that the user/wallet has configured as the prefered one for the currency.

Whenever the wallet reports an amount, scope information should be
present in the same message with the following format:

.. code:: TypeScript

   type ScopeInfo =
     | { kind: "global" }
     | { kind: "exchange", baseUrl: string }
     | { kind: "auditor", baseUrl: string };

Prettified base URLs
^^^^^^^^^^^^^^^^^^^^

The base URLs should be rendered without the ``https://`` and without
trailing slashes:

* ``https://exchange.demo.taler.net/`` would be rendered as
  ``exchange.demo.taler.net``.

* ``http://ex1.demo.taler.net/`` would be rendered as
  ``http://ex1.demo.taler.net``.

* ``https://ex2.demo.taler.net/foo/bar/`` would be rendered as
  ``ex2.demo.taler.net/foo/bar``.


Currency Scope Info in the Wallet
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For each configured currency code, the wallet should store following information:

* List of global-scope exchanges and currencies for the currency.  When this is an empty list,
  the currency will always be shown with exchange/auditor scope info.

Examples
^^^^^^^^

The following example shows how a wallet would render balances with
global-scope EUR (i.e. a user would expect these to be "official" EUR that can
be used with multiple vendors in Europe), two exchange-scoped MANA balances and
one auditor-scoped MANA balance.

.. code:: none

  Balances:

  1.5 EUR

  3 MANA ([exchange-icon] exchange.leipzig.ccc.de)
  3.3 MANA ([exchange-icon] exchange.berlin.ccc.de)
  5 MANA ([auditor-icon] auditor.ccc.it]

Scope information in requests for payments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In wallet-to-merchant payments, the merchant specifies which exchanges and
auditors the merchant accepts.  It is desirable that the wallet renders the
scope information for a requested amount in a similar way that a balance amount
would be rendered.

The amount should always be shown in the scope that is compatible with the
merchant and that the wallet holds the highest amount in.

Let's say a wallet has "auditor.ccc.it" as the global-scope auditor for MANA and holds
mana audited by this auditor. A merchant accepts MANA from this auditor as well
as from the exchange "mana.my-hackerspace.it".

A payment request could then be rendered like this:

.. code:: none

  Summary: Club Mate (5x)
  Amount: MANA:50


If a wallet (by a non-Italian hacker) would not have "auditor.ccc.it" as the
global-scope auditor for MANA, it would show as:

.. code:: none

  Summary: Cart #123 at Foomerchant
  Amount: MANA:123 ([auditor-icon] auditor.ccc.it)

  Other currencies supported by the merchant:
  [exchange-icon] mana.my-hackerspace.it

The last part should probably be hidden by default.  There might be nicer ways to render
this, such as some hoverable (?) icon after the amount that shows details about what currencies the merchant
accepts.


Implementation Breakdown
^^^^^^^^^^^^^^^^^^^^^^^^

* we need test currencies in each scope
* wallet-core needs to add scope information to balances response
  and various other requests
* the UI needs to render those
* wallet-core needs to expose new requests to manage currency information
* the UI needs to allow the user to manage currency information


Alternatives
============

* Completely distinguish regional currencies (non-three-letter currency code) and official currencies 
  (three letter ISO currency code).  This does not help with overlapping regional currency names,
  we can't expect them to be unique.

Drawbacks
=========

* API and UI changes are required, but they can be made in an incremental and
  backwards-compatible manner.
* Scope information could be attached to the currency code.
  That's a bad idea, because the scope information is totally local to the wallet.

Discussion / Q&A
================

* Should we allow users to customize how scopes are displayed (e.g. an alias
  instead of the full prettified base URL?)
* Do we still need the auditor/exchange URLs with this proposal?
* How does this affect the insufficient balance details?  Should we also take scopes
  into account here?
