DD 30: Offline payments
#######################

Summary
=======

This design document discusses support for offline payments.

Motivation
==========

Many proposed CBDCs claim to support offline payments.
Taler is explicitly meant to be an online payment system.  However, since there
recently seems to be an increased interest in offline CBDC solutions,
we have decided to still explore how Taler could support offline payments in the future.

While we still recommend online-only payments, this work operates under the the
following theme: "If Taler can support offline payments that are no worse than
those of competing systems (that often offer less freedom and privacy to
users), why should we claim we can't support them and fare worse in comparisons"?

Requirements
============

TBD.

Possible Solutions
==================

Approach 1: Trust-based offline payments
----------------------------------------

The merchant simply trusts payments without depositing them at the exchange, up
to a certain threshold and when an emergency mode is activated.

Advantages:

- Offers the most user freedom
- Very few protocol/implementation changes required

Disadvantages:

- Requires manual switching to "emergency mode" where merchant
  just trusts payments without verification.
- Linkability of transactions unavoidable, because refresh is not available offline
- Unclear if/how business logic based on order state will be affected.
  (Will there be a "paid-offline" / "paid-unconfirmed" state for orders?)

Approach 2: Full HSM wallet
---------------------------

Implement the full wallet in a hardware security module.  When paying, the HSM
wallet attests itself to the merchant, who then (if configured appropriately)
trusts that the coins are still valid without talking to the exchange first.

Advantages:

- Easy to lift coins out of the HSM into a software wallet.
  The HSM would internally mark the coins as "online" and give
  the coin secrets to the software wallet.
- Few exchange/merchant protocol changes required.

Disadvantages:

- Complex implementation, might not be supported on commodity HSMs.
- Requires medium-to-major merchant backend changes (to verify HSM sigs and
  deposit coins once offline)
- Difficult to support P2P payments
- Depending on available coins, linkable transactions
  might be unavoidable.
- Since the HSM wallet supports the full protocol, regulators might
  be encouraged to make the HSM a requirement for *all* payments.
- The usual (justified!) HSM security and freedom concerns

Approach 3: Light-weight HSM balance register
---------------------------------------------

The HSM maintains an offline balance, effectively as a single, HSM protected
register.  To obtain offline cash, the software wallet spends coins to get a
signature that can be passed to the HSM to increment the offline balance.
To spend, the software wallet requests an "offline deposit permission"
that decrements the HSM balance register.  The exchange accepts these offline
deposit permissions in lieu of a normal coin deposit permission.

Advantages:

- Trivially supports P2P payments
- Cleanly separates normal Taler functionality from offline functionality

Disadvantages:

- Requires non-trivial (but backwards compatible) protocol changes
- The usual (justified!) HSM security and freedom concerns


Discussion / Q&A
================

(This should be filled in with results from discussions on mailing lists / personal communication.)
