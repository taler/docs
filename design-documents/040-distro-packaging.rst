DD 40: Distro Packaging
#######################

.. admonition:: Metadata

   Status
    proposed

Summary
=======

This DD discusses considerations for disto packages of GNU Taler components,
especially with regards to configuration and setup.  We focus on Debian
packages for now.

Motivation
==========

The current way that configuration files are handled does not work well with
automated setup tools and furthermore does not easily allow restoring
configuration files in ``/etc/`` that the admin deleted or manually modified.

The database configuration is currently handled inconsistently. While some
packages use Debian's dbconfig-common facilities, others don't, even though
they require a database for operation.

The guidelines in this document are based on pratical experience
with third parties setting up Taler based on the Debian packages
and scripting supplied by us (i.e. ``deployment.git/netzbon``).

Requirements
============

* The distro package should work nicely both for a manual setup
  process by a sysadmin, as well as for automated installation
  via helper scripts or other tools.
* Major differences between different distributions should be minimized, the
  more code and config templates that can be shared the better.

Proposed Solution
=================

This section contains the new guidelines that we want to apply to all our
distro packages, specifically the Debian packages.

General Considerations
----------------------

* Packages may not enable a systemd service by default.

Config Files: Taler-specific
----------------------------

The "pristine" version of config files must be installed into
``/usr/share/taler/etc-original``.  These files should not be modified by
tooling or the user.  These files may contain direct placeholders or
placeholder comments that are replaced (but not in-place, only in ``etc/``!)
when the package is configured.

During the postinstall step, the files from ``/usr/share/taler/etc-original``
are copied to ``/etc/`` (using the ``ucf`` tool on Debian) and, if required,
placeholders are filled in.

When using tooling to set up Taler, the tooling **should not**
use files from ``/etc/`` as template, but instead from ``/usr/share/taler/etc-original`` or alternatively generate custom configuration files.

Rationale: Debian manages conffiles in ``/etc/`` with special logic.
In particular, when files are deleted from ``/etc/taler`` and the package
is reinstalled (even with ``--reinstall``), there is no easy way for
tooling (or the admin) to restore the unmodified config files.
The only way to restore it is ``apt install --reinstall libtalerexchange -o Dpkg::Options::="--force-confmiss"``, which might be unsafe as it forces
overriding of *all* config files of the package.

Config Files: HTTP Server
-------------------------

The same considerations apply to configuration files of HTTP
servers (nginx, apache, caddy, ...).  Additionally:

* Configuration files *must* either have a well-known name
  or particular suffix to easily identify them

  * In particular, file names like ``sites-available/exchange.$domain``
    are unacceptable, as they are very difficult to uninstall
    or remove when ``$domain`` is changed.

* Configuration files for the HTTP server must not be
  active by default, i.e. they must be placed in ``sites-available``
  but not ``sites-enabled``.

Database
--------

Packages should *not* use ``dbconfig-common``.  Reasons are:

* ``dbconfig-common`` is lacking in documentation and very difficult
  to use for packagers.
* ``dbconfig-common`` offers too much flexibility and
  asks too many questions to the administrator, especially when
  reconfiguring a package. The ``taler-merchant`` package
  currently breaks when the user chooses anything else than ``ident`` auth.
* Using ``debconfig-common`` makes the database setup logic difficult to test.
  That is not a problem with simple packages, but most Taler packages
  require a non-trivial database setup.
* Very few packages in Debian (<30) actually use ``dbconfig-common``;
  even fewer are notable or widely used packages.

Instead, each package should document how to set up
the database and *optionally* ship an executable named
``taler-$component-dbconfig`` that:

1. Creates the database and adjusts permissions
2. Checks if the database is accessible
3. Runs ``taler-$component-dbinit`` if applicable
   and unless supressed by the user.

For now, our tooling shall only support PostgreSQL and only set up ``ident``
authentication or set up ``password`` authentication with a random password for
components that do not support DB connections via unix domain sockets.

Definition of Done
==================

* All Taler and Anastasis packages follow the guidelines from this DD
* Packages installation has been manually tested
* Automated setup scripts (``deployment.git``) have been adjusted to use the
  configuration file templates shipped in the package,
  instead of using their own config templates.

Alternatives
============

* Do not ship with distro-specific configuration files, instead only ship
  tooling to generate config files and set up the database.

Discussion / Q&A
================

(This should be filled in with results from discussions on mailing lists / personal communication.)
