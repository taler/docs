DD 48: Wallet Exchange Lifecycle and Management
###############################################

Summary
=======

This design document covers the lifecycle and management
of exchanges in the wallet.

Motivation
==========

The current wallet implementation lacks requests to manage exchanges.
It also always fetches the /keys info of all exchanges, because it doens't
distinguish between used and preset/added exchanges.

Requirements
============

The following properties of of exchanges managed by the wallet
are important:

* is the exchange used for something (coins, (account-)reserves, purses, ...)
* did the user (ever) accept the ToS?
* does the exchange have newer ToS available?
* is a current version of /keys (including former /wire) downloaded?
* were there errors downloading or checking the keys info?
* is the exchange permanently added or ephemeral?

  * ephemeral exchange records are created when the user
    checks fees of an exchange but doesn't use it,
    they would typically be hidden in the UI


Proposed Solution
=================

Exchange Entry Status
---------------------

The wallet exposes three separate status fields for each exchange entry:

* the entry status
* the update status
* the ToS status


Entry Status
~~~~~~~~~~~~

* ``preset``: Exchange record has been added to the exchange (typically as
  a hardcoded preset in wallet-core).  While the exchange can be selected for operations,
  the wallet doesn't update the status yet, i.e. no /keys queries are done.
* ``ephemeral``: Exchange has been updated (or update has been attempted) at
  least once (for example as part of checking the fees for a transaction using
  the exchange).  However, the exchange is not yet used for any resource in the wallet.
  In this state, the exchange record will be garbage-collected eventually.
* ``used``: The exchange is known and used, the wallet regularly queries /keys.

Transitions:

* A transition from ``used`` to ``ephemeral`` is not possible,
  since this would eventually remove preset exchanges and likely confuse
  the user.

Update Status
~~~~~~~~~~~~~

* ``initial``: Not updated, no need to update
* ``initial(update)``: Update pending, possibly with error
* ``suspended``: Exchange was manually disabled, should not be contacted
  anymore, but record is kept in the wallet. Mostly useful for testing.
* ``failed``: Updating the exchange info failed permanently, the exchange is
  not usable for any operations.
* ``outdated(update)``
* ``ready``: Exchange is useable.
* ``ready(update)``: Exchange is useable, but currently being updated.

ToS Status
~~~~~~~~~~

* ``pending``: The wallet is still trying to download the ToS.
  Possibly the last download attempt failed, will be reflected in an
  error details object.
* ``proposed``: The user needs to accept the current ToS.
* ``accepted``: The user has accepted the latest version of the ToS.

Management Requests
-------------------

* ``listExchanges``: List exchanges with their status info.
* ``addExchange``: Adds an exchange, entry status will be ``ephemeral``
  until the user actually uses the exchange.
* ``getExchangeResources``: List resources (number of coins, reserves, ...) associated
  with the exchange.
* ``deleteExchange({exchangeUrl: string, purge: boolean})``: Removes an exchange.
  Unless ``purge: true`` is specified, only an exchange without any associated
  resources can be deleted.
* ``getExchangeTos({exchangeUrl: string, acceptLanguage?: string[], acceptFormat?: string[]}) => { language: string, mime: string, content: string, altLanguages: string[] }``

FIXME: Purging should probably delete *all* resources associated with the exchange.
But does it also remove the associated transactions?

ToS Management
--------------

The wallet only stores the last accepted ToS ETag and the ETag last offered by
the exchange.  The ToS contents are not stored by the wallet database (and thus
not included in backups).  However, the wallet implementation may cache the
``/terms`` response on the level of the HTTP client.

In future iterations, the UI might specify the prefered language and content type
for ``/terms``, so that the wallet can already download the full response (not just ``HEAD``).


Definition of Done
==================

 * states implemented in wallet-core
 * exchange management specified on a UI level 
 * webex implemented 
 * android wallet implemented
 * ios wallet implemented

Discussion / Q&A
================

(This should be filled in with results from discussions on mailing lists / personal communication.)
