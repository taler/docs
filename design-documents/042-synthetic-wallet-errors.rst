DD 42: Wallet Dev Experiments
#############################

Summary
=======

This design document defines new ``taler://`` URIs to cause synthetic errors
or special states in the wallet that can then be rendered by the UI.

Motivation
==========

UIs need to handle various (error-) states and responses of wallet-core.  It's
not easy to cover all of these states and responses manually.  Some of them are
hard to reach or simulate without an elaborate test setup.

Requirements
============

The implementation of synthetic errors should be as separate from production
code as possible, to avoid making the normal code paths unreadable.


Proposed Solution
=================

Special taler:// URIs
---------------------

* ``taler://dev-experiment/$STATE_ID``
* ``taler://pay/...?dev-experiment=``

Special http(s):// URIs
-----------------------

* ``http(s)://*.dev-experiment.taler.net/``
 
  * URLs for this subdomain are handled specially by the
    wallet's HTTP layer and return fixed / mocked responses
    instead of making real requests.


List of experiments
-------------------



Alternatives
============

Drawbacks
=========

Discussion / Q&A
================

(This should be filled in with results from discussions on mailing lists / personal communication.)
