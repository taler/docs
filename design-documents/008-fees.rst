XX 08: Fee Structure Metrics
############################

.. note::

   This design document is deprecated.


Summary
=======

We discuss criteria for evaluating an exchange's denomination and fee structure.

Motivation
==========

Currently, users of a Taler wallet can't easily judge the fee structure of an
exchange and what it will mean for them.  Thus we want to define some metrics
that allow a user to make more informed decisions.

Similarly, the fee structure metrics might be used by exchange operators
as a sanity check.

An auditor might also enforce ranges on these metrics as a condition for
auditing a denomination structure.

Requirements
============

* Privacy:  A denomination structure that has too many denominations
  will result in reduced anonymity.
* Efficiency:  A denomination structure that has too few denominations
  is inefficient.
* Predictability:

  * The amount of fees for an operation should not come
    as a total surprise for the user.

  * The user should know beforehand when they have to be online
    to refresh their balance, and they should know how much
    this will cost them.

The metrics for the exchange should be advisory, i.e. an informed user should
be able to accept the withdrawal anyway.

The exchange's business interests may be in conflict with (1) a transparency of
cost for the user and and (2) restrictions on denomination/fee structures.
Thus the metrics should still allow some degree of variability between
exchanges.

We make the assumption that wallet always prefer operations with better
privacy.  For example, a single coin should only be used for at most one
spend operation and at most one refresh operation.

Proposed Solution
=================

The following yes/no criteria are applied to determine if a denomination/fee structure
is *reasonable*:

* For a denomination of value ``v``, either

  (a) ``v`` must be the smallest offered denomination, or
  (b) there must be another denomination with a value of at least ``v / 10``.

Is there a relationship between the smallest denomination and the size of fees?

Idea: When when only doing spends on a coin that are a multiple of the smallest spending amount,
we constrain the number of coins that are refreshed into.

When evaluating the e2e cost of a denomination, look at:
 * the cost of withdrawing the coin by itself and spending it fully, directly
 * the cost of withdrawing the coin, directly refreshing it into the next smallest
   fully fitting currency (or use greedy fit!) and add up the withdraw, refresh and re-withdraw fees.
 * percentage is always computed with sum against the full coin's value
 * smallest coins are an exception

Actually, smallest spendable amount should be a multiple of the smallest coins, since
we still need to deal with fees even at that level


Refund fees should not be higher than if the user got a refund through
a different channel than Taler and then purchased the product again.



The following metrics are also defined:

* Shortest time from withdrawal expiration to deposit expiration.
* Upkeep, i.e. how much does it cost the customer to keep coins in their wallet
  per time period, on average over a long period.
* Assurance period, i.e. the time span for which the exchange has already announced
  its fees.
* Spending amount range.
* End-to-end fee range.  This is the minimum/maximum cost to withdraw electronic cash
  (within the spending range), spend some of it and then obtain change for the transaction.
* Merchant contribution range.  This is the percentage of the end-to-end fees that the merchant
  can cover if they choose to do so.


Alternatives
============

* Users can manually study the fee structure.
* The auditor could impose a fee structure and not
  allow any variability between exchanges

Drawbacks
=========

* The approach does not work well in some special-purpose deployments,
  where the coin structure is tailored to the products of merchants,
  and refreshing would never even happen.

* The approach also does not consider more "creative" fee structures,
  such as ones where coins that are valid for a longer time have higher
  fees.

Discussion / Q&A
================
The Taler protocol offers the following fee types:

1. **Withdrawal**: For each successful withdrawal from the checking account, **per coin**
2. **Deposit**: For spending, **per coin**
3. **Refresh**: **Per coin** for
    a. Refresh transactions for receiving change
    b. Refresh of coins at the end of their validity
    c. Abort of transactions due to network failure
    d. Refund
4. **Refund**: For refunds or in case of contract cancellation by seller, **per coin**
5. **Wire fee**: For aggregated amounts wired by the Exchange to the merchant's checking account, **per wire transfer**
6. **Closing**: In case a withdrawal process could not be accomplished (the user's wallet did not withdraw the value from the reserve), **per wire transfer** from the Exchange's escrow account to the account of origin
