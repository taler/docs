..
  This file is part of GNU TALER.
  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Marcello Stanisci
  @author Florian Dold
  @author Christian Grothoff

.. _merchant-api-tutorial:

GNU Taler Merchant API Tutorial
###############################

Introduction
============

About GNU Taler
---------------

GNU Taler is an open protocol for an electronic payment system with a
free software reference implementation. GNU Taler offers secure, fast
and easy payment processing using well understood cryptographic
techniques. GNU Taler allows customers to remain anonymous, while
ensuring that merchants can be held accountable by governments. Hence,
GNU Taler is compatible with anti-money-laundering (AML) and
know-your-customer (KYC) regulation, as well as data protection
regulation (such as GDPR).

About this tutorial
-------------------

This tutorial addresses how to process payments using the GNU Taler merchant
Backend.  The audience for this tutorial are *developers* of merchants (such
as Web shops) that are working on integrating GNU Taler with the
customer-facing Frontend and the staff-facing Backoffice.

This chapter explains some basic concepts. In the
second chapter, you will learn how to do basic payments.

This version of the tutorial has examples for Python3. It uses the
``requests`` library for HTTP requests. Versions for other
languages/environments are available as well.

If you want to look at some simple, running examples, check out these:

-  The `essay
   merchant <https://git.taler.net/taler-merchant-demos.git/tree/talermerchantdemos/blog>`__
   that sells single chapters of a book.

-  The `donation
   page <https://git.taler.net/taler-merchant-demos.git/tree/talermerchantdemos/donations>`__
   that accepts donations for software projects and gives donation
   receipts.

-  The
   `survey <https://git.taler.net/taler-merchant-demos.git/tree/talermerchantdemos/survey>`__
   that gives users who answer a question a small reward.

-  The `WooCommerce plugin <https://git.taler.net/gnu-taler-payment-for-woocommerce.git/>`__
   which is a comprehensive integration into a Web shop including the refund business
   process.


Architecture overview
---------------------

The Taler software stack for a merchant consists of the following main
components:

.. index:: frontend

-  A frontend which interacts with the customer’s browser. The frontend
   enables the customer to build a shopping cart and place an order.
   Upon payment, it triggers the respective business logic to satisfy
   the order. This component is not included with Taler, but rather
   assumed to exist at the merchant. This tutorial describes how to
   develop a Taler frontend.

.. index:: backend

-  A Taler-specific payment backend which makes it easy for the frontend
   to process financial transactions with Taler. For this tutorial, you
   will use a public sandbox backend. For production use, you must
   either set up your own backend or ask another person to do so for
   you.

The following image illustrates the various interactions of these key
components:

.. image:: images/arch-api.png

The backend provides the cryptographic protocol support, stores
Taler-specific financial information and communicates with the GNU Taler
exchange over the Internet. The frontend accesses the backend via a
RESTful API. As a result, the frontend never has to directly communicate
with the exchange, and also does not deal with sensitive data. In
particular, the merchant’s signing keys and bank account information are
encapsulated within the Taler backend.

Some functionality of the backend (the “public interface“) is exposed to the
customer’s browser directly. In the HTTP API, all private endpoints (for the
Backoffice) are prefixed with ``/private/``.  This tutorial focuses on the
``/private/`` endpoints. The public interface is directly used by the wallet
and not relevant for the merchant (other than that the API must be exposed).


.. index:: sandbox, authorization

Public Sandbox Backend and Authentication
-----------------------------------------

How the frontend authenticates to the Taler backend depends on the
configuration. See :doc:`taler-merchant-manual`.

The public sandbox backend https://backend.demo.taler.net/ uses an API
key in the ``Authorization`` header. The value of this header must be
``Bearer secret-token:sandbox`` for the public sandbox backend.

.. code-block:: python

   >>> import requests
   >>> requests.get("https://backend.demo.taler.net/private/orders",
   ...              headers={"Authorization": "Bearer secret-token:sandbox"})
   <Response [200]>

If an HTTP status code other than 200 is returned, something went wrong.
You should figure out what the problem is before continuing with this
tutorial.

The sandbox backend https://backend.demo.taler.net/ uses ``KUDOS`` as an
imaginary currency. Coins denominated in ``KUDOS`` can be withdrawn from
https://bank.demo.taler.net/.

.. index:: instance

Merchant Instances
------------------

A single Taler merchant backend server can be used by multiple
merchants that are separate business entities. Each of these separate
business entities is assigned a *merchant instance* which is identified by
an alphanumeric *instance id*. If the instance is omitted, the instance
id ``default`` is assumed.

The following merchant instances are configured on
https://backend.demo.taler.net/:

-  ``GNUnet`` (The GNUnet project), reachable at https://backend.demo.taler.net/instances/gnunet/

-  ``FSF`` (The Free Software Foundation), reachable at https://backend.demo.taler.net/instances/fsf/

-  ``Tor`` (The Tor Project), reachable at https://backend.demo.taler.net/instances/tor/

-  ``default`` (Kudos Inc.), reachable at https://backend.demo.taler.net/

.. Note:: These are fictional merchants used for our demonstrators and
   not affiliated with or officially approved by the respective projects.

All endpoints for instances offer the same API. Thus, which instance
to be used is simply included in the base URL of the merchant backend.


.. _Merchant-Payment-Processing:

Merchant Payment Processing
===========================

.. index:: order

Creating an Order for a Payment
-------------------------------

Payments in Taler revolve around an *order*, which is a machine-readable
description of the business transaction for which the payment is to be
made. Before accepting a Taler payment as a merchant you must create
such an order.

This is done by POSTing a JSON object to the backend’s ``/private/orders`` API
endpoint. At least the following fields must be given inside the ``order``
field:

.. index:: summary
.. index:: fulfillment URL

-  ``amount``: The amount to be paid, as a string in the format
   ``CURRENCY:DECIMAL_VALUE``, for example ``EUR:10`` for 10 Euros or
   ``KUDOS:1.5`` for 1.5 KUDOS.

-  ``summary``: A human-readable summary for what the payment is about. The
   summary should be short enough to fit into titles, though no hard
   limit is enforced.

-  ``fulfillment_url``: A URL that will be displayed once the payment is
   completed. For digital goods, this should be a page that displays the
   product that was purchased. On successful payment, the wallet
   automatically appends the ``order_id`` as a query parameter, as well
   as the ``session_sig`` for session-bound payments (discussed below).

Orders can have many more fields, see `The Taler Order
Format <#The-Taler-Order-Format>`__.  When POSTing an order,
you can also specify additional details such as an override
for the refund duration and instructions for inventory
management. These are rarely needed and not covered in this
tutorial; please see the :doc:`core/api-merchant` reference
manual for details.

A minimal Python snippet for creating an order would look like this:

.. code-block:: python

   >>> import requests
   >>> body = dict(order=dict(amount="KUDOS:10",
   ...                        summary="Donation",
   ...                        fulfillment_url="https://example.com/thanks.html"),
   ...             create_token=False)
   >>> response = requests.post("https://backend.demo.taler.net/private/orders",
   ...               json=body,
   ...               headers={"Authorization": "Bearer secret-token:sandbox"})
   <Response [200]>


.. index:: claim token

The backend will fill in some details missing in the order, such as the
address of the merchant instance. The full details are called the
*contract terms*.

.. index:: contract terms

.. note::
   The above request disables the use of claim tokens by setting the
   ``create_token`` option to ``false``.  If you need claim tokens,
   you must adjust the code to construct the ``taler://pay/`` URI
   given below to include the claim token.

After successfully ``POST``\ ing to ``/private/orders``, a JSON with just an
``order_id`` field with a string representing the order ID will be returned.
If you also get a claim token, please double-check that you used the request
as described above.

Together with the merchant ``instance``, the order id uniquely identifies the
order within a merchant backend.  Using the order ID, you can trivially
construct the respective ``taler://pay/`` URI that must be provided to the
wallet.  Let ``example.com`` be the domain name where the public endpoints of
the instance are reachable. The Taler pay URI is then simply
``taler://pay/example.com/$ORDER_ID/`` where ``$ORDER_ID`` must be replaced
with the ID of the order that was returned.

You can put the ``taler://`` URI as the target of a link to open the Taler
wallet via the ``taler://`` schema, or put it into a QR code.  However, for a
Web shop, the easiest way is to simply redirect the browser to
``https://example.com/orders/$ORDER_ID``.  That page will then trigger the
Taler wallet. Here the backend generates the right logic to trigger the
wallet, supporting the various types of Taler wallets in existence.  Instead
of constructing the above URL by hand, it is best to obtain it by checking for
the payment status as described in the next section.


Checking Payment Status and Prompting for Payment
-------------------------------------------------

Given the order ID, the status of a payment can be checked with the
``/private/orders/$ORDER_ID`` endpoint. If the payment is yet to be completed
by the customer, ``/private/orders/$ORDER_ID`` will give the frontend a URL
(under the name ``payment_redirect_url``) that will trigger the customer’s
wallet to execute the payment. This is basically the
``https://example.com/orders/$ORDER_ID`` URL we discussed above.

Note that the best way to obtain the ``payment_redirect_url`` is to check the
status of the payment, even if you know that the user did not pay yet.  There
are a few corner cases to consider when constructing this URL, so asking the
backend to do it is the safest method.

.. code-block:: python

   >>> import requests
   >>> r = requests.get("https://backend.demo.taler.net/private/orders/" + order_id,
   ...                  headers={"Authorization": "Bearer secret-token:sandbox"})
   >>> print(r.json())

If the ``order_status`` field in the response is ``paid``, you will not
get a ``payment_redirect_url`` and instead information about the
payment status, including:

-  ``contract_terms``: The full contract terms of the order.

-  ``refunded``: ``true`` if a (possibly partial) refund was granted for
   this purchase.

-  ``refunded_amount``: Amount that was refunded

Once the frontend has confirmed that the payment was successful, it
usually needs to trigger the business logic for the merchant to fulfill
the merchant’s obligations under the contract.

.. Note::
   You do not need to keep querying to notice changes
   to the order's transaction status.  The endpoints
   support long polling, simply specify a ``timeout_ms``
   query parameter with how long you want to wait at most
   for the order status to change to ``paid``.


.. _Giving-Refunds:
.. index:: refunds

Giving Refunds
==============

A refund in GNU Taler is a way to “undo” a payment. It needs to be
authorized by the merchant. Refunds can be for any fraction of the
original amount paid, but they cannot exceed the original payment.
Refunds are time-limited and can only happen while the exchange holds
funds for a particular payment in escrow. The time during which a refund
is possible can be controlled by setting the ``refund_deadline`` in an
order. The default value for this refund deadline is specified in the
configuration of the merchant’s backend.

The frontend can instruct the merchant backend to authorize a refund by
``POST``\ ing to the ``/private/orders/$ORDER_ID/refund`` endpoint.

The refund request JSON object has only two fields:

-  ``refund``: Amount to be refunded. If a previous refund was authorized
   for the same order, the new amount must be higher, otherwise the
   operation has no effect. The value indicates the total amount to be
   refunded, *not* an increase in the refund.

-  ``reason``: Human-readable justification for the refund. The reason is
   only used by the Back Office and is not exposed to the customer.

If the request is successful (indicated by HTTP status code 200), the
response includes a ``taler_refund_uri``. The frontend must redirect
the customer’s browser to that URL to allow the refund to be processed
by the wallet.

This code snipped illustrates giving a refund:

.. code-block:: python

   >>> import requests
   >>> refund_req = dict(refund="KUDOS:10",
   ...                   reason="Customer did not like the product")
   >>> requests.post("https://backend.demo.taler.net/private/orders/"
   ...               + order_id + "/refund", json=refund_req,
   ...               headers={"Authorization": "Bearer secret-token:sandbox"})
   <Response [200]>

.. Note::
   After granting a refund, the public
   ``https://example.com/orders/$ORDER_ID`` endpoint will
   change its wallet interaction from requesting payment to
   offering a refund.  Thus, frontends may again redirect
   browsers to this endpoint.  However, to do so, a
   ``h_contract`` field must be appended
   (``?h_contract=$H_CONTRACT``) as the public endpoint requires
   it to authenticate the client.  The required
   ``$H_CONTRACT`` value is returned in the refund response
   under the ``h_contract`` field.


.. index:: repurchase
.. _repurchase:

Repurchase detection and fulfillment URLs
=========================================

A possible problem for merchants selling access to digital articles
is that a customer may have paid for an article on one device, but
may then want to read it on a different device, possibly one that
does not even have a Taler wallet installed.

Naturally, at this point the customer would at first still be prompted to pay
for the article again. If the customer then opens the ``taler://`` link in the
wallet that did previously pay for the article (for example by scanning the QR
code on the desktop with the Android App), the wallet will claim the contract,
detect that the fulfillment URL is identical to one that it already has made a
payment for in the past, and initiate **repurchase redirection**: Here, the
wallet will contact the merchant and replay the previous payment, except this
time using the (current) session ID of the browser (it learns the session ID
from the QR code).

The merchant backend then updates the session ID of the existing order to
the current session ID of the browser.  When the payment status for the
"new" unpaid order is checked (or already in long-polling), the backend
detects that for the browser's *session ID* and *fulfillment URL* there is an
existing paid contract. It then tells the browser to immediately redirect to
the fulfillment URL where the already paid article is available.

To ensure this mechanism works as designed, merchants must make sure to not
use the same fulfillment URL for different products or for physical products
where customers may be expected to buy the article repeatedly.  Similarly,
it is crucial that merchants consistently use the same fulfillment URL for
the same digital product where repurchase detection is desired.

Note that changing the session ID to a different device requires the
involvement of the wallet that made the payment, thus reasonably limiting the
possibility of broadly sharing the digital purchases.  Repurchase detection is
also *only* done for HTTP(S) fulfillment URLs. In particular, this means
fulfillment URIs like ``taler://fulfillment-success/$MESSAGE`` are not
considered to identify a resource you can pay for and thus do not have to be
unique.


.. _Giving-Customers-Rewards:
.. index:: rewards

Giving Customers Rewards
========================

GNU Taler allows Web sites to grant digital cash directly to a visitor. The
idea is that some sites may want incentivize actions such as filling out a
survey or trying a new feature. It is important to note that receiving rewards is
not enforceable for the visitor, as there is no contract.  It is simply a
voluntary gesture of appreciation of the site to its visitor. However, once a
reward has been granted, the visitor obtains full control over the funds provided
by the site.

The merchant backend of the site must be properly configured for rewards, and
sufficient funds must be made available for rewards. See the :ref:`Taler User
Guide <Rewarding-visitors>` for details.

To check if rewards are configured properly and if there are sufficient
funds available for granting rewards, query the ``/private/reserves`` endpoint:

.. code-block:: python

   >>> import requests
   >>> requests.get("https://backend.demo.taler.net/private/reserves",
   ...              headers={"Authorization": "Bearer secret-token:sandbox"})
   <Response [200]>

Check that a reserve exists where the ``merchant_initial_amount`` is below the
``committed_amount`` and that the reserve is ``active``.

.. _authorize-reward:

To authorize a reward, ``POST`` to ``/private/rewards``. The following fields
are recognized in the JSON request object:

-  ``amount``: Amount that should be given to the visitor as a reward.

-  ``justification``: Description of why the reward was granted. Human-readable
   text not exposed to the customer, but used by the Back Office.

-  ``next_url``: The URL that the user’s browser should be redirected to by
   the wallet, once the reward has been processed.

The response from the backend contains a ``taler_reward_url``. The
customer’s browser must be redirected to this URL for the wallet to pick
up the reward.

.. _pick-up-reward:

This code snipped illustrates giving a reward:

.. code-block:: python

   >>> import requests
   >>> reward_req = dict(amount="KUDOS:0.5",
   ...                justification="User filled out survey",
   ...                next_url="https://merchant.com/thanks.html")
   >>> requests.post("https://backend.demo.taler.net/private/rewards", json=reward_req,
   ...              headers={"Authorization": "Bearer secret-token:sandbox"})
   <Response [200]>


.. _Advanced-topics:

Advanced topics
===============

.. _Session_002dBound-Payments:

Session-Bound Payments
----------------------

.. index:: session

Sometimes checking if an order has been paid for is not enough. For
example, when selling access to online media, the publisher may want to
be paid for exactly the same product by each customer. Taler supports
this model by allowing the mechant to check whether the “payment
receipt” is available on the user’s current device. This prevents users
from easily sharing media access by transmitting a link to the
fulfillment page. Of course, sophisticated users could share payment
receipts as well, but this is not as easy as sharing a link, and in this
case they are more likely to just share the media directly.

To use this feature, the merchant must first assign the user’s current
browser an ephemeral ``session_id``, usually via a session cookie. When
executing or re-playing a payment, the wallet will receive an additional
signature (``session_sig``). This signature certifies that the wallet
showed a payment receipt for the respective order in the current
session.

.. index:: cookie

Session-bound payments are triggered by passing the ``session_id``
parameter to the ``/check-payment`` endpoint. The wallet will then
redirect to the fulfillment page, but include an additional
``session_sig`` parameter. The frontend can query ``/check-payment``
with both the ``session_id`` and the ``session_sig`` to verify that the
signature is correct.

The last session ID that was successfully used to prove that the payment
receipt is in the user’s wallet is also available as ``last_session_id``
in the response to ``/check-payment``.

.. _Product-Identification:

Product Identification
----------------------

.. index:: resource url

In some situations the user may have paid for some digital good, but the
frontend does not know the exact order ID, and thus cannot instruct the
wallet to reveal the existing payment receipt. This is common for simple
shops without a login system. In this case, the user would be prompted
for payment again, even though they already purchased the product.

To allow the wallet to instead find the existing payment receipt, the
shop must use a unique fulfillment URL for each product. Then, the
frontend must provide an additional ``resource_url`` parameter to to
``/check-payment``. It should identify this unique fulfillment URL for
the product. The wallet will then check whether it has paid for a
contract with the same ``resource_url`` before, and if so replay the
previous payment.


.. _The-Taler-Order-Format:

The Taler Order Format
----------------------

A Taler order can specify many details about the payment. This section
describes each of the fields in depth.

Financial amounts are always specified as a string in the format
``"CURRENCY:DECIMAL_VALUE"``.

.. index:: amount

amount
   Specifies the total amount to be paid to the merchant by the
   customer.

.. index:: fees
.. index:: maximum deposit fee

max_fee
   This is the maximum total amount of deposit fees that the merchant is
   willing to pay. If the deposit fees for the coins exceed this amount,
   the customer has to include it in the payment total. The fee is
   specified using the same format used for ``amount``.

.. index:: fees
.. index:: maximum wire fee

max_wire_fee
   Maximum wire fee accepted by the merchant (customer share to be
   divided by the ``wire_fee_amortization`` factor, and further reduced if
   deposit fees are below ``max_fee``). Default if missing is zero.

.. index:: fees
.. index:: maximum fee amortization

wire_fee_amortization
   Over how many customer transactions does the merchant expect to
   amortize wire fees on average? If the exchange’s wire fee is above
   ``max_wire_fee``, the difference is divided by this number to compute
   the expected customer’s contribution to the wire fee. The customer’s
   contribution may further be reduced by the difference between the
   ``max_fee`` and the sum of the actual deposit fees. Optional, default
   value if missing is 1. Zero and negative values are invalid and also
   interpreted as 1.

.. index:: pay_url

pay_url
   Which URL accepts payments. This is the URL where the wallet will
   POST coins.

.. index:: fulfillment URL

fulfillment_url
   Which URL should the wallet go to for obtaining the fulfillment, for
   example the HTML or PDF of an article that was bought, or an order
   tracking system for shipments, or a simple human-readable Web page
   indicating the status of the contract.

.. index:: order ID

order_id
   Alphanumeric identifier, freely definable by the merchant. Used by
   the merchant to uniquely identify the transaction.

.. index:: summary

summary
   Short, human-readable summary of the contract. To be used when
   displaying the contract in just one line, for example in the
   transaction history of the customer.

timestamp
   Time at which the offer was generated.

.. index:: payment deadline

pay_deadline
   Timestamp of the time by which the merchant wants the exchange to
   definitively wire the money due from this contract. Once this
   deadline expires, the exchange will aggregate all deposits where the
   contracts are past the ``refund_deadline`` and execute one large wire
   payment for them. Amounts will be rounded down to the wire transfer
   unit; if the total amount is still below the wire transfer unit, it
   will not be disbursed.

.. index:: refund deadline

refund_deadline
   Timestamp until which the merchant willing (and able) to give refunds
   for the contract using Taler. Note that the Taler exchange will hold
   the payment in escrow at least until this deadline. Until this time,
   the merchant will be able to sign a message to trigger a refund to
   the customer. After this time, it will no longer be possible to
   refund the customer. Must be smaller than the ``pay_deadline``.

.. index:: product description

products
   Array of products that are being sold to the customer. Each entry
   contains a tuple with the following values:

   description
      Description of the product.

   quantity
      Quantity of the items to be shipped. May specify a unit (e.g. ``1 kg``)
      or just the count.

   price
      Price for ``quantity`` units of this product shipped to the given
      ``delivery_location``. Note that usually the sum of all of the prices
      should add up to the total amount of the contract, but it may be
      different due to discounts or because individual prices are
      unavailable.

   product_id
      Unique ID of the product in the merchant’s catalog. Can generally
      be chosen freely as it only has meaning for the merchant, but
      should be a number in the range :math:`[0,2^{51})`.

   taxes
      Map of applicable taxes to be paid by the merchant. The label is
      the name of the tax, i.e. VAT, sales tax or income tax, and the
      value is the applicable tax amount. Note that arbitrary labels are
      permitted, as long as they are used to identify the applicable tax
      regime. Details may be specified by the regulator. This is used to
      declare to the customer which taxes the merchant intends to pay,
      and can be used by the customer as a receipt. The information is
      also likely to be used by tax audits of the merchant.

   delivery_date
      Time by which the product is to be delivered to the
      ``delivery_location``.

   delivery_location
      This should give a label in the ``locations`` map, specifying where
      the item is to be delivered.

   Values can be omitted if they are not applicable. For example, if a
   purchase is about a bundle of products that have no individual prices
   or product IDs, the ``product_id`` or ``price`` may not be specified in the
   contract. Similarly, for virtual products delivered directly via the
   fulfillment URI, there is no ``delivery_location``.

merchant
   address
      This should give a label in the ``locations`` map, specifying where
      the merchant is located.

   name
      This should give a human-readable name for the merchant’s
      business.

   jurisdiction
      This should give a label in the ``locations`` map, specifying the
      jurisdiction under which this contract is to be arbitrated.

.. index:: location

locations
   Associative map of locations used in the contract. Labels for
   locations in this map can be freely chosen and used whenever a
   location is required in other parts of the contract. This way, if the
   same location is required many times (such as the business address of
   the customer or the merchant), it only needs to be listed (and
   transmitted) once, and can otherwise be referred to via the label. A
   non-exhaustive list of location attributes is the following:

   name
      Receiver name for delivery, either business or person name.

   country
      Name of the country for delivery, as found on a postal package,
      e.g. “France”.

   state
      Name of the state for delivery, as found on a postal package, e.g.
      “NY”.

   region
      Name of the region for delivery, as found on a postal package.

   province
      Name of the province for delivery, as found on a postal package.

   city
      Name of the city for delivery, as found on a postal package.

   zip_code
      ZIP code for delivery, as found on a postal package.

   street
      Street name for delivery, as found on a postal package.

   street_number
      Street number (number of the house) for delivery, as found on a
      postal package.


.. Note::
   Locations are not required to specify all of these fields,
   and they is also allowed to have additional fields. Contract
   renderers must render at least the fields listed above, and should
   render fields that they do not understand as a key-value list.
