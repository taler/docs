..
  This file is part of GNU TALER.

  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Christian Grothoff

.. _taler-merchant-backend-operator-manual:

GNU Taler Merchant Backend Operator Manual
##########################################

Introduction
============

About GNU Taler
---------------

.. include:: frags/about-taler.rst


.. _About-this-manual:

About this manual
-----------------

This manual targets system administrators who want to install a GNU
Taler merchant *backend*.

We expect some moderate familiarity with the compilation and
installation of Free Software packages. An understanding of cryptography
is not required.

This first chapter of the manual will give a brief overview of the
overall Taler architecture, describing the environment in which the
Taler backend operates. The second chapter then explains how to install
the software, including key dependencies. The third chapter will explain
how to configure the backend, including in particular the configuration
of the bank account details of the merchant.

The last chapter gives some additional information about advanced topics
which will be useful for system administrators but are not necessary for
operating a basic backend.

.. _Architecture-overview:

Architecture overview
---------------------

.. index:: crypto-currency
.. index:: KUDOS

Taler is a pure payment system, not a new crypto-currency. As such, it
operates in a traditional banking context. In particular, this means that in
order to receive funds via Taler, the merchant must have a regular bank
account, and payments can be executed in ordinary currencies such as USD or
EUR.  Taler can also be used as a regional currency; for such scenarios, the
Taler system also includes its own stand-alone bank.

.. index:: frontend
.. index:: back-office
.. index:: backend
.. index:: DBMS
.. index:: PostgreSQL

The Taler software stack for a merchant consists of four main components:

-  A *frontend* which interacts with the customer’s browser. The frontend
   enables the customer to build a shopping cart and place an order.
   Upon payment, it triggers the respective business logic to satisfy
   the order. This component is not included with Taler, but rather
   assumed to exist at the merchant.
   The :ref:`Merchant API Tutorial <merchant-api-tutorial>` gives an
   introduction for how to integrate Taler with Web shop frontends.
-  A *back-office* application that enables the shop operators to view
   customer orders, match them to financial transfers, and possibly
   approve refunds if an order cannot be satisfied. This component is
   not included with Taler, but rather assumed to exist at the
   merchant. The :ref:`Merchant Backend API <merchant-api>` provides
   the API specification that should be reviewed to integrate such a
   back-office with the Taler backend.
-  A Taler-specific payment *backend* which makes it easy for the frontend
   to process financial transactions with Taler. This manual primarily
   describes how to install and configure this backend.
-  A *DBMS* which stores the transaction history for the Taler backend.
   For now, the GNU Taler reference implementation only supports
   PostgreSQL, but the code could be easily extended to support another
   DBMS.  Please review the PostgreSQL documentation for details on
   how to configure the database.

The following image illustrates the various interactions of these key
components:

.. image:: images/arch-api.png

.. index:: RESTful

Basically, the backend provides the cryptographic protocol support, stores
Taler-specific financial information in a DBMS and communicates with the GNU
Taler exchange over the Internet. The frontend accesses the backend via a
RESTful API. As a result, the frontend never has to directly communicate with
the exchange, and also does not deal with sensitive data. In particular, the
merchant’s signing keys and bank account information are encapsulated within
the Taler merchant backend.

A typical deployment will additionally include a full-blown Web server (like
Apache or Nginx). Such a Web server would be responsible for TLS termination and
access control to the ``/private/`` and ``/management/`` API endpoints of the
merchant backend. Please carefully review the section on :ref:`secure setup
<Secure-setup>` before deploying a Taler merchant backend into production.


Terminology
===========

This chapter describes some of the key concepts used throughout the manual.

Instances
---------

.. index:: instance

The backend allows a single HTTP server to support multiple independent shops
with distinct business entities sharing a single backend.  An *instance* is
the name or identifier that allows the single HTTP server to determine which
shop a request is intended for.  Each instance has its own base URL in the
REST API of the merchant backend (``/instances/$INSTANCE/``).  Each instance
can use its own bank accounts and keys for signing contracts. All major
accounting functionality is separate per instance.  Access to each instance is
controlled via a bearer token (to be set in the HTTP "Authorization" header).
All instances share the same *database*, top-level HTTP(S) address and the
main Taler configuration (especially the accepted *currency* and *exchanges*).

  .. note::

    This documentation does not use the term "user" or "username" in
    conjunction with instances as that might create confusion between
    instances with paying customers using the system.  We also do not use the
    term "account" in conjunction with instances, as that might cause
    confusion with bank accounts.  That said, conceptually it is of course
    acceptable to consider instances to be the "users" or "accounts" of a
    merchant backend and the bearer token is equivalent to a passphrase.


Instance Bank Accounts
----------------------

.. index:: instance-bank-account

To receive payments, an instance must have configured one or more bank
*accounts*.  When configuring the bank account of an instance, one should
ideally also provide the address and credentials of an HTTP service
implementing the :ref:`Taler Bank Revenue HTTP API
<taler-bank-merchant-http-api>`.  Given such a service, the GNU Taler merchant
backend can automatically reconcile wire transfers from the exchange to the
merchant's bank account with the orders that are being settled.

This documentation exclusively uses the term *account* for the bank
accounts of a merchant or shop that may be associated with an instance.

Inventory
---------

.. index:: inventory
.. index:: product
.. index:: lock
.. index:: unit
.. index:: order

The Taler backend offers inventory management as an optional function.
Inventory is tracked per instance and consists of *products* sold in
*units*. Inventory can be finite (physical stock) or infinite (for digital
products).  Products may include previews (images) to be shown to the user as
well as other meta-data. Inventory management allows the frontend to *lock*
products, reserving a number of units from stock for a particular (unpaid)
*order*. The backend can keep track of how many units of a product remain in
stock and ensure that the number of units sold does not exceed the number of
units in stock.

Inventory management is optional, and it is possible for the frontend to
include products in orders that are not in the inventory. The frontend
can also override prices of products in the inventory or set a total price
for an order that is different from the price of the sum of the products
in the order.


Orders and Contracts
--------------------

.. index:: order
.. index:: terms
.. index:: contract
.. index:: claim
.. index:: pay
.. index:: refund
.. index:: wire deadline
.. index:: lock
.. index:: legal expiration

In Taler, users pay merchants for *orders*. An order is first created by the
merchant. To create an order, the merchant must specify the specific *terms*
of the order.  Order *terms* include details such as the total amount to be
paid, payment fees the merchant is willing to cover, the set of products to
deliver, a delivery location and many other details.  The `merchant API
specification <contract-terms>`_ specifies the full set of possible order
terms.

After an order is created, it is *claimed* by a wallet. Once an order is
claimed by a specific wallet, only that wallet will be able to pay for this
order, to the exclusion of other wallets even if they see the same order URL.
Sharing order URLs is explicitly allowed: if a user shares an order URL
with another user, that other user should be given the opportunity to
purchase the same product.

To prevent unauthorized wallets from claiming an order, merchants can specify
that claims require authorization in the form of a *claim token*. This is
useful in case the order ID is predictable (say because an existing order ID
scheme with predictable order IDs from the merchant frontend is used) and at
the same time malicious actors claiming orders is problematic (say because of
limited stocks). The use of claim tokens is optional, but if a claim token is
used, it must be provided to the wallet as part of the order URI.

Additionally, when stocks are limited, you can configure Taler to set a
*product lock* on items (say, while composing the shopping cart).  These
locks will ensure that the limited stock is respected when making offers
to consumers.

A wallet may *pay* for a claimed order, at which point the order turns into a
(paid) *contract*.  Orders have a configurable expiration date (the
``pay_deadline``) after which the commercial offer expires and any stock of
products *locked* by the order will be automatically released, allowing the
stock to be sold in other orders.  When an unpaid order expires, the customer
must request a fresh order if they still want to make a purchase.

Once a contract has been paid, the merchant should fulfill the contract.  It
is possible for the merchant to *refund* a contract order, for example if the
contract cannot be fulfilled after all. Refunds are only possible after the
customer paid and before the exchange has *wired* the payment to the
merchant. Once the funds have been wired, refunds are no longer allowed by the
Taler exchange.  The *wire deadline* specifies the latest point in time by
which an exchange must wire the funds, while the (earlier) *refund deadline*
specifies the earliest point in time when an exchange may wire the funds.
Thus, refunds are always possible between the time of purchase and the
refund deadline, but may remain possible until the wire deadline.

Contract information is kept for legal reasons in the merchant database.  The
main legal reason is typically to provide tax records in case of a tax audit.
After the *legal expiration* (by default: a decade), contract information is
deleted when running the garbage collector using ``taler-merchant-dbinit``.


Transfers
---------

.. index:: transfer
.. index:: wire transfer

The Taler backend can be used to verify that the exchange correctly wired all
of the funds to the merchant. However, if no :ref:`Taler Bank Revenue HTTP API
<taler-bank-merchant-http-api>` was provided for the respective bank account,
the backend does not have access to the incoming wire transfers of the
merchant's bank account. In this case, merchants should manually provide the
backend with wire *transfer* data that specifies the *wire transfer subject*
and the amount that was received. Given this information, the backend can
detect and report any irregularities that might arise.  

Rewards
-------

.. index:: reward
.. index:: pick up

Taler does not only allow a Website to be paid, but also to make voluntary,
non-contractual payments to visitors, called *rewards*.  Such rewards could be
granted as a reward for filling in surveys or watching advertizements. For
rewards, there is no contract, rewards are always voluntary actions by the Web
site that do not arise from a contractual obligation.  Before a Web site
can create rewards, it must establish a reserve.  Once a reserve has been
established, the merchant can *grant* rewards, allowing wallets to *pick up*
the reward.

.. note::

    Rewards are an optional feature, and exchanges may disable rewards (usually
    if they see compliance issues). In this case, the reward feature will
    not be available.


Reserves
--------

.. index:: reserve
.. index:: close

A *reserve* is a pool of electronic cash at an exchange under the control of
a private key.  Merchants withdraw coins from a reserve when granting
rewards.  A reserve is established by first generating the required key material
in the merchant backend, and then wiring the desired amount of funds to the
exchange.

An exchange will automatically *close* a reserve after a fixed period of time
(typically about a month), wiring any remaining funds back to the merchant.
While exchange APIs exists to (1) explicitly *open* a reserve to prevent it
from being automatically closed and to (2) explicitly *close* a reserve at any
time, the current merchant backend does not make use of these APIs.


Installation
============

This chapter describes how to install the GNU Taler merchant backend.

.. _Generic-instructions:

Installing from source
----------------------

The following instructions will show how to install a GNU Taler
merchant backend from source.

The package sources can be find in our
`download directory <http://ftpmirror.gnu.org/taler/>`__.

.. include:: frags/semver.rst

First, the following packages need to be installed before we can compile the
backend:

.. include:: frags/list-of-dependencies.rst

.. include:: frags/installing-gnunet.rst

.. include:: frags/install-before-check.rst

There is no need to actually run a GNUnet peer to use the Taler merchant
backend -- all the merchant needs from GNUnet is a number of headers and
libraries!

.. include:: frags/installing-taler-exchange.rst

There is no need to actually run a Taler exchange to use the Taler merchant
backend -- all the merchant needs from the Taler exchange is a few headers and
libraries!

.. include:: frags/install-before-check.rst

.. include:: frags/installing-taler-merchant.rst

.. include:: frags/install-before-check.rst


Installing the GNU Taler binary packages on Debian
--------------------------------------------------

.. include:: frags/installing-debian.rst

.. include:: frags/apt-install-taler-merchant.rst


Installing the GNU Taler binary packages on Trisquel
----------------------------------------------------

.. include:: frags/installing-trisquel.rst


Installing the GNU Taler binary packages on Ubuntu
--------------------------------------------------

.. include:: frags/installing-ubuntu.rst

.. include:: frags/apt-install-taler-merchant.rst


How to configure the merchant’s backend
=======================================

.. index:: taler-config
.. index:: taler.conf

The installation already provides reasonable defaults for most of the
configuration options. However, some must be provided, in particular the
database that the backend should use. By default, the file
``$HOME/.config/taler.conf`` is where the Web shop administrator specifies
configuration values that augment or override the defaults.
Note that when using our binary packages, the systemd service files
force the use of ``/etc/taler.conf`` as the main configuration file.


.. include:: frags/configuration-format.rst

.. include:: frags/using-taler-config.rst


.. _Backend-options:

Backend options
---------------

.. index:: DBMS
.. index:: PostgreSQL
.. index:: UNIX domain socket
.. index:: TCP
.. index:: port
.. index:: currency
.. index:: KUDOS
.. index:: exchange
.. index:: instance
.. index:: wire format

The following table describes the options that commonly need to be
modified. Here, the notation ``[$SECTION]/$OPTION`` denotes the option
``$OPTION`` under the section ``[$SECTION]`` in the configuration file.


Service address
^^^^^^^^^^^^^^^

The following option sets the transport layer address used by the
merchant backend:

.. code-block:: ini

      [MERCHANT]/SERVE = tcp | unix

If this option is set to

-  ``tcp`` then we need to set the TCP port in ``[MERCHANT]/PORT``;

-  ``unix`` then we need to set the unix domain socket path and mode
   in ``[MERCHANT]/UNIXPATH`` and ``[MERCHANT]/UNIXPATH_MODE``. The
   latter takes the usual permission mask given as a number, e.g. 660
   for user/group read-write access.

The frontend can then connect to the backend over HTTP using the specified
address. If frontend and backend run within the same operating system, the
use of a UNIX domain socket is recommended to avoid accidentally exposing
the backend to the network.

To run the Taler backend on TCP port 8888, use:

.. code-block:: console

   $ taler-config -s MERCHANT -o SERVE -V tcp
   $ taler-config -s MERCHANT -o PORT -V 8888

.. note::

   When using the Debian/Ubuntu packages, these options are already
   configured in the ``/etc/taler/conf.d/merchant.conf`` configuration file.

   If you need to change them, you should edit
   ``/etc/taler/merchant-overrides.conf``.  By default, the Taler merchant
   package will use a UNIX domain socket at
   ``/run/taler/merchant-httpd/merchant-http.sock``. For the best possible
   security it is recommended to leave this in place and configure a reverse
   proxy (Nginx or Apache) as described below.



Currency
^^^^^^^^

Which currency the Web shop deals in, i.e. “EUR” or “USD”, is
specified using the option

.. code-block:: ini

      [TALER]/CURRENCY

When testing with the Taler demonstration exchange at
https://exchange.demo.taler.net/ you must set this
value to ``KUDOS``:

.. code-block:: console

   $ taler-config -s TALER -o CURRENCY -V KUDOS

.. note::

   When using the Debian/Ubuntu packages, these options should be
   configured in the ``/etc/taler/taler.conf`` configuration file
   (alternatively, you can also edit ``/etc/taler/merchant-overrides.conf``).
   However, you must edit the ``taler.conf`` file manually and **must not**
   use ``taler-config`` to do this, as that would inline the include
   directives and destroy the carefully setup path structure.


Database
^^^^^^^^

In principle it is possible for the backend to support different DBMSs.
The option

.. code-block:: ini

      [MERCHANT]/DB

specifies which DBMS is to be used. However, currently only the value
``postgres`` is supported. This is also the default.

In addition to selecting the DBMS software, the backend requires
DBMS-specific options to access the database.

.. note::

   The **taler-merchant-dbconfig** tool can be used to automate the database
   setup. When using the Debian/Ubuntu packages, the user should already have
   been created, so you can just run the tool without any arguments and should
   have a working database configuration.


For the ``postgres`` backend, you need to specify:

.. code-block:: ini

      [MERCHANTDB-postgres]
      CONFIG = "postgres://..."

This option specifies a PostgreSQL access path, typicallly using the format
``postgres:///$DBNAME``, where ``$DBNAME`` is the name of the PostgreSQL
database you want to use. Suppose ``$USER`` is the name of the user who will
run the backend process (usually ``taler-merchant-httpd``). Then, you need to
first run:

.. code-block:: console

   $ sudo -u postgres createuser -d $USER

as the PostgreSQL database administrator (usually ``postgres``) to
grant ``$USER`` the ability to create new databases. Next, you should
as ``$USER`` run:

.. code-block:: console

   $ createdb $DBNAME

to create the backend’s database. Here, ``$DBNAME`` must match the
database name given in the configuration file.

Now you should be able to create the tables and indices. To do this, run as
``$USER`` (usually ``taler-merchant-httpd``):

.. code-block:: console

   $ taler-merchant-dbinit

You may improve your security posture if you now REVOKE the rights to CREATE,
DROP or ALTER tables from ``$USER``. However, if you do so, please be aware
that you may have to temporarily GRANT those rights again when you update the
merchant backend.  For details on how to REVOKE or GRANT these rights, consult
the PostgreSQL documentation.

.. include:: frags/db-stores-sensitive-data.rst


.. index: MASTER_KEY

Exchange
^^^^^^^^

To add an exchange to the list of trusted payment service providers, you
create a section with a name that starts with “MERCHANT-EXCHANGE-”. In that
section, the following options need to be configured:

-  The ``EXCHANGE_BASE_URL`` option specifies the exchange’s base URL.
   For example, to use the Taler demonstrator, specify:

   .. code-block:: ini

      [MERCHANT-EXCHANGE-demo]
      EXCHANGE_BASE_URL = "https://exchange.demo.taler.net/"

-  The ``MASTER_KEY`` option specifies the exchange’s master public key
   in base32 encoding. For the Taler demonstrator, use:

   .. code-block:: ini

      [MERCHANT-EXCHANGE-demo]
      MASTER_KEY = "FH1Y8ZMHCTPQ0YFSZECDH8C9407JR3YN0MF1706PTG24Q4NEWGV0"

-  The ``CURRENCY`` option specifies the exchange’s currency.
   For the Taler demonstrator, use:

   .. code-block:: ini

      [MERCHANT-EXCHANGE-demo]
      CURRENCY = "KUDOS"


Note that multiple exchanges can be added to the system by using different
identifiers in place of ``demo`` in the example above. Note that all of the
exchanges actually used will use the same currency: If the currency does not
match the main ``CURRENCY`` option from the ``TALER`` section, the respective
``MERCHANT-EXCHANGE-`` section is automatically ignored. If you need support
for multiple currencies, you need to deploy one backend per currency.

.. note::

   Manually setting up exchanges is only recommended under special
   circumstances. In general, GNU Taler distributions will include trustworthy
   exchanges (for each currency) in the default configuration, and there is
   rarely a good reason for trusting an exchange that has no relationship
   with the GNU Taler development team.


.. _Sample-backend-configuration:

Sample backend configuration
----------------------------

.. index:: configuration

The following is an example for a complete backend configuration:

.. code-block:: ini

   [TALER]
   CURRENCY = KUDOS

   [MERCHANT]
   SERVE = TCP
   PORT = 8888
   DATABASE = postgres

   [MERCHANTDB-postgres]
   CONFIG = postgres:///donations

   [merchant-exchange-NAME]
   EXCHANGE_BASE_URL = https://exchange.demo.taler.net/
   MASTER_KEY = FH1Y8ZMHCTPQ0YFSZECDH8C9407JR3YN0MF1706PTG24Q4NEWGV0
   # If currency does not match [TALER] section, the exchange
   # will be ignored!
   CURRENCY = KUDOS

Given the above configuration, the backend will use a PostgreSQL database
named ``donations`` running on the same host.

The backend will deposit the coins it receives to the exchange at
https://exchange.demo.taler.net/, which has the master key
``FH1Y8ZMHCTPQ0YFSZECDH8C9407JR3YN0MF1706PTG24Q4NEWGV0``.


.. _Launching-the-backend:

Launching the backend
---------------------

.. index:: backend
.. index:: taler-merchant-httpd

Assuming you have configured everything correctly, you can launch the
merchant backend as ``$USER`` using

.. code-block:: console

   $ taler-merchant-httpd &
   $ taler-merchant-webhook &
   $ taler-merchant-wirewatch &

You only need to run ``taler-merchant-webhook`` if one of the instances is
configured to trigger web hooks.  Similarly, ``taler-merchant-wirewatch`` is
only required if instances have accounts configured with automatic import of
wire transfers via a bank wire gateway.

To ensure these processes runs always in the background and also after
rebooting, you should use systemd, cron or some other init system of your
operating system to launch the process.  You should also periodically re-start
these services to prevent them from exhausing the memory utilization of the
PostgreSQL database.  Consult the documentation of your operating system for
how to start and stop daemons.

.. note::

   When using the Debian/Ubuntu packages, the systemd configuration
   will already exist. You only need to enable and start the service
   using ``systemctl enable taler-merchant-httpd`` and
   ``systemctl start taler-merchant-httpd``. Additionally, you should
   review the ``/etc/apache2/sites-available/taler-merchant.conf``
   or ``/etc/nginx/sites-available/taler-merchant`` (these files
   contain additional instructions to follow), symlink it to
   ``sites-enabled/`` and restart your HTTP server. After that, you
   should be able to visit the merchant backend at the respective
   HTTP(S) endpoint.


If everything worked as expected, the command

.. code-block:: console

   $ wget -O - http://localhost:8888/config

should return some basic configuration status data about the service.

Please note that your backend might then be globally reachable without
any access control.  You can either:

  * Use the ``--auth=$TOKEN`` command-line option to set an access token to be provided in an ``Authorize: Bearer $TOKEN`` HTTP header. Note that this can be used at anytime to override access control, but remains only in effect until a first instance is created or an existing instance authentication setting is modified.
  * Set the ``TALER_MERCHANT_TOKEN`` environment variable to ``$TOKEN`` for the same effect. This method has the advantage of ``$TOKEN`` not being visible as a command-line interface to other local users on the same machine.
  * Set up an instance with an authentication token before some unauthorized person has a chance to access the backend.  As the backend is useless without any instance and the chances of remote attackers during the initial configuration is low, this is probably sufficient for most use-cases. Still, keep the first two scenarios in mind in case you ever forget your access token!

Production systems should additionally be configured to bind to a UNIX domain socket
and use TLS for improved network privacy, see :ref:`Secure setup <Secure-setup>`.


.. index:: instance
.. _Instance-setup:


Instance setup
==============

First of all, we recommend the use of the single-page administration
application (SPA) that is served by default at the base URL of the merchant
backend.  You can use it to perform all steps described in this section (and
more!), using a simple Web interface instead of the ``wget`` commands given
below.

Regardless of which tool you use, the first step for using the backend
involves the creation of a ``default`` instance. The ``default`` instance can
also create / delete / configure other instances, similar to the ``root``
account on UNIX.  When no instance exists and ``taler-merchant-httpd`` was
started without the ``--auth`` option, then the backend is reachable without
any access control (unless you configured some in the reverse proxy).

The following documentation shows how to handle any instance. Thus, if you
want to have multiple instances, you may need to perform the steps multiple
times, once for each instance.

.. note::

  A potential security concern is that normal API usage leaks instance existence.
  This means unauthorized users can distinguish between the case where the
  instance does not exist (HTTP 404) and the case where access is denied
  (HTTP 403).
  This is concern can be addressed using a properly configured
  :ref:`reverse proxy <reverse-proxy-configuration>`.


Setup without the Web interface
-------------------------------

Instances can be created by POSTing a request to ``/management/instances``
without using the Web interface.  This could be useful if you want to create
many instances programmatically.  To create an instance without the Web
interface create a file ``instance.json`` with an
`InstanceConfigurationMessage`:

.. code-block:: json

   {
     "accounts" : [{"payto_uri":"$PAYTO_URI"}],
     "id" : "default",
     "name": "example.com",
     "address": { "country" : "zz" },
     "auth": { "method" : "external"} ,
     "jurisdiction": { "country" : "zz" },
     "use_stefan": true,
     "default_wire_transfer_delay": { "d_ms" : 1209600000 },
     "default_pay_delay": { "d_ms" : 1209600000 }
   }

In the text above, you must replace ``$PAYTO_URI`` with your actual
``payto://``-URI.  You may also leave the account array empty. The instance
owner must then configure the accounts before the instance becomes usable.

Be sure to replace ``KUDOS`` with the fiat currency if the setup is for an
actual bank. The ``name`` field will be shown as the name of your shop. The
``address`` field is expected to contain your shop's physical address. The
various defaults specify defaults for transaction fees your shop is willing to
cover, how long offers made to the customer are valid, and how long the
exchange has before it must wire the funds to your bank account. Those
defaults can be modified for individual orders.  For details, see the
:ref:`contract terms <contract-terms>` specification.

You can then create the instance using:

.. code-block:: console

   $ wget --post-file=instance.json http://localhost:8888/management/instances

The base URL for the instance will then be
``http://localhost:8888/instances/default``.  You can create additional
instances by changing the ``id`` value to identifies other than ``default``.

Endpoints to modify (reconfigure), permanently disable (while keeping the data)
or purge (deleting all associated data) instances exist as well and are documented
in the :ref:`Merchant Backend API documentation <merchant-api>`.


.. _Secure-setup:

Secure setup
============

.. index:: security
.. index:: TLS

The Taler backend does not include even the most basic forms of access control
or transport layer security.  Thus, production setups **must** deploy the
Taler backend behind an HTTP(S) server that acts as a *reverse proxy*,
performs TLS termination and authentication and then forwards requests to the
backend.

Using UNIX domain sockets
-------------------------

To ensure that the merchant backend is not exposed directly to the network,
you *should* bind the backend to a UNIX domain socket:

.. code-block:: console

   $ taler-config -s MERCHANT -o SERVE -V unix
   $ taler-config -s MERCHANT -o UNIXPATH -V "/some/path/here.sock"

Do not use a UNIX domain socket path in "/tmp": systemd (or other init
systems) may give Web servers a private "/tmp" thereby hiding UNIX domain
sockets created by other users/processes in "/tmp".

If UNIX domain sockets are for some reason not possible, you *may* use a
host-based firewall to block access to the TCP port of the merchant backend,
but this is *not recommended*.  If you do need a TCP socket, you should
instead strongly consider using the "BIND_TO" option to at least bind it only
to "localhost".

.. _reverse-proxy-configuration:

Reverse proxy configuration
---------------------------

Nginx
^^^^^

For Nginx, a possible basic reverse proxy configuration would be:

.. code-block:: nginx

      proxy_pass http://unix:/some/path/here.sock;
      proxy_redirect off;
      proxy_set_header Host $host;
      proxy_set_header X-Forwarded-Host "example.com";
      proxy_set_header X-Forwarded-Proto "https";

Note that the above assumes your domain name is ``example.com`` and that you
have TLS configured.  Leave out the last line if your Nginx reverse proxy does
not have HTTPS enabled.  Make sure to restart the ``taler-merchant-httpd``
process after changing the ``SERVE`` configuration.

Apache
^^^^^^

In Apache, make sure you have ``mod_proxy``, ``mod_proxy_http`` and
``mod_headers`` enabled:

.. code-block:: console

   $ a2enmod proxy
   $ a2enmod proxy_http
   $ a2enmod headers

Then configure your Apache reverse proxy like this (you may change the
endpoint):

.. code-block:: apacheconf

       <Location "/">
       ProxyPass "unix:/some/path/here.sock|http://example.com/"
       RequestHeader add "X-Forwarded-Proto" "https"
       </Location>

Note that the above again assumes your domain name is ``example.com`` and that
you have TLS configured.  Note that you must add the ``https`` header unless
your site is not available via TLS.

Access control
--------------

All endpoints with ``/private/`` in the URL must be restricted to authorized
users of the respective instance.  Specifically, the HTTP server must be
configured to only allow access to ``$BASE_URL/private/`` to the authorized
users of the default instance, and to ``$BASE_URL/instances/$ID/private/`` to
the authorized users of the instance ``$ID``.

By default, the GNU Taler merchant backend simply requires the respective
HTTP requests to include an "Authorization" header with a "Bearer" token
set to the respective shared secret which must begin with "secret-token:"
(following RFC 8959).

Note that all of the other endpoints (without ``/private/``)
are expected to be fully exposed to the Internet, and wallets may have to
interact with those endpoints directly without client authentication.


Status code remapping
---------------------

Normal API usage leaks instance existence information.  Distinguishing between
404 (Not found) and 403 (Forbidden) is useful for diagnostics.

For higher security (by leaking less information), you can add the following
fragment, which remaps all 404 response codes to 403.

Nginx
^^^^^

.. code-block:: nginx

      error_page 404 =403 /empty.gif;

Apache
^^^^^^

.. code-block:: apacheconf

       cond %{STATUS} =404
       set-status 403


Customization
=============

Legal conditions for using the service
--------------------------------------

.. include:: frags/legal.rst


Mustach HTML Templates
----------------------

The installation process will install various HTML templates to be served
to trigger the wallet interaction. You may change those templates to your
own design. The templating language used is Mustach, and the templates
are in the ``share/taler/merchant/templates/`` directory.


Static files
------------

The merchant backend also has the ability to serve small static files
under the ``/static/{FILENAME}`` endpoint.  This is used by the templating
logic to load a CSS file, but you can also put other resources such as
images or JavaScript.

Internationalization
--------------------

Both templates and static files can be internationalized.  This is done
by having the language of the resource be a part of the filename.
For templates the format is ``{BASENAME}.{LANGUAGE}.must``.  The
language is mandatory for templates, the default language is English (en).

For static files, the format is ``{BASENAME}.{LANGUAGE}.{EXT}`` for
internationalized files, and ``{BASENAME}.{EXT}`` for resources that do not
support internationalization.  The HTTP client will always request
``/static/{BASENAME}.{EXT}``. If ``{BASENAME}.{EXT}`` exists, that resource is
returned. Otherwise, an internationalized file based on the language
preferences indicated by the browser is returned.

Limitations
-----------

All of the static files must fit into memory and it must be possible for the
process to hold open file handles for all of these files.  You may want
to increase the ``ulimit`` of the ``taler-merchant-httpd`` process if you have
many static files. Note that Mustach templates do not increase the number of
open files.

The backend determines the MIME type based on the file's extension. The list
of supported extensions is hard-coded and includes common text and image
formats.

The current backend only provides a limited set of variables for the Mustach
template expansion, and does not make use of scopes and other Mustach
features.



Upgrade procedure
=================

This section describes the general upgrade procedure.  Please see the release
notes for your specific version to check if a particular release has special
upgrade requirements.

Please note that upgrades are ONLY supported for released version of the
merchant. Attempting to upgrade from or to a version in Git is not supported
and may result in subtle data loss.

To safely upgrade the merchant, you should first stop the existing
``taler-merchant-httpd`` process, backup your merchant database (see
PostgreSQL manual), and then install the latest version of the code.

If you REVOKED database permissions, ensure that the rights to CREATE,
DROP, and ALTER tables are GRANTed to ``$USER`` again.  Then, run:

.. code-block:: console

   $ taler-merchant-dbinit

to upgrade the database to the latest schema.  After that, you may again
REVOKE the database permissions. Finally, restart the merchant services
processes, either via your systemd or init system, or directly.




Advanced topics
===============

.. _MerchantDatabaseScheme:

Database Scheme
---------------

The merchant database must be initialized using ``taler-merchant-dbinit``.
This tool creates the tables required by the Taler merchant to operate.
The tool also allows you to reset the Taler merchant database, which is
useful for test cases but should never be used in production. Finally,
``taler-merchant-dbinit`` has a function to garbage collect a database,
allowing administrators to purge records that are no longer required.

The database scheme used by the merchant looks as follows:

.. image:: images/merchant-db.png

.. _MerchantBenchmarking:

Benchmarking
------------

The merchant codebase offers the ``taler-merchant-benchmark`` tool to populate
the database with fake payments.  The main goal of the benchmarking tool is to
serve as a starting point (!) for merchants that are interested in developing
stress tests to see how far their infrastructure can scale. As is, it
currently is not actually good at stressing the payment system.

The ``taler-unified-setup.sh`` script can be used to launch all required
services and clients. However, the resulting deployment is simplistic
(everything on the local machine, one single-threaded process per service
type) and not optimized for performance at all. However, this can still be
useful to assess the performance impact of changes
to the code or configuration.


Various configuration files that can be used in the code snippets in this
section can be found in the ``src/merchant-tools/`` directory of the
merchant. These are generally intended as starting points.  Note that the
configuration files ending in ``.edited`` are created by
``taler-unified-setup.sh`` and contain some options that are determined at
runtime by the setup logic provided by ``taler-unified-setup.sh``.

See :ref:`Taler Exchange Manual <Benchmark-choose-bank>` for how to use ``taler-unified-setup.sh`` to setup the system and in particular on how to specify the bank to be used.

Running taler-merchant-benchmark
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can run the tool as follows:

.. code-block:: console

    $ CONF=benchmark-rsa.conf
    $ taler-unified-setup.sh -emwt -c "$CONF" -f -u exchange-account-1
    $ time taler-merchant-benchmark ordinary -c "$CONF".edited -u exchange-account-1 -f -p 20

The current tool has already a few options, but we expect that to deliver
*relevant* results it will need to be customized to better reflect the
workload of a particular merchant.  This customization would at this point
likely involve writing (C) code.  We welcome contributions to make it easier
to customize the benchmark and/or to cover more realistic workloads from the
start.

The tool takes all of the values it needs from the command line, with
some of them being common to all subcommands:

-  ``--exchange-account-section=SECTION`` Specifies which configuration
   section specifies the bank account for the exchange that
   should be used for the benchmark. For the example
   configuration above, the SECTION value provided must be
   ``exchange-account-exchange``.
-  ``--fakebank`` Specifies that the benchmark should expect to interact
   with a fakebank (instead of libeufin).

The tool comes with two operation modes: *ordinary*, and *corner*.
The first just executes normal payments, meaning that it uses the
default instance and make sure that all payments get aggregated. The
second gives the chance to leave some payments unaggregated, and also to
use merchant instances other than the default (which is, actually, the
one used by default by the tool).

Note: the ability of driving the aggregation policy is useful for testing
the back-office facility.

Any subcommand is also equipped with the canonical ``--help`` option, so
feel free to issue the following command in order to explore all the
possibilities. For example:

.. code-block:: console

   $ taler-merchant-benchmark corner --help

will show all the options offered by the *corner* mode. Among the most
interesting, there are:

-  ``--two-coins=TC`` This option instructs the tool to perform *TC*
   many payments that use two coins, because normally only one coin is
   spent per payment.

-  ``--unaggregated-number=UN`` This option instructs the tool to
   perform *UN* (one coin) payments that will be left unaggregated.

As for the ``ordinary`` subcommand, it is worth explaining the following
option:

-  ``--payments-number=PN`` Instructs the tool to perform *PN* payments.






Temporarily Abandoned Features
==============================

.. [1]
   https://docs.docker.com/


Installing Taler using Docker
-----------------------------

This section provides instructions for the merchant backend installation
using ‘Docker‘.

For security reasons, we run Docker against a VirtualBox instance, so
the ``docker`` command should connect to a ``docker-machine`` instance
that uses the VirtualBox driver.

Therefore, the needed tools are: “docker“, “docker-machine“, and
“docker-compose“. Please refer to Docker’s official  [1]_ documentation
in order to get those components installed, as that is not in this
manual’s scope.

Before starting to build the merchant’s image, make sure a
“docker-machine“ instance is up and running.

Because all of the Docker source file are kept in our “deployment“
repository, we start by checking out the ``git://git.taler.net/deployment``
codebase:

.. code-block:: console

   $ git clone git://git.taler.net/deployment

Now we actually build the merchant’s image. From the same directory as
above:

.. code-block:: console

   $ cd deployment/docker/merchant/
   $ docker-compose build

If everything worked as expected, the merchant is ready to be launched.
From the same directory as the previous step:

.. code-block:: console

   # Recall: the docker-machine should be up and running.
   $ docker-compose up

You should see some live logging from all the involved containers. At
this stage of development, you should also ignore some (harmless) error
message from postresql about already existing roles and databases.

To test if everything worked as expected, it suffices to issue a simple
request to the merchant, for example:

.. code-block:: console

   $ wget -O - http://$(docker-machine ip)/
   # A greeting message should be returned by the merchant.
