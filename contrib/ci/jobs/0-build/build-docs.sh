#!/bin/bash

set -eu

make html
make latexpdf

rm -rf /artifacts/docs_build

mkdir -p /artifacts/docs_build/docs/html/
mkdir -p /artifacts/docs_build/docs/pdf/

cp -r _build/html/* /artifacts/docs_build/docs/html/
cp -r _build/latex/*.pdf /artifacts/docs_build/docs/pdf/
