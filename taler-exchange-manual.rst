..
  This file is part of GNU TALER.

  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Christian Grothoff
  @author Florian Dold

GNU Taler Exchange Operator Manual
##################################

Introduction
============

About GNU Taler
---------------

.. include:: frags/about-taler.rst


About this manual
-----------------

This manual targets system administrators who want to install and
operate a GNU Taler exchange.

Organizational prerequisites
----------------------------

Operating a GNU Taler exchange means that you are operating a payment service
provider, which means that you will most likely need a bank license and/or
follow applicable financial regulation. Exceptions may apply, especially if
you are operating a regional currency or a payment system for an event with a
closed user group.

GNU Taler payment service providers generally need to ensure high availability
and should have *really* good backups (synchronous replication, asynchronous
remote replication, off-site backup, 24/7 monitoring, etc.).  This manual will
not cover these aspects of operating a payment service provider.

We will assume that you can operate a (sufficiently high-availability,
high-assurance) PostgreSQL database. Furthermore, we expect some moderate
familiarity with the compilation and installation of free software
packages. You need to understand the cryptographic concepts of private and
public keys and must be able to protect private keys stored in files on disk.

.. include:: frags/db-stores-sensitive-data.rst


Architecture overview
---------------------

GNU Taler is a pure payment system, not a crypto-currency. As such, it
operates in a traditional banking context. In particular, this means that
payments can be executed in ordinary currencies such as USD or EUR.
Furthermore, a typical merchant in Taler has a regular bank account, and would
use it to receive funds via Taler.

Consequently, a typical Taler exchange must interact with a bank. The bank of
the exchange holds funds in an account where the balance is basically
equivalent to the value of all coins in circulation. (Small mismatches arise
whenever customers are about to withdraw coins and have already send the funds
into the bank account, or if merchants just deposited coins and are about to
receive wire transfers for deposited coins, or due to fees charged by the
exchange and the operator not yet having drained the fees from the account.)

The exchange uses an intermediary system to talk to its bank.  This shifts the
technical burden (XML-based communications, additional cryptography, and a
vast variety of standards) for this interaction into another bank-specific
subsystem.  Such intermediary system abstracts the native banking protocol by
exposing the *Taler Wire Gateway API*; this way, the exchange can conduct its
banking operations in a simplified and JSON-based style.

When customers wire money to the exchange's bank account, the Taler Wire
Gateway API must notify the exchange about the incoming wire transfers. The
exchange then creates a *reserve* based on the subject of the wire
transfer. The wallet which knows the secret key matching the wire transfer
subject can then withdraw coins from the reserve, thereby draining it. The
liability of the exchange against the reserve is thereby converted into a
liability against digital coins issued by the exchange. When the customer
later spends the coins at a merchant, and the merchant *deposits* the coins at
the exchange, the exchange first *aggregates* the amount from multiple
deposits from the same merchant and then instructs its bank to make a wire
transfer to the merchant, thereby fulfilling its obligation and eliminating
the liability. The exchange charges *fees* for some or all of its operations
to cover costs and possibly make a profit.

*Auditors* are third parties, for example financial regulators, that verify
that the exchange operates correctly. The same software is also used to
calculate the exchange’s profits, risk and liabilities by the accountants of
the exchange.

The Taler software stack for an exchange consists of the following
components:

-  **HTTP frontend**:
   The HTTP frontend interacts with Taler wallets and merchant backends.
   It is used to withdraw coins, deposit coins, refresh coins, issue
   refunds, map wire transfers to Taler transactions, inquire about the
   exchange’s bank account details, signing keys and fee structure. The
   binary is the ``taler-exchange-httpd``.

-  **Crypto-Helpers**:
   The ``taler-exchange-secmod-rsa``, ``taler-exchange-secmod-cs`` and
   ``taler-exchange-secmod-eddsa``
   are three programs that are responsible for managing the exchange's
   online signing keys. They must run on the same machine as the
   ``taler-exchange-httpd`` as the HTTP frontend communicates with the
   crypto helpers using UNIX Domain Sockets.

-  **Aggregator**:
   The aggregator combines multiple deposits made by the same merchant
   and (eventually) triggers wire transfers for the aggregate amount.
   The merchant can control how quickly wire transfers are made. The
   exchange may charge a fee per wire transfer to discourage
   excessively frequent transfers. The binary is the
   ``taler-exchange-aggregator``.

-  **Closer**:
   The ``taler-exchange-closer`` tool check that reserves are properly
   closed. If a customer wires funds to an exchange and then fails
   to withdraw them, the closer will (eventually) trigger a wire
   transfer that sends the customer's funds back to the originating
   wire account.

-  **Transfer**:
   The ``taler-exchange-transfer`` tool is responsible for actually
   executing the aggregated wire transfers. It is the only process
   that needs to have the credentials to execute outgoing wire
   transfers.  The tool uses the Taler Wire Gateway API to execute
   wire transfers.  This API is provided by the Taler Python Bank
   for stand-alone deployments (like those with ``KUDOS``) and
   by LibEuFin.  LibEuFin is an adapter which maps the Taler Wire
   REST API to traditional banking protocols like EBICS and FinTS.

-  **Wirewatch**:
   The ``taler-exchange-wirewatch`` tool is responsible for observing
   incoming wire transfers to the exchange. It needs to have the
   credentials to obtain a list of incoming wire transfers.
   The tool also uses the Taler Wire Gateway API to observe such
   incoming transfers.  It is possible that observing incoming and
   making outgoing wire transfers is done via different bank accounts
   and/or credentials.

-  **Wire adapter**:
   A wire adapter is a component that enables exchange to talk to a bank.
   Each wire adapter must implement the Taler Wire Gateway API.  Three
   wire adapters are currently provided:

   (1) The **libtalerfakebank** implements a bank with a wire adapter API
       inside of a testcase.  ``taler-fakebank-run`` is a stand-alone
       process using libtalerfakebank.  Note that this adapter is only
       useful for tests, as all transaction data is kept in memory.
   (2) For production, **libeufin**'s ``libeufin-nexus`` component
       implements a wire adapter towards the traditional SEPA banking
       system with IBAN accounts using the EBICS protocol.
   (3) To use GNU Taler with blockchains, the **Depolymerization**
       component provides a wire gateway API that runs on top of
       blockchains like Bitcoin and Ethereum.

   The client-side wire adapter API is implemented in **libtalerbank** and
   is used by ``taler-exchange-transfer`` to execute wire transfers and by
   ``taler-exchange-wirewatch`` and the Taler auditor auditor to query bank
   transaction histories.

-  **DBMS**:
   The exchange requires a DBMS to stores the transaction history for
   the Taler exchange and aggregator, and a (typically separate) DBMS
   for the Taler auditor. For now, the GNU Taler reference implementation
   only supports PostgreSQL, but the code could be easily extended to
   support another DBMS.
   .. index:: PostgreSQL

-  **Auditor**:
   The auditor verifies that the transactions performed by the exchange
   were done properly. It checks the various signatures, totals up the
   amounts and alerts the operator to any inconsistencies. It also
   computes the expected bank balance, revenue and risk exposure of the
   exchange operator. The main binary is the ``taler-auditor``.
   Aside from the key setup procedures, the most critical setup for
   deploying an auditor is providing the auditor with an up-to-date
   copy of the exchange's database.


.. _KeyTypes:

Key Types
---------

The exchange works with four types of keys:

-  master key (kept offline, configured manually at merchants and wallets)

-  online message signing keys (signs normal messages from the exchange)

-  denomination keys (signs digital coins)

-  security module keys (signs online message signing keys and denomination keys)

Additionally, the exchange is sometimes concerned with the auditor's public
key (to verify messages signed by auditors approved by the exchange operator)
and the merchant's public key (to verify refunds are authorized by the
merchant).

Most of the keys are managed fully automatically or configured as part of the
denomination configuration.  Some configuration settings must be manually
set with regards to the exchange's master key.


Offline keys
------------

The exchange (and ideally also its auditor(s)) uses a long-term offline master
siging key that identifies the operator and is used to authenticate critical
information, such as the exchange's bank account and the actual keys the
exchange uses online.

Interactions with the offline system are performed using the
``taler-exchange-offline`` tool.  To use the offline system will require
exchange operators to copy JSON files from or to the offline system (say using
an USB stick).  The offline system does not need any significant amount of
computing power, a Raspberry-Pi is perfectly sufficient and the form-factor
might be good for safe-keeping! (You should keep a copy of the (encrypted)
private offline key on more than one physical medium though.)

Exchange operators are strongly advised to secure their private master key and
any copies on encrypted, always-offline computers. Again, this manual assumes
that you are familiar with good best practices in operational security,
including securing key material.


Online signing key security
---------------------------

To provide an additional level of protection for the private *online* signing
keys used by the exchange, the actual cryptographic signing operations are
performed by three helper processes, ``taler-exchange-secmod-rsa``,
``taler-exchange-secmod-cs`` and ``taler-exchange-secmod-eddsa``.

The current implementation does not yet support the use of a hardware security
module (HSM).  If you have such a device with adequate functionality and are
interested in Taler supporting it, please contact the developers for HSM
integration support.


Functionality
^^^^^^^^^^^^^

The UNIX domain sockets of the *secmod* helpers have mode 0620 (u+rw, g+w).
The exchange process MUST thus be in the same group as the crypto helper
processes to enable access to the keys. No other users should be in that
group!

The two helper processes will create the required private keys, and allow
anyone with access to the UNIX domain socket to sign arbitrary messages with
the keys or to inform them about a key being revoked.  The helper processes
are also responsible for deleting the private keys if their validity period
expires or if they are informed about a key having been revoked.


Security goals
^^^^^^^^^^^^^^

From a security point of view, the helpers are designed to *only* make it
harder for an attacker who took control of the HTTP daemon's account to
extract the private keys, limiting the attackers ability to creating
signatures to the duration of their control of that account.

.. note::
   In the future, the helper processes should additionally provide a mechanism
   to track the total number of signatures they have made for the various keys.

Setup
^^^^^

The helper processes should be run under a user ID that is separate from that
of the user running the main ``taler-exchange-httpd`` service.  To get any
security benefit from this, it is important that helpers run under a different
user ID than the main HTTP frontend. In fact, ideally, each helper should run
under its own user ID.  The ``taler-exchange-httpd`` service's will securely
communicate with the helpers using UNIX domain sockets.

Configuration
^^^^^^^^^^^^^

The helpers and the exchange HTTP service need both access to the same
configuration information.  Having divergent configurations may result in
run-time failures.  It is recommended that the configuration file (``-c``
option) is simply shared between all of the different processes, even though
they run as different system users. The configuration does not contain any
sensitive information.


.. _ExchangeInstallation:

Installation
============

Before installing a Taler exchange, please make sure that your system does not
have swap space enabled.  Swap space is a security risk that Taler does not
try to mitigate against.

We recommend the setup of offline signing keys to be done on a second machine that
does not have Internet access.

In this guide's shell-session fragments, the command prompt shows two pieces
of information:

* Who is performing the command
  (``$user`` vs ``root``, and ending character ``$`` vs ``#``).
* Host where the command is supposed to be executed
  (``exchange-offline`` vs ``exchange-online``).
  It is possible to do the entire setup on one machine,
  but we do not recommend this for security reasons.

Before you start
----------------

To deploy this with a real bank, you need:

  * IBAN of the bank account to use
  * BIC of the bank
  * EBICS host, user and partner IDs

Information to write down during the installation:

  * LibEuFin Nexus superuser password
  * Taler facade base URL
  * exchange Nexus username and password



Installing from source
----------------------

The following instructions will show how to install libgnunetutil and
the GNU Taler exchange from source.

The package sources can be find in our
`download directory <http://ftpmirror.gnu.org/taler/>`__.

.. include:: frags/semver.rst

First, the following packages need to be installed before we can compile the
backend:

.. include:: frags/list-of-dependencies.rst

.. include:: frags/installing-gnunet.rst

.. include:: frags/install-before-check.rst

There is no need to actually run a GNUnet peer to use the Taler merchant
backend -- all the merchant needs from GNUnet is a number of headers and
libraries!

.. include:: frags/installing-taler-exchange.rst

.. include:: frags/install-before-check.rst



Installing the GNU Taler binary packages on Debian
--------------------------------------------------

.. include:: frags/installing-debian.rst

To install the Taler exchange, you can now simply run:

.. code-block:: shell-session

   [root@exchange-online]# apt install taler-exchange

Note that the package does not perform any configuration work except for
setting up the various users and the systemd service scripts. You still must
configure at least the database, HTTP reverse proxy (typically with TLS
certificates), denomination and fee structure, bank account, auditor(s),
offline signing and the terms of service.

On the offline system, you should run at least:

.. code-block:: shell-session

   [root@exchange-offline]# apt install taler-exchange-offline


Installing the GNU Taler binary packages on Trisquel
----------------------------------------------------

.. include:: frags/installing-trisquel.rst

Installing the GNU Taler binary packages on Ubuntu
--------------------------------------------------

.. include:: frags/installing-ubuntu.rst

To install the Taler exchange, you can now simply run:

.. code-block:: shell-session

   [root@exchange-online]# apt install taler-exchange

Note that the package does not perform any configuration work except for
setting up the various users and the systemd service scripts. You still must
configure at least the database, HTTP reverse proxy (typically with TLS
certificates), denomination and fee structure, bank account, auditor(s),
offline signing and the terms of service.

On the offline system, you should run at least:

.. code-block:: shell-session

   [root@exchange-offline]# apt install taler-exchange-offline


Services, users, groups and file system hierarchy
-------------------------------------------------

The *taler-exchange* package will create several system users
to compartmentalize different parts of the system:

* ``taler-exchange-httpd``: runs the HTTP daemon with the core business logic.
* ``taler-exchange-secmod-rsa``: manages the RSA private online signing keys.
* ``taler-exchange-secmod-cs``: manages the CS private online signing keys.
* ``taler-exchange-secmod-eddsa``: manages the EdDSA private online signing keys.
* ``taler-exchange-closer``: closes idle reserves by triggering wire transfers that refund the originator.
* ``taler-exchange-aggregator``: aggregates deposits into larger wire transfer requests.
* ``taler-exchange-transfer``: performs wire transfers with the bank (via LibEuFin/Nexus).
* ``taler-exchange-wirewatch``: checks for incoming wire transfers with the bank (via LibEuFin/Nexus).
* ``postgres``: runs the PostgreSQL database (from *postgresql* package).
* ``www-data``: runs the frontend HTTPS service with the TLS keys (from *nginx* package).

.. note::

   The *taler-merchant* package additionally creates a ``taler-merchant-httpd`` user
   to run the HTTP daemon with the merchant business logic.


The exchange setup uses the following system groups:

* ``taler-exchange-db``: group for all Taler users with direct database access, specifically taler-exchange-httpd, taler-exchange-wire, taler-exchange-closer and taler-exchange-aggregator.
* ``taler-exchange-secmod``: group for processes with access to online signing keys; this group must have four users: taler-exchange-secmod-rsa, taler-exchange-secmod-cs, taler-exchange-secmod-eddsa and taler-exchange-httpd.
* ``taler-exchange-offline``: group for the access to the offline private key (only used on the offline host and not used on the online system).


The package will deploy systemd service files in
``/usr/lib/systemd/system/`` for the various components:

* ``taler-exchange-aggregator.service``: service that schedules wire transfers
  which combine multiple deposits to the same merchant.
* ``taler-exchange-closer.service``: service that watches for reserves that have been abandoned and schedules wire transfers to send the money back to the originator.
* ``taler-exchange-httpd.service``: main Taler exchange logic with the public REST API.
* ``taler-exchange-httpd.socket``: systemd socket activation for the Taler exchange HTTP daemon.
* ``taler-exchange-secmod-eddsa.service``: software security module for making EdDSA signatures.
* ``taler-exchange-secmod-rsa.service``: software security module for making RSA signatures.
* ``taler-exchange-secmod-cs.service``: software security module for making CS signatures.
* ``taler-exchange-transfer.service``: service that triggers outgoing wire transfers (pays merchants).
* ``taler-exchange-wirewatch.service``: service that watches for incoming wire transfers (first step of withdraw).
* ``taler-exchange.target``: Main target for the Taler exchange to be operational.


The deployment creates the following key locations in the system:

* ``/etc/taler/``: configuration files.
* ``/run/taler/``: contains the UNIX domain sockets for inter-process communication (IPC).
* ``/var/lib/taler/``: serves as the $HOME for all Taler users and contains sub-directories
  with the private keys; which keys are stored here depends on the host:

  * online system: exchange-secmod-eddsa, exchange-secmod-cs and exchange-secmod-rsa keys.
  * offline system: exchange-offline keys.


Configuration Fundamentals
==========================

This chapter provides fundamental details about the exchange configuration.

The configuration for all Taler components uses a single configuration file
as entry point: ``/etc/taler/taler.conf``.

System defaults are automatically loaded from files in
``/usr/share/taler/config.d``.  These default files should never be modified.

The default configuration ``taler.conf`` configuration file also includes all
configuration files in ``/etc/taler/conf.d``.  The settings from files in
``conf.d`` are only relevant to particular components of Taler, while
``taler.conf`` contains settings that affect all components.


The directory ``/etc/taler/secrets`` contains configuration file snippets with
values that should only be readable to certain users.  They are included with the ``@inline-secret@``
directive and should end with ``.secret.conf``.

To view the entire configuration annotated with the source of each configuration option, you
can use the ``taler-config`` helper:


.. code-block:: shell-session

  [root@exchange-online]# taler-config --diagnostics
  < ... annotated, full configuration ... >

.. warning::

  While ``taler-config`` also supports rewriting configuration files, we strongly
  recommend to edit configuration files manually, as ``taler-config`` does not
  preserve comments and, by default, rewrites ``/etc/taler/taler.conf``.


.. include:: frags/configuration-format.rst


.. _Using-taler_002dconfig-exchange:

.. include:: frags/using-taler-config.rst


Exchange Database Setup
=======================

The access credentials for the exchange's database are configured in
``/etc/taler/secrets/exchange-db.secret.conf``.  Currently, only PostgreSQL is
supported as a database backend.

The following users must have access to the exchange database:

* taler-exchange-httpd
* taler-exchange-wire
* taler-exchange-aggregator
* taler-exchange-closer

These users are all in the taler-exchange-db group, and the
``exchange-db.secret.conf`` should be only readable by users in
this group.

.. note::

   The **taler-exchange-dbconfig** tool can be used to automate the database
   setup. When using the Debian/Ubuntu packages, the users should already have
   been created, so you can just run the tool without any arguments and should
   have a working database configuration. The rest of this section only
   explains what the **taler-exchange-dbconfig** shell script fully automates.

To create a database for the Taler exchange on the local system, run:

.. code-block:: shell-session

  [root@exchange-online]# su - postgres
  [postgres@exchange-online]# createuser taler-exchange-httpd
  [postgres@exchange-online]# createuser taler-exchange-wire
  [postgres@exchange-online]# createuser taler-exchange-aggregator
  [postgres@exchange-online]# createuser taler-exchange-closer
  [postgres@exchange-online]# createdb -O taler-exchange-httpd taler-exchange
  [postgres@exchange-online]# exit

This will create a ``taler-exchange`` database owned by the
``taler-exchange-httpd`` user.  We will use that user later to perform
database maintenance operations.


Assuming the above database setup, the database credentials to configure
in the configuration file would simply be:

.. code-block:: ini
  :caption: /etc/taler/secrets/exchange-db.secret.conf

  [exchange]
  DB = postgres

  [exchangedb-postgres]
  CONFIG=postgres:///taler-exchange


If the database is run on a different host, please follow the instructions
from the PostgreSQL manual for configuring remote access.

After configuring the database credentials, the exchange database needs
to be initialized with the following command:

.. code-block:: shell-session

  [root@exchange-online]# sudo -u taler-exchange-httpd taler-exchange-dbinit

  ..note::

    To run this command, the user must have ``CREATE TABLE``, ``CREATE
    INDEX``, ``ALTER TABLE`` and (in the future possibly even) ``DROP TABLE``
    permissions.  Those permissions are only required for this step (which may
    have to be repeated when upgrading a deployment).  Afterwards, during
    normal operation, permissions to ``CREATE`` or ``ALTER`` tables are not
    required by any of the Taler exchange processes and thus should not be
    granted.  For more information, see
    :doc:`manpages/taler-exchange-dbinit.1`.

Finally we need to grant the other accounts limited access:

.. code-block:: shell-session

  [root@exchange-online]# sudo -u taler-exchange-httpd bash
  [taler-exchange-httpd@exchange-online]# echo 'GRANT SELECT,INSERT,UPDATE ON ALL TABLES IN SCHEMA exchange TO "taler-exchange-aggregator";' \
    | psql taler-exchange
  [taler-exchange-httpd@exchange-online]# echo 'GRANT SELECT,INSERT,UPDATE ON ALL TABLES IN SCHEMA exchange TO "taler-exchange-closer";' \
    | psql taler-exchange
  [taler-exchange-httpd@exchange-online]# echo 'GRANT SELECT,INSERT,UPDATE ON ALL TABLES IN SCHEMA exchange TO "taler-exchange-wire";' \
    | psql taler-exchange
  [taler-exchange-httpd@exchange-online]# echo 'GRANT USAGE ON ALL SEQUENCES IN SCHEMA exchange TO "taler-exchange-aggregator";' \
    | psql taler-exchange
  [taler-exchange-httpd@exchange-online]# echo 'GRANT USAGE ON ALL SEQUENCES IN SCHEMA exchange TO "taler-exchange-closer";' \
    | psql taler-exchange
  [taler-exchange-httpd@exchange-online]# echo 'GRANT USAGE ON ALL SEQUENCES IN SCHEMA exchange TO "taler-exchange-wire";' \
    | psql taler-exchange
  [taler-exchange-httpd@exchange-online]# exit

.. note::

   The above instructions for changing database permissions only work *after*
   having initialized the database with ``taler-exchange-dbinit``, as
   the tables need to exist before permissions can be granted on them. The
   ``taler-exchange-dbinit`` tool cannot setup these permissions, as it
   does not know which users will be used for which processes.


Basic Setup: Currency, Denominations and Keys
=============================================

A Taler exchange only supports a single currency.  The currency
and the smallest currency unit supported by the bank system
must be specified in ``/etc/taler/taler.conf``.

.. code-block:: ini
  :caption: /etc/taler/taler.conf

   [taler]
   CURRENCY = EUR
   CURRENCY_ROUND_UNIT = EUR:0.01

   # ... rest of file ...

.. warning::

   When editing ``/etc/taler/taler.conf``, take care to not accidentally remove
   the ``@inline-matching@`` directive to include the configuration files in ``conf.d``.

 .. _Coins-denomination-keys:

Coins (denomination keys)
-------------------------

Next, the electronic cash denominations that the exchange offers must be
specified.

Sections specifying denomination (coin) information start with ``coin_``.  By
convention, the name continues with ``$CURRENCY_[$SUBUNIT]_$VALUE_$REVISION``,
i.e. ``[coin_eur_ct_10_0]`` for a 10 cent piece. However, only the ``coin_``
prefix is mandatory.  Once configured, these configuration values must not
change.  The ``$REVISION`` part of the section name should be incremented if
any of the coin attributes in the section changes.  Each ``coin_``-section
must then have the following options:

-  ``VALUE``: How much is the coin worth, the format is
   CURRENCY:VALUE.FRACTION. For example, a 10 cent piece is "EUR:0.10".

-  ``DURATION_WITHDRAW``: How long can a coin of this type be withdrawn?
   This limits the losses incurred by the exchange when a denomination
   key is compromised.

-  ``DURATION_SPEND``: How long is a coin of the given type valid? Smaller
   values result in lower storage costs for the exchange.

-  ``DURATION_LEGAL``: How long is the coin of the given type legal?

-  ``FEE_WITHDRAW``: What does it cost to withdraw this coin? Specified
   using the same format as value.

-  ``FEE_DEPOSIT``: What does it cost to deposit this coin? Specified using
   the same format as value.

-  ``FEE_REFRESH``: What does it cost to refresh this coin? Specified using
   the same format as value.

-  ``FEE_REFUND``: What does it cost to refund this coin?
   Specified using the same format as value.

-  ``CIPHER``: Which cipher to use for this coin? Must be either ``RSA`` or
   ``CS``.

-  ``RSA_KEYSIZE``: How many bits should the RSA modulus (product of the two
   primes) have for this type of coin.

- ``AGE_RESTRICTED``: Set to ``YES`` to make this a denomination with support
   for age restrictions. See age restriction extension below for details.
   This option is optional and defaults to ``NO``.

See :doc:`manpages/taler.conf.5` for information on *duration* values
(i.e. ``DURATION_WITHDRAW`` and ``DURATION_SPEND`` above,
and ``OVERLAP_DURATION`` and ``DURATION`` below).
Additionally, there are two global configuration options of note:

-  ``[taler-exchange-secmod-rsa/OVERLAP_DURATION]``: What is the overlap of the
   withdrawal timespan for denomination keys?  The value given here must
   be smaller than any of the ``DURATION_WITHDRAW`` values for any of the coins.

-  ``[taler-exchange-secmod-rsa/LOOKAHEAD_SIGN]``: For how far into the future
   should denomination keys be pre-generated?  This allows the exchange and
   auditor operators to download, offline-sign, and upload denomination key
   signatures for denomination keys that will be used in the future by the
   exchange.

.. index:: maintenance
.. note::
   We recommend setting the ``LOOKAHEAD_SIGN`` value to at least one year and
   then to perform the offline-signing procedure at least once every 6 months
   to ensure that there is sufficient time for wallets to learn the new keys
   and to avoid unavailability in case this critical maintenance procedure is
   delayed.

.. note::
   It is crucial that the configuration provided in these sections is identical (!)
   for the exchange and the crypto helpers.  We recommend pointing both users
   to the same configuration file!

The ``taler-wallet-cli`` has a helper command that generates a
reasonable denomination structure.

.. code-block:: shell-session

  [root@exchange-online]# taler-wallet-cli deployment gen-coin-config \
                            --min-amount EUR:0.01 \
                            --max-amount EUR:100 \
                            > /etc/taler/conf.d/exchange-coins.conf

You can manually review and edit the generated configuration file. The main
change that is possibly required is updating the various fees.  Note that you
MUST NOT edit a coin configuration section after the initial setup. If you
must ``change`` the values, you must instead create a new section with a
different unique name (still with the ``coin-`` prefix) and comment out or
remove the existing section.  Do take care to not introduce the name of the
disabled section again in the future.


.. _Sign-keys:

Sign keys
---------

There are three global configuration options of note for sign keys:

-  ``[taler-exchange-secmod-eddsa/DURATION]``: How long are sign keys
   used to sign messages? After this time interval expires, a fresh
   sign key will be used (key rotation).  We recommend using
   a ``DURATION`` of a few weeks to a few months for sign keys.

-  ``[taler-exchange-secmod-eddsa/OVERLAP_DURATION]``: What is the overlap of the
   timespan for sign keys?  We recommend a few minutes or hours.  Must
   be smaller than ``DURATION``.

-  ``[taler-exchange-secmod-eddsa/LOOKAHEAD_SIGN]``: For how far into the future
   should sign keys be pre-generated?  This allows the exchange and
   auditor operators to download, offline-sign, and upload sign key
   signatures for sign keys that will be used in the future by the exchange.

.. note::
   We recommend setting the ``LOOKAHEAD_SIGN`` value to at least one year and
   then to perform the offline-signing procedure at least once every 6 months
   to ensure that there is sufficient time for wallets to learn the new keys
   and to avoid unavailability in case this critical maintenance procedure is
   delayed.


.. _OfflineConfiguration:

Setting up the offline signing key
----------------------------------

Before launching an exchange, the offline signing (master) key must be
generated and set in the configuration.  The offline signing keys of the
exchange should be stored on a different machine.  The responsibilities of
this offline signing machine are:

* Generation of the exchange's offline master signing key.
* Secure storage of the exchange's offline master signing key.
* Generation of certificates (signed with the offline master signing key) that will be imported by the exchange.
* Revocation of keys when the online system was compromised or is being terminated


Configuration file options related to the master key are:

- ``[exchange-offline/MASTER_PRIV_FILE]``: Path to the exchange’s master
   private file.  Only needs to be provided on the offline system where the
   ``taler-exchange-offline`` command is used.  The default value is usually
   fine and does not require adjustment.

- ``[exchange/MASTER_PUBLIC_KEY]``: Must specify the exchange’s master public
   key.  Needed for the exchange to verify information signed by the offline
   system.  This value must almost always be set explicitly by hand.



.. code-block:: shell-session

  [root@exchange-offline]# taler-exchange-offline setup
  < ... prints the exchange master public key >

The public key printed as the output of this command must be put into the
configuration of the online machine:

.. code-block:: ini
  :caption: /etc/taler/conf.d/exchange-business.conf

   [exchange]
   MASTER_PUBLIC_KEY = YE6Q6TR1ED...

   # ... rest of file ...


Wire Gateway Setup
==================

The Taler Wire Gateway is an API that connects the Taler exchange to
the underlying core banking system.

LibEuFin is an implementation of the Wire Gateway API for the EBICS protocol.
This section will walk through (1) installing and configuring LibEuFin and
(2) connecting the Taler Exchange to LibEuFin.

.. note::

   If you do not have a bank account with EBICS but want to test these instructions,
   you can use the EBICS sandbox as described in the
   :doc:`LibEuFin Tutorial <libeufin/nexus-tutorial>`.


Installation and Basic Configuration
------------------------------------

First, install the ``libeufin`` package.  This can be done on the ``exchange-online``
machine or a different one.

.. code-block:: shell-session

  [root@exchange-online]# apt-get install -y libeufin

The main component of LibEuFin is called the Nexus.  It implements a Web
service that provides a JSON abstraction layer to access bank accounts.

The HTTP port and database connection string can be edited in the configuration:

.. code-block:: ini
  :caption: /etc/libeufin/nexus.env

  LIBEUFIN_NEXUS_PORT=5017
  LIBEUFIN_NEXUS_DB_CONNECTION=jdbc:sqlite:/var/lib/libeufin/nexus/nexus-db.sqlite3

After configuring the database, you can start the service.
The database is initialized automatically.


.. code-block:: shell-session

   [root@exchange-online]# systemctl enable libeufin-nexus
   [root@exchange-online]# systemctl start libeufin-nexus

You can now create a superuser account.  The command to
create the superuser needs direct database access, thus
the configuration file is sourced first, and the relevant
environment variable is exported.

.. code-block:: console

  [root@exchange-online]# source /etc/libeufin/nexus.env
  [root@exchange-online]# export LIBEUFIN_NEXUS_DB_CONNECTION
  [root@exchange-online]# NEXUS_ADMIN_PW=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 13)
  [root@exchange-online]# libeufin-nexus superuser admin --password $NEXUS_ADMIN_PW

If you omit ``--password $NEXUS_ADMIN_PW``, you will interactively be asked for a password.
For simplicity, a superuser can as well act as a normal user, but an API
to create less privileged users is offered.

.. note::

  User and permissions management in LibEuFin is still under development.
  In particular, permissions for non-superusers are very limited at the moment.


Connecting Nexus with an EBICS account
--------------------------------------

The command line interface of the LibEuFin Nexus needs the following three
values to be defined in the environment: ``LIBEUFIN_NEXUS_URL``,
``LIBEUFIN_NEXUS_USERNAME``, and ``LIBEUFIN_NEXUS_PASSWORD``.  In this example,
``LIBEUFIN_NEXUS_USERNAME`` should be set to ``admin``, and
``LIBEUFIN_NEXUS_PASSWORD`` to the value hold in ``NEXUS_ADMIN_PW`` from the
previous step (the ``libeufin-nexus superuser`` command).  The
``LIBEUFIN_NEXUS_URL`` could be given as ``http://localhost:5017/``.

Next, we create a EBICS *bank connection* that Nexus can use to communicate with the bank.

.. code-block:: console

  [root@exchange-online]# libeufin-cli \
      connections \
        new-ebics-connection \
          --ebics-url $EBICS_BASE_URL \
          --host-id $EBICS_HOST_ID \
          --partner-id $EBICS_PARTNER_ID \
          --ebics-user-id $EBICS_USER_ID \
          $CONNECTION_NAME

If this step executes correctly, Nexus will have created all the cryptographic
material that is needed on the client side; in this EBICS example, it created
the signature and identification keys.  It is therefore advisable to *make
a backup copy* of such keys.

.. code-block:: console

  [root@exchange-online]# libeufin-cli \
      connections \
        export-backup \
          --passphrase $SECRET \
          --output-file $BACKUP_FILE \
          $CONNECTION_NAME

At this point, Nexus needs to both communicate its keys to the bank, and
download the bank's keys.  This synchronization happens through the INI, HIA, and
finally, HPB message types.

After the electronic synchronization, the subscriber must confirm their keys
by sending a physical mail to the bank.  The following command helps generating
such letter:

.. code-block:: console

  [root@exchange-online]# libeufin-cli connections get-key-letter $CONNECTION_NAME out.pdf

.. code-block:: console

  [root@exchange-online]# libeufin-cli \
      connections \
        connect \
          $CONNECTION_NAME

..
  FIXME: Maybe is not 100% clear that 'connecting' means exchanging keys
  with the bank?

Once the connection is synchronized, Nexus needs to import locally the data
corresponding to the bank accounts offered by the bank connection just made.
The command below downloads the list of the bank accounts offered by ``$CONNECTION_NAME``.

.. code-block:: console

  [root@exchange-online]# libeufin-cli \
      connections \
        download-bank-accounts \
          $CONNECTION_NAME

It is now possible to list the accounts offered by the connection.

.. code-block:: console

  [root@exchange-online]# libeufin-cli \
      connections \
        list-offered-bank-accounts \
          $CONNECTION_NAME

.. note::

   The ``nexusBankAccountId`` field should at this step be ``null``,
   as we have not yet imported the bank account and thus the account
   does not yet have a local name.

Nexus now needs an explicit import of the accounts it should manage.  This
step is needed to let the user pick a custom name for such accounts.

.. code-block:: console

  [root@exchange-online]# libeufin-cli \
      connections \
        import-bank-account \
          --offered-account-id testacct01 \
          --nexus-bank-account-id $LOCAL_ACCOUNT_NAME \
          $CONNECTION_NAME

Once a Nexus user imported a bank account (``$LOCAL_ACCOUNT_NAME``)
under a certain connection (``$CONNECTION_NAME``), it is possible
to accomplish the usual operations for any bank account: asking for the
list of transactions, and making a payment.

Testing: Requesting the transaction history
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The LibEuFin Nexus keeps a local copy of the bank account's transaction
history.  Before querying transactions locally, it is necessary
to request transactions for the bank account via the bank connection.

This command asks Nexus to download the latest transaction reports/statements
through the bank connection:

.. code-block:: console

  [root@exchange-online]# libeufin-cli accounts fetch-transactions $LOCAL_ACCOUNT_NAME

.. note::

   By default, the latest available transactions are fetched.  It is also
   possible to specify a custom date range (or even all available transactions)
   and the type of transactions to fetch (inter-day statements or intra-day
   reports).

..
  FIXME: Possibly the date range filter is still missing, see #6243.

Once Nexus has stored all the information in the database, the
client can ask to actually see the transactions:

.. code-block:: console

  [root@exchange-online]# libeufin-cli accounts transactions $LOCAL_ACCOUNT_NAME

Testing: Making payments
^^^^^^^^^^^^^^^^^^^^^^^^

Payments pass through two phases: preparation and submission.  The preparation
phase assigns the payment initiation a unique ID, which prevents accidental
double submissions of payments in case of network failures or other
disruptions.


The following command prepares a payment:

.. code-block:: console

  [root@exchange-online]# libeufin-cli accounts prepare-payment \
          --creditor-iban=$IBAN_TO_SEND_MONEY_TO \
          --creditor-bic=$BIC_TO_SEND_MONEY_TO \
          --creditor-name=$CREDITOR_NAME \
          --payment-amount=$AMOUNT \
          --payment-subject=$SUBJECT \
          $LOCAL_ACCOUNT_NAME

Note: the ``$AMOUNT`` value needs the format ``X.Y:CURRENCY``; for example
``EUR:10``, or ``EUR:1.01``.

The previous command should return a value (``$UUID``) that uniquely
identifies the prepared payment in the Nexus system.  That is needed
in the next step, to **send the payment instructions to the bank**:

.. code-block:: console

  [root@exchange-online]# libeufin-cli accounts submit-payments \
        --payment-uuid $UUID \
        $LOCAL_ACCOUNT_NAME

Automatic scheduling
^^^^^^^^^^^^^^^^^^^^

With an EBICS bank connection, the LibEuFin Nexus needs to regularly query for
new transactions and (re-)submit prepared payments.

It is possible to schedule these tasks via an external task scheduler such as
cron(8).  However, the nexus also has an internal task scheduling mechanism for
accounts.


The following three commands create a schedule for submitting payments hourly,
fetching transactions (intra-day reports) every 5 minutes, and (inter-day statements)
once at 11pm every day:

.. code-block:: console

   [root@exchange-online]# libeufin-cli accounts task-schedule $LOCAL_ACCOUNT_NAME \
       --task-type="submit" \
       --task-name='submit-payments-hourly' \
       --task-cronspec='0 0 *'

   [root@exchange-online]# libeufin-cli accounts task-schedule $LOCAL_ACCOUNT_NAME \
       --task-type="fetch" \
       --task-name='fetch-5min' \
       --task-cronspec='0 */5 *' \
       --task-param-level=report \
       --task-param-range-type=latest

   [root@exchange-online]# libeufin-cli accounts task-schedule $LOCAL_ACCOUNT_NAME \
       --task-type="fetch" \
       --task-name='fetch-daily' \
       --task-cronspec='0 0 23' \
       --task-param-level=statement \
       --task-param-range-type=latest

The cronspec has the following format, which is slightly non-standard due to
the ``SECONDS`` field

.. code-block:: none

  SECONDS MINUTES HOURS DAY-OF-MONTH[optional] MONTH[optional] DAY-OF-WEEK[optional]


Creating a Taler facade
^^^^^^^^^^^^^^^^^^^^^^^

Facades are additional abstraction layers that can serve
specific purposes.  For example, one application might need
a filtered version of the transaction history, or it might
want to refuse payments that do not conform to certain rules.

At this moment, only the *Taler facade type* is implemented
in the Nexus, and the command below instantiates one under a
existing bank account / connection pair.  You can freely
assign an identifier for the ``$FACADE_NAME`` below:

.. code-block:: console

  [root@exchange-online]# libeufin-cli facades new-taler-wire-gateway-facade \
      --currency EUR \
      --facade-name $FACADE_NAME \
      $CONNECTION_NAME \
      $LOCAL_ACCOUNT_NAME

At this point, the additional :doc:`taler-wire-gateway API <core/api-bank-wire>`
becomes offered by the Nexus.  The purpose is to let a Taler exchange rely on
Nexus to manage its bank account.

The base URL of the facade that can be used by the Taler exchange
as the Taler Wire Gateway base URL can be seen by listing the facades:

.. code-block:: console

  [root@exchange-online]# libeufin-cli facades list

Managing Permissions and Users
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This guide has so far assumed that a superuser is accessing the LibEuFin Nexus.
However, it is advisable that the Nexus is accessed with users that only have a
minimal set of permissions.

The Nexus currently only has support for giving non-superusers access to Taler
wire gateway facades.

To create a new user, use the ``users`` subcommand of the CLI:

.. code-block:: console

  [root@exchange-online]# libeufin-cli users list
  # [ ... shows available users ... ]

  [root@exchange-online]# libeufin-cli users create $USERNAME
  # [ ... will prompt for password ... ]

Permissions are managed with the ``permissions`` subcommand.
The following commands grant permissions to view the transaction history
and create payment initiations with a Taler wire gateway facade:


.. code-block:: console

  [root@exchange-online]# libeufin-cli permissions grant \
     user $USERNAME \
     facade $FACADENAME \
     facade.talerwiregateway.history

  [root@exchange-online]# libeufin-cli permissions grant \
     user $USERNAME \
     facade $FACADENAME \
     facade.talerwiregateway.transfer

..
  FIXME: The two commands above output an empty JSON object
  when successful.  Possibly, we should suppress that (just like
  the other commands do).

The list of all granted permissions can be reviewed:

.. code-block:: console

  [root@exchange-online]# libeufin-cli permissions list


.. _Bank-account:

Exchange Bank Account Configuration
-----------------------------------

An exchange must be configured with the right settings to access its bank
account via a Taler Wire Gateway.  An exchange can be configured to use
multiple bank accounts by using multiple Wire Gateways.  Typically only one
Wire Gateway is used.

To configure a bank account in Taler, we need to furnish two pieces of
information:

-  The ``payto://`` URI of the bank account, which uniquely idenfies the
   account. Examples for such URIs include
   ``payto://iban/CH9300762011623852957`` for a bank account with
   an IBAN or
   ``payto://x-taler-bank/localhost:8080/2`` for the 2nd bank account a
   the Taler bank demonstrator running at ``localhost`` on port 8080.
   The first part of the URI following ``payto://`` (“iban” or
   “x-taler-bank”) is called the wire method.

-  The ``taler-exchange-wirewatch`` and ``taler-exchange-transfer``
   tools needs to be provided resources for authentication
   to the respective banking service. The format in which the
   authentication information is currently a username and password
   for HTTP basic authentication.


A Taler Wire Gateway is configured in a configuration section that follows the
pattern ``exchange-account-$id``, where ``$id`` is an internal identifier for
the bank account accessed by the exchange.  The basic information for an
account should be put in ``/etc/taler/conf.d/exchange-business.conf``.  The
secret credentials to access the Taler Wire Gateway API should be put into a
corresponding ``exchange-accountcredentials-$id`` section in
``/etc/taler/secrets/exchange-accountcredentials.conf``.  The latter file
should already be only readable for the ``taler-exchange-wire`` user.  Other
exchange processes should not have access to this information.

You can configure multiple accounts for an exchange by creating sections
starting with “exchange-account-” for the section name. You can ENABLE for
each account whether it should be used, and for what (incoming or outgoing
wire transfers):

.. code-block:: ini
  :caption: /etc/taler/conf.d/exchange-business.conf

  [exchange-account-1]
  # Account identifier in the form of an RFC-8905 payto:// URI.
  # For SEPA, looks like payto://iban/$IBAN?receiver-name=$NAME
  # Make sure to URL-encode spaces in $NAME!
  #
  # With x-taler-bank (for Fakebank)
  # PAYTO_URI = "payto://x-taler-bank/bank.demo.taler.net/Exchange?receiver-name=exop"
  #
  # Example using IBAN (for use with LibEuFin)
  PAYTO_URI = "payto://iban/CH9300762011623852957?receiver=name=exop"

  # URL for talking to the bank wire the wire API.
  WIRE_GATEWAY_URL = https://bank.demo.taler.net/taler-wire-gateway/Exchange

  # Use for exchange-aggregator (outgoing transfers)
  ENABLE_DEBIT = YES
  # Use for exchange-wirewatch (and listed in /wire)
  ENABLE_CREDIT = YES

  @inline-secret@ exchange-accountcredentials-1 ../secrets/exchange-accountcredentials.secret.conf


.. code-block:: ini
  :caption: /etc/taler/secrets/exchange-accountcredentials.secret.conf

  [exchange-accountcredentials-1]

  # LibEuFin expects basic auth.
  WIRE_GATEWAY_AUTH_METHOD = basic

  # Username and password set in LibEuFin.
  USERNAME = ...
  PASSWORD = ...

  # Base URL of the wire gateway set up with LibEuFin.
  WIRE_GATEWAY_URL = ...


Such a Wire Gateway configuration can be tested with the following commands:

.. code-block:: shell-session

   [root@exchange-online]# taler-exchange-wire-gateway-client \
     --section exchange-accountcredentials-1 --debit-history
   [root@exchange-online]# taler-exchange-wire-gateway-client \
     --section exchange-accountcredentials-1 --credit-history



.. _LegalSetup:

Legal Setup
===========

This chapter describes how to setup certain legal aspects of a GNU Taler
exchange. Users that just want to set up an exchange as an experiment without
legal requirements can safely skip these steps.


Legal conditions for using the service
--------------------------------------

.. include:: frags/legal.rst


KYC Configuration
=================

To legally operate, Taler exchange operators may have to comply with KYC
regulation that requires financial institutions to identify parties involved
in transactions at certain points.

Taler permits an exchange to require KYC data under the following circumstances:

  * Customer withdraws money over a threshold
  * Wallet receives (via refunds) money resulting in a balance over a threshold
  * Wallet receives money via P2P payments over a threshold
  * Merchant receives money over a threshold
  * Reserve is "opened" for invoicing or rewards (**planned feature**)


Taler KYC Terminology
---------------------

* **Check**: A check establishes a particular attribute of a user, such as
  their name based on an ID document and lifeness, mailing address, phone
  number, taxpayer identity, etc.

* **Type of operation**: The operation type determines which Taler-specific
  operation has triggered the KYC requirement. We support four types of
  operation: withdraw (by customer), deposit (by merchant), P2P receive (by
  wallet) and (high) wallet balance.

* **Condition**: A condition specifies when KYC is required. Conditions
  include the *type of operation*, a threshold amount (e.g. above EUR:1000)
  and possibly a time period (e.g. over the last month).

* **Cost**: Metric for the business expense for a KYC check at a certain
  *provider*. Not in any currency, costs are simply relative and non-negative
  values. Costs are considered when multiple choices are allowed by the
  *configuration*.

* **Expiration**: KYC legitimizations may be outdated. Expiration rules
  determine when *checks* have to be performed again.

* **Legitimization rules**: The legitimization rules determine under which
  *conditions* which *checks* must be performend and the *expiration* time
  period for the *checks*.

* **Logic**: Logic refers to a specific bit of code (realized as an exchange
  plugin) that enables the interaction with a specific *provider*.  Logic
  typically requires configuration for access control (such as an
  authorization token) and possibly the endpoint of the specific *provider*
  implementing the respective API.

* **Provider**: A provider performs a specific set of *checks* at a certain
  *cost*. Interaction with a provider is performed by provider-specific
  *logic*.


KYC Configuration Options
-------------------------

The KYC configuration determines the *legitimization rules*, and specifies
which providers offer which *checks* at what *cost*.

The configuration specifies a set of providers, one per configuration section. The names of the configuration
sections must being with ``kyc-proider-`` followed by
an arbitrary ``$PROVIDER_ID``:

.. code-block:: ini
  :caption: /etc/taler/conf.d/exchange-kyc-providers.conf

  [kyc-provider-$PROVIDER_ID]
  # How expensive is it to use this provider?
  # Used to pick the cheapest provider possible.
  COST = 42
  # Which plugin is responsible for this provider?
  # Choices include "oauth2", "kycaid" and "persona".
  LOGIC = oauth2
  # Which type of user does this provider handle?
  # Either INDIVIDUAL or BUSINESS.
  USER_TYPE = INDIVIDUAL
  # Which checks does this provider provide?
  # List of strings, no specific semantics.
  PROVIDED_CHECKS = SMS GOVID PHOTO
  # Plus additional logic-specific options, e.g.:
  AUTHORIZATION_TOKEN = superdupersecret
  FORM_ID = business_legi_form
  # How long is the check considered valid?
  EXPIRATION = 3650d

The configuration also must specify a set of legitimization requirements, again one
per configuration section:

.. code-block:: ini
  :caption: /etc/taler/conf.d/exchange-kyc-rules.conf

  [kyc-legitimization-$RULE_NAME]
  # Operation that triggers this legitimization.
  # Must be one of WITHDRAW, DEPOSIT, P2P-RECEIVE
  # or WALLET-BALANCE.
  OPERATION_TYPE = WITHDRAW
  # Required checks to be performed.
  # List of strings, must individually match the
  # strings in one or more provider's PROVIDED_CHECKS.
  REQUIRED_CHECKS = SMS GOVID
  # Threshold amount above which the legitimization is
  # triggered.  The total must be exceeded in the given
  # timeframe.
  THRESHOLD = KUDOS:100
  # Timeframe over which the amount to be compared to
  # the  THRESHOLD is calculated.  Can be 'forever'.
  # Ignored for WALLET-BALANCE.
  TIMEFRAME = 30d


OAuth 2.0 specifics
-------------------

In terms of configuration, the OAuth 2.0 logic requires the respective client
credentials to be configured apriori to enable access to the legitimization
service.  The OAuth 2.0 configuration options are:

.. code-block:: ini
  :caption: /etc/taler/conf.d/exchange-oauth2.conf

  [kyc-provider-example-oauth2]
  LOGIC = oauth2
  # (generic options omitted)
  # How long is the KYC check valid?
  KYC_OAUTH2_VALIDITY = forever

  # URL to which we redirect the user for the login process
  KYC_OAUTH2_AUTHORIZE_URL = "http://kyc.example.com/authorize"
  # URL where we POST the user's authentication information
  KYC_OAUTH2_TOKEN_URL = "http://kyc.example.com/token"
  # URL of the user info access point.
  KYC_OAUTH2_INFO_URL = "http://kyc.example.com/info"

  # Where does the client get redirected upon completion?
  KYC_OAUTH2_POST_URL = "http://example.com/thank-you"

  # For authentication to the OAuth2.0 service
  KYC_OAUTH2_CLIENT_ID = testcase
  KYC_OAUTH2_CLIENT_SECRET = password

  # Mustach template that converts OAuth2.0 data about the user
  # into GNU Taler standardized attribute data.
  KYC_OAUTH2_CONVERTER_HELPER = taler-exchange-kyc-oauth2-challenger.sh

The converter helper is expected to be customized to the selected OAuth2.0
service: different services may return different details about the user or
business, hence there cannot be a universal converter for all purposes. The
default shell script uses the ``jq`` tool to convert the JSON returned by the
service into the KYC attributes (also in JSON) expected by the exchange.  The
script will need to be adjusted based on the attributes collected by the
specific backend.

The Challenger service for address validation supports OAuth2.0, but does not
have a static AUTHORIZE_URL. Instead, the AUTHORIZE_URL must be enabled by the client
using a special authenticated request to the Challenger's ``/setup`` endpoint.
The exchange supports this by appending ``#setup`` to the AUTHORIZE_URL (note
that fragments are illegal in OAuth2.0 URLs).  Be careful to quote the URL,
as ``#`` is otherwise interpreted as the beginning of a comment by the
configuration file syntax.

.. code-block:: ini
  :caption: /etc/taler/conf.d/exchange-challenger-oauth2.conf

  [kyc-provider-challenger-oauth2]
  LOGIC = oauth2
  KYC_OAUTH2_AUTHORIZE_URL = "http://challenger.example.com/authorize/#setup"
  KYC_OAUTH2_TOKEN_URL = "http://challenger.example.com/token"
  KYC_OAUTH2_INFO_URL = "http://challenger.example.com/info"

When using OAuth 2.0, the *CLIENT REDIRECT URI* must be set to the
``/kyc-proof/$PROVIDER_SECTION`` endpoint. For example, given the
configuration above and an exchange running on the host
``exchange.example.com``, the redirect URI would be
``https://exchange.example.com/kyc-proof/kyc-provider-challenger-oauth2/``.



Persona specifics
-----------------

We use the hosted flow. The Persona endpoints return a ``request-id``, which
we log for diagnosis.

Persona should be configured to use the ``/kyc-webhook/`` endpoint of the
exchange to notify the exchange about the completion of KYC processes.  The
webhook is authenticated using a shared secret, which should be in the
configuration.  To use the Persona webhook, you must set the webhook URL in
the Persona service to ``$EXCHANGE_BASE_URL/kyc-webhook/$SECTION_NAME/`` where
``$SECTION_NAME`` is the name of the configuration section.  You should also
extract the authentication token for the webhook and put it into the
configuration as shown above.


.. code-block:: ini
  :caption: /etc/taler/conf.d/exchange-persona.conf

  [kyclogic-persona]
  # Webhook authorization token. Global for all uses
  # of the persona provider!
  WEBHOOK_AUTH_TOKEN = wbhsec_698b5a19-c790-47f6-b396-deb572ec82f9

  [kyc-provider-example-persona]
  LOGIC = persona
  # (generic options omitted)

  # How long is the KYC check valid?
  KYC_PERSONA_VALIDITY = 365d

  # Which subdomain is used for our API?
  KYC_PERSONA_SUBDOMAIN = taler

  # Authentication token to use.
  KYC_PERSONA_AUTH_TOKEN = persona_sandbox_42XXXX

  # Form to use.
  KYC_PERSONA_TEMPLATE_ID = itempl_Uj6Xxxxx

  # Where do we redirect to after KYC finished successfully.
  KYC_PERSONA_POST_URL = "https://taler.net/kyc-done"

  # Salt to give to requests for idempotency.
  # Optional.
  # KYC_PERSONA_SALT = salt

  # Helper to convert JSON with KYC data returned by Persona into GNU Taler
  # internal format. Should probably always be set to some variant of
  # "taler-exchange-kyc-persona-converter.sh".
  KYC_PERSONA_CONVERTER_HELPER = "taler-exchange-kyc-persona-converter.sh"

The converter helper is expected to be customized to the
selected template: different templates may return different details
about the user or business, hence there cannot be a universal converter
for all purposes. The default shell script uses the ``jq`` tool to
convert the JSON returned by Persona into the KYC attributes (also
in JSON) expected by the exchange.  The script will need to be adjusted
based on the attributes collected by the specific template.


KYC AID specifics
-----------------

We use the hosted flow.

KYCAID must be configured to use the ``/kyc-webhook/$SECTION_NAME/`` endpoint
of the exchange to notify the exchange about the completion of KYC processes.

.. code-block:: ini
  :caption: /etc/taler/conf.d/exchange-kycaid.conf

  [kyc-provider-example-kycaid]
  LOGIC = kycaid
  # (generic options omitted)

  # How long is the KYC check valid?
  KYC_KYCAID_VALIDITY = 365d

  # Authentication token to use.
  KYC_KYCAID_AUTH_TOKEN = XXX

  # Form to use.
  KYC_KYCAID_FORM_ID = XXX

  # URL to go to after the process is complete.
  KYC_KYCAID_POST_URL = "https://taler.net/kyc-done"

  # Script to convert the KYCAID data into the Taler format.
  KYC_KYCAID_CONVERTER_HELPER = taler-exchange-kyc-kycaid-converter.sh


The converter helper is expected to be customized to the selected template:
different templates may return different details about the user or business,
hence there cannot be a universal converter for all purposes. The default
shell script uses the ``jq`` tool to convert the JSON returned by Persona into
the KYC attributes (also in JSON) expected by the exchange.  The script will
need to be adjusted based on the attributes collected by the specific
template.


.. _Deployment:

Deployment
==========

This chapter describes how to deploy the exchange once the basic installation
and configuration are completed.

.. _Serving:

Serving
-------

The exchange can serve HTTP over both TCP and UNIX domain socket.

The following options are to be configured in the section ``[exchange]``:

-  ``SERVE``: Must be set to ``tcp`` to serve HTTP over TCP, or ``unix`` to serve
   HTTP over a UNIX domain socket.

-  ``PORT``: Set to the TCP port to listen on if ``SERVE`` is ``tcp``.

-  ``UNIXPATH``: Set to the UNIX domain socket path to listen on if ``SERVE`` is
   ``unix``.

- ``UNIXPATH_MODE``: Number giving the mode with the access permission mask
   for the ``UNIXPATH`` (i.e. 660 = ``rw-rw---``). Make sure to set it in such
   a way that your reverse proxy has permissions to access the UNIX domain
   socket.  The default (660) assumes that the reverse proxy is a member of
   the group under which the exchange HTTP server is running.

.. _ReverseProxy:

Reverse Proxy Setup
-------------------

By default, the ``taler-exchange-httpd`` service listens for HTTP connections
on a UNIX domain socket.  To make the service publicly available, a reverse
proxy such as nginx should be used.  We strongly recommend to configure nginx
to use TLS.

The public URL that the exchange will be served under should
be put in ``/etc/taler/conf.d/exchange-business.conf`` configuration file.

.. code-block:: ini
  :caption: /etc/taler/conf.d/exchange-business.conf

   [exchange]
   BASE_URL = https://example.com/

   # ... rest of file ...

The ``taler-exchange`` package ships with a sample configuration that can be
enabled in nginx:

.. code-block:: shell-session

  [root@exchange-online]# vim /etc/nginx/sites-available/taler-exchange
  < ... customize configuration ... >
  [root@exchange-online]# ln -s /etc/nginx/sites-available/taler-exchange \
                                /etc/nginx/sites-enabled/taler-exchange
  [root@exchange-online]# systemctl reload nginx

Note that the reverse proxy must set a HTTP ``X-Forwarded-Host`` header to
refer to the hostname used by nginx and a HTTP ``X-Forwarded-Proto`` header to
inform the exchange whether the external protocol was ``http`` or ``https``.
Thus, depending on your setup, you will likely have to edit those parts of the
provided ``taler-exchange`` configuration file.

With this last step, we are finally ready to launch the
main exchange process.

.. _Launch:

Launching an exchange
---------------------

A running exchange requires starting the following processes:

-   ``taler-exchange-secmod-rsa`` (as special user, sharing group with the HTTPD)
-   ``taler-exchange-secmod-cs`` (as special user, sharing group with the HTTPD)
-   ``taler-exchange-secmod-eddsa`` (as special user, sharing group with the HTTPD)
-   ``taler-exchange-httpd`` (needs database access)
-   ``taler-exchange-aggregator`` (only needs database access)
-   ``taler-exchange-closer`` (only needs database access)
-   ``taler-exchange-wirewatch`` (needs bank account read credentials and database access)
-   ``taler-exchange-transfer`` (needs credentials to initiate outgoing wire transfers and database access)

The crypto helpers (``secmod``) must be started before the ``taler-exchange-httpd`` and
they should use the same configuration file.

For the most secure deployment, we recommend using separate users for each of
these processes to minimize information disclosures should any of them be
compromised.  The helpers do not need access to the PostgreSQL database (and
thus also should not have it).

The processes that require access to the bank account need to have a
configuration file with the respective credentials in it. We recommend using a
separate configuration at least for ``taler-exchange-transfer`` which is the
*only* process that needs to know the credentials to execute outgoing wire
transfers.

All of these processes should also be started via a hypervisor like
``systemd`` or ``gnunet-arm`` that automatically re-starts them should they
have terminated unexpectedly.  If the bank is down (say for maintenance), it is
*possible* to halt the ``taler-exchange-wirewatch`` and/or
``taler-exchange-transfer`` processes (to avoid them making requests to the
bank API that can only fail) without impacting other operations of the
exchange. Naturally, incoming wire transfers will only be observed once
``taler-exchange-wirewatch`` is resumed, and merchants may complain if the
disabled ``taler-exchange-transfer`` process causes payment deadlines to be
missed.

.. note::
   The ``taler-exchange-httpd`` does not ship with HTTPS enabled by default.
   In production, it should be run behind an HTTPS reverse proxy that performs
   TLS termination on the same system.  Thus, it would typically be configured
   to listen on a UNIX domain socket.  The ``/management`` and ``/auditors``
   APIs do technically not have to be exposed on the Internet (only to the
   administrators running ``taler-exchange-offline``) and should be blocked
   by the reverse proxy for requests originating from outside of the bank.
   (However, this is not a strong security assumption: in principle having
   these endpoints available should do no harm. However, it increases the
   attack surface.)


Given proper packaging, all of the above are realized via a simple systemd
target. This enables the various processes of an exchange service to be
started using a simple command:

.. code-block:: shell-session

  [root@exchange-online]# systemctl start taler-exchange.target

.. note::

   At this point, the exchange service is not yet fully operational.


To check whether the exchange is running correctly under the advertized
base URL, run:

.. code-block:: shell-session

  [root@exchange-online]# export BASE_URL=$(taler-config -s exchange -o base_url)
  [root@exchange-online]# wget ${BASE_URL}management/keys

The request might take some time to complete on slow machines, because
a lot of key material will be generated.


Offline Signing Setup, Key Maintenance and Tear-Down
====================================================

The exchange HTTP service must be running before you can complete the
following offline signing procedure. Note that when an exchange is running
without offline keys its not fully operational.  To make the exchange HTTP
service fully operational, the following steps involving the offline signing
machine must be completed:

  1. The public keys of various online keys used by the exchange service are exported
     via a management HTTP API.
  2. The offline signing system validates this request and signs it.
     Additionally, the offline signing system signs policy messages
     to configure the exchange's bank accounts and associated fees.
  3. The messages generated by the offline signing system are uploaded
     via the management API of the exchange HTTP service.

A typical minimal setup would look something like this:

.. code-block:: shell-session

  [anybody@exchange-online]# taler-exchange-offline \
    download > sig-request.json

  [root@exchange-offline]# taler-exchange-offline \
    sign < sig-request.json > sig-response.json
  [root@exchange-offline]# taler-exchange-offline \
    enable-account payto://iban/$IBAN?receiver-name=$NAME > acct-response.json
  [root@exchange-offline]# taler-exchange-offline \
    wire-fee now iban EUR:0 EUR:0 > fee-response.json
  [root@exchange-offline]# taler-exchange-offline \
    global-fee now EUR:0 EUR:0 EUR:0 4w 6y 4 > global-response.json

  [anybody@exchange-online]# taler-exchange-offline upload < sig-response.json
  [anybody@exchange-online]# taler-exchange-offline upload < acct-response.json
  [anybody@exchange-online]# taler-exchange-offline upload < fee-response.json
  [anybody@exchange-online]# taler-exchange-offline upload < global-response.json

The following sections will discuss these steps in more depth.

.. _Keys-generation:

Signing the online signing keys
-------------------------------

To sign the online signing keys, first the *future* key material should be downloaded using:

.. code-block:: console

   $ taler-exchange-offline download > future-keys.json

Afterwards, *future-keys.json* contains data about denomination and
online signing keys that the exchange operator needs to sign with
the offline tool.  The file should be copied to the offline system.
There, the operator should run:

.. code-block:: console

   $ taler-exchange-offline show < future-keys.json

and verify that the output contains the fee structure and key lifetimes
they expect to see. They should also note the public keys being shown
and communicate those to the *auditors* over a secure channel.  Once
they are convinced the file is acceptable, they should run:

.. code-block:: console

   $ taler-exchange-offline sign < future-keys.json > offline-sigs.json

The *offline-sigs.json* file must then be copied to an online system
that is able to again communicate with the exchange. On that system, run:

.. code-block:: console

   $ taler-exchange-offline upload < offline-sigs.json

to provision the signatures to the exchange.

The ``download sign upload`` sequence in the commands above has to be done
periodically, as it signs the various online signing keys of the exchange
which periodically expire.


Account signing
---------------

The ``enable-account`` step is important to must be used to sign the
``payto://`` URI in a way suitable to convince wallets that this is the
correct address to wire funds to.  Note that for each bank account, additional
options **must** be set in the configuration file to tell the exchange how to
access the bank account. The offline tool *only* configures the externally
visible portions of the setup.  The chapter on `Bank account <_Bank-account>`_ configuration has further details.

taler-exchange-offline accepts additional options to configure the use of the
account. For example, additional options can be used to add currency
conversion or to restrict interactions to bank accounts from certain
countries:

.. code-block:: console

   $ taler-exchange-offline \
       enable-account payto://iban/CH9300762011623852957
         conversion-url https://conversion.example.com/

For details on optional ``enable-account`` arguments,
see :doc:`manpages/taler-exchange-offline.1`.

.. _Wire-fee-structure:

Wire fee structure
------------------

.. index:: wire fee
.. index:: fee

For each wire method (“iban” or “x-taler-bank”) the
exchange must know about applicable wire fees. This is also done
using the ``taler-exchange-offline`` tool:

.. code-block:: console

   $ taler-exchange-offline wire-fee 2040 iban EUR:0.05 EUR:0.10

The above sets the wire fees for wire transfers involving ``iban`` accounts
(in Euros) in the year 2040 to 5 cents (wire fee) and 10 cents (closing fee).
The tool only supports setting fees that applies for the entire calendar year.

We recommend provisioning an exchange with wire fees at least for the next two
years.  Note that once the fees have been set for a year, they cannot be
changed (basically, by signing the fees the exchange makes a legally binding
offer to the customers).

.. index:: maintenance
.. note::
   Provisioning future wire fees, like provisioning future denomination
   and signing keys, are key regular maintenance procedures for every
   exchange operator.  We recommend setting automated reminders for
   this maintenance activity!


.. _Auditor-configuration:

Auditor configuration
---------------------

At this point, the exchange will be able to use those keys, but wallets and
merchants may not yet trust them!  Thus, the next step is for an auditor to
affirm that they are auditing this exchange.  Before an auditor can do this,
the exchange service must be informed about any auditor that is expected to
provision it with auditor signatures.

This is also done using the ``taler-exchange-offline`` tool on the offline
system.  First, the auditor must be configured and provide the exchange
operator with its public key (using ``taler-auditor-offline setup``) and the
URL of it's REST API.  The exchange operator also needs a human-readable name
that may be shown to users to identify the auditor.  For more information on
how to setup and operate an auditor, see
:doc:`manpages/taler-auditor-offline.1` and :doc:`taler-auditor-manual`.

Given this information, the exchange operator can enable the auditor:

.. code-block:: console

   $ taler-exchange-offline enable-auditor $PUB_KEY $REST_URL "$AUDITOR_NAME" > auditor.json

As before, the *auditor.json* file must then be copied from the offline system
to a system connected to the exchange and there ``uploaded`` to the exchange using ``taler-exchange-offline upload``.


.. _Revocations:

Revocations
-----------

When an exchange goes out of business or detects that the private key of
a denomination key pair has been compromised, it may revoke some or all
of its denomination keys. At this point, the hashes of the revoked keys
must be returned as part of the ``/keys`` response under “recoup”.
Wallets detect this, and then return unspent coins of the respective
denomination key using the ``/recoup`` API.

To revoke a denomination key, you need to know the hash of the denomination
public key, ``$HDP``.  The ``$HDP`` value is usually included in the security
report that is generated when a compromise is detected).  Given this
value, the key revocation can be approved on the offline system:

.. code-block:: console

   $ taler-exchange-offline revoke-denominatin $HDP > revocation.json

The resulting *revocation.json* must be copied to a system connected to the
exchange and uploaded to the exchange using the ``upload`` subcommand
of ``taler-exchange-offline``.

.. note::
   Denomination key revocations should only happen
   under highly unusual (“emergency”) conditions and not in normal
   operation.

AML Configuration
=================

The AML configuration steps are used to add or remove keys of exchange
operator staff that are responsible for anti-money laundering (AML)
compliance.  These AML officers are shown suspicious transactions and are
granted access to the KYC data of an exchange. They can then investigate the
transaction and decide on freezing or permitting the transfer. They may also
request additional KYC data from the consumer and can change the threshold
amount above which a further AML review is triggered.

AML Officer Setup
-----------------

To begin the AML setup, AML staff should launch the GNU Taler
exchange AML SPA Web interface. (FIXME-Sebastian: how?). The
SPA will generate a public-private key pair and store it in the
local storage of the browser.  The public key will be displayed
and must be securely transmitted to the offline system for
approval.  Using the offline system, one can then configure
which staff has access to the AML operations:

.. code-block:: shell-session

  [root@exchange-offline]# taler-exchange-offline \
     aml-enable $PUBLIC_KEY "Legal Name" rw > aml.json
  [root@exchange-online]# taler-exchange-offline \
     upload < aml.json

The above commands would add an AML officer with the given "Legal Name" with
read-write (rw) access to the AML officer database.  Using "ro" instead of
"rw" would grant read-only access to the data, leaving out the ability to
actually make AML decisions.  Once AML access has been granted, the AML
officer can use the SPA to review cases and (with "rw" access) take AML
decisions.

Access rights can be revoked at any time using:

.. code-block:: shell-session

  [root@exchange-offline]# taler-exchange-offline \
     aml-disable $PUBLIC_KEY "Legal Name" > aml-off.json
  [root@exchange-online]# taler-exchange-offline \
     upload < aml-off.json


AML Triggers
------------

AML decision processes are automatically triggered under certain configurable
conditions.  The primary condition that *must* be configured is the
``AML_THRESHOLD``:

.. code-block:: ini
  :caption: /etc/taler/conf.d/exchange-business.conf

  [exchange]
  # Accounts or wallets with monthly transaction volumes above this threshold
  # are considered suspicious and are automatically flagged for AML review
  # and put on hold until an AML officer has reached a decision.
  AML_THRESHOLD = "EUR:1000000"

Additionally, certain KYC attributes (such as the user being a
politically exposed person) may lead to an account being
flagged for AML review.  The specific logic is configured by
providing the exchange with an external helper program that
makes the decision given the KYC attributes:

.. code-block:: ini
  :caption: /etc/taler/conf.d/exchange-business.conf

  [exchange]
  # Specifies a program to run on KYC attribute data to decide
  # whether we should immediately flag an account for AML review.
  KYC_AML_TRIGGER = taler-exchange-kyc-aml-pep-trigger.sh

The given program will be given the KYC attributes in JSON format on standard
input, and must return 0 to continue without AML and non-zero to flag the
account for manual review.  To disable this triger, simply leave the option to
its default value of '[/usr/bin/]true'. To flag all new users for manual
review, simply set the program to '[/usr/bin/]false'.



Setup Linting
=============

The ``taler-wallet-cli`` package comes with an experimental tool that runs various
checks on the current GNU Taler exchange deployment:

.. code-block:: shell-session

  [root@exchange-online]# apt install taler-wallet-cli
  [root@exchange-online]# taler-wallet-cli deployment lint-exchange

You can optionally pass the ``--debug`` option to get more verbose output, and
``--continue`` to continue with further checks even though a previous one has
failed.


Testing and Troubleshooting
===========================

We recommend testing whether an exchange deployment is functional by using the
Taler wallet command line interface.  The tool can be used to withdraw and
deposit electronic cash via the exchange without having to deploy and operate a
separate merchant backend and storefront.

The following shell session illustrates how the wallet can be used to withdraw
electronic cash from the exchange and subsequently spend it.  For these steps,
a merchant backend is not required, as the wallet acts as a merchant.

.. code-block:: shell-session

  # This will now output a payto URI that money needs to be sent to in order to allow withdrawal
  # of taler coins.
  $ taler-wallet-cli advanced withdraw-manually --exchange $EXCHANGE_URL --amount EUR:10.50


Show the status of the manual withdrawal operation.

.. code-block:: shell-session

  $ taler-wallet-cli transactions

At this point, a bank transfer to the exchange's bank account
needs to be made with the correct subject / remittance information
as instructed by the wallet after the first step.  With the
above configuration, it should take about 5 minutes after the
wire transfer for the incoming transfer to be observed by the
Nexus.

Run the following command to check whether the exchange received
an incoming bank transfer:

.. code-block:: shell-session

  [root@exchange-online]# taler-exchange-wire-gateway-client \
     --section exchange-accountcredentials-1 --credit-history

Once the transfer has been made, try completing the withdrawal
using:

.. code-block:: shell-session

  $ taler-wallet-cli run-pending

Afterwards, check the status of transactions and show the
current wallet balance:

.. code-block:: shell-session

  $ taler-wallet-cli transactions
  $ taler-wallet-cli balance


Now, we can directly deposit coins via the exchange into a target
account.  (Usually, a payment is made via a merchant.  The wallet
provides this functionality for testing.)

.. code-block:: shell-session

  $ taler-wallet-cli deposit create EUR:5 \
    payto://iban/$IBAN?receiver-name=Name
  $ taler-wallet-cli run-pending


Check if this transaction was successful (from the perspective
of the wallet):

.. code-block:: shell-session

  $ taler-wallet-cli transactions

If the transaction failed, fix any open issue(s) with the exchange and
run the "run-pending" command.

The wallet can also track if the exchange wired the money to the merchant
account.  The "deposit group id" can be found in the output of the
transactions list.

.. code-block:: shell-session

  $ taler-wallet-cli deposit track $DEPOSIT_GROUP_ID

You can also check using the exchange-tools whether the exchange sent
the an outgoing transfer:

.. code-block:: shell-session

  [root@exchange-online]# taler-exchange-wire-gateway-client \
    --section exchange-accountcredentials-1 --debit-history

After enough time has passed, the money should arrive at the specified IBAN.

For more information on the taler-wallet-cli tool, see
:doc:`taler-wallet`.


Private key storage
-------------------

Keeping the private keys the helpers create secret is paramount. If the
private keys are lost, it is easy to provision fresh keys (with the help of
the auditor).  Thus, we recommend that the private keys of the crypto helpers
are *not* backed up: in the rare event of a disk failure, they can be
regenerated.  However, we do recommend using RAID (1+1 or 1+1+1) for all
disks of the system.


.. _Internal-audit:

Internal audits
---------------

While an exchange should use an external auditor to attest to regulators that
it is operating correctly, an exchange operator can also use the auditor's
logic to perform internal checks.  For this, an exchange operator can generally
follow the auditor guide.  However, instead of using ``taler-auditor-sync``,
an internal audit can and likely should be performed either directly against
the production exchange database or against a synchronous copy created using
standard database replication techniques. After all, the exchange operator
runs this for diagnostics and can generally trust its own database to maintain
the database invariants.

Running the auditor against a the original the production database (without
using ``taler-auditor-sync``) enables the auditing logic to perform a few
additional checks that can detect inconsistencies.  These checks are enabled
by passing the **-i** option to the ``taler-auditor`` command.  As always,
the resulting report should be read carefully to see if there are any problems
with the setup.

Reports are generally created incrementally, with ``taler-auditor`` reporting
only incidents and balance changes that were not covered in previous reports.
While it is possible to reset the auditor database and to restart the audit
from the very beginning, this is generally not recommended as this may be too
expensive.


.. _Database-Scheme:

Database Scheme
---------------

The exchange database must be initialized using ``taler-exchange-dbinit``.
This tool creates the tables required by the Taler exchange to operate.
The tool also allows you to reset the Taler exchange database, which is
useful for test cases but should never be used in production. Finally,
``taler-exchange-dbinit`` has a function to garbage collect a database,
allowing administrators to purge records that are no longer required.

The database scheme used by the exchange looks as follows:

.. image:: images/exchange-db.png


.. _Database-upgrades:

Database upgrades
-----------------

Before installing a new exchange version, you should probably make a backup of
the existing database and study the release notes on migration.  In general,
the way to migrate is to stop all existing Taler exchange processes and run:

.. code-block:: console

   $ taler-exchange-dbinit

This will migrate the existing schema to the new schema. You also may need
to grant Taler exchange processes the rights to the new tables (see last
step of database setup).

.. note::

   The **taler-exchange-dbconfig** tool can be used to automate the database
   migration. In general, simply invoking it again should trigger the
   migration including **taler-exchange-dbinit** and setting the permissions.


If you do not want to keep any data from the previous installation, the
exchange database can be fully re-initialized using:

.. code-block:: console

   $ taler-exchange-dbinit --reset

However, running this command will result in all data in the database
being lost, which may result in significant financial liabilities as the
exchange can then not detect double-spending. Hence this operation must
not be performed in a production system. You still also need to then
grant the permissions to the other exchange processes again.



.. _ExchangeBenchmarking:

Benchmarking
============

This chapter describes how to run various benchmarks against a Taler exchange.
These benchmark can be used to measure the performance of the exchange by
running a (possibly large) number of simulated clients against one Taler
deployment with a bank, exchange and (optionally) auditor.

Real benchmarks that are intended to demonstrate the scalability of GNU Taler
should not use the tools presented in this section: they may be suitable for
microbenchmarking and tuning, but the setup is inherently not optimzied for
performance or realism, both for the load generation and the server side.
Thus, we do not recommend using these performance numbers to assess the
scalability of GNU Taler.  That said, the tools can be useful to help identify
performance issues.

The ``taler-unified-setup.sh`` script can be used to launch all required
services and clients. However, the resulting deployment is simplistic
(everything on the local machine, one single-threaded process per service
type) and not optimized for performance at all. However, this can still be
useful to assess the performance impact of changes
to the code or configuration.

The various configuration files used in the code snippets in this section can
be found in the ``src/benchmark/`` directory of the exchange. These are
generally intended as starting points.  Note that the configuration files
ending in ``.edited`` are created by ``taler-unified-setup.sh`` and contain
some options that are determined at runtime by the setup logic provided by
``taler-unified-setup.sh``.


.. _Benchmark-choose-bank:

Choosing a bank
---------------

For the bank, both a fakebank (``-f``) and libeufin-based (``-ns``)
bank deployment are currently supported by all benchmark tools and
configuration templates.

Fakebank is an ultra-fast in-memory implementation of the Taler bank API. It
is suitable when the goal is to benchmark the core GNU Taler payment system
and to ignore the real-time gross settlement (RTGS) system typically provided
by an existing bank.  When using the fakebank, ``taler-unified-setup.sh`` must
be started with the ``-f`` option and be told to use the right exchange bank
account from the configuration files via ``-u exchange-account-1``.

.. code-block:: console

    $ dropdb talercheck; createdb talercheck
    $ taler-unified-setup.sh -emwt -c $CONF -f -u exchange-account-1


libeufin is GNU Taler's adapter to the core banking system using the EBICS
banking protocol standard.  It uses a Postgres database to persist data and is
thus much slower than fakebank.  If your GNU Taler deployment uses libeufin in
production, it likely makes sense to benchmark with libeufin.  When using the
fakebank, ``taler-unified-setup.sh`` must be started with the ``-ns`` options
(starting libeufin-nexus and libeufin-sandbox) and be told to use the right
exchange bank account from the configuration files via ``-u
exchange-account-2``.  Note that ``taler-unified-setup.sh`` currently cannot
reset a libeufin database, and also will not run if the database is already
initialized. Thus, you must re-create the database every time before
running the command:

.. code-block:: console

    $ dropdb talercheck; createdb talercheck
    $ taler-unified-setup.sh -emwt -c $CONF -ns -u exchange-account-2


taler-bank-benchmark
--------------------

This is the simplest benchmarking tool, simulating only the bank
interaction.

.. code-block:: console

    $ CONF="benchmark-cs.conf"
    $ # or with libeufin
    $ dropdb talercheck; createdb talercheck
    $ taler-unified-setup.sh -emwt -c "$CONF" -f -u exchange-account-1
    $ # Once <<READY>>, in another shell (remember to set $CONF):
    $ time taler-bank-benchmark -c "$CONF" -r 40 -p 4 -P4 -u exchange-account-1 -f
    $ # or with libeufin
    $ dropdb talercheck; createdb talercheck
    $ taler-unified-setup.sh -emwt -c "$CONF" -ns -u exchange-account-2
    $ # Once <<READY>>, in another shell (remember to set $CONF):
    $ time taler-bank-benchmark -c "$CONF" -r 40 -p 1 -P1 -u exchange-account-2

For each *parallel* (``-p``) client, a number of *reserves* (``-r``) is first
established by **transferring** money from a "user" account (42) to the
Exchange's account with the respective reserve public key as wire subject.
Processing is then handled by *parallel* (``-P``) service workers.


taler-exchange-benchmark
------------------------

This is the benchmarking tool simulates a number of clients withdrawing,
depositing and refreshing coins.  Operations that are not covered by the
``taler-exchange-benchmark`` tool today include closing reserves, refunds,
recoups and P2P payments.

.. code-block:: console

    $ CONF="benchmark-cs.conf" # -rsa also makes sense
    $ # With fakebank
    $ dropdb talercheck; createdb talercheck
    $ taler-unified-setup.sh -aemwt -c "$CONF" -f -u exchange-account-1
    $ # Once <<READY>>, in another shell (remember to set $CONF):
    $ taler-exchange-benchmark -c "$CONF".edited -u exchange-account-1 -n 1 -p1 -r 5 -f
    $ #
    $ # With libeufin
    $ dropdb talercheck; createdb talercheck
    $ taler-unified-setup.sh -aemwt -c "$CONF" -ns -u exchange-account-2
    $ # Once <<READY>>, in another shell (remember to set $CONF):
    $ taler-exchange-benchmark -c "$CONF".edited -u exchange-account-2 -L WARNING -n 1 -p1 -r 5

For each *parallel* (``-p``) client, a number of *reserves* (``-r``) is first
established by **transferring** money from a "user" account (42) to the
Exchange's account with the respective reserve public key as wire subject.
Next, the client will **withdraw** a *number of coins* (``-n``) from the
reserve and **deposit** them. Additionally, a *fraction* (``-R``) of the dirty
coins will then be subject to **refreshing**.  For some deposits, the auditor
will receive **deposit confirmations**.

The output of ``taler-exchange-benchmark`` will include for each parallel
client the total time spent in each of the major operations, possible
repetitions (i.e. if the operation failed the first time), total execution
time (operating system and user space) and other details.


taler-aggregator-benchmark
--------------------------

This is another simple benchmark tool that merely prepares an exchange
database to run a stand-alone benchmark of the ``taler-exchange-aggregator``
tool.  After preparing a database and running the tool, you can then
run one or more ``taler-exchange-aggregator`` processes and measure how
quickly they perform the aggregation work.

.. code-block:: console

    $ CONF=benchmark-rsa.conf
    $ taler-exchange-dbinit -c "$CONF" --reset
    $ ./taler-aggregator-benchmark -c "$CONF" -m 500 -r 10 -d 100
    $ time taler-exchange-aggregator -c "$CONF" --test

This above commands will first create 100 deposits with 10 refunds into each
of 500 merchant accounts using randomized time stamps.  Afterwards, it will
time a single aggregator process in ``--test`` mode (asking it to terminate
as soon as there is no more pending work).


FIXMEs
======

* We should have some summary with the inventory of services that should be
  running.  Systemd by default doesn't show this nicely.  Maybe suggest running
  "systemd list-dependencies taler-exchange.target"?
* What happens when the TWG doesn't like one particular outgoing transaction?
  How to recover from that as a sysadmin when it happens in practice?
