..
  This file is part of GNU TALER.
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Christian Grothoff
  @author Martin Schanzenbach


======================
The TalDir RESTful API
======================

This is a proposed API for the TalDir service which allows Taler wallets to
securely associate an inbox service (URL and public key) with the address of a
messaging service used by the wallet's user.  Wallets can also lookup the
inbox of other users. This will enable wallets to make wallet-to-wallet
payments to distant wallets where the target user is only identified by their
address in a messaging service. Examples for messaging services include E-mail
and SMS.

The API specified here follows the :ref:`general conventions <http-common>`
for all details not specified in the individual requests.
The `glossary <https://docs.taler.net/glossary.html#glossary>`_
defines all specific terms used in this section.


.. include:: tos.rst


-------------------------
Configuration information
-------------------------

.. http:get:: /config

  Return the protocol version and currency supported by this service.

  **Response:**

  :http:statuscode:`200 OK`:
    The body is a `VersionResponse`.

  .. ts:def:: VersionResponse

    interface VersionResponse {
      // libtool-style representation of the Merchant protocol version, see
      // https://www.gnu.org/software/libtool/manual/html_node/Versioning.html#Versioning
      // The format is "current:revision:age".
      version: string;

      // Name of the protocol.
      name: "taler-directory";

      // Supported registration methods
      methods: TaldirMethod[];

      // fee for one month of registration
      monthly_fee: Amount;

    }

  .. ts:def:: TaldirMethod

    interface TaldirMethod {
      // Name of the method, e.g. "email" or "sms".
      name: string;

      // per challenge fee
      challenge_fee: Amount;

    }

--------------------
Address registration
--------------------

.. http:post:: /register/$METHOD

  Endpoint to register, extend or modify the registration for an address in
  the directory.
  Here, $METHOD is the type of address to register, e.g. "email", or "phone".
  Supported methods are listed in the VersionResponse.
  Note that duration should be given as a multiple of a month in microseconds.
  If the duration is not a multiple of a month it will be rounded to the
  nearest multiple. Halfway values will be rounded away from zero.
  The cost calculation and resulting registration validity will be adjusted
  automatically.
  In order to only modify the data, the duration  may be set to 0.
  When the call is made with unmodified data and a duration of 0, the
  endpoint will return how long this registration is currently paid for.

  **Request**

  .. ts:def:: IdentityMessage

    interface IdentityMessage {
      // Address, in $METHOD-specific format
      address: string;

      // Public key of the user to register. As string in Crockfor base32 encoding.
      public_key: EddsaPublicKey;

      // (HTTPS) endpoint URL for the inbox service.
      inbox_url: string;

      // For how long should the registration last/be extended.
      duration: RelativeTime;

    }

  **Response**

  :http:statuscode:`200 Ok`
     Registration already exists for this address for the specified duration.
     Returns for how long this registration is paid for.
     The response format is given by `AlreadyPaidResponse`_.
  :http:statuscode:`202 Accepted`
     Registration was initiated, the client should check for receiving
     a challenge at the address where registration was attempted.
  :http:statuscode:`402 Payment Required`
     Client needs to make a Taler payment before proceeding. See
     standard Taler payment procedure.
  :http:statuscode:`404 Not found`
     The TalDir service does not support the specified method.
     This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`429 Too Many Requests`:
    The client exceeded the number of allowed attempts for initiating
    a challenge for this address in the given timeframe.
    The response format is given by `RateLimitedResponse`_.

  .. _RateLimitedResponse:
  .. ts:def:: RateLimitedResponse

    interface RateLimitedResponse {

      // Taler error code, TALER_EC_TALDIR_REGISTER_RATE_LIMITED.
      code: number;

      // At what frequency are new registrations allowed.
      request_frequency: RelativeTime;

      // The human readable error message.
      hint: string;

    }

  .. _AlreadyPaidResponse:
  .. ts:def:: AlreadyPaidResponse

    interface AlreadyPaidResponse {

      // The remaining duration for which this registration is still paid for
      valid_for: RelativeTime;

    }


.. http:get:: /register/$H_ADDRESS/$PINTAN

  Endpoint that generates an HTML Web site with a QR code and
  ``taler://taldir/$H_ADDRESS/$PINTAN-wallet`` link for completing the
  registration. Useful to open the registration challenge in a browser (say if
  it was received on a different device than where the wallet is running).
  Does NOT complete the registration, as some providers automatically click on
  all links in messages. Yes, we do not like them doing so either, but ``GET``
  is a "safe" method according to the HTTP standard, so technically this is
  allowed.

  Opening the link will lead the **wallet** to do the POST call below.  If the
  Taler wallet can somehow intercept the URL (say for SMS, if it has the right
  permissions) it can skip this request and directly do the POST, as all of
  the required new information is already encoded in the URL.

  Note that the wallet must be involved before the POST is made, as the
  wallet's public key from the registration must be hashed with the ``$PINTAN``
  to protect the user against phishing. Otherwise, someone else might attempt
  a concurrent registration of a different public key, and the user might
  accidentally authorize the registration of the public key of a different
  wallet.

.. http:post:: /$H_ADDRESS

  This request is the last step of a registration, proving to the TalDir that
  the user of the wallet is indeed able to receive messages at the specified
  address.  ``$H_ADDRESS`` is the SHA-512 hash of the address to be registered in
  Crockford base32 encoding.

  **Request**

  .. ts:def:: IdentityConfirmation

    interface IdentityConfirmation {
      // The solution is the SHA-512 hash of the challenge ($PINTAN) value
      // chosen by TalDir (encoded as string just as given in the URL, but
      // excluding the 0-termination) concatenated with the binary 32-byte
      // value representing the wallet's EdDSA public key.
      // The hash is provided as string in Crockford base32 encoding.
      solution: HashCode;

    }

  **Response**

  :http:statuscode:`204 No Content`:
     Registration complete.
  :http:statuscode:`403 Forbidden`:
     The ``solution`` is invalid. Retrying immediately is allowed.
  :http:statuscode:`404 Not found`:
     The address is unknown (original registration attempt may have expired).
  :http:statuscode:`429 Too Many Requests`:
    The client exceeded the number of allowed attempts for solving
    a challenge for this address in the given timeframe.

--------------
Address lookup
--------------

.. http:get:: /$H_ADDRESS

  Lookup the public key (and mailbox service base URL) associated with
  an address in the TalDir. Here, ``$H_ADDRESS`` is the SHA-512 hash of
  a (presumably) registered address in Crockford base32 encoding.

  **Response**

  Standard HTTP cache control headers are used to specify how long the
  registration is still expected to be valid.

  :http:statuscode:`200 Ok`:
     Registration information returned, of type `MailboxDetailResponse`
  :http:statuscode:`404 Not found`:
     The address is unknown (original registration may have expired).

  .. _MailboxDetailResponse:
  .. ts:def:: MailboxDetailResponse

    interface MailboxDetailResponse {

      // Registered public key of the user. As string in Crockford base32 encoding.
      public_key: EddsaPublicKey;

      // (HTTPS) endpoint URL for the inbox service.
      inbox_url: string;

    }
