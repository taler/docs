..
  This file is part of GNU TALER.
  Copyright (C) 2021-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

.. _taler-bank-merchant-http-api:

===========================
Taler Bank Revenue HTTP API
===========================

This section describes an API offered by libeufin-nexus and libeufin-bank. The API is
used by the merchant (or other parties) to query for incoming transactions to their account.


--------------
Authentication
--------------

The bank library authenticates requests to the bank merchant API using
`HTTP basic auth <https://tools.ietf.org/html/rfc7617>`_.

--------------------------------
Querying the transaction history
--------------------------------

.. http:get:: /history

  Return a list of transactions made from an exchange to the merchant.

  Incoming transactions must contain a valid wire transfer identifier and
  exchange base URL.  If a bank transaction does not conform to the right
  syntax, the wire gateway must not report it to the merchant via this
  endpoint.

  The bank account of the merchant is determined via the base URL and/or the
  user name in the ``Authorization`` header.  In fact, the transaction history
  might come from a "virtual" account, where multiple real bank accounts are
  merged into one history.

  Transactions are identified by an opaque numeric identifier, referred to here
  as *row ID*.  The semantics of the row ID (including its sorting order) are
  determined by the bank server and completely opaque to the client.

  The list of returned transactions is determined by a row ID *starting point*
  and a signed non-zero integer *delta*:

  * If *delta* is positive, return a list of up to *delta* transactions (all matching
    the filter criteria) strictly **after** the starting point.  The transactions are sorted
    in **ascending** order of the row ID.
  * If *delta* is negative, return a list of up to *-delta* transactions (all matching
    the filter criteria) strictly **before** the starting point.  The transactions are sorted
    in **descending** order of the row ID.

  If *starting point* is not explicitly given, it defaults to:

  * A value that is **smaller** than all other row IDs if *delta* is **positive**.
  * A value that is **larger** than all other row IDs if *delta* is **negative**.

  **Request:**

  :query start: *Optional.*
    Row identifier to explicitly set the *starting point* of the query.
  :query delta:
    The *delta* value that determines the range of the query.
  :query long_poll_ms: *Optional.*  If this parameter is specified and the
    result of the query would be empty, the bank will wait up to ``long_poll_ms``
    milliseconds for new transactions that match the query to arrive and only
    then send the HTTP response.  A client must never rely on this behavior, as
    the bank may return a response immediately or after waiting only a fraction
    of ``long_poll_ms``.

  **Response:**

  :http:statuscode:`200 OK`:
    JSON object of type `MerchantIncomingHistory`.
  :http:statuscode:`400 Bad request`:
    Request malformed. The bank replies with an `ErrorDetail` object.
  :http:statuscode:`401 Unauthorized`:
    Authentication failed, likely the credentials are wrong.
  :http:statuscode:`404 Not found`:
    The endpoint is wrong or the user name is unknown. The bank replies with an `ErrorDetail` object.

  **Details:**

  .. ts:def:: MerchantIncomingHistory

    interface MerchantIncomingHistory {
      // Array of incoming transactions.
      incoming_transactions : MerchantIncomingBankTransaction[];

      // Payto URI to identify the receiver of funds.
      // This must be one of the merchant's bank accounts.
      // Credit account is shared by all incoming transactions
      // as per the nature of the request.
      credit_account: string;

    }

  .. ts:def:: MerchantIncomingBankTransaction

    interface MerchantIncomingBankTransaction {
      // Opaque identifier of the returned record.
      row_id: SafeUint64;

      // Date of the transaction.
      date: Timestamp;

      // Amount transferred.
      amount: Amount;

      // Payto URI to identify the sender of funds.
      debit_account: string;

      // Base URL of the exchange where the transfer originated form.
      exchange_url: string;

      // The wire transfer identifier.
      wtid: WireTransferIdentifierRawP;
    }
