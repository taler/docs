..
  This file is part of GNU TALER.

  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Marcello Stanisci
  @author Christian Grothoff

==========================
Taler Bank Integration API
==========================

This chapter describe the APIs that banks need to offer towards Taler wallets
to tightly integrate with GNU Taler.

.. contents:: Table of Contents

.. http:get:: /config

  Get configuration information about the bank.

  **Response:**

  :http:statuscode:`200 OK`:
    The exchange responds with a `IntegrationConfig` object. This request should
    virtually always be successful.

  **Details:**

  .. ts:def:: IntegrationConfig

    interface IntegrationConfig {
      // libtool-style representation of the Bank protocol version, see
      // https://www.gnu.org/software/libtool/manual/html_node/Versioning.html#Versioning
      // The format is "current:revision:age".
      version: string;

      // Currency used by this bank.
      currency: string;

      // How the bank SPA should render this currency.
      currency_specification: CurrencySpecification;

      // Name of the API.
      name: "taler-bank-integration";
    }


-----------
Withdrawing
-----------

Withdrawals with a Taler-integrated bank are based on withdrawal operations.
Some user interaction (on the bank's website or a Taler-enabled ATM) creates a
withdrawal operation record in the bank's database.  The wallet can use a unique identifier
for the withdrawal operation (the ``WITHDRAWAL_ID``) to interact with the withdrawal operation.

.. http:get:: /withdrawal-operation/$WITHDRAWAL_ID

  Query information about a withdrawal operation, identified by the ``WITHDRAWAL_ID``.

  **Request:**

  :query long_poll_ms:
    *Optional.*  If specified, the bank will wait up to ``long_poll_ms``
    milliseconds for operationt state to be different from ``old_state`` before sending the HTTP
    response.  A client must never rely on this behavior, as the bank may
    return a response immediately.
  :query old_state:
    *Optional.* Default to "pending".

  **Response:**

  :http:statuscode:`200 OK`:
    The withdrawal operation is known to the bank, and details are given
    in the `BankWithdrawalOperationStatus` response body.
  :http:statuscode:`404 Not found`:
    The operation was not found

  **Details:**

  .. ts:def:: BankWithdrawalOperationStatus

    export class BankWithdrawalOperationStatus {
      // Current status of the operation
      // pending: the operation is pending parameters selection (exchange and reserve public key)
      // selected: the operations has been selected and is pending confirmation
      // aborted: the operation has been aborted
      // confirmed: the transfer has been confirmed and registered by the bank
      status: "pending" | "selected" | "aborted" | "confirmed";

      // Amount that will be withdrawn with this operation
      // (raw amount without fee considerations).
      amount: Amount;

      // Bank account of the customer that is withdrawing, as a
      // ``payto`` URI.
      sender_wire?: string;

      // Suggestion for an exchange given by the bank.
      suggested_exchange?: string;

      // URL that the user needs to navigate to in order to
      // complete some final confirmation (e.g. 2FA).
      // It may contain withdrawal operation id
      confirm_transfer_url?: string;

      // Wire transfer types supported by the bank.
      wire_types: string[];

      // Reserve public key selected by the exchange,
      // only non-null if ``status`` is ``selected`` or ``confirmed``.
      selected_reserve_pub?: string;

      // Exchange account selected by the wallet
      // only non-null if ``status`` is ``selected`` or ``confirmed``.
      selected_exchange_account?: string;

      // Deprecated field use ``status`` instead
      // Indicates whether the withdrawal was aborted.
      aborted: boolean;

      // Deprecated field use ``status`` instead
      // Has the wallet selected parameters for the withdrawal operation
      // (exchange and reserve public key) and successfully sent it
      // to the bank?
      selection_done: boolean;

      // Deprecated field use ``status`` instead
      // The transfer has been confirmed and registered by the bank.
      // Does not guarantee that the funds have arrived at the exchange already.
      transfer_done: boolean;
    }

.. http:post:: /withdrawal-operation/$WITHDRAWAL_ID

  **Request:**

  .. ts:def:: BankWithdrawalOperationPostRequest

    interface BankWithdrawalOperationPostRequest {
      // Reserve public key.
      reserve_pub: string;

      // Payto address of the exchange selected for the withdrawal.
      selected_exchange: string;
    }

  **Response:**

  :http:statuscode:`200 OK`:
    The bank has accepted the withdrawal operation parameters chosen by the wallet.
    The response is a `BankWithdrawalOperationPostResponse`.
  :http:statuscode:`404 Not found`:
    The bank does not know about a withdrawal operation with the specified ``WITHDRAWAL_ID``.
  :http:statuscode:`409 Conflict`:
    * ``TALER_EC_BANK_WITHDRAWAL_OPERATION_RESERVE_SELECTION_CONFLICT`` : 
      The wallet selected a different exchange or reserve public key under the same withdrawal ID.
    * ``TALER_EC_BANK_DUPLICATE_RESERVE_PUB_SUBJECT`` : the reserve public key is already used.
    * ``TALER_EC_BANK_ACCOUNT_NOT_FOUND`` : the selected exchange account was not found.
    * ``TALER_EC_BANK_ACCOUNT_IS_NOT_EXCHANGE`` : the selected account is not an exchange.

  **Details:**

  .. ts:def:: BankWithdrawalOperationPostResponse

    interface BankWithdrawalOperationPostResponse {
      // Current status of the operation
      // pending: the operation is pending parameters selection (exchange and reserve public key)
      // selected: the operations has been selected and is pending confirmation
      // aborted: the operation has been aborted
      // confirmed: the transfer has been confirmed and registered by the bank
      status: "selected" | "aborted" | "confirmed";

      // URL that the user needs to navigate to in order to
      // complete some final confirmation (e.g. 2FA).
      //
      // Only applicable when ``status`` is ``selected``.
      // It may contain withdrawal operation id
      confirm_transfer_url?: string;

      // Deprecated field use ``status`` instead
      // The transfer has been confirmed and registered by the bank.
      // Does not guarantee that the funds have arrived at the exchange already.
      transfer_done: boolean;
    }
