..
  This file is part of GNU TALER.

  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Marcello Stanisci
  @author Christian Grothoff
  @author Florian Dold

=========================
Taler Conversion Info API
=========================

This chapter describes the conversion info API. The conversion info API
is used by wallets for withdrawals that involve a currency conversion.


.. contents:: Table of Contents

.. http:get:: /config

  Get configuration information about the bank.

  **Response:**

  :http:statuscode:`200 OK`:
    Response is a `ConversionConfig`.
  :http:statuscode:`501 Not implemented`:
    This server does not support conversion, client should check config response.

  **Details:**

  .. ts:def:: ConversionConfig

    interface ConversionConfig {
      // libtool-style representation of the Bank protocol version, see
      // https://www.gnu.org/software/libtool/manual/html_node/Versioning.html#Versioning
      // The format is "current:revision:age".
      version: string;
      
      // Name of the API.
      name: "taler-conversion-info";

      // Currency used by this bank.
      regional_currency: string;

      // How the bank SPA should render this currency.
      regional_currency_specification: CurrencySpecification;

      // External currency used during conversion.
      fiat_currency: string;

      // How the bank SPA should render this currency.
      fiat_currency_specification: CurrencySpecification;

      // Additional information on conversion rates.
      // Those informations should never be used to perform conversions, 
      // use /cashin-rate or /cashout-rate instead.
      // Conversion rates can change at any time. Clients must deal with 
      // any resulting errors and call /cashin-rate or /cashout-rate again
      // to use the new rates.
      conversion_rate: ConversionRate;
    }


.. http:get:: /cashin-rate

  This public endpoint allows clients to calculate
  the exchange rate between the regional currency
  and the fiat currency of the banking system.

  This endpoint shows how the bank would apply the cash-in
  ratio and fee to one input amount.  Typically, wallets would
  request this endpoint before creating withdrawals that involve
  a currency conversion.

  **Request:**

  :query amount_debit: this is the amount that the user will get
    deducted from their fiat bank account.

  or
 
  :query amount_credit: this is the amount that the user will receive
    in their regional bank account.

  **Response:**

  :http:statuscode:`200 OK`:
    Response is a `CashinConversionResponse`.
  :http:statuscode:`400 Bad request`:
    * ``TALER_EC_GENERIC_PARAMETER_MISSING`` : none of the parameters have been provided. 
    * ``TALER_EC_GENERIC_PARAMETER_MALFORMED`` : both of the parameters have been provided or one of them is not a valid Taler amount.
    * ``TALER_EC_GENERIC_CURRENCY_MISMATCH`` : the parameter is in the wrong currency.
  :http:statuscode:`409 Conflict`:
    The amount is too small to be converted, either because it produces produce an amount less than zero, or because the server requires a higher minimum amount than that supplied.
  :http:statuscode:`501 Not implemented`:
    This server does not support conversion, client should check config response.

  **Details:**

  .. ts:def:: CashinConversionResponse

    interface CashinConversionResponse {
      // Amount that the user will get deducted from their fiat
      // bank account, according to the 'amount_credit' value.
      amount_debit: Amount;
      // Amount that the user will receive in their regional
      // bank account, according to 'amount_debit'.
      amount_credit: Amount;
    }

.. http:get:: /cashout-rate

  This public endpoint allows clients to calculate
  the exchange rate between the regional currency
  and the fiat currency of the banking system.

  This endpoint shows how the bank would apply the cash-out
  ratio and fee to one input amount.  Typically, frontends
  ask this endpoint before creating cash-in operations.

  **Request:**

  :query amount_debit: this is the amount that the user will get
    deducted from their regional bank account.

  or
 
  :query amount_credit: this is the amount that the user will receive
    in their fiat bank account.

  **Response:**

  :http:statuscode:`200 OK`:
    Response is a `CashoutConversionResponse`.
  :http:statuscode:`400 Bad request`:
    * ``TALER_EC_GENERIC_PARAMETER_MISSING`` : none of the parameters have been provided. 
    * ``TALER_EC_GENERIC_PARAMETER_MALFORMED`` : both of the parameters have been provided or one of them is not a valid Taler amount.
    * ``TALER_EC_GENERIC_CURRENCY_MISMATCH`` : the parameter is in the wrong currency.
  :http:statuscode:`409 Conflict`:
    The amount is too small to be converted, either because it produces produce an amount less than zero, or because the server requires a higher minimum amount than that supplied.
  :http:statuscode:`501 Not implemented`:
    This server does not support conversion, client should check config response.

  **Details:**

  .. ts:def:: CashoutConversionResponse

    interface CashoutConversionResponse {
      // Amount that the user will get deducted from their regional
      // bank account, according to the 'amount_credit' value.
      amount_debit: Amount;
      // Amount that the user will receive in their fiat
      // bank account, according to 'amount_debit'.
      amount_credit: Amount;
    }

.. http:post:: /conversion-rate

  This endpoint allows the administrator to update 
  the exchange rate between the regional currency
  and the fiat currency of the banking system.

  **Request:**

  .. ts:def:: ConversionRate
  
    interface ConversionRate {
      // Exchange rate to buy regional currency from fiat
      cashin_ratio: DecimalNumber; 

      // Fee to subtract after applying the cashin ratio.
      cashin_fee: Amount;

      // Minimum amount authorised for cashin, in fiat before conversion
      cashin_min_amount: Amount;

      // Smallest possible regional amount, converted amount is rounded to this amount
      cashin_tiny_amount: Amount;

      // Rounding mode used during cashin conversion
      cashin_rounding_mode: "zero" | "up" | "nearest";

      // Exchange rate to sell regional currency for fiat
      cashout_ratio: DecimalNumber; 

      // Fee to subtract after applying the cashout ratio.
      cashout_fee: Amount;

      // Minimum amount authorised for cashout, in regional before conversion
      cashout_min_amount: Amount;

      // Smallest possible fiat amount, converted amount is rounded to this amount
      cashout_tiny_amount: Amount;

      // Rounding mode used during cashout conversion
      cashout_rounding_mode: "zero" | "up" | "nearest";
    }

  **Response:**

  :http:statuscode:`204 No content`:
    Operation successful.
  :http:statuscode:`401 Unauthorized`:
    Invalid credentials or missing rights.
  :http:statuscode:`501 Not implemented`:
    This server does not support conversion, client should check config response.
