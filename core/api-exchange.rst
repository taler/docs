..
  This file is part of GNU TALER.
  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Christian Grothoff

========================
The Exchange RESTful API
========================

The API specified here follows the :ref:`general conventions <http-common>`
for all details not specified in the individual requests.
The `glossary <https://docs.taler.net/glossary.html#glossary>`_
defines all specific terms used in this section.

.. contents:: Table of Contents

.. include:: tos.rst

.. _keys:

---------------------------
Exchange status information
---------------------------

This API is used by wallets and merchants to obtain global information about
the exchange, such as online signing keys, available denominations and the fee
structure.  This is typically the first call any exchange client makes, as it
returns information required to process all of the other interactions with the
exchange.  The returned information is secured by (1) signature(s) from the exchange,
especially the long-term offline signing key of the exchange, which clients should
cache; (2) signature(s) from auditors, and the auditor keys should be
hard-coded into the wallet as they are the trust anchors for Taler; (3)
possibly by using HTTPS.


.. http:get:: /seed

  Return an entropy seed. The exchange will return a high-entropy
  value that will differ for every call.  The response is NOT in
  JSON, but simply high-entropy binary data in the HTTP body.
  This API should be used by wallets to guard themselves against
  running on low-entropy (bad PRNG) hardware. Naturally, the entropy
  returned MUST be mixed with locally generated entropy.


.. http:get:: /config

  Return the protocol version and currency supported by this exchange backend,
  as well as the list of possible KYC requirements.  This endpoint is largely
  for the SPA for AML officers. Merchants should use ``/keys`` which also
  contains the protocol version and currency.

  **Response:**

  :http:statuscode:`200 OK`:
    The body is a `VersionResponse`.

  .. ts:def:: ExchangeVersionResponse

    interface ExchangeVersionResponse {
      // libtool-style representation of the Exchange protocol version, see
      // https://www.gnu.org/software/libtool/manual/html_node/Versioning.html#Versioning
      // The format is "current:revision:age".
      version: string;

      // Name of the protocol.
      name: "taler-exchange";

      // Currency supported by this exchange.
      currency: string;

      // How wallets should render this currency.
      currency_specification: CurrencySpecification;

      // Names of supported KYC requirements.
      supported_kyc_requirements: string[];

    }

  .. ts:def:: CurrencySpecification

    interface CurrencySpecification {
      // Name of the currency.
      name: string;

      // Code of the currency
      currency: string;

      // how many digits the user may enter after the decimal_separator
      num_fractional_input_digits: Integer;

      // Number of fractional digits to render in normal font and size.
      num_fractional_normal_digits: Integer;

      // Number of fractional digits to render always, if needed by
      // padding with zeros.
      num_fractional_trailing_zero_digits: Integer;

      // map of powers of 10 to alternative currency names / symbols, must
      // always have an entry under "0" that defines the base name,
      // e.g.  "0 => €" or "3 => k€". For BTC, would be "0 => BTC, -3 => mBTC".
      // Communicates the currency symbol to be used.
      alt_unit_names: { log10 : string };
    }


.. http:get:: /keys

  Get a list of all denomination keys offered by the exchange,
  as well as the exchange's current online signing key.

  **Request:**

  :query last_issue_date: Optional argument specifying the maximum value of any of the ``stamp_start`` members of the denomination keys of a ``/keys`` response that is already known to the client. Allows the exchange to only return keys that have changed since that timestamp.  The given value must be an unsigned 64-bit integer representing seconds after 1970.  If the timestamp does not exactly match the ``stamp_start`` of one of the denomination keys, all keys are returned.

  **Response:**

  :http:statuscode:`200 OK`:
    The exchange responds with a `ExchangeKeysResponse` object. This request should
    virtually always be successful. It only fails if the exchange is misconfigured or
    has not yet been provisioned with key signatures via ``taler-exchange-offline``.

  **Details:**

  .. ts:def:: ExchangeKeysResponse

    interface ExchangeKeysResponse {
      // libtool-style representation of the Exchange protocol version, see
      // https://www.gnu.org/software/libtool/manual/html_node/Versioning.html#Versioning
      // The format is "current:revision:age".
      version: string;

      // The exchange's base URL.
      base_url: string;

      // The exchange's currency or asset unit.
      currency: string;

      // How wallets should render this currency.
      currency_specification: CurrencySpecification;

      // Absolute cost offset for the STEFAN curve used
      // to (over) approximate fees payable by amount.
      stefan_abs: Amount;

      // Factor to multiply the logarithm of the amount
      // with to (over) approximate fees payable by amount.
      // Note that the total to be paid is first to be
      // divided by the smallest denomination to obtain
      // the value that the logarithm is to be taken of.
      stefan_log: Amount;

      // Linear cost factor for the STEFAN curve used
      // to (over) approximate fees payable by amount.
      //
      // Note that the total to be paid is first to be
      // divided by the smallest denomination to obtain
      // the value to be multiplied with.
      stefan_lin: Amount;

      // Type of the asset. "fiat", "crypto", "regional"
      // or "stock".  Wallets should adjust their UI/UX
      // based on this value.
      asset_type: string;

      // Array of wire accounts operated by the exchange for
      // incoming wire transfers.
      accounts: WireAccount[];

      // Object mapping names of wire methods (i.e. "iban" or "x-taler-bank")
      // to wire fees.
      wire_fees: { method : AggregateTransferFee[] };

      // List of exchanges that this exchange is partnering
      // with to enable wallet-to-wallet transfers.
      wads: ExchangePartner[];

      // Set to true if this exchange allows the use
      // of reserves for rewards.
      rewards_allowed: boolean;

      // EdDSA master public key of the exchange, used to sign entries
      // in ``denoms`` and ``signkeys``.
      master_public_key: EddsaPublicKey;

      // Relative duration until inactive reserves are closed;
      // not signed (!), can change without notice.
      reserve_closing_delay: RelativeTime;

      // Threshold amounts beyond which wallet should
      // trigger the KYC process of the issuing
      // exchange.  Optional option, if not given there is no limit.
      // Currency must match ``currency``.
      wallet_balance_limit_without_kyc?: Amount[];

      // Denominations offered by this exchange
      denominations: DenomGroup[];

      // Compact EdDSA `signature` (binary-only) over the
      // contatentation of all of the master_sigs (in reverse
      // chronological order by group) in the arrays under
      // "denominations".  Signature of `TALER_ExchangeKeySetPS`
      exchange_sig: EddsaSignature;

      // Public EdDSA key of the exchange that was used to generate the signature.
      // Should match one of the exchange's signing keys from ``signkeys``.  It is given
      // explicitly as the client might otherwise be confused by clock skew as to
      // which signing key was used for the ``exchange_sig``.
      exchange_pub: EddsaPublicKey;

      // Denominations for which the exchange currently offers/requests recoup.
      recoup: Recoup[];

      // Array of globally applicable fees by time range.
      global_fees: GlobalFees[];

      // The date when the denomination keys were last updated.
      list_issue_date: Timestamp;

      // Auditors of the exchange.
      auditors: AuditorKeys[];

      // The exchange's signing keys.
      signkeys: SignKey[];

      // Optional field with a dictionary of (name, object) pairs defining the
      // supported and enabled extensions, such as ``age_restriction``.
      extensions?: { name: ExtensionManifest };

      // Signature by the exchange master key of the SHA-256 hash of the
      // normalized JSON-object of field extensions, if it was set.
      // The signature has purpose TALER_SIGNATURE_MASTER_EXTENSIONS.
      extensions_sig?: EddsaSignature;

    }

  .. ts:def:: GlobalFees

    interface GlobalFees {

      // What date (inclusive) does these fees go into effect?
      start_date: Timestamp;

      // What date (exclusive) does this fees stop going into effect?
      end_date: Timestamp;

      // Account history fee, charged when a user wants to
      // obtain a reserve/account history.
      history_fee: Amount;

      // Annual fee charged for having an open account at the
      // exchange.  Charged to the account.  If the account
      // balance is insufficient to cover this fee, the account
      // is automatically deleted/closed. (Note that the exchange
      // will keep the account history around for longer for
      // regulatory reasons.)
      account_fee: Amount;

      // Purse fee, charged only if a purse is abandoned
      // and was not covered by the account limit.
      purse_fee: Amount;

      // How long will the exchange preserve the account history?
      // After an account was deleted/closed, the exchange will
      // retain the account history for legal reasons until this time.
      history_expiration: RelativeTime;

      // Non-negative number of concurrent purses that any
      // account holder is allowed to create without having
      // to pay the purse_fee.
      purse_account_limit: Integer;

      // How long does an exchange keep a purse around after a purse
      // has expired (or been successfully merged)?  A 'GET' request
      // for a purse will succeed until the purse expiration time
      // plus this value.
      purse_timeout: RelativeTime;

      // Signature of `TALER_GlobalFeesPS`.
      master_sig: EddsaSignature;

    }

  .. ts:def:: AgeMask

    // Binary representation of the age groups.
    // The bits set in the mask mark the edges at the beginning of a next age
    // group.  F.e. for the age groups
    //     0-7, 8-9, 10-11, 12-13, 14-15, 16-17, 18-21, 21-*
    // the following bits are set:
    //
    //   31     24        16        8         0
    //   |      |         |         |         |
    //   oooooooo  oo1oo1o1  o1o1o1o1  ooooooo1
    //
    // A value of 0 means that the exchange does not support the extension for
    // age-restriction.
    type AgeMask = Integer;

  .. ts:def:: DenomGroup

    type DenomGroup =
      | DenomGroupRsa
      | DenomGroupCs
      | DenomGroupRsaAgeRestricted
      | DenomGroupCsAgeRestricted;

  .. ts:def:: DenomGroupRsa

    interface DenomGroupRsa extends DenomGroupCommon {
      cipher: "RSA";

      denoms: ({
        rsa_pub: RsaPublicKey;
      } & DenomCommon)[];
    }

  .. ts:def:: DenomGroupCs

    interface DenomGroupCs extends DenomGroupCommon {
      cipher: "CS";

      denoms: ({
        cs_pub: Cs25519Point;
      } & DenomCommon)[];
    }

  .. ts:def:: DenomGroupRsaAgeRestricted

    interface DenomGroupRsaAgeRestricted extends DenomGroupCommon {
      cipher: "RSA+age_restricted";
      age_mask: AgeMask;

      denoms: ({
        rsa_pub: RsaPublicKey;
      } & DenomCommon)[];
    }

  .. ts:def:: DenomGroupCsAgeRestricted

    interface DenomGroupCSAgeRestricted extends DenomGroupCommon {
      cipher: "CS+age_restricted";
      age_mask: AgeMask;

      denoms: ({
        cs_pub: Cs25519Point;
      } & DenomCommon)[];
    }

  .. ts:def:: DenomGroupCommon

    // Common attributes for all denomination groups
    interface DenomGroupCommon {
      // How much are coins of this denomination worth?
      value: Amount;

      // Fee charged by the exchange for withdrawing a coin of this denomination.
      fee_withdraw: Amount;

      // Fee charged by the exchange for depositing a coin of this denomination.
      fee_deposit: Amount;

      // Fee charged by the exchange for refreshing a coin of this denomination.
      fee_refresh: Amount;

      // Fee charged by the exchange for refunding a coin of this denomination.
      fee_refund: Amount;

    }

  .. ts:def:: DenomCommon

    interface DenomCommon {
      // Signature of `TALER_DenominationKeyValidityPS`.
      master_sig: EddsaSignature;

      // When does the denomination key become valid?
      stamp_start: Timestamp;

      // When is it no longer possible to withdraw coins
      // of this denomination?
      stamp_expire_withdraw: Timestamp;

      // When is it no longer possible to deposit coins
      // of this denomination?
      stamp_expire_deposit: Timestamp;

      // Timestamp indicating by when legal disputes relating to these coins must
      // be settled, as the exchange will afterwards destroy its evidence relating to
      // transactions involving this coin.
      stamp_expire_legal: Timestamp;

      // Set to 'true' if the exchange somehow "lost"
      // the private key. The denomination was not
      // necessarily revoked, but still cannot be used
      // to withdraw coins at this time (theoretically,
      // the private key could be recovered in the
      // future; coins signed with the private key
      // remain valid).
      lost?: boolean;
    }

  .. ts:def:: DenominationKey

    type DenominationKey =
      | RsaDenominationKey
      | CSDenominationKey;

  .. ts:def:: RsaDenominationKey

    interface RsaDenominationKey {
      cipher: "RSA";

      // 32-bit age mask.
      age_mask: Integer;

      // RSA public key
      rsa_public_key: RsaPublicKey;
    }

  .. ts:def:: CSDenominationKey

    interface CSDenominationKey {
      cipher: "CS";

      // 32-bit age mask.
      age_mask: Integer;

      // Public key of the denomination.
      cs_public_key: Cs25519Point;

    }

  Fees for any of the operations can be zero, but the fields must still be
  present. The currency of the ``fee_deposit``, ``fee_refresh`` and ``fee_refund`` must match the
  currency of the ``value``.  Theoretically, the ``fee_withdraw`` could be in a
  different currency, but this is not currently supported by the
  implementation.

  .. ts:def:: Recoup

    interface Recoup {
      // Hash of the public key of the denomination that is being revoked under
      // emergency protocol (see ``/recoup``).
      h_denom_pub: HashCode;

      // We do not include any signature here, as the primary use-case for
      // this emergency involves the exchange having lost its signing keys,
      // so such a signature here would be pretty worthless.  However, the
      // exchange will not honor ``/recoup`` requests unless they are for
      // denomination keys listed here.
    }

  A signing key in the ``signkeys`` list is a JSON object with the following fields:

  .. ts:def:: SignKey

    interface SignKey {
      // The actual exchange's EdDSA signing public key.
      key: EddsaPublicKey;

      // Initial validity date for the signing key.
      stamp_start: Timestamp;

      // Date when the exchange will stop using the signing key, allowed to overlap
      // slightly with the next signing key's validity to allow for clock skew.
      stamp_expire: Timestamp;

      // Date when all signatures made by the signing key expire and should
      // henceforth no longer be considered valid in legal disputes.
      stamp_end: Timestamp;

      // Signature over ``key`` and ``stamp_expire`` by the exchange master key.
      // Signature of `TALER_ExchangeSigningKeyValidityPS`.
      // Must have purpose ``TALER_SIGNATURE_MASTER_SIGNING_KEY_VALIDITY``.
      master_sig: EddsaSignature;
    }

  An entry in the ``auditors`` list is a JSON object with the following fields:

  .. ts:def:: AuditorKeys

    interface AuditorKeys {
      // The auditor's EdDSA signing public key.
      auditor_pub: EddsaPublicKey;

      // The auditor's URL.
      auditor_url: string;

      // The auditor's name (for humans).
      auditor_name: string;

      // An array of denomination keys the auditor affirms with its signature.
      // Note that the message only includes the hash of the public key, while the
      // signature is actually over the expanded information including expiration
      // times and fees.  The exact format is described below.
      denomination_keys: AuditorDenominationKey[];
    }

  .. ts:def:: AuditorDenominationKey

    interface AuditorDenominationKey {
      // Hash of the public RSA key used to sign coins of the respective
      // denomination.  Note that the auditor's signature covers more than just
      // the hash, but this other information is already provided in ``denoms`` and
      // thus not repeated here.
      denom_pub_h: HashCode;

      // Signature of `TALER_ExchangeKeyValidityPS`.
      auditor_sig: EddsaSignature;
    }

  The same auditor may appear multiple times in the array for different subsets
  of denomination keys, and the same denomination key hash may be listed
  multiple times for the same or different auditors.  The wallet or merchant
  just should check that the denomination keys they use are in the set for at
  least one of the auditors that they accept.

  .. note::

    Both the individual denominations *and* the denomination list is signed,
    allowing customers to prove that they received an inconsistent list.

.. _wire-req:

.. http:get:: /wire

  Returns a list of payment methods supported by the exchange.  The idea is that wallets may use this information to instruct users on how to perform wire transfers to top up their wallets.

  **Response:**

  :http:statuscode:`200 OK`:
     The exchange responds with a `WireResponse` object. This request should virtually always be successful.

  **Details:**

  .. ts:def:: WireResponse

    interface WireResponse {

      // Master public key of the exchange, must match the key returned in ``/keys``.
      master_public_key: EddsaPublicKey;

      // Array of wire accounts operated by the exchange for
      // incoming wire transfers.
      accounts: WireAccount[];

      // Object mapping names of wire methods (i.e. "iban" or "x-taler-bank")
      // to wire fees.
      fees: { method : AggregateTransferFee[] };

      // List of exchanges that this exchange is partnering
      // with to enable wallet-to-wallet transfers.
      wads: ExchangePartner[];
    }

  The specification for the account object is:

  .. ts:def:: WireAccount

    interface WireAccount {
      // ``payto://`` URI identifying the account and wire method
      payto_uri: string;

      // URI to convert amounts from or to the currency used by
      // this wire account of the exchange. Missing if no
      // conversion is applicable.
      conversion_url?: string;

      // Restrictions that apply to bank accounts that would send
      // funds to the exchange (crediting this exchange bank account).
      // Optional, empty array for unrestricted.
      credit_restrictions: AccountRestriction[];

      // Restrictions that apply to bank accounts that would receive
      // funds from the exchange (debiting this exchange bank account).
      // Optional, empty array for unrestricted.
      debit_restrictions: AccountRestriction[];

      // Signature using the exchange's offline key over
      // a `TALER_MasterWireDetailsPS`
      // with purpose ``TALER_SIGNATURE_MASTER_WIRE_DETAILS``.
      master_sig: EddsaSignature;
    }

  .. ts:def:: AccountRestriction

    type AccountRestriction =
      | RegexAccountRestriction
      | DenyAllAccountRestriction

  .. ts:def:: DenyAllAccountRestriction

    // Account restriction that disables this type of
    // account for the indicated operation categorically.
    interface DenyAllAccountRestriction {

      type: "deny";
    }

  .. ts:def:: RegexAccountRestriction

    // Accounts interacting with this type of account
    // restriction must have a payto://-URI matching
    // the given regex.
    interface RegexAccountRestriction {

      type: "regex";

      // Regular expression that the payto://-URI of the
      // partner account must follow.  The regular expression
      // should follow posix-egrep, but without support for character
      // classes, GNU extensions, back-references or intervals. See
      // https://www.gnu.org/software/findutils/manual/html_node/find_html/posix_002degrep-regular-expression-syntax.html
      // for a description of the posix-egrep syntax. Applications
      // may support regexes with additional features, but exchanges
      // must not use such regexes.
      payto_regex: string;

      // Hint for a human to understand the restriction
      // (that is hopefully easier to comprehend than the regex itself).
      human_hint: string;

      // Map from IETF BCP 47 language tags to localized
      // human hints.
      human_hint_i18n?: { [lang_tag: string]: string };

    }

  Aggregate wire transfer fees representing the fees the exchange
  charges per wire transfer to a merchant must be specified as an
  array in all wire transfer response objects under ``fees``.  The
  respective array contains objects with the following members:

  .. ts:def:: AggregateTransferFee

    interface AggregateTransferFee {
      // Per transfer wire transfer fee.
      wire_fee: Amount;

      // Per transfer closing fee.
      closing_fee: Amount;

      // Per exchange-to-exchange transfer (wad) fee.
      wad_fee: Amount;

      // What date (inclusive) does this fee go into effect?
      // The different fees must cover the full time period in which
      // any of the denomination keys are valid without overlap.
      start_date: Timestamp;

      // What date (exclusive) does this fee stop going into effect?
      // The different fees must cover the full time period in which
      // any of the denomination keys are valid without overlap.
      end_date: Timestamp;

      // Signature of `TALER_MasterWireFeePS` with
      // purpose ``TALER_SIGNATURE_MASTER_WIRE_FEES``.
      sig: EddsaSignature;
    }

  .. ts:def:: ExchangePartner

    interface ExchangePartner {
      // Base URL of the partner exchange.
      partner_base_url: string;

      // Public master key of the partner exchange.
      partner_master_pub: EddsaPublicKey;

      // Exchange-to-exchange wad (wire) transfer frequency.
      wad_frequency: RelativeTime;

      // When did this partnership begin (under these conditions)?
      start_date: Timestamp;

      // How long is this partnership expected to last?
      end_date: Timestamp;

      // Signature using the exchange's offline key over
      // `TALER_WadPartnerSignaturePS`
      // with purpose ``TALER_SIGNATURE_MASTER_PARTNER_DETAILS``.
      master_sig: EddsaSignature;
    }




----------------------------------------------
Management operations authorized by master key
----------------------------------------------

.. http:get:: /management/keys

  Get a list of future public keys to be used by the exchange.  Only to be
  used by the exchange's offline key management team. Not useful for anyone
  else (but also not secret, so access is public).

  **Response:**

  :http:statuscode:`200 OK`:
    The exchange responds with a `FutureKeysResponse` object. This request should
    virtually always be successful.

  **Details:**

  .. ts:def:: FutureKeysResponse

    interface FutureKeysResponse {

      // Future denominations to be offered by this exchange
      // (only those lacking a master signature).
      future_denoms: FutureDenom[];

      // The exchange's future signing keys (only those lacking a master signature).
      future_signkeys: FutureSignKey[];

      // Master public key expected by this exchange (provided so that the
      // offline signing tool can check that it has the right key).
      master_pub: EddsaPublicKey;

      // Public key of the denomination security module.
      denom_secmod_public_key: EddsaPublicKey;

      // Public key of the signkey security module.
      signkey_secmod_public_key: EddsaPublicKey;

    }

  .. ts:def:: FutureDenom

    interface FutureDenom {
      // Name in the configuration file that defines this denomination.
      section_name: string;

      // How much are coins of this denomination worth?
      value: Amount;

      // When does the denomination key become valid?
      stamp_start: Timestamp;

      // When is it no longer possible to withdraw coins
      // of this denomination?
      stamp_expire_withdraw: Timestamp;

      // When is it no longer possible to deposit coins
      // of this denomination?
      stamp_expire_deposit: Timestamp;

      // Timestamp indicating by when legal disputes relating to these coins must
      // be settled, as the exchange will afterwards destroy its evidence relating to
      // transactions involving this coin.
      stamp_expire_legal: Timestamp;

      // Public key for the denomination.
      denom_pub: DenominationKey;

      // Fee charged by the exchange for withdrawing a coin of this denomination.
      fee_withdraw: Amount;

      // Fee charged by the exchange for depositing a coin of this denomination.
      fee_deposit: Amount;

      // Fee charged by the exchange for refreshing a coin of this denomination.
      fee_refresh: Amount;

      // Fee charged by the exchange for refunding a coin of this denomination.
      fee_refund: Amount;

      // Signature by the denomination security module
      // over `TALER_DenominationKeyAnnouncementPS`
      // for this denomination with purpose
      // ``TALER_SIGNATURE_SM_DENOMINATION_KEY``.
      denom_secmod_sig: EddsaSignature;

    }

  .. ts:def:: FutureSignKey

    interface SignKey {
      // The actual exchange's EdDSA signing public key.
      key: EddsaPublicKey;

      // Initial validity date for the signing key.
      stamp_start: Timestamp;

      // Date when the exchange will stop using the signing key, allowed to overlap
      // slightly with the next signing key's validity to allow for clock skew.
      stamp_expire: Timestamp;

      // Date when all signatures made by the signing key expire and should
      // henceforth no longer be considered valid in legal disputes.
      stamp_end: Timestamp;

      // Signature over `TALER_SigningKeyAnnouncementPS`
      // for this signing key by the signkey security
      // module using purpose ``TALER_SIGNATURE_SM_SIGNING_KEY``.
      signkey_secmod_sig: EddsaSignature;
    }


.. http:post:: /management/keys

  Provide master signatures for future public keys to be used by the exchange.
  Only to be used by the exchange's offline key management team. Not useful
  for anyone else.

  **Request:** The request body must be a `MasterSignatures` object.

  **Response:**

  :http:statuscode:`204 No content`:
    The request was successfully processed.
  :http:statuscode:`403 Forbidden`:
    A provided signature is invalid.
  :http:statuscode:`404 Not found`:
    One of the keys for which a signature was provided is unknown to the exchange.

  **Details:**

  .. ts:def:: MasterSignatures

    interface MasterSignatures {

      // Provided master signatures for future denomination keys.
      denom_sigs: DenomSignature[];

      // Provided master signatures for future online signing keys.
      signkey_sigs: SignKeySignature[];

    }

  .. ts:def:: DenomSignature

    interface DenomSignature {

      // Hash of the public key of the denomination.
      h_denom_pub: HashCode;

      // Signature over `TALER_DenominationKeyValidityPS`.
      // Must have purpose ``TALER_SIGNATURE_MASTER_DENOMINATION_KEY_VALIDITY``
      master_sig: EddsaSignature;

    }

  .. ts:def:: SignKeySignature

    interface SignKeySignature {
      // The actual exchange's EdDSA signing public key.
      key: EddsaPublicKey;

      // Signature by the exchange master key over
      // `TALER_ExchangeSigningKeyValidityPS`.
      // Must have purpose ``TALER_SIGNATURE_MASTER_SIGNING_KEY_VALIDITY``.
      master_sig: EddsaSignature;

    }


.. http:post:: /management/denominations/$H_DENOM_PUB/revoke

  Revoke denomination key, preventing further use by the exchange.
  Only to be used by the exchange's offline key management team. Not useful
  for anyone else.

  **Request:** The request body must be a `DenomRevocationSignature` object.

  **Response:**

  :http:statuscode:`204 No content`:
    The request was successfully processed.
  :http:statuscode:`403 Forbidden`:
    The provided signature is invalid.

  **Details:**

  .. ts:def:: DenomRevocationSignature

    interface DenomRevocationSignature {

      // Signature by the exchange master key over a
      // `TALER_MasterDenominationKeyRevocationPS`.
      // Must have purpose ``TALER_SIGNATURE_MASTER_DENOMINATION_KEY_REVOKED``.
      master_sig: EddsaSignature;

    }

.. http:post:: /management/signkeys/$EXCHANGE_PUB/revoke

  Revoke exchange online signing key, preventing further use by the exchange.
  Only to be used by the exchange's offline key management team. Not useful
  for anyone else.

  **Request:** The request body must be a `SignkeyRevocationSignature` object.

  **Response:**

  :http:statuscode:`204 No content`:
    The request was successfully processed.
  :http:statuscode:`403 Forbidden`:
    The provided signature is invalid.

  **Details:**

  .. ts:def:: SignkeyRevocationSignature

    interface SignkeyRevocationSignature {

      // Signature by the exchange master key over a
      // `TALER_MasterSigningKeyRevocationPS`.
      // Must have purpose ``TALER_SIGNATURE_MASTER_SIGN_KEY_REVOKED``.
      master_sig: EddsaSignature;

    }


.. http:post:: /management/auditors

  This request will be used to enable an auditor.

  **Request:**

  The request must be a `AuditorSetupMessage`.

  **Response:**

  :http:statuscode:`204 No content`:
    The auditor was successfully enabled.
  :http:statuscode:`403 Forbidden`:
    The master signature is invalid.
  :http:statuscode:`409 Conflict`:
    The exchange has a more recent request related to this auditor key (replay detected).

  **Details:**

  .. ts:def:: AuditorSetupMessage

    interface AuditorSetupMessage {

      // Base URL of the auditor.
      auditor_url: string;

      // Human-readable name of the auditor.
      auditor_name: string;

      // The auditor's EdDSA signing public key.
      auditor_pub: EddsaPublicKey;

      // Signature by the exchange master ke yover a
      // `TALER_MasterAddAuditorPS`.
      // Must have purpose ``TALER_SIGNATURE_MASTER_ADD_AUDITOR``.
      master_sig: EddsaSignature;

      // When does the auditor become active?
      // Should be the time when the signature was created,
      // using the (monotonic!) local time of the system
      // with the offline master public key. Note that
      // even if the time is in the future, the auditor will
      // become active immediately! Used ONLY to detect replay attacks.
      validity_start: Timestamp;

    }

.. http:post:: /management/auditors/$AUDITOR_PUB/disable

  This request will be used to disable the use of the given auditor.
  We use POST instead of DELETE because the exchange will retain state
  about the auditor (specifically the end date) to prevent replay
  attacks abusing the `AuditorSetupMessage`.  Also, DELETE would not
  support a body, which is needed to provide the signature authorizing
  the operation.

  **Request:**

  The request must be a `AuditorTeardownMessage`.

  **Response**

  :http:statuscode:`204 No content`:
    The auditor has successfully disabled the auditor. The body is empty.
  :http:statuscode:`403 Forbidden`:
    The signature is invalid.
  :http:statuscode:`404 Not found`:
    The auditor is unknown to the exchange.
  :http:statuscode:`409 Conflict`:
    The exchange has a more recent request related to this auditor key (replay detected).

  **Details:**

  .. ts:def:: AuditorTeardownMessage

    interface AuditorTeardownMessage {

      // Signature by the exchange master key over a
      // `TALER_MasterDelAuditorPS`.
      // Must have purpose ``TALER_SIGNATURE_MASTER_AUDITOR_DEL``.
      master_sig: EddsaSignature;

      // When does the auditor become inactive?
      // Should be the time when the signature was created,
      // using the (monotonic!) local time of the system
      // with the offline master public key.  Note that
      // even if the time is in the future, the auditor will
      // become inactive immediately! Used ONLY to detect replay attacks.
      validity_end: Timestamp;

    }


.. http:post:: /management/wire-fee

  This request is used to configure wire fees.

  **Request:**

  The request must be a `WireFeeSetupMessage`.

  **Response:**

  :http:statuscode:`204 No content`:
    The wire fee was successfully configured.
  :http:statuscode:`403 Forbidden`:
    The master signature is invalid.
  :http:statuscode:`409 Conflict`:
    The exchange has a conflicting wire fee already set up.

  **Details:**

  .. ts:def:: WireFeeSetupMessage

    interface WireFeeSetupMessage {

      // Wire method the fee applies to.
      wire_method: string;

      // Signature using the exchange's offline key
      // with purpose ``TALER_SIGNATURE_MASTER_WIRE_FEES``.
      master_sig_wire: EddsaSignature;

      // When does the wire fee validity period start?
      fee_start: Timestamp;

      // When does the wire fee validity period end (exclusive).
      fee_end: Timestamp;

      // Closing fee to charge during that time period for this wire method.
      closing_fee: Amount;

      // Wire fee to charge during that time period for this wire method.
      wire_fee: Amount;

    }

.. http:post:: /management/global-fees

  Provides global fee configuration for a timeframe.

  **Request:**

  The request must be a `GlobalFees` message.

  **Response**

  :http:statuscode:`204 No content`:
    The configuration update has been processed successfully. The body is empty.
  :http:statuscode:`403 Forbidden`:
    The signature is invalid.
  :http:statuscode:`409 Conflict`:
    The exchange has previously received a conflicting configuration message.



.. http:post:: /management/wire

  This request will be used to enable a wire method (exchange bank account).

  **Request:**

  The request must be a `WireSetupMessage`.

  **Response:**

  :http:statuscode:`204 No content`:
    The wire method was successfully enabled.
  :http:statuscode:`403 Forbidden`:
    The master signature is invalid.
  :http:statuscode:`409 Conflict`:
    The exchange has a more recent request related to this wire method (replay detected).

  **Details:**

  .. ts:def:: WireSetupMessage

    interface WireSetupMessage {

      // ``payto://`` URL identifying the account and wire method
      payto_uri: string;

      // Signature using the exchange's offline key
      // over a `TALER_MasterWireDetailsPS`
      // with purpose ``TALER_SIGNATURE_MASTER_WIRE_DETAILS``.
      master_sig_wire: EddsaSignature;

      // Signature using the exchange's offline key over a
      // `TALER_MasterAddWirePS`
      // with purpose ``TALER_SIGNATURE_MASTER_WIRE_ADD``.
      master_sig_add: EddsaSignature;

      // When does the wire method become active?
      // Should be the time when the signature was created,
      // using the (monotonic!) local time of the system
      // with the offline master public key. Note that
      // even if the time is in the future, the wire method will
      // become active immediately! Used ONLY to detect replay attacks.
      validity_start: Timestamp;

    }

.. http:post:: /management/wire/disable

  This request will be used to disable the use of the given wire method.
  We use POST instead of DELETE because the exchange will retain state
  about the wire method (specifically the end date) to prevent replay
  attacks abusing the `WireSetupMessage`.  Also, DELETE would not
  support a body, which is needed to provide the signature authorizing
  the operation.

  **Request:**

  The request must be a `WireTeardownMessage`.

  **Response**

  :http:statuscode:`204 No content`:
    The auditor has successfully disabled the wire method. The body is empty.
  :http:statuscode:`403 Forbidden`:
    The signature is invalid.
  :http:statuscode:`404 Not found`:
    The wire method is unknown to the exchange.
  :http:statuscode:`409 Conflict`:
    The exchange has a more recent request related to this wire method (replay detected).

  **Details:**

  .. ts:def:: WireTeardownMessage

    interface WireTeardownMessage {

      // ``payto://`` URL identifying the account and wire method
      payto_uri: string;

      // Signature using the exchange's offline key over a
      // `TALER_MasterDelWirePS`.
      // with purpose ``TALER_SIGNATURE_MASTER_WIRE_DEL``.
      master_sig_del: EddsaSignature;

      // Should be the time when the signature was created,
      // using the (monotonic!) local time of the system
      // with the offline master public key.  Note that
      // even if the time is in the future, the wire method will
      // become inactive immediately! Used ONLY to detect replay attacks.
      validity_end: Timestamp;

    }


.. http:post:: /management/drain

  This request is used to drain profits from the
  exchange's escrow account to another regular
  bank account of the exchange.  The actual drain
  requires running the ``taler-exchange-drain`` tool.

  **Request:**

  The request must be a `DrainProfitsMessage`.

  **Response:**

  :http:statuscode:`204 No content`:
    The profit drain was scheduled.
  :http:statuscode:`403 Forbidden`:
    The master signature is invalid.

  **Details:**

  .. ts:def:: DrainProfitsMessage

    interface DrainProfitsMessage {

      // Configuration section of the account to debit.
      debit_account_section: string;

      // Credit payto URI
      credit_payto_uri: string;

      // Wire transfer identifier to use.
      wtid: Base32;

      // Signature by the exchange master key over a
      // `TALER_MasterDrainProfitPS`.
      // Must have purpose ``TALER_SIGNATURE_MASTER_DRAIN_PROFITS``.
      master_sig: EddsaSignature;

      // When was the message created.
      date: Timestamp;

      // Amount to be drained.
      amount: Amount;

    }


.. http:post:: /management/aml-officers

  Update settings for an AML Officer status.

  **Request:**

  The request must be an `AmlOfficerSetup` message.

  **Response**

  :http:statuscode:`204 No content`:
    The officer settings have been updated successfully.
  :http:statuscode:`403 Forbidden`:
    The signature is invalid.
  :http:statuscode:`409 Conflict`:
    The exchange has previously received a conflicting configuration message.

  **Details:**

  .. ts:def:: AmlOfficerSetup

    interface AmlOfficerSetup {

      // Public key of the AML officer
      officer_pub: EddsaPublicKey;

      // Legal full name of the AML officer
      officer_name: string;

      // Is the account active?
      is_active: boolean;

      // Is the account read-only?
      read_only: boolean;

      // Signature by the exchange master key over a
      // `TALER_MasterAmlOfficerStatusPS`.
      // Must have purpose ``TALER_SIGNATURE_MASTER_AML_KEY``.
      master_sig: EddsaSignature;

      // When will the change take effect?
      change_date: Timestamp;

    }


  .. http:post:: /management/partners

    Enables a partner exchange for wad transfers.

  **Request:**

  The request must be an `ExchangePartner` message.

  **Response**

  :http:statuscode:`204 No content`:
    The partner has been added successfully.
  :http:statuscode:`403 Forbidden`:
    The signature is invalid.
  :http:statuscode:`409 Conflict`:
    The exchange has previously received a conflicting configuration message.

  **Details:**

  .. ts:def:: ExchangePartner

    interface ExchangePartner {

      // Base URL of the partner exchange
      partner_base_url: string;

      // Master (offline) public key of the partner exchange.
      partner_pub: EddsaPublicKey;

      // How frequently will wad transfers be made
      wad_frequency: RelativeTime;

      // Signature by the exchange master key over a
      // `TALER_PartnerConfigurationPS`.
      // Must have purpose ``TALER_SIGNATURE_MASTER_PARTNER_DETAILS``.
      master_sig: EddsaSignature;

      // When will the partner relationship start (inclusive).
      start_date: Timestamp;

      // When will the partner relationship end (exclusive).
      end_date: Timestamp;

      // Wad fee to be charged (to customers).
      wad_fee: Amount;

    }

--------------
AML operations
--------------

This API is only for designated AML officers. It is used
to allow exchange staff to monitor suspicious transactions
and freeze or unfreeze accounts suspected of money laundering.


.. http:get:: /aml/$OFFICER_PUB/decisions/$STATE

  Obtain list of AML decisions (filtered by $STATE).  ``$STATE`` must be either ``normal``, ``pending`` or ``frozen``.

  *Taler-AML-Officer-Signature*: The client must provide Base-32 encoded EdDSA signature with ``$OFFICER_PRIV``, affirming the desire to obtain AML data.  Note that this is merely a simple authentication mechanism, the details of the request are not protected by the signature.

  :query delta: *Optional*. takes value of the form ``N (-N)``, so that at most ``N`` values strictly older (younger) than ``start`` are returned.  Defaults to ``-20`` to return the last 20 entries (before ``start``).
  :query start: *Optional*. Row number threshold, see ``delta`` for its interpretation.  Defaults to ``INT64_MAX``, namely the biggest row id possible in the database.

  **Response**

  :http:statuscode:`200 OK`:
    The responds will be an `AmlRecords` message.
  :http:statuscode:`204 No content`:
    There are no matching AML records.
  :http:statuscode:`403 Forbidden`:
    The signature is invalid.
  :http:statuscode:`404 Not found`:
    The designated AML account is not known.
  :http:statuscode:`409 Conflict`:
    The designated AML account is not enabled.

  **Details:**

  .. ts:def:: AmlRecords

    interface AmlRecords {

      // Array of AML records matching the query.
      records: AmlRecord[];
    }

  .. ts:def:: AmlRecord

    interface AmlRecord {

      // Which payto-address is this record about.
      // Identifies a GNU Taler wallet or an affected bank account.
      h_payto: PaytoHash;

      // What is the current AML state.
      current_state: Integer;

      // Monthly transaction threshold before a review will be triggered
      threshold: Amount;

      // RowID of the record.
      rowid: Integer;

    }


.. http:get:: /aml/$OFFICER_PUB/decision/$H_PAYTO

  Obtain deails about an AML decision.

  *Taler-AML-Officer-Signature*: The client must provide Base-32 encoded EdDSA signature with ``$OFFICER_PRIV``, affirming the desire to obtain AML data.  Note that this is merely a simple authentication mechanism, the details of the request are not protected by the signature.

  :query history: *Optional*. If set to yes, we return all historic decisions and not only the last one.

  **Response**

  :http:statuscode:`200 OK`:
    The responds will be an `AmlDecisionDetails` message.
  :http:statuscode:`204 No content`:
    There are no matching AML records for the given payto://-URI.
  :http:statuscode:`403 Forbidden`:
    The signature is invalid.
  :http:statuscode:`404 Not found`:
    The designated AML account is not known.
  :http:statuscode:`409 Conflict`:
    The designated AML account is not enabled.

  **Details:**

  .. ts:def:: AmlDecisionDetails

    interface AmlDecisionDetails {

      // Array of AML decisions made for this account. Possibly
      // contains only the most recent decision if "history" was
      // not set to 'true'.
      aml_history: AmlDecisionDetail[];

      // Array of KYC attributes obtained for this account.
      kyc_attributes: KycDetail[];
    }

  .. ts:def:: AmlDecisionDetail

    interface AmlDecisionDetail {

      // What was the justification given?
      justification: string;

      // What is the new AML state.
      new_state: Integer;

      // When was this decision made?
      decision_time: Timestamp;

      // What is the new AML decision threshold (in monthly transaction volume)?
      new_threshold: Amount;

      // Who made the decision?
      decider_pub: AmlOfficerPublicKeyP;

    }

  .. ts:def:: KycDetail

    interface KycDetail {

      // Name of the configuration section that specifies the provider
      // which was used to collect the KYC details
      provider_section: string;

      // The collected KYC data.  NULL if the attribute data could not
      // be decrypted (internal error of the exchange, likely the
      // attribute key was changed).
      attributes?: Object;

      // Time when the KYC data was collected
      collection_time: Timestamp;

      // Time when the validity of the KYC data will expire
      expiration_time: Timestamp;

    }


  .. http:post:: /aml/$OFFICER_PUB/decision

  Make an AML decision. Triggers the respective action and
  records the justification.

  **Request:**

  The request must be an `AmlDecision` message.

  **Response**

  :http:statuscode:`204 No content`:
    The AML decision has been executed and recorded successfully.
  :http:statuscode:`403 Forbidden`:
    The signature is invalid.
  :http:statuscode:`404 Not found`:
    The address the decision was made upon is unknown to the exchange or
    the designated AML account is not known.
  :http:statuscode:`409 Conflict`:
    The designated AML account is not enabled or a more recent
    decision was already submitted.

  **Details:**

  .. ts:def:: AmlDecision

    interface AmlDecision {

      // Human-readable justification for the decision.
      justification: string;

      // At what monthly transaction volume should the
      // decision be automatically reviewed?
      new_threshold: Amount;

      // Which payto-address is the decision about?
      // Identifies a GNU Taler wallet or an affected bank account.
      h_payto: PaytoHash;

      // What is the new AML state (e.g. frozen, unfrozen, etc.)
      // Numerical values are defined in `AmlDecisionState`.
      new_state: Integer;

      // Signature by the AML officer over a
      // `TALER_MasterAmlOfficerStatusPS`.
      // Must have purpose ``TALER_SIGNATURE_MASTER_AML_KEY``.
      officer_sig: EddsaSignature;

      // When was the decision made?
      decision_time: Timestamp;

      // Optional argument to impose new KYC requirements
      // that the customer has to satisfy to unblock transactions.
      kyc_requirements?: string[];
    }


---------------
Auditor actions
---------------

.. _auditor_action:

This part of the API is for the use by auditors interacting with the exchange.


.. http:post:: /auditors/$AUDITOR_PUB/$H_DENOM_PUB

  This is used to add an auditor signature to the ``/keys`` response. It
  affirms to wallets and merchants that this auditor is indeed auditing
  the coins issued by the respective denomination.  There is no "delete"
  operation for this, as auditors can only stop auditing a denomination
  when it expires.

  **Request:**

  The request must be a `AuditorSignatureAddMessage`.

  **Response:**

  :http:statuscode:`204 No content`:
    The backend has successfully stored the auditor signature.
  :http:statuscode:`403 Forbidden`:
    The auditor signature is invalid.
  :http:statuscode:`404 Not found`:
    The denomination key for which the auditor is providing a signature is unknown.
    The response will be a `DenominationUnknownMessage`.
  :http:statuscode:`410 Gone`:
    This auditor is no longer supported by the exchange.
  :http:statuscode:`412 Precondition failed`:
    This auditor is not yet known to the exchange.

  **Details:**

  .. ts:def:: DenominationUnknownMessage

    interface DenominationUnknownMessage {

      // Taler error code.
      code: number;

      // Signature by the exchange over a
      // `TALER_DenominationUnknownAffirmationPS`.
      // Must have purpose ``TALER_SIGNATURE_EXCHANGE_AFFIRM_DENOM_UNKNOWN``.
      exchange_sig: EddsaSignature;

      // Public key of the exchange used to create
      // the 'exchange_sig.
      exchange_pub: EddsaPublicKey;

      // Hash of the denomination public key that is unknown.
      h_denom_pub: HashCode;

      // When was the signature created.
      timestamp: Timestamp;

    }

  .. ts:def:: AuditorSignatureAddMessage

    interface AuditorSignatureAddMessage {

      // Signature by the auditor over a
      // `TALER_ExchangeKeyValidityPS`.
      // Must have purpose ``TALER_SIGNATURE_AUDITOR_EXCHANGE_KEYS``.
      auditor_sig: EddsaSignature;

    }

.. _exchange-withdrawal:

----------
Withdrawal
----------

This API is used by the wallet to obtain digital coins.

When transferring money to the exchange such as via SEPA transfers, the exchange creates
a *reserve*, which keeps the money from the customer.  The customer must
specify an EdDSA reserve public key as part of the transfer, and can then
withdraw digital coins using the corresponding private key.  All incoming and
outgoing transactions are recorded under the corresponding public key by the
exchange.

.. note::

   Eventually the exchange will need to advertise a policy for how long it will
   keep transaction histories for inactive or even fully drained reserves.  We
   will therefore need some additional handler similar to ``/keys`` to
   advertise those terms of service.


.. http:get:: /reserves/$RESERVE_PUB

  Request summary information about a reserve.

  **Request:**

  :query timeout_ms=MILLISECONDS: *Optional.*  If specified, the exchange will wait up to MILLISECONDS for incoming funds before returning a 404 if the reserve does not yet exist.

  **Response:**

  :http:statuscode:`200 OK`:
    The exchange responds with a `ReserveSummary` object; the reserve was known to the exchange.
  :http:statuscode:`404 Not found`:
    The reserve key does not belong to a reserve known to the exchange.

  **Details:**

  .. ts:def:: ReserveSummary

    interface ReserveSummary {
      // Balance left in the reserve.
      balance: Amount;

      // If set, age restriction is required to be set for each coin to this
      // value during the withdrawal from this reserve. The client then MUST
      // use a denomination with support for age restriction enabled for the
      // withdrawal.
      // The value represents a valid age group from the list of permissible
      // age groups as defined by the exchange's output to /keys.
      maximum_age_group?: number;
    }


Withdraw
~~~~~~~~

.. http:post:: /csr-withdraw

  Obtain exchange-side input values in preparation for a
  withdraw step for certain denomination cipher types,
  specifically at this point for Clause-Schnorr blind
  signatures.

  **Request:** The request body must be a `WithdrawPrepareRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The request was successful, and the response is a `WithdrawPrepareResponse`.  Note that repeating exactly the same request
    will again yield the same response (assuming none of the denomination is expired).
  :http:statuscode:`404 Not found`:
    The denomination key is not known to the exchange.
  :http:statuscode:`410 Gone`:
    The requested denomination key is not yet or no longer valid.
    It either before the validity start, past the expiration or was revoked. The response is a
    `DenominationExpiredMessage`. Clients must evaluate
    the error code provided to understand which of the
    cases this is and handle it accordingly.

  **Details:**

  .. ts:def:: WithdrawPrepareRequest

    interface WithdrawPrepareRequest {

      // Nonce to be used by the exchange to derive
      // its private inputs from. Must not have ever
      // been used before.
      nonce: CSNonce;

      // Hash of the public key of the denomination the
      // request relates to.
      denom_pub_hash: HashCode;

    }

  .. ts:def:: WithdrawPrepareResponse

    type WithdrawPrepareResponse =
      | ExchangeWithdrawValue;

  .. ts:def:: ExchangeWithdrawValue

    type ExchangeWithdrawValue =
      | ExchangeRsaWithdrawValue
      | ExchangeCsWithdrawValue;

  .. ts:def:: ExchangeRsaWithdrawValue

    interface ExchangeRsaWithdrawValue {
      cipher: "RSA";
    }

  .. ts:def:: ExchangeCsWithdrawValue

    interface ExchangeCsWithdrawValue {
      cipher: "CS";

      // CSR R0 value
      r_pub_0: CsRPublic;

      // CSR R1 value
      r_pub_1: CsRPublic;
    }


Batch Withdraw
~~~~~~~~~~~~~~

.. http:post:: /reserves/$RESERVE_PUB/batch-withdraw

  Withdraw multiple coins from the same reserve.  Note that the client should
  commit all of the request details, including the private key of the coins and
  the blinding factors, to disk *before* issuing this request, so that it can
  recover the information if necessary in case of transient failures, like
  power outage, network outage, etc.

  **Request:** The request body must be a `BatchWithdrawRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The request was successful, and the response is a `BatchWithdrawResponse`.
    Note that repeating exactly the same request will again yield the same
    response, so if the network goes down during the transaction or before the
    client can commit the coin signature to disk, the coin is not lost.
  :http:statuscode:`403 Forbidden`:
    A signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    A denomination key or the reserve are not known to the exchange.  If the
    denomination key is unknown, this suggests a bug in the wallet as the
    wallet should have used current denomination keys from ``/keys``.
    In this case, the response will be a `DenominationUnknownMessage`.
    If the reserve is unknown, the wallet should not report a hard error yet, but
    instead simply wait for up to a day, as the wire transaction might simply
    not yet have completed and might be known to the exchange in the near future.
    In this case, the wallet should repeat the exact same request later again
    using exactly the same blinded coin.
  :http:statuscode:`409 Conflict`:
    One of the following reasons occured:

    1. The balance of the reserve is not sufficient to withdraw the coins of the
    indicated denominations.  The response is `WithdrawError` object.

    2. The reserve has a birthday set and requires a request to ``/age-withdraw`` instead.
    The response comes with a standard `ErrorDetail` response with error-code ``TALER_EC_EXCHANGE_RESERVES_AGE_RESTRICTION_REQUIRED`` and an additional field ``maximum_allowed_age`` for the maximum age (in years) that the client can commit to in the call to ``/age-withdraw``
  :http:statuscode:`410 Gone`:
    A requested denomination key is not yet or no longer valid.
    It either before the validity start, past the expiration or was revoked.
    The response is a `DenominationExpiredMessage`. Clients must evaluate the
    error code provided to understand which of the cases this is and handle it
    accordingly.
  :http:statuscode:`451 Unavailable for Legal Reasons`:
    This reserve has received funds from a purse or the amount withdrawn
    exceeds another legal threshold and thus the reserve must
    be upgraded to an account (with KYC) before the withdraw can
    complete.  Note that this response does NOT affirm that the
    withdraw will ultimately complete with the requested amount.
    The user should be redirected to the provided location to perform
    the required KYC checks to open the account before withdrawing.
    Afterwards, the request should be repeated.
    The response will be an `KycNeededRedirect` object.

    Implementation note: internally, we need to
    distinguish between upgrading the reserve to an
    account (due to P2P payment) and identifying the
    owner of the origin bank account (due to exceeding
    the withdraw amount threshold), as we need to create
    a different payto://-URI for the KYC check depending
    on the case.


  **Details:**

  .. ts:def:: BatchWithdrawRequest

    interface BatchWithdrawRequest {
      // Array of requests for the individual coins to withdraw.
      planchets: WithdrawRequest[];

    }

  .. ts:def:: WithdrawRequest

    interface WithdrawRequest {
      // Hash of a denomination public key, specifying the type of coin the client
      // would like the exchange to create.
      denom_pub_hash: HashCode;

      // Coin's blinded public key, should be (blindly) signed by the exchange's
      // denomination private key.
      coin_ev: CoinEnvelope;

      // Signature of `TALER_WithdrawRequestPS` created with
      // the `reserves's private key <reserve-priv>`
      // using purpose ``TALER_SIGNATURE_WALLET_RESERVE_WITHDRAW``.
      reserve_sig: EddsaSignature;

    }


  .. ts:def:: BatchWithdrawResponse

    interface BatchWithdrawResponse {
      // Array of blinded signatures, in the same order as was
      // given in the request.
      ev_sigs: WithdrawResponse[];

    }

  .. ts:def:: WithdrawResponse

    interface WithdrawResponse {
      // The blinded signature over the 'coin_ev', affirms the coin's
      // validity after unblinding.
      ev_sig: BlindedDenominationSignature;

    }

  .. ts:def:: BlindedDenominationSignature

    type BlindedDenominationSignature =
      | RsaBlindedDenominationSignature
      | CSBlindedDenominationSignature;

  .. ts:def:: RsaBlindedDenominationSignature

    interface RsaBlindedDenominationSignature {
      cipher: "RSA";

      // (blinded) RSA signature
      blinded_rsa_signature: BlindedRsaSignature;
    }

  .. ts:def:: CSBlindedDenominationSignature

    interface CSBlindedDenominationSignature {
      type: "CS";

      // Signer chosen bit value, 0 or 1, used
      // in Clause Blind Schnorr to make the
      // ROS problem harder.
      b: Integer;

      // Blinded scalar calculated from c_b.
      s: Cs25519Scalar;

    }

  .. ts:def:: KycNeededRedirect

    interface KycNeededRedirect {

      // Numeric `error code <error-codes>` unique to the condition.
      // Should always be ``TALER_EC_EXCHANGE_GENERIC_KYC_REQUIRED``.
      code: number;

      // Human-readable description of the error, i.e. "missing parameter", "commitment violation", ...
      // Should give a human-readable hint about the error's nature. Optional, may change without notice!
      hint?: string;

      // Hash of the payto:// account URI that identifies
      // the account which is being KYCed.
      h_payto:  PaytoHash;

      // Legitimization target that the merchant should
      // use to check for its KYC status using
      // the ``/kyc-check/$REQUIREMENT_ROW/...`` endpoint.
      requirement_row: Integer;

    }

  .. ts:def:: WithdrawError

    interface WithdrawError {
      // Text describing the error.
      hint: string;

      // Detailed error code.
      code: Integer;

      // Amount left in the reserve.
      balance: Amount;

      // History of the reserve's activity, in the same format
      // as returned by ``/reserve/$RID/history``.
      history: TransactionHistoryItem[]
    }

  .. ts:def:: DenominationExpiredMessage

    interface DenominationExpiredMessage {

      // Taler error code.  Note that beyond
      // expiration this message format is also
      // used if the key is not yet valid, or
      // has been revoked.
      code: number;

      // Signature by the exchange over a
      // `TALER_DenominationExpiredAffirmationPS`.
      // Must have purpose ``TALER_SIGNATURE_EXCHANGE_AFFIRM_DENOM_EXPIRED``.
      exchange_sig: EddsaSignature;

      // Public key of the exchange used to create
      // the 'exchange_sig.
      exchange_pub: EddsaPublicKey;

      // Hash of the denomination public key that is unknown.
      h_denom_pub: HashCode;

      // When was the signature created.
      timestamp: Timestamp;

      // What kind of operation was requested that now
      // failed?
      oper: string;
    }





Withdraw with Age Restriction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If the reserve was marked with a maximum age group, the client has to perform a
cut&choose protocol with the exchange.  It first calls
``/reserves/$RESERVE_PUB/age-withdraw`` and commits to ``n*kappa`` coins.  On
success, the exchange answers this request with an noreveal-index.  The client
then has to call ``/age-withdraw/$ACH/reveal`` to reveal all ``n*(kappa - 1)``
coins along with their age commitments to proof that they were appropriate.
If so, the exchange will blindly sign ``n`` undisclosed coins from the request.


.. http:post:: /reserves/$RESERVE_PUB/age-withdraw

  Withdraw multiple coins *with age restriction* from the same reserve.
  Note that the client should commit all of the request details, including the
  private key of the coins and the blinding factors, to disk *before* issuing
  this request, so that it can recover the information if necessary in case of
  transient failures, like power outage, network outage, etc.

  **Request:** The request body must be a `AgeWithdrawRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The request was successful, and the response is a `AgeWithdrawResponse`.
    Note that repeating exactly the same request will again yield the same
    response, so if the network goes down during the transaction or before the
    client can commit the coin signature to disk, the coin is not lost.
  :http:statuscode:`403 Forbidden`:
    A signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`409 Conflict`:
    One of two reasons occured:

    1. The balance of the reserve is not sufficient to withdraw the coins of the
    given amount.  The response is a `WithdrawError` object.

    2. The provided value for ``max_age`` is higher than the allowed value according to the reserve's birthday.
    The response comes with a standard `ErrorDetail` response with error-code ``TALER_EC_EXCHANGE_AGE_WITHDRAW_MAXIMUM_AGE_TOO_LARGE`` and an additional field ``maximum_allowed_age`` for the maximum age (in years) that the client can commit to in a call to ``/age-withdraw``
  :http:statuscode:`410 Gone`:
    A requested denomination key is not yet or no longer valid.
    It either before the validity start, past the expiration or was revoked.
    The response is a `DenominationExpiredMessage`. Clients must evaluate the
    error code provided to understand which of the cases this is and handle it
    accordingly.
  :http:statuscode:`451 Unavailable for Legal Reasons`:
    This reserve has received funds from a purse or the amount withdrawn
    exceeds another legal threshold and thus the reserve must
    be upgraded to an account (with KYC) before the withdraw can
    complete.  Note that this response does NOT affirm that the
    withdraw will ultimately complete with the requested amount.
    The user should be redirected to the provided location to perform
    the required KYC checks to open the account before withdrawing.
    Afterwards, the request should be repeated.
    The response will be an `KycNeededRedirect` object.

  .. ts:def:: AgeWithdrawRequest

    interface AgeWithdrawRequest {
      // Array of ``n`` hash codes of denomination public keys to order.
      // These denominations MUST support age restriction as defined in the
      // output to /keys.
      // The sum of all denomination's values and fees MUST be at most the
      // balance of the reserve.  The balance of the reserve will be
      // immediatley reduced by that amount.
      denoms_h: HashCode[];

      // ``n`` arrays of ``kappa`` entries with blinded coin envelopes.  Each
      // (toplevel)  entry represents ``kappa`` canditates for a particular
      // coin.  The exchange  will respond with an index ``gamma``, which is
      // the index that shall remain undisclosed during the reveal phase.
      // The SHA512 hash $ACH over the blinded coin envelopes is the commitment
      // that is later used as the key to the reveal-URL.
      blinded_coins_evs:  CoinEnvelope[][];

      // The maximum age to commit to.  MUST be the same as the maximum
      // age in the reserve.
      max_age: number;

      // Signature of `TALER_AgeWithdrawRequestPS` created with
      // the `reserves's private key <reserve-priv>`
      // using purpose ``TALER_SIGNATURE_WALLET_RESERVE_AGE_WITHDRAW``.
      reserve_sig: EddsaSignature;
    }

  .. ts:def:: AgeWithdrawResponse

    interface AgeWithdrawResponse {
      // index of the commitments that the client doesn't
      // have to disclose
      noreveal_index: Integer;

      // Signature of `TALER_AgeWithdrawConfirmationPS` whereby
      // the exchange confirms the ``noreveal_index``.
      exchange_sig: EddsaSignature;

      // `Public EdDSA key <sign-key-pub>` of the exchange that was used to
      // generate the signature.  Should match one of the exchange's signing
      // keys from ``/keys``.  Again given explicitly as the client might
      // otherwise be confused by clock skew as to which signing key was used.
      exchange_pub: EddsaPublicKey;
    }



.. http:post:: /age-withdraw/$ACH/reveal

  The client has previously committed to multiple coins with age restriction
  in a call to ``/reserve/$RESERVE_PUB/age-withdraw`` and got a
  `AgeWithdrawResponse` from the exchange.  By calling this
  endpoint, the client has to reveal each coin and their ``kappa - 1``
  age commitments, except for the age commitments with index
  ``noreveal_index``.  The hash of all commitments from the former withdraw
  request is given as the ``$ACH`` value in the URL to this endpoint.


  **Request:** The request body must be a `AgeWithdrawRevealRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The request was successful, and the response is a `AgeWithdrawRevealResponse`.
    Note that repeating exactly the same request will again yield the same
    response, so if the network goes down during the transaction or before the
    client can commit the coin signature to disk, the coin is not lost.
  :http:statuscode:`404 Not found`:
    The provided commitment $ACH is unknown.
  :http:statuscode:`409 Conflict`:
    The reveal operation failed and the response is an `WithdrawError` object.
    The error codes indicate one of two cases:

    1. An age commitment for at least one of the coins did not fulfill the
       required maximum age requirement of the corresponding reserve.
       Error code:
       ``TALER_EC_EXCHANGE_GENERIC_COIN_AGE_REQUIREMENT_FAILURE``.
    2. The computation of the hash of the commitment with provided input does
       result in the value $ACH.
       Error code:
       ``TALER_EC_EXCHANGE_AGE_WITHDRAW_REVEAL_INVALID_HASH``


  .. ts:def:: AgeWithdrawRevealRequest

    interface AgeWithdrawRevealRequest {
      // Array of ``n`` of ``(kappa - 1)`` disclosed coin master secrets, from
      // which the coins' private key, blinding, nonce (for Clause-Schnorr) and
      // age-restriction  is calculated.
      //
      // Given each coin's private key and age commitment, the exchange will
      // calculate each coin's blinded hash value und use all those (disclosed)
      // blinded hashes together with the non-disclosed envelopes ``coin_evs``
      // during the verification of the original age-withdraw-commitment.
      disclosed_coin_secrets: AgeRestrictedCoinSecret[][];
    }

  .. ts:def:: AgeRestrictedCoinSecret

    // The Master key material from which the coins' private key ``coin_priv``,
    // blinding ``beta`` and nonce ``nonce`` (for Clause-Schnorr) itself are
    // derived as usually in wallet-core.  Given a coin's master key material,
    // the age commitment for the coin MUST be derived from this private key as
    // follows:
    //
    // Let m ∈  {1,...,M} be the maximum age group as defined in the reserve
    // that the wallet can commit to.
    //
    // For age group $AG ∈  {1,...m}, set
    //     seed = HDKF(coin_secret, "age-commitment", $AG)
    //   p[$AG] = Edx25519_generate_private(seed)
    // and calculate the corresponding Edx25519PublicKey as
    //   q[$AG] = Edx25519_public_from_private(p[$AG])
    //
    // For age groups $AG ∈  {m,...,M}, set
    //   f[$AG] = HDKF(coin_secret, "age-factor", $AG)
    // and calculate the corresponding Edx25519PublicKey as
    //   q[$AG] = Edx25519_derive_public(`PublishedAgeRestrictionBaseKey`, f[$AG])
    //
    type AgeRestrictedCoinSecret = string;

  .. ts:def:: PublishedAgeRestrictionBaseKey

    // The value for ``PublishedAgeRestrictionBaseKey`` is a randomly chosen
    // `Edx25519PublicKey` for which the private key is not known to the clients.  It is
    // used during the age-withdraw protocol so that clients can proof that they
    // derived all public keys to age groups higher than their allowed maximum
    // from this particular value.
    const PublishedAgeRestrictionBaseKey =
        new Edx25519PublicKey("CH0VKFDZ2GWRWHQBBGEK9MWV5YDQVJ0RXEE0KYT3NMB69F0R96TG");

  .. ts:def:: AgeWithdrawRevealResponse

    interface AgeWithdrawRevealResponse {
      // List of the exchange's blinded RSA signatures on the new coins.
      ev_sigs : BlindedDenominationSignature[];
    }


.. _reserve-history:

---------------
Reserve History
---------------

.. http:get:: /reserves/$RESERVE_PUB/history

  Request information about the full history of
  a reserve or an account.

  **Request:**

  The GET request should come with the following HTTP headers:

  *If-None-Match*: The client MAY provide an ``If-None-Match`` header with an
  Etag.  In that case, the server MUST additionally respond with an ``304``
  status code in case the reserve history matches the provided Etag.

  *Taler-Reserve-History-Signature*: The client MUST provide Base-32 encoded
   EdDSA signature over a TALER_SIGNATURE_RESERVE_HISTORY_REQUEST made with
   the respective ``$RESERVE_PRIV``, affirming desire to download the current
   reserve transaction history.

  :query start=OFFSET: *Optional.* Only return reserve history entries with
                       offsets above the given OFFSET. Allows clients to not
                       retrieve history entries they already have.

  **Response:**

  :http:statuscode:`200 OK`:
    The exchange responds with a `ReserveHistory` object; the reserve was known to the exchange.
  :http:statuscode:`204 No content`:
    The reserve history is known, but at this point from the given starting point it is empty. Can only happen if OFFSET was positive.
  :http:statuscode:`304 Not modified`:
    The reserve history matches the one identified by the "If-none-match" HTTP header of the request.
  :http:statuscode:`403 Forbidden`:
    The *TALER_SIGNATURE_RESERVE_HISTORY_REQUEST* is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The reserve key does not belong to a reserve known to the exchange.

  **Details:**

  .. ts:def:: ReserveHistory

    interface ReserveHistory {
      // Balance left in the reserve.
      balance: Amount;

      // If set, gives the maximum age group that the client is required to set
      // during withdrawal.
      maximum_age_group: number;

      // Transaction history for this reserve.
      // May be partial (!).
      history: TransactionHistoryItem[];
    }

  Objects in the transaction history have the following format:

  .. ts:def:: TransactionHistoryItem

    // Union discriminated by the "type" field.
    type TransactionHistoryItem =
      | AccountSetupTransaction
      | ReserveWithdrawTransaction
      | ReserveAgeWithdrawTransaction
      | ReserveCreditTransaction
      | ReserveClosingTransaction
      | ReserveOpenRequestTransaction
      | ReserveCloseRequestTransaction
      | PurseMergeTransaction;

  .. ts:def:: AccountSetupTransaction

    interface AccountSetupTransaction {
      type: "SETUP";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // KYC fee agreed to by the reserve owner.
      kyc_fee: Amount;

      // Time when the KYC was triggered.
      kyc_timestamp: Timestamp;

      // Hash of the wire details of the account.
      // Note that this hash is unsalted and potentially
      // private (as it could be inverted), hence access
      // to this endpoint must be authorized using the
      // private key of the reserve.
      h_wire: HashCode;

      // Signature created with the reserve's private key.
      // Must be of purpose ``TALER_SIGNATURE_ACCOUNT_SETUP_REQUEST`` over
      // a ``TALER_AccountSetupRequestSignaturePS``.
      reserve_sig: EddsaSignature;

    }

  .. ts:def:: ReserveWithdrawTransaction

    interface ReserveWithdrawTransaction {
      type: "WITHDRAW";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // Amount withdrawn.
      amount: Amount;

      // Hash of the denomination public key of the coin.
      h_denom_pub: HashCode;

      // Hash of the blinded coin to be signed.
      h_coin_envelope: HashCode;

      // Signature over a `TALER_WithdrawRequestPS`
      // with purpose ``TALER_SIGNATURE_WALLET_RESERVE_WITHDRAW``
      // created with the reserve's private key.
      reserve_sig: EddsaSignature;

      // Fee that is charged for withdraw.
      withdraw_fee: Amount;
     }

  .. ts:def:: ReserveAgeWithdrawTransaction

    interface ReserveAgeWithdrawTransaction {
      type: "AGEWITHDRAW";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // Total Amount withdrawn.
      amount: Amount;

      // Commitment of all ``n*kappa`` blinded coins.
      h_commitment: HashCode;

      // Signature over a `TALER_AgeWithdrawRequestPS`
      // with purpose ``TALER_SIGNATURE_WALLET_RESERVE_AGE_WITHDRAW``
      // created with the reserve's private key.
      reserve_sig: EddsaSignature;

      // Fee that is charged for withdraw.
      withdraw_fee: Amount;
     }


  .. ts:def:: ReserveCreditTransaction

    interface ReserveCreditTransaction {
      type: "CREDIT";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // Amount deposited.
      amount: Amount;

      // Sender account ``payto://`` URL.
      sender_account_url: string;

      // Opaque identifier internal to the exchange that
      // uniquely identifies the wire transfer that credited the reserve.
      wire_reference: Integer;

      // Timestamp of the incoming wire transfer.
      timestamp: Timestamp;
    }


  .. ts:def:: ReserveClosingTransaction

    interface ReserveClosingTransaction {
      type: "CLOSING";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // Closing balance.
      amount: Amount;

      // Closing fee charged by the exchange.
      closing_fee: Amount;

      // Wire transfer subject.
      wtid: Base32;

      // ``payto://`` URI of the wire account into which the funds were returned to.
      receiver_account_details: string;

      // This is a signature over a
      // struct `TALER_ReserveCloseConfirmationPS` with purpose
      // ``TALER_SIGNATURE_EXCHANGE_RESERVE_CLOSED``.
      exchange_sig: EddsaSignature;

      // Public key used to create 'exchange_sig'.
      exchange_pub: EddsaPublicKey;

      // Time when the reserve was closed.
      timestamp: Timestamp;
    }


  .. ts:def:: ReserveOpenRequestTransaction

    interface ReserveOpenRequestTransaction {
      type: "OPEN";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // Open fee paid from the reserve.
      open_fee: Amount;

      // This is a signature over
      // a struct `TALER_ReserveOpenPS` with purpose
      // ``TALER_SIGNATURE_WALLET_RESERVE_OPEN``.
      reserve_sig: EddsaSignature;

      // Timestamp of the open request.
      request_timestamp: Timestamp;

      // Requested expiration.
      requested_expiration: Timestamp;

      // Requested number of free open purses.
      requested_min_purses: Integer;

    }

  .. ts:def:: ReserveCloseRequestTransaction

    interface ReserveCloseRequestTransaction {
      type: "CLOSE";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // This is a signature over
      // a struct `TALER_ReserveClosePS` with purpose
      // ``TALER_SIGNATURE_WALLET_RESERVE_CLOSE``.
      reserve_sig: EddsaSignature;

      // Target account ``payto://``, optional.
      h_payto?: PaytoHash;

      // Timestamp of the close request.
      request_timestamp: Timestamp;
    }

  .. ts:def:: ReserveCreditTransaction

    interface ReserveCreditTransaction {
      type: "CREDIT";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // Amount deposited.
      amount: Amount;

      // Sender account ``payto://`` URL.
      sender_account_url: string;

      // Opaque identifier internal to the exchange that
      // uniquely identifies the wire transfer that credited the reserve.
      wire_reference: Integer;

      // Timestamp of the incoming wire transfer.
      timestamp: Timestamp;
    }

  .. ts:def:: PurseMergeTransaction

    interface PurseMergeTransaction {
      type: "MERGE";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // SHA-512 hash of the contact of the purse.
      h_contract_terms: HashCode;

      // EdDSA public key used to approve merges of this purse.
      merge_pub: EddsaPublicKey;

      // Minimum age required for all coins deposited into the purse.
      min_age: Integer;

      // Number that identifies who created the purse
      // and how it was paid for.
      flags: Integer;

      // Purse public key.
      purse_pub: EddsaPublicKey;

      // EdDSA signature of the account/reserve affirming the merge
      // over a `TALER_AccountMergeSignaturePS`.
      // Must be of purpose ``TALER_SIGNATURE_ACCOUNT_MERGE``
      reserve_sig: EddsaSignature;

      // Client-side timestamp of when the merge request was made.
      merge_timestamp: Timestamp;

      // Indicative time by which the purse should expire
      // if it has not been merged into an account. At this
      // point, all of the deposits made should be
      // auto-refunded.
      purse_expiration: Timestamp;

      // Purse fee the reserve owner paid for the purse creation.
      purse_fee: Amount;

      // Total amount merged into the reserve.
      // (excludes fees).
      amount: Amount;

      // True if the purse was actually merged.
      // If false, only the purse_fee has an impact
      // on the reserve balance!
      merged: boolean;
    }


.. _coin-history:

------------
Coin History
------------

.. http:get:: /coins/$COIN_PUB/history

  Obtain the transaction history of a coin.  Used only in special cases, like
  when the exchange claims a double-spending error and the wallet does not
  believe it. Usually, the wallet knows the transaction history of each coin
  and thus has no need to inquire.

  **Request:**

  The GET request should come with the following HTTP headers:

  *If-None-Match*: The client MAY provide an ``If-None-Match`` header with an
  Etag.  In that case, the server MUST additionally respond with an ``304``
  status code in case the coin history matches the provided Etag.

  *Taler-Coin-History-Signature*: The client MUST provide Base-32 encoded
   EdDSA signature over a TALER_SIGNATURE_COIN_HISTORY_REQUEST made with
   the respective ``$RESERVE_PRIV``, affirming desire to download the current
   coin transaction history.

   :query start=OFFSET: *Optional.* Only return coin history entries with
                       offsets above the given OFFSET. Allows clients to not
                       retrieve history entries they already have.


  **Response:**

  :http:statuscode:`200 OK`:
    The operation succeeded, the exchange confirms that no double-spending took
    place.  The response will include a `CoinHistoryResponse` object.
  :http:statuscode:`204 No content`:
    The reserve history is known, but at this point from the given starting point it is empty. Can only happen if OFFSET was positive.
  :http:statuscode:`304 Not modified`:
    The coin history has not changed since the previous query (detected via Etag
    in "If-none-match" header).
  :http:statuscode:`403 Forbidden`:
    The *TALER_SIGNATURE_COIN_HISTORY_REQUEST* is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The coin public key is not (yet) known to the exchange.

  .. ts:def:: CoinHistoryResponse

    interface CoinHistoryResponse {
      // Current balance of the coin.
      balance: Amount;

      // Hash of the coin's denomination.
      h_denom_pub: HashCode;

      // Transaction history for the coin.
      history: CoinSpendHistoryItem[];
    }

  .. ts:def:: CoinSpendHistoryItem

    // Union discriminated by the "type" field.
    type CoinSpendHistoryItem =
      | CoinDepositTransaction
      | CoinMeltTransaction
      | CoinRefundTransaction
      | CoinRecoupTransaction
      | CoinOldCoinRecoupTransaction
      | CoinRecoupRefreshTransaction
      | CoinPurseDepositTransaction
      | CoinPurseRefundTransaction
      | CoinReserveOpenDepositTransaction;

  .. ts:def:: CoinDepositTransaction

    interface CoinDepositTransaction {
      type: "DEPOSIT";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // The total amount of the coin's value absorbed (or restored in the
      // case of a refund) by this transaction.
      // The amount given includes
      // the deposit fee. The current coin value can thus be computed by
      // subtracting this amount.
      amount: Amount;

      // Deposit fee.
      deposit_fee: Amount;

      // Public key of the merchant.
      merchant_pub: EddsaPublicKey;

      // Date when the operation was made.
      timestamp: Timestamp;

      // Date until which the merchant can issue a refund to the customer via the
      // exchange, possibly zero if refunds are not allowed.
      refund_deadline?: Timestamp;

      // Signature over `TALER_DepositRequestPS`, made by the customer with the
      // `coin's private key <coin-priv>`.
      coin_sig: EddsaSignature;

      // Hash of the bank account from where we received the funds.
      h_wire: HashCode;

      // Hash of the public denomination key used to sign the coin.
      // Needed because 'coin_sig' signs over this, and
      // that is important to fix the coin's denomination.
      h_denom_pub: HashCode;

      // Hash over the proposal data of the contract that
      // is being paid.
      h_contract_terms: HashCode;

    }

  .. ts:def:: CoinMeltTransaction

    interface CoinMeltTransaction {
      type: "MELT";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // The total amount of the coin's value absorbed by this transaction.
      // Note that for melt this means the amount given includes
      // the melt fee. The current coin value can thus be computed by
      // subtracting the amounts.
      amount: Amount;

      // Signature by the coin over a
      // `TALER_RefreshMeltCoinAffirmationPS` of
      // purpose ``TALER_SIGNATURE_WALLET_COIN_MELT``.
      coin_sig: EddsaSignature;

      // Melt fee.
      melt_fee: Amount;

      // Commitment from the melt operation.
      rc: TALER_RefreshCommitmentP;

      // Hash of the public denomination key used to sign the coin.
      // Needed because 'coin_sig' signs over this, and
      // that is important to fix the coin's denomination.
      h_denom_pub: HashCode;

    }

 .. ts:def:: CoinRefundTransaction

    interface CoinRefundTransaction {
      type: "REFUND";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // The total amount of the coin's value restored
      // by this transaction.
      // The amount given excludes the transaction fee.
      // The current coin value can thus be computed by
      // adding the amounts to the coin's denomination value.
      amount: Amount;

      // Refund fee.
      refund_fee: Amount;

      // Hash over the proposal data of the contract that
      // is being refunded.
      h_contract_terms: HashCode;

      // Refund transaction ID.
      rtransaction_id: Integer;

      // `EdDSA Signature <eddsa-sig>` authorizing the REFUND over a
      // `TALER_MerchantRefundConfirmationPS` with
      // purpose ``TALER_SIGNATURE_MERCHANT_REFUND_OK``. Made with
      // the `public key of the merchant <merchant-pub>`.
      merchant_sig: EddsaSignature;

    }

 .. ts:def:: CoinRecoupTransaction

    interface CoinRecoupTransaction {
      type: "RECOUP";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // The total amount of the coin's value absorbed
      // by this transaction.
      // The current coin value can thus be computed by
      // subtracting the amount from
      // the coin's denomination value.
      amount: Amount;

      // Date when the operation was made.
      timestamp: Timestamp;

      // Signature by the coin over a
      // `TALER_RecoupRequestPS` with purpose
      // ``TALER_SIGNATURE_WALLET_COIN_RECOUP``.
      coin_sig: EddsaSignature;

      // Hash of the public denomination key used to sign the coin.
      // Needed because 'coin_sig' signs over this, and
      // that is important to fix the coin's denomination.
      h_denom_pub: HashCode;

      // Coin blinding key.
      coin_blind: DenominationBlindingKeyP;

      // Reserve receiving the recoup.
      reserve_pub: EddsaPublicKey;

      // Signature by the exchange over a
      // `TALER_RecoupConfirmationPS`, must be
      // of purpose ``TALER_SIGNATURE_EXCHANGE_CONFIRM_RECOUP``.
      exchange_sig: EddsaSignature;

      // Public key of the private key used to create 'exchange_sig'.
      exchange_pub: EddsaPublicKey;

    }

 .. ts:def:: CoinOldCoinRecoupTransaction

    interface CoinOldCoinRecoupTransaction {
      type: "OLD-COIN-RECOUP";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // The total amount of the coin's value restored
      // by this transaction.
      // The current coin value can thus be computed by
      // adding the amount to the coin's denomination value.
      amount: Amount;

      // Date when the operation was made.
      timestamp: Timestamp;

      // Signature by the exchange over a
      // `TALER_RecoupRefreshConfirmationPS`
      // of purpose ``TALER_SIGNATURE_EXCHANGE_CONFIRM_RECOUP_REFRESH``.
      exchange_sig: EddsaSignature;

      // Public key of the private key used to create 'exchange_sig'.
      exchange_pub: EddsaPublicKey;

    }

 .. ts:def:: CoinRecoupRefreshTransaction

    interface CoinRecoupRefreshTransaction {
      type: "RECOUP-REFRESH";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // The total amount of the coin's value absorbed
      // by this transaction.
      // The current coin value can thus be computed by
      // subtracting this amounts from
      // the coin's denomination value.
      amount: Amount;

      // Date when the operation was made.
      timestamp: Timestamp;

      // Signature by the coin over a `TALER_RecoupRequestPS`
      // with purpose ``TALER_SIGNATURE_WALLET_COIN_RECOUP``.
      coin_sig: EddsaSignature;

      // Hash of the public denomination key used to sign the coin.
      // Needed because 'coin_sig' signs over this, and
      // that is important to fix the coin's denomination.
      h_denom_pub: HashCode;

      // Coin blinding key.
      coin_blind: DenominationBlindingKeyP;

      // Signature by the exchange over a
      // `TALER_RecoupRefreshConfirmationPS`
      // of purpose ``TALER_SIGNATURE_EXCHANGE_CONFIRM_RECOUP_REFRESH``.
      exchange_sig: EddsaSignature;

      // Public key used to sign 'exchange_sig'.
      exchange_pub: EddsaPublicKey;

      // Blinding factor of the revoked new coin.
      new_coin_blinding_secret: DenominationBlindingKeySecret;

      // Blinded public key of the revoked new coin.
      new_coin_ev: DenominationBlindingKeySecret;
    }

 .. ts:def:: CoinPurseDepositTransaction

    interface CoinPurseDepositTransaction {
      type: "PURSE-DEPOSIT";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // The total amount of the coin's value absorbed
      // by this transaction.
      // Note that this means the amount given includes
      // the deposit fee. The current coin value can thus be computed by
      // subtracting the amount from
      // the coin's denomination value.
      amount: Amount;

      // Deposit fee.
      deposit_fee: Amount;

      // Public key of the purse.
      purse_pub: EddsaPublicKey;

      // Date when the purse was set to expire.
      purse_expiration: Timestamp;

      // Signature by the coin over a
      // `TALER_PurseDepositSignaturePS` of
      // purpose ``TALER_SIGNATURE_PURSE_DEPOSIT``.
      coin_sig: EddsaSignature;

      // Hash of the public denomination key used to sign the coin.
      // Needed because 'coin_sig' signs over this, and
      // that is important to fix the coin's denomination.
      h_denom_pub: HashCode;

    }

 .. ts:def:: CoinPurseRefundTransaction

    interface CoinPurseRefundTransaction {
      type: "PURSE-REFUND";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // The total amount of the coin's value restored
      // by this transaction.
      // The amount given excludes the refund fee.
      // The current coin value can thus be computed by
      // adding the amount to the coin's denomination value.
      amount: Amount;

      // Refund fee (of the coin's denomination). The deposit
      // fee will be waived.
      refund_fee: Amount;

      // Public key of the purse that expired.
      purse_pub: EddsaPublicKey;

      // Signature by the exchange over a
      // ``TALER_CoinPurseRefundConfirmationPS``
      // of purpose ``TALER_SIGNATURE_EXCHANGE_CONFIRM_PURSE_REFUND``.
      exchange_sig: EddsaSignature;

     // Public key used to sign 'exchange_sig'.
      exchange_pub: EddsaPublicKey;

    }

 .. ts:def:: CoinReserveOpenDepositTransaction

    interface CoinReserveOpenDepositTransaction {
      type: "RESERVE-OPEN-DEPOSIT";

      // Offset of this entry in the reserve history.
      // Useful to request incremental histories via
      // the "start" query parameter.
      history_offset: Integer;

      // The total amount of the coin's value absorbed
      // by this transaction.
      // Note that this means the amount given includes
      // the deposit fee.
      coin_contribution: Amount;

      // Signature of the reserve open operation being paid for.
      reserve_sig: EddsaSignature;

      // Signature by the coin over a
      // `TALER_ReserveOpenDepositSignaturePS` of
      // purpose ``TALER_SIGNATURE_RESERVE_OPEN_DEPOSIT``.
      coin_sig: EddsaSignature;

    }


.. _deposit-par:

-------
Deposit
-------

Deposit operations are requested f.e. by a merchant during a transaction or a
bidder during an auction.

For the deposit operation during purchase, the merchant has to obtain the
deposit permission for a coin from their customer who owns the coin.  When
depositing a coin, the merchant is credited an amount specified in the deposit
permission, possibly a fraction of the total coin's value, minus the deposit
fee as specified by the coin's denomination.

For auctions, a bidder performs an deposit operation and provides all relevant
information for the auction policy (such as timeout and public key as bidder)
and can use the ``exchange_sig`` field from the `DepositSuccess` message as a
proof to the seller for the escrow of sufficient fund.


.. _deposit:

.. http:post:: /batch-deposit

  Deposit multiple coins and ask the exchange to transfer the given :ref:`amount`
  into the merchant's bank account.  This API is used by the merchant to redeem
  the digital coins.

  **Request:**

  The request body must be a `BatchDepositRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The operation succeeded, the exchange confirms that no double-spending took
    place.  The response will include a `DepositSuccess` object.
  :http:statuscode:`403 Forbidden`:
    One of the signatures is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    Either one of the denomination keys is not recognized (expired or invalid),
    or the wire type is not recognized.
    If a denomination key is unknown, the response will be
    a `DenominationUnknownMessage`.
  :http:statuscode:`409 Conflict`:
    The deposit operation has either failed because a coin has insufficient
    residual value, or because the same public key of a coin has been
    previously used with a different denomination.
    Which case it is
    can be decided by looking at the error code
    (``TALER_EC_EXCHANGE_DEPOSIT_CONFLICTING_CONTRACT`` (same coin used in different ways),
    ``TALER_EC_EXCHANGE_GENERIC_INSUFFICIENT_FUNDS`` (balance insufficient) or
    ``TALER_EC_EXCHANGE_GENERIC_COIN_CONFLICTING_DENOMINATION_KEY``
    (same coin public key, but different denomination)).
    The fields of the response are still evolving (see bug 7267),
    for now the format of the response is a `DepositDoubleSpendError`.
    The request should not be repeated again with this coin.
  :http:statuscode:`410 Gone`:
    The requested denomination key is not yet or no longer valid.
    It either before the validity start, past the expiration or was revoked. The response is a
    `DenominationExpiredMessage`. Clients must evaluate
    the error code provided to understand which of the
    cases this is and handle it accordingly.

  **Details:**

  .. ts:def:: BatchDepositRequest

    interface BatchDepositRequest {

      // The merchant's account details.
      merchant_payto_uri: string;

      // The salt is used to hide the ``payto_uri`` from customers
      // when computing the ``h_wire`` of the merchant.
      wire_salt: WireSalt;

      // SHA-512 hash of the contract of the merchant with the customer.  Further
      // details are never disclosed to the exchange.
      h_contract_terms: HashCode;

      // The list of coins that are going to be deposited with this Request.
      coins: BatchDepositRequestCoin[];

      // Timestamp when the contract was finalized.
      timestamp: Timestamp;

      // Indicative time by which the exchange undertakes to transfer the funds to
      // the merchant, in case of successful payment. A wire transfer deadline of 'never'
      // is not allowed.
      wire_transfer_deadline: Timestamp;

      // EdDSA `public key of the merchant <merchant-pub>`, so that the client can identify the
      // merchant for refund requests.
      merchant_pub: EddsaPublicKey;

      // Date until which the merchant can issue a refund to the customer via the
      // exchange, to be omitted if refunds are not allowed.
      //
      // THIS FIELD WILL BE DEPRICATED, once the refund mechanism becomes a
      // policy via extension.
      refund_deadline?: Timestamp;

      // CAVEAT: THIS IS WORK IN PROGRESS
      // (Optional) policy for the batch-deposit.
      // This might be a refund, auction or escrow policy.
      policy?: DepositPolicy;
    }

  .. ts:def:: BatchDepositRequestCoin

    interface BatchDepositRequestCoin {
      // EdDSA public key of the coin being deposited.
      coin_pub: EddsaPublicKey;

      // Hash of denomination RSA key with which the coin is signed.
      denom_pub_hash: HashCode;

      // Exchange's unblinded RSA signature of the coin.
      ub_sig: DenominationSignature;

      // Amount to be deposited, can be a fraction of the
      // coin's total value.
      contribution: Amount;

      // Signature over `TALER_DepositRequestPS`, made by the customer with the
      // `coin's private key <coin-priv>`.
      coin_sig: EddsaSignature;
    }

  .. ts:def:: DenominationSignature

    type DenominationSignature =
      | RsaDenominationSignature
      | CSDenominationSignature;

  .. ts:def:: RsaDenominationSignature

    interface RsaDenominationSignature {
      cipher: "RSA";

      // RSA signature
      rsa_signature: RsaSignature;
    }

  .. ts:def:: CSDenominationSignature

    interface CSDenominationSignature {
      type: "CS";

      // R value component of the signature.
      cs_signature_r: Cs25519Point;

      // s value component of the signature.
      cs_signature_s: Cs25519Scalar:

    }

  .. ts:def:: DepositPolicy

    type DepositPolicy =
    | PolicyMerchantRefund
    | PolicyBrandtVickreyAuction
    | PolicyEscrowedPayment;

  .. ts:def:: PolicyMerchantRefund

    // CAVEAT: THIS IS STILL WORK IN PROGRESS.
    // This policy is optional and might not be supported by the exchange.
    // If it does, the exchange MUST show support for this policy in the
    // ``extensions`` field in the response to ``/keys``.
    interface PolicyMerchantRefund {
      type: "merchant_refund";

      // EdDSA `public key of the merchant <merchant-pub>`, so that the client
      // can identify the merchant for refund requests.
      merchant_pub: EddsaPublicKey;

      // Date until which the merchant can issue a refund to the customer via
      // the ``/extensions/policy_refund``-endpoint of the exchange.
      deadline: Timestamp;
    }

  .. ts:def:: PolicyBrandtVickreyAuction

    // CAVEAT: THIS IS STILL WORK IN PROGRESS.
    // This policy is optional and might not be supported by the exchange.
    // If it does, the exchange MUST show support for this policy in the
    // ``extensions`` field in the response to ``/keys``.
    interface PolicyBrandtVickreyAuction {
      type: "brandt_vickrey_auction";

      // Public key of this bidder.
      //
      // The bidder uses this key to sign the auction information and
      // the messages it sends to the seller during the auction.
      bidder_pub: EddsaPublicKey;

      // Hash of the auction terms
      //
      // The hash should be taken over a normalized JSON object of type
      // `BrandtVickreyAuction`.
      h_auction: HashCode;

      // The amount that this bidder commits to for this auction
      //
      // This amount can be larger than the contribution of a single coin.
      // The bidder can increase funding of this auction policy by using
      // sufficiently many coins during the deposit operation (single or batch)
      // with the same policy.
      commitment: Amount;

      // Date until the auction must have been successfully executed and
      // a valid transcript provided to the
      // ``/extensions/policy_brandt_vickrey_auction``-endpoint of the
      // exchange.
      //
      // [If the auction has not been executed by then] OR [has been executed
      // before then, but this bidder did not win], the coin's value doesn't
      // change and the owner can refresh the coin.
      //
      // If this bidder won the auction, the winning price/amount from the
      // outcome will be substracted from the coin and transfered to the
      // merchant's ``payout_uri`` from the deposit request (minus a potential
      // auction fee).  For any remaining value, the bidder can refresh the
      // coin to retrieve change.
      deadline: Timestamp;
    }

  .. ts:def:: BrandtVickreyAuction

    // CAVEAT: THIS IS STILL WORK IN PROGRESS.
    // This structure defines an auction of Brandt-Vickory kind.
    // It is used for the `PolicyBrandtVickreyAuction`.
    interface BrandtVickreyAuction {
      // Start date of the auction
      time_start: Timestamp;

      // Maximum duration per round.  There are four rounds in an auction of
      // Brandt-Vickrey kind.
      time_round: RelativeTime;

      // This integer m refers to the (m+1)-type of the Brandt-Vickrey-auction.
      // - Type 0 refers to an auction with one highest-price winner,
      // - Type 1 refers to an auction with one winner, paying the second
      //   highest price,
      // - Type 2 refers to an auction with two winners, paying
      //   the third-highest price,
      // - etc.
      auction_type: number;

      // The vector of prices for the Brandt-Vickrey auction.  The values MUST
      // be in strictly increasing order.
      prices: Amount[];

      // The type of outcome of the auction.
      // In case the auction is declared public, each bidder can calculate the
      // winning price.  This field is not relevant for the replay of a
      // transcript, as the transcript must be provided by the seller who sees
      // the winner(s) and winning price of the auction.
      outcome_public: boolean;

      // The public key of the seller.
      pubkey: EddsaPublicKey;

      // The seller's account details.
      payto_uri: string;
    }


  .. ts:def:: PolicyEscrowedPayment

    // CAVEAT: THIS IS STILL WORK IN PROGRESS
    // This policy is optional and might not be supported by the exchange.
    // If it does, the exchange MUST show support for this policy in the
    // ``extensions`` field in the response to ``/keys``.
    interface PolicyEscrowedPayment {
      type: "escrowed_payment";

      // Public key of this trustor, the owner of the coins.
      //
      // To claim the deposit, the merchant must provide the valid signature
      // of the ``h_contract_terms`` field from the deposit, signed by _this_
      // key, to the ``/extensions/policy_escrow``-endpoint of the exchange,
      // after the date specified in ``not_before`` and before the date
      // specified in ``not_after``.
      trustor_pub: EddsaPublicKey;

      // Latest date by which the deposit must be claimed.  If the deposit
      // has not been claimed by that date, the deposited coins can be
      // refreshed by the (still) owner.
      deadline: Timestamp;
    }

  The deposit operation succeeds if the coin is valid for making a deposit and
  has enough residual value that has not already been deposited or melted.

  .. ts:def:: DepositSuccess

     interface DepositSuccess {
      // Optional base URL of the exchange for looking up wire transfers
      // associated with this transaction.  If not given,
      // the base URL is the same as the one used for this request.
      // Can be used if the base URL for ``/transactions/`` differs from that
      // for ``/coins/``, i.e. for load balancing.  Clients SHOULD
      // respect the ``transaction_base_url`` if provided.  Any HTTP server
      // belonging to an exchange MUST generate a 307 or 308 redirection
      // to the correct base URL should a client uses the wrong base
      // URL, or if the base URL has changed since the deposit.
      transaction_base_url?: string;

      // Timestamp when the deposit was received by the exchange.
      exchange_timestamp: Timestamp;

      // `Public EdDSA key of the exchange <sign-key-pub>` that was used to
      // generate the signature.
      // Should match one of the exchange's signing keys from ``/keys``.  It is given
      // explicitly as the client might otherwise be confused by clock skew as to
      // which signing key was used.
      exchange_pub: EddsaPublicKey;

      // Deposit confirmation signature from the exchange.
      // The EdDSA signature of `TALER_DepositConfirmationPS` using a current
      // `signing key of the exchange <sign-key-priv>` affirming the successful
      // deposit and that the exchange will transfer the funds after the refund
      // deadline, or as soon as possible if the refund deadline is zero.
      exchange_sig: EddsaSignature;
    }

  .. ts:def:: DepositDoubleSpendError

    interface DepositDoubleSpendError {

      // Must be TALER_EC_EXCHANGE_GENERIC_INSUFFICIENT_FUNDS
      code: Integer;

      // A string explaining that the user tried to
      // double-spend.
      hint: string;

      // EdDSA public key of a coin being double-spent.
      coin_pub: EddsaPublicKey;

      // Transaction history for the coin that is
      // being double-spended.
      // DEPRECATED! Will be removed soon. Use
      // GET /coins/$COIN_PUB to get the history!
      history: CoinSpendHistoryItem[];
    }


----------
Refreshing
----------

Refreshing creates ``n`` new coins from ``m`` old coins, where the sum of
denominations of the new coins must be smaller than the sum of the old coins'
denominations plus melting (refresh) and withdrawal fees charged by the exchange.
The refreshing API can be used by wallets to melt partially spent coins, making
transactions with the freshly exchangeed coins unlinkabe to previous transactions
by anyone except the wallet itself.

However, the new coins are linkable from the private keys of all old coins
using the ``/refresh/link`` request.  While ``/refresh/link`` must be implemented by
the exchange to achieve taxability, wallets do not really ever need that part of
the API during normal operation.


.. http:post:: /csr-melt

  Obtain exchange-side input values in preparation for a
  melt step for certain denomination cipher types,
  specifically at this point for Clause-Schnorr blind
  signatures.

  **Request:** The request body must be a `MeltPrepareRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The request was successful, and the response is a `MeltPrepareResponse`.  Note that repeating exactly the same request
    will again yield the same response (assuming none of the denomination is expired).
  :http:statuscode:`404 Not found`:
    A denomination key is not known to the exchange.
  :http:statuscode:`410 Gone`:
    A requested denomination key is not yet or no longer valid.
    It either before the validity start, past the expiration or was revoked. The response is a
    `DenominationExpiredMessage`. Clients must evaluate
    the error code provided to understand which of the
    cases this is and handle it accordingly.

  **Details:**

  .. ts:def:: MeltPrepareRequest

    interface WithdrawPrepareRequest {

      // Master seed for the Clause-schnorr R-value
      // creation.
      // Must not have been used in any prior request.
      rms: RefreshMasterSeed;

      // Array of denominations and coin offsets for
      // each of the fresh coins with a CS-cipher
      // denomination.
      nks: MeltPrepareDenomNonce[];

    }

  .. ts:def:: MeltPrepareDenomNonce

    interface MeltPrepareDenomNonce {

      // Offset of this coin in the list of
      // fresh coins. May not match the array offset
      // as the fresh coins may include non-CS
      // denominations as well.
      coin_offset: Integer;

      // Hash of the public key of the denomination the
      // request relates to. Must be a CS denomination type.
      denom_pub_hash: HashCode;
    }


  .. ts:def:: MeltPrepareResponse

    interface MeltPrepareResponse {
      // Responses for each request, in the same
      // order that was used in the request.
      ewvs: ExchangeWithdrawValue[];
    }


.. _refresh:
.. http:post:: /coins/$COIN_PUB/melt

  "Melts" a coin.  Invalidates the coins and prepares for exchanging of fresh
  coins.  Taler uses a global parameter ``kappa`` for the cut-and-choose
  component of the protocol, for which this request is the commitment.  Thus,
  various arguments are given ``kappa``-times in this step.  At present ``kappa``
  is always 3.

  The base URL for ``/coins/``-requests may differ from the main base URL of the
  exchange. The exchange MUST return a 307 or 308 redirection to the correct
  base URL if this is the case.

  :http:statuscode:`200 OK`:
    The request was successful.  The response body is `MeltResponse` in this case.
  :http:statuscode:`403 Forbidden`:
    One of the signatures is invalid.
  :http:statuscode:`404 Not found`:
    The exchange does not recognize the denomination key as belonging to the exchange,
    or it has expired.
    If the denomination key is unknown, the response will be
    a `DenominationUnknownMessage`.
  :http:statuscode:`409 Conflict`:
    The operation is not allowed as the coin has insufficient
    residual value, or because the same public key of the coin has been
    previously used with a different denomination.  Which case it is
    can be decided by looking at the error code
    (``TALER_EC_EXCHANGE_GENERIC_INSUFFICIENT_FUNDS`` or
    ``TALER_EC_EXCHANGE_GENERIC_COIN_CONFLICTING_DENOMINATION_KEY``).
    The response is `MeltForbiddenResponse` in both cases.
  :http:statuscode:`410 Gone`:
    The requested denomination key is not yet or no longer valid.
    It either before the validity start, past the expiration or was revoked. The response is a
    `DenominationExpiredMessage`. Clients must evaluate
    the error code provided to understand which of the
    cases this is and handle it accordingly.

  **Details:**


  .. ts:def:: MeltRequest

    interface MeltRequest {

      // Hash of the denomination public key, to determine total coin value.
      denom_pub_hash: HashCode;

      // Signature over the `coin public key <eddsa-coin-pub>` by the denomination.
      denom_sig: DenominationSignature;

      // Signature by the `coin <coin-priv>` over the melt commitment.
      confirm_sig: EddsaSignature;

      // Amount of the value of the coin that should be melted as part of
      // this refresh operation, including melting fee.
      value_with_fee: Amount;

      // Melt commitment.  Hash over the various coins to be withdrawn.
      // See also ``TALER_refresh_get_commitment()``.
      rc: TALER_RefreshCommitmentP;

      // Master seed for the Clause-schnorr R-value
      // creation. Must match the /csr-melt request.
      // Must not have been used in any prior melt request.
      // Must be present if one of the fresh coin's
      // denominations is of type Clause-Schnorr.
      rms?: RefreshMasterSeed;

      // IFF the denomination has age restriction support, the client MUST
      // provide the SHA256 hash of the age commitment of the coin.
      // MUST be omitted otherwise.
      age_commitment_hash?: AgeCommitmentHash;
    }

  For details about the HKDF used to derive the new coin private keys and
  the blinding factors from ECDHE between the transfer public keys and
  the private key of the melted coin, please refer to the
  implementation in ``libtalerutil``.

  .. ts:def:: MeltResponse

    interface MeltResponse {
      // Which of the ``kappa`` indices does the client not have to reveal.
      noreveal_index: Integer;

      // Signature of `TALER_RefreshMeltConfirmationPS` whereby the exchange
      // affirms the successful melt and confirming the ``noreveal_index``.
      exchange_sig: EddsaSignature;

      // `Public EdDSA key <sign-key-pub>` of the exchange that was used to generate the signature.
      // Should match one of the exchange's signing keys from ``/keys``.  Again given
      // explicitly as the client might otherwise be confused by clock skew as to
      // which signing key was used.
      exchange_pub: EddsaPublicKey;

      // Base URL to use for operations on the refresh context
      // (so the reveal operation).  If not given,
      // the base URL is the same as the one used for this request.
      // Can be used if the base URL for ``/refreshes/`` differs from that
      // for ``/coins/``, i.e. for load balancing.  Clients SHOULD
      // respect the refresh_base_url if provided.  Any HTTP server
      // belonging to an exchange MUST generate a 307 or 308 redirection
      // to the correct base URL should a client uses the wrong base
      // URL, or if the base URL has changed since the melt.
      //
      // When melting the same coin twice (technically allowed
      // as the response might have been lost on the network),
      // the exchange may return different values for the ``refresh_base_url``.
      refresh_base_url?: string;

    }


  .. ts:def:: MeltForbiddenResponse

    interface MeltForbiddenResponse {
      // Text describing the error.
      hint: string;

      // Detailed error code.
      code: Integer;

      // The transaction list of the respective coin that failed to have sufficient funds left.
      // Note that only the transaction history for one bogus coin is given,
      // even if multiple coins would have failed the check.
      history: CoinSpendHistoryItem[];
    }


.. http:post:: /refreshes/$RCH/reveal

  Reveal previously committed values to the exchange, except for the values
  corresponding to the ``noreveal_index`` returned by the ``/coins/``-melt step.

  The $RCH is the hash over the refresh commitment from the ``/coins/``-melt step
  (note that the value is calculated independently by both sides and has never
  appeared *explicitly* in the protocol before).

  The base URL for ``/refreshes/``-requests may differ from the main base URL of
  the exchange. Clients SHOULD respect the ``refresh_base_url`` returned for the
  coin during melt operations. The exchange MUST return a
  307 or 308 redirection to the correct base URL if the client failed to
  respect the ``refresh_base_url`` or if the allocation has changed.

  Errors such as failing to do proper arithmetic when it comes to calculating
  the total of the coin values and fees are simply reported as bad requests.
  This includes issues such as melting the same coin twice in the same session,
  which is simply not allowed.  However, theoretically it is possible to melt a
  coin twice, as long as the ``value_with_fee`` of the two melting operations is
  not larger than the total remaining value of the coin before the melting
  operations. Nevertheless, this is not really useful.

  :http:statuscode:`200 OK`:
    The transfer private keys matched the commitment and the original request was well-formed.
    The response body is a `RevealResponse`.
  :http:statuscode:`409 Conflict`:
    There is a problem between the original commitment and the revealed private
    keys.  The returned information is proof of the mismatch, and therefore
    rather verbose, as it includes most of the original /refresh/melt request,
    but of course expected to be primarily used for diagnostics.
    The response body is a `RevealConflictResponse`.
  :http:statuscode:`410 Gone`:
    The requested denomination key (for the fresh coins) is not yet or no longer valid.
    It either before the validity start, past the expiration or was revoked. The response is a
    `DenominationExpiredMessage`. Clients must evaluate
    the error code provided to understand which of the
    cases this is and handle it accordingly.

  **Details:**

  Request body contains a JSON object with the following fields:

  .. ts:def:: RevealRequest

    interface RevealRequest {

      // Array of ``n`` new hash codes of denomination public keys to order.
      new_denoms_h: HashCode[];

      // Array of ``n`` entries with blinded coins,
      // matching the respective entries in ``new_denoms``.
      coin_evs: CoinEnvelope[];

      // ``kappa - 1`` transfer private keys (ephemeral ECDHE keys).
      transfer_privs: EddsaPrivateKey[];

      // Transfer public key at the ``noreveal_index``.
      transfer_pub: EddsaPublicKey;

      // Array of ``n`` signatures made by the wallet using the old coin's private key,
      // used later to verify the /refresh/link response from the exchange.
      // Signs over a `TALER_CoinLinkSignaturePS`.
      link_sigs: EddsaSignature[];

      // IFF the corresponding denomination has support for age restriction,
      // the client MUST provide the original age commitment, i. e. the
      // vector of public keys.
      // The size of the vector MUST be the number of age groups as defined by the
      // Exchange in the field ``.age_groups`` of the extension ``age_restriction``.
      old_age_commitment?: Edx25519PublicKey[];

    }


  .. ts:def:: RevealResponse

    interface RevealResponse {
      // List of the exchange's blinded RSA signatures on the new coins.
      ev_sigs : Array<{ ev_sig: BlindedDenominationSignature }>;
    }


  .. ts:def:: RevealConflictResponse

    interface RevealConflictResponse {
      // Text describing the error.
      hint: string;

      // Detailed error code.
      code: Integer;

      // Commitment as calculated by the exchange from the revealed data.
      rc_expected: HashCode;

    }


.. http:get:: /coins/$COIN_PUB/link

  Link the old public key of a melted coin to the coin(s) that were exchanged during the refresh operation.

  **Request:**

  **Response:**

  :http:statuscode:`200 OK`:
    All commitments were revealed successfully.  The exchange returns an array (typically consisting of only one element), in which each each element of the array contains a `LinkResponse` entry with information about a melting session that the coin was used in.
  :http:statuscode:`404 Not found`:
    The exchange has no linkage data for the given public key, as the coin has not
    yet been involved in a refresh operation.

  **Details:**

  .. ts:def:: LinkResponse

    interface LinkResponse {
      // Transfer ECDHE public key corresponding to the ``coin_pub``, used to
      // compute the blinding factor and private key of the fresh coins.
      transfer_pub: EcdhePublicKey;

      // Array with (encrypted/blinded) information for each of the coins
      // exchangeed in the refresh operation.
      new_coins: NewCoinInfo[];
    }

  .. ts:def:: NewCoinInfo

    interface NewCoinInfo {
      // RSA public key of the exchangeed coin.
      denom_pub: RsaPublicKey;

      // Exchange's blinded signature over the fresh coin.
      ev_sig: BlindedDenominationSignature;

      // Blinded coin.
      coin_ev : CoinEnvelope;

      // Values contributed by the exchange during the
      // withdraw operation (see /csr-melt).
      ewv: ExchangeWithdrawValue;

      // Offset of this coin in the refresh operation.
      // Input needed to derive the private key.
      coin_idx: Integer;

      // Signature made by the old coin over the refresh request.
      // Signs over a `TALER_CoinLinkSignaturePS`.
      link_sig: EddsaSignature;

    }


------
Recoup
------

This API is only used if the exchange is either about to go out of
business or has had its private signing keys compromised (so in
either case, the protocol is only used in **abnormal**
situations).  In the above cases, the exchange signals to the
wallets that the emergency cash back protocol has been activated
by putting the affected denomination keys into the cash-back
part of the ``/keys`` response.  If and only if this has happened,
coins that were signed with those denomination keys can be cashed
in using this API.

.. http:post:: /coins/$COIN_PUB/recoup

  Demand that a coin be refunded via wire transfer to the original owner.

  The base URL for ``/coins/``-requests may differ from the main base URL of the
  exchange. The exchange MUST return a 307 or 308 redirection to the correct
  base URL if this is the case.

  The remaining amount on the coin will be credited to the reserve
  that ``$COIN_PUB`` was withdrawn from.

  Note that the original withdrawal fees will **not** be recouped.


  **Request:** The request body must be a `RecoupRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The request was successful, and the response is a `RecoupWithdrawalConfirmation`.
    Note that repeating exactly the same request
    will again yield the same response, so if the network goes down during the
    transaction or before the client can commit the coin signature to disk, the
    coin is not lost.
  :http:statuscode:`403 Forbidden`:
    The coin's signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The denomination key is unknown, or the blinded
    coin is not known to have been withdrawn.
    If the denomination key is unknown, the response will be
    a `DenominationUnknownMessage`.
  :http:statuscode:`409 Conflict`:
    The operation is not allowed as the coin has insufficient
    residual value, or because the same public key of the coin has been
    previously used with a different denomination.  Which case it is
    can be decided by looking at the error code
    (usually ``TALER_EC_EXCHANGE_GENERIC_INSUFFICIENT_FUNDS``).
    The response is a `DepositDoubleSpendError`.
  :http:statuscode:`410 Gone`:
    The requested denomination key is not yet or no longer valid.
    It either before the validity start, past the expiration or was not yet revoked. The response is a
    `DenominationExpiredMessage`. Clients must evaluate
    the error code provided to understand which of the
    cases this is and handle it accordingly.

  **Details:**

  .. ts:def:: RecoupRequest

    interface RecoupRequest {
      // Hash of denomination public key, specifying the type of coin the client
      // would like the exchange to pay back.
      denom_pub_hash: HashCode;

      // Signature over the `coin public key <eddsa-coin-pub>` by the denomination.
      denom_sig: DenominationSignature;

      // Exchange-contributed values during the refresh
      // operation (see /csr-withdraw).
      ewv: ExchangeWithdrawValue;

      // Signature of `TALER_RecoupRequestPS` created with
      // the `coin's private key <coin-priv>`.
      coin_sig: EddsaSignature;

      // Coin's blinding factor.
      coin_blind_key_secret: DenominationBlindingKeySecret;

      // Nonce that was used by the exchange to derive
      // its private inputs from during withdraw. Only
      // present if the cipher of the revoked denomination
      // is of type Clause-Schnorr (CS).
      cs_nonce?: CSNonce;
    }


  .. ts:def:: RecoupWithdrawalConfirmation

    interface RecoupWithdrawalConfirmation {
      // Public key of the reserve that will receive the recoup.
      reserve_pub: EddsaPublicKey;
    }


.. http:post:: /coins/$COIN_PUB/recoup-refresh

  Demand that a coin be refunded via wire transfer to the original owner.

  The base URL for ``/coins/``-requests may differ from the main base URL of the
  exchange. The exchange MUST return a 307 or 308 redirection to the correct
  base URL if this is the case.

  The remaining amount on the coin will be credited to
  the old coin that ``$COIN_PUB`` was refreshed from.

  Note that the original refresh fees will **not** be recouped.


  **Request:** The request body must be a `RecoupRefreshRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The request was successful, and the response is a `RecoupRefreshConfirmation`.
    Note that repeating exactly the same request
    will again yield the same response, so if the network goes down during the
    transaction or before the client can commit the coin signature to disk, the
    coin is not lost.
  :http:statuscode:`403 Forbidden`:
    The coin's signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The denomination key is unknown, or the blinded
    coin is not known to have been withdrawn.
    If the denomination key is unknown, the response will be
    a `DenominationUnknownMessage`.
  :http:statuscode:`409 Conflict`:
    The operation is not allowed as the coin has insufficient
    residual value, or because the same public key of the coin has been
    previously used with a different denomination.  Which case it is
    can be decided by looking at the error code
    (usually ``TALER_EC_EXCHANGE_GENERIC_INSUFFICIENT_BALANCE``).
    The response is a `DepositDoubleSpendError`.
  :http:statuscode:`410 Gone`:
    The requested denomination key is not yet or no longer valid.
    It either before the validity start, past the expiration or was not yet revoked. The response is a
    `DenominationExpiredMessage`. Clients must evaluate
    the error code provided to understand which of the
    cases this is and handle it accordingly.

  **Details:**

  .. ts:def:: RecoupRefreshRequest

    interface RecoupRefreshRequest {
      // Hash of denomination public key, specifying the type of coin the client
      // would like the exchange to pay back.
      denom_pub_hash: HashCode;

      // Signature over the `coin public key <eddsa-coin-pub>` by the denomination.
      denom_sig: DenominationSignature;

      // Exchange-contributed values during the refresh
      // operation (see /csr-melt).
      ewv: ExchangeWithdrawValue;

      // Signature of `TALER_RecoupRequestPS` created with
      // the `coin's private key <coin-priv>`.
      coin_sig: EddsaSignature;

      // Coin's blinding factor.
      coin_blind_key_secret: DenominationBlindingKeySecret;

      // Nonce that was used by the exchange to derive
      // its private inputs from during withdraw. Only
      // present if the cipher of the revoked denomination
      // is of type Clause-Schnorr (CS).
      cs_nonce?: CSNonce;
    }


  .. ts:def:: RecoupRefreshConfirmation

    interface RecoupRefreshConfirmation {
      // Public key of the old coin that will receive the recoup.
      old_coin_pub: EddsaPublicKey;
    }


-----------------------
Tracking wire transfers
-----------------------

This API is used by merchants that need to find out which wire
transfers (from the exchange to the merchant) correspond to which deposit
operations.  Typically, a merchant will receive a wire transfer with a
**wire transfer identifier** and want to know the set of deposit
operations that correspond to this wire transfer.  This is the
preferred query that merchants should make for each wire transfer they
receive.  If a merchant needs to investigate a specific deposit
operation (i.e. because it seems that it was not paid), then the
merchant can also request the wire transfer identifier for a deposit
operation.

Sufficient information is returned to verify that the coin signatures
are correct. This also allows governments to use this API when doing
a tax audit on merchants.

Naturally, the returned information may be sensitive for the merchant.
We do not require the merchant to sign the request, as the same requests
may also be performed by the government auditing a merchant.
However, wire transfer identifiers should have sufficient entropy to
ensure that obtaining a successful reply by brute-force is not practical.
Nevertheless, the merchant should protect the wire transfer identifiers
from his bank statements against unauthorized access, lest his income
situation is revealed to an adversary. (This is not a major issue, as
an adversary that has access to the line-items of bank statements can
typically also view the balance.)


.. http:get:: /transfers/$WTID

  Provides deposits associated with a given wire transfer.  The
  wire transfer identifier (WTID) and the base URL for tracking
  the wire transfer are both given in the wire transfer subject.

  **Request:**

  **Response:**

  :http:statuscode:`200 OK`:
    The wire transfer is known to the exchange, details about it follow in the body.
    The body of the response is a `TrackTransferResponse`.
  :http:statuscode:`404 Not found`:
    The wire transfer identifier is unknown to the exchange.

  .. ts:def:: TrackTransferResponse

    interface TrackTransferResponse {
      // Actual amount of the wire transfer, excluding the wire fee.
      total: Amount;

      // Applicable wire fee that was charged.
      wire_fee: Amount;

      // Public key of the merchant (identical for all deposits).
      merchant_pub: EddsaPublicKey;

      // Hash of the payto:// account URI (identical for all deposits).
      h_payto: PaytoHash;

      // Time of the execution of the wire transfer by the exchange.
      execution_time: Timestamp;

      // Details about the deposits.
      deposits: TrackTransferDetail[];

      // Signature from the exchange made with purpose
      // ``TALER_SIGNATURE_EXCHANGE_CONFIRM_WIRE_DEPOSIT``
      // over a `TALER_WireDepositDataPS`.
      exchange_sig: EddsaSignature;

      // Public EdDSA key of the exchange that was used to generate the signature.
      // Should match one of the exchange's signing keys from ``/keys``.  Again given
      // explicitly as the client might otherwise be confused by clock skew as to
      // which signing key was used.
      exchange_pub: EddsaSignature;
    }

  .. ts:def:: TrackTransferDetail

    interface TrackTransferDetail {
      // SHA-512 hash of the contact of the merchant with the customer.
      h_contract_terms: HashCode;

      // Coin's public key, both ECDHE and EdDSA.
      coin_pub: CoinPublicKey;

      // The total amount the original deposit was worth.
      deposit_value: Amount;

      // Applicable fees for the deposit.
      deposit_fee: Amount;

    }

.. http:get:: /deposits/$H_WIRE/$MERCHANT_PUB/$H_CONTRACT_TERMS/$COIN_PUB

  Provide the wire transfer identifier associated with an (existing) deposit operation.
  The arguments are the hash of the merchant's payment details (H_WIRE), the
  merchant's public key (EdDSA), the hash of the contract terms that were paid
  (H_CONTRACT_TERMS) and the public key of the coin used for the payment (COIN_PUB).

  **Request:**

  :query merchant_sig: EdDSA signature of the merchant made with purpose
    ``TALER_SIGNATURE_MERCHANT_TRACK_TRANSACTION`` over a
    ``TALER_DepositTrackPS``, affirming that it is really the merchant who
    requires obtaining the wire transfer identifier.
  :query timeout_ms=NUMBER: *Optional.* If specified, the exchange will wait
    up to ``NUMBER`` milliseconds for completion of a deposit operation before
    sending the HTTP response.

  **Response:**

  :http:statuscode:`200 OK`:
    The deposit has been executed by the exchange and we have a wire transfer identifier.
    The response body is a `TrackTransactionResponse` object.
  :http:statuscode:`202 Accepted`:
    The deposit request has been accepted for processing, but was not yet
    executed.  Hence the exchange does not yet have a wire transfer identifier.  The
    merchant should come back later and ask again.
    The response body is a `TrackTransactionAcceptedResponse`.
  :http:statuscode:`403 Forbidden`:
    A signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The deposit operation is unknown to the exchange.

  **Details:**

  .. ts:def:: TrackTransactionResponse

    interface TrackTransactionResponse {

      // Raw wire transfer identifier of the deposit.
      wtid: Base32;

      // When was the wire transfer given to the bank.
      execution_time: Timestamp;

      // The contribution of this coin to the total (without fees)
      coin_contribution: Amount;

      // Binary-only Signature_ with purpose ``TALER_SIGNATURE_EXCHANGE_CONFIRM_WIRE``
      // over a `TALER_ConfirmWirePS`
      // whereby the exchange affirms the successful wire transfer.
      exchange_sig: EddsaSignature;

      // Public EdDSA key of the exchange that was used to generate the signature.
      // Should match one of the exchange's signing keys from /keys.  Again given
      // explicitly as the client might otherwise be confused by clock skew as to
      // which signing key was used.
      exchange_pub: EddsaPublicKey;
    }

  .. ts:def:: TrackTransactionAcceptedResponse

    interface TrackTransactionAcceptedResponse {

      // Legitimization target that the merchant should
      // use to check for its KYC status using
      // the ``/kyc-check/$REQUIREMENT_ROW/...`` endpoint.
      // Optional, not present if the deposit has not
      // yet been aggregated to the point that a KYC
      // need has been evaluated.
      requirement_row?: Integer;

      // Current AML state for the target account. Non-zero
      // values indicate that the transfer is blocked due to
      // AML enforcement.
      aml_decision: Integer;

      // True if the KYC check for the merchant has been
      // satisfied.  False does not mean that KYC
      // is strictly needed, unless also a
      // legitimization_uuid is provided.
      kyc_ok: boolean;

      // Time by which the exchange currently thinks the deposit will be executed.
      // Actual execution may be later if the KYC check is not satisfied by then.
      execution_time: Timestamp;
    }


.. _exchange_refund:

-------
Refunds
-------

.. http:post:: /coins/$COIN_PUB/refund

  Undo deposit of the given coin, restoring its value.

  **Request:** The request body must be a `RefundRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The operation succeeded, the exchange confirms that the coin can now be refreshed.  The response will include a `RefundSuccess` object.
  :http:statuscode:`403 Forbidden`:
    Merchant signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The refund operation failed as we could not find a matching deposit operation (coin, contract, transaction ID and merchant public key must all match).
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`409 Conflict`:
    The exchange has previously received a refund request for the same coin, merchant and contract, but specifying a different amount for the same refund transaction ID.  The response will be a `RefundFailure` object.
  :http:statuscode:`410 Gone`:
    It is too late for a refund by the exchange, the money was already sent to the merchant.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`412 Precondition failed`:
    The request transaction ID is identical to a previous refund request by the same
    merchant for the same coin and contract, but the refund amount differs. (The
    failed precondition is that the ``rtransaction_id`` is not unique.)
    The response will be a `RefundFailure` object with the conflicting refund request.

  **Details:**

  .. ts:def:: RefundRequest

     interface RefundRequest {

      // Amount to be refunded, can be a fraction of the
      // coin's total deposit value (including deposit fee);
      // must be larger than the refund fee.
      refund_amount: Amount;

      // SHA-512 hash of the contact of the merchant with the customer.
      h_contract_terms: HashCode;

      // 64-bit transaction id of the refund transaction between merchant and customer.
      rtransaction_id: Integer;

      // EdDSA public key of the merchant.
      merchant_pub: EddsaPublicKey;

      // EdDSA signature of the merchant over a
      // `TALER_RefundRequestPS` with purpose
      // ``TALER_SIGNATURE_MERCHANT_REFUND``
      // affirming the refund.
      merchant_sig: EddsaPublicKey;

    }

  .. ts:def:: RefundSuccess

    interface RefundSuccess {

      // The EdDSA :ref:`signature` (binary-only) with purpose
      // ``TALER_SIGNATURE_EXCHANGE_CONFIRM_REFUND`` over
      // a `TALER_RecoupRefreshConfirmationPS`
      // using a current signing key of the
      // exchange affirming the successful refund.
      exchange_sig: EddsaSignature;

      // Public EdDSA key of the exchange that was used to generate the signature.
      // Should match one of the exchange's signing keys from ``/keys``.  It is given
      // explicitly as the client might otherwise be confused by clock skew as to
      // which signing key was used.
      exchange_pub: EddsaPublicKey;
   }

  .. ts:def:: RefundFailure

    interface RefundFailure {

      // Numeric error code unique to the condition, which can be either
      // related to the deposit value being insufficient for the requested
      // refund(s), or the requested refund conflicting due to refund
      // transaction number re-use (with different amounts).
      code: Integer;

      // Human-readable description of the error message.
      hint: string;

      // Information about the conflicting refund request(s).
      // This will not be the full history of the coin, but only
      // the relevant subset of the transactions.
      history: CoinSpendHistoryItem[];
    }


.. _exchange_w2w:

--------------------------
Wallet-to-wallet transfers
--------------------------

.. http:get:: /purses/$PURSE_PUB/merge
.. http:get:: /purses/$PURSE_PUB/deposit

  Obtain information about a purse.  Depending on the suffix,
  the long-polling (if any) will wait for either a merge or
  a deposit event.

  **Request:**

  :query timeout_ms=NUMBER: *Optional.*  If specified,
    the exchange
    will wait up to ``NUMBER`` milliseconds for completion
    of a merge operation before sending the HTTP response.
  :query deposit_timeout_ms=NUMBER: *Optional.*  If specified,
    the exchange
    will wait up to ``NUMBER`` milliseconds for completion
    of a deposit operation before sending the HTTP response.

  **Response:**

  :http:statuscode:`200 OK`:
    The operation succeeded, the exchange provides details
    about the purse.
    The response will include a `PurseStatus` object.
  :http:statuscode:`404 Not found`:
    The purse is unknown to the exchange.
  :http:statuscode:`410 Gone`:
    The purse expired before the deposit or merge was completed.

  **Details:**

  .. ts:def:: PurseStatus

     interface PurseStatus {

      // Total amount deposited into the purse so far.
      // If 'total_deposit_amount' minus 'deposit_fees'
      // exceeds 'merge_value_after_fees', and a
      // 'merge_request' exists for the purse, then the
      // purse will (have been) merged with the account.
      balance: Amount;

      // When does the purge expire.
      purse_expiration: Timestamp;

      // Time of the merge, missing if "never".
      merge_timestamp?: Timestamp;

      // Time of the deposits being complete, missing if "never".
      // Note that this time may not be "stable": once sufficient
      // deposits have been made, is "now" before the purse
      // expiration, and otherwise set to the purse expiration.
      // However, this should also not be relied upon. The key
      // property is that it is either "never" or in the past.
      deposit_timestamp?: Timestamp;

      // Time when the purse expires and
      // funds that were not merged are refunded
      // on the deposited coins.
      // FIXME: Document the exchange protocol version
      //        in which this field became available.
      purse_expiration: Timestamp;

      // EdDSA signature of the exchange over a
      // `TALER_PurseStatusResponseSignaturePS`
      // with purpose ``TALER_SIGNATURE_PURSE_STATUS_RESPONSE``
      // affirming the purse status.
      exchange_sig: EddsaSignature;

      // EdDSA public key exchange used for 'exchange_sig'.
      exchange_pub: EddsaPublicKey;

    }



.. http:post:: /purses/$PURSE_PUB/create

  Create a purse by depositing money into it. First step of a PUSH payment.

  **Request:**

  The request body must be a `PurseCreate` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The operation succeeded, the exchange confirms that all
    coins were deposited into the purse.
    The response will include a `PurseDepositSuccess` object.
  :http:statuscode:`403 Forbidden`:
    A coin, denomination or contract signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not Found`:
    The denomination of one of the coins is unknown to the exchange.
  :http:statuscode:`409 Conflict`:
    The deposit operation has either failed because a coin has insufficient
    residual value, or because the same public key of the coin has been
    previously used with a different denomination, or because a purse with
    the same public key but different meta data was created previously.
    Which case it is
    can be decided by looking at the error code
    (``TALER_EC_EXCHANGE_GENERIC_INSUFFICIENT_FUNDS`` or
    ``TALER_EC_EXCHANGE_GENERIC_COIN_CONFLICTING_DENOMINATION_KEY`` or
    ``TALER_EC_EXCHANGE_PURSE_CREATE_CONFLICTING_META_DATA`` or
    ``TALER_EC_EXCHANGE_PURSE_DEPOSIT_CONFLICTING_META_DATA`` or
    ``TALER_EC_EXCHANGE_PURSE_ECONTRACT_CONFLICTING_META_DATA``).
    The specific fields of the response depend on the error code
    and include the signatures (and what was signed over) proving the
    conflict.
  :http:statuscode:`425 Too Early`:
    This response type is used if the given purse expiration time
    is too far in the future (at least from the perspective
    of the exchange). Thus, retrying at a later time may
    succeed. The client should look at the ``Date:`` header
    of the response to see if a minor time difference is to
    blame and possibly adjust the request accordingly.
    (Note: this status code is not yet used.)


  **Details:**

  .. ts:def:: PurseCreate

    interface PurseCreate {

      // Total value of the purse, excluding fees.
      amount: Amount;

      // Minimum age required for all coins deposited into the purse.
      min_age: Integer;

      // Optional encrypted contract, in case the buyer is
      // proposing the contract and thus establishing the
      // purse with the payment.
      econtract?: EncryptedContract;

      // EdDSA public key used to approve merges of this purse.
      merge_pub: EddsaPublicKey;

      // EdDSA signature of the purse over a
      // `TALER_PurseRequestSignaturePS`
      // of purpose ``TALER_SIGNATURE_WALLET_PURSE_CREATE``
      // confirming the key
      // invariants associated with the purse.
      // (amount, h_contract_terms, expiration).
      purse_sig: EddsaSignature;

      // SHA-512 hash of the contact of the purse.
      h_contract_terms: HashCode;

      // Array of coins being deposited into the purse.
      // Maximum length is 128.
      deposits: PurseDeposit[];

      // Indicative time by which the purse should expire
      // if it has not been merged into an account. At this
      // point, all of the deposits made will be auto-refunded.
      purse_expiration: Timestamp;

    }

  .. ts:def:: EncryptedContract

    interface EncryptedContract {

      // Encrypted contract.
      econtract: string;

      // Signature over the (encrypted) contract.
      econtract_sig: EddsaSignature;

      // Ephemeral public key for the DH operation to decrypt the encrypted contract.
      contract_pub: EddsaPublicKey;

    }

  .. ts:def:: PurseDeposit

    interface PurseDeposit {

      // Amount to be deposited, can be a fraction of the
      // coin's total value.
      amount: Amount;

      // Hash of denomination RSA key with which the coin is signed.
      denom_pub_hash: HashCode;

      // Exchange's unblinded RSA signature of the coin.
      ub_sig: DenominationSignature;

      // Age commitment for the coin, if the denomination is age-restricted.
      age_commitment?: AgeCommitment;

      // Attestation for the minimum age, if the denomination is age-restricted.
      attest?: Attestation;

      // Signature over `TALER_PurseDepositSignaturePS`
      // of purpose ``TALER_SIGNATURE_WALLET_PURSE_DEPOSIT``
      // made by the customer with the
      // `coin's private key <coin-priv>`.
      coin_sig: EddsaSignature;

      // Public key of the coin being deposited into the purse.
      coin_pub: EddsaPublicKey;

    }

  .. ts:def:: PurseDepositSuccess

     interface PurseDepositSuccess {

      // Total amount deposited into the purse so far (without fees).
      total_deposited: Amount;

      // Time at the exchange.
      exchange_timestamp: Timestamp;

      // EdDSA signature of the exchange affirming the payment,
      // of purpose ``TALER_SIGNATURE_PURSE_DEPOSIT_CONFIRMED``
      // over a `TALER_PurseDepositConfirmedSignaturePS`.
      // Signs over the above and the purse public key and
      // the hash of the contract terms.
      exchange_sig: EddsaSignature;

      // public key used to create the signature.
      exchange_pub: EddsaPublicKey;

    }

  .. ts:def:: PurseConflict

    // Union discriminated by the "code" field.
    type PurseConflict =
    | DepositDoubleSpendError
    | PurseCreateConflict
    | PurseDepositConflict
    | PurseContractConflict;

  .. ts:def:: PurseCreateConflict

    interface PurseCreateConflict {
      // Must be equal to TALER_EC_EXCHANGE_PURSE_CREATE_CONFLICTING_META_DATA
      code: Integer;

      // Total amount to be merged into the reserve.
      // (excludes fees).
      amount: Amount;

      // Minimum age required for all coins deposited into the purse.
      min_age: Integer;

      // Indicative time by which the purse should expire
      // if it has not been merged into an account. At this
      // point, all of the deposits made should be
      // auto-refunded.
      purse_expiration: Timestamp;

      // EdDSA signature of the purse over
      // `TALER_PurseMergeSignaturePS` of
      // purpose ``TALER_SIGNATURE_WALLET_PURSE_MERGE``
      // confirming that the
      // above details hold for this purse.
      purse_sig: EddsaSignature;

      // SHA-512 hash of the contact of the purse.
      h_contract_terms: HashCode;

      // EdDSA public key used to approve merges of this purse.
      merge_pub: EddsaPublicKey;
    }

  .. ts:def:: PurseDepositConflict

    interface PurseDepositConflict {
      // Must be equal to TALER_EC_EXCHANGE_PURSE_DEPOSIT_CONFLICTING_META_DATA
      code: Integer;

      // Public key of the coin being deposited into the purse.
      coin_pub: EddsaPublicKey;

      // Signature over `TALER_PurseDepositSignaturePS`
      // of purpose ``TALER_SIGNATURE_WALLET_PURSE_DEPOSIT``
      // made by the customer with the
      // `coin's private key <coin-priv>`.
      coin_sig: EddsaSignature;

      // Target exchange URL for the purse. Not present for the
      // same exchange.
      partner_url?: string;

      // Amount to be contributed to the purse by this coin.
      amount: Amount;

    }

  .. ts:def:: PurseContractConflict

    interface PurseContractConflict {
      // Must be equal to TALER_EC_EXCHANGE_PURSE_ECONTRACT_CONFLICTING_META_DATA
      code: Integer;

      // Hash of the encrypted contract.
      h_econtract: HashCode;

      // Signature over the contract.
      econtract_sig: EddsaSignature;

      // Ephemeral public key for the DH operation to decrypt the contract.
      contract_pub: EddsaPublicKey;

    }



.. http:delete:: /purses/$PURSE_PUB

  Delete a purse that is unmerged and not yet expired. Refunds any money that
  is already in the purse.

  **Request:**

  The request body must be empty, as recommended for HTTP delete in general.
  To authorize the request, the header must contain a
  ``Taler-Purse-Signature: $PURSE_SIG`` where ``$PURSE_SIG`` is the Crockford base32-encoded
  EdDSA signature of purpose TALER_SIGNATURE_WALLET_PURSE_DELETE.

  **Response:**

  :http:statuscode:`204 No Content`:
    The operation succeeded, the exchange confirms that the purse
    was deleted.
  :http:statuscode:`403 Forbidden`:
    The signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not Found`:
    The purse is not known. Might have already been deleted previously.
  :http:statuscode:`409 Conflict`:
    It is too late to delete the purse, its fate (merge or expiration)
    was already decided.


.. http:post:: /purses/$PURSE_PUB/merge

  Merge purse with account, adding the value of the purse into
  the account.  Endpoint to be used by the receiver of a PUSH payment.

  **Request:**

  The request body must be a `MergeRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The operation succeeded, the exchange confirms that the
    funds were merged into the account.
    The response will include a `MergeSuccess` object.
  :http:statuscode:`402 Payment Required`:
    The purse is not yet full and more money needs to be deposited
    before the merge can be made.
  :http:statuscode:`403 Forbidden`:
    The signature of the merge request or the reserve was invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The merge operation failed as we could not find the purse
    or the partner exchange.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`409 Conflict`:
    The purse was already merged into a different reserve.
    The response will include a `MergeConflict` object.
  :http:statuscode:`410 Gone`:
    The purse has already expired and thus can no longer be merged.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`451 Unavailable For Legal Reasons`:
    This account has not yet passed the KYC checks.
    The client must pass KYC checks before proceeding with the merge.
    The response will be an `KycNeededRedirect` object.

  **Details:**

  .. ts:def:: MergeRequest

    interface MergeRequest {

      // payto://-URI of the account the purse is to be merged into.
      // Must be of the form: 'payto://taler/$EXCHANGE_URL/$RESERVE_PUB'.
      payto_uri: string;

      // EdDSA signature of the account/reserve affirming the merge
      // over a `TALER_AccountMergeSignaturePS`.
      // Must be of purpose ``TALER_SIGNATURE_ACCOUNT_MERGE``
      reserve_sig: EddsaSignature;

      // EdDSA signature of the merge private key affirming the merge
      // over a `TALER_PurseMergeSignaturePS`.
      // Must be of purpose ``TALER_SIGNATURE_PURSE_MERGE``.
      merge_sig: EddsaSignature;

      // Client-side timestamp of when the merge request was made.
      merge_timestamp: Timestamp;

    }

  .. ts:def:: MergeSuccess

     interface MergeSuccess {

      // Amount merged (excluding deposit fees).
      merge_amount: Amount;

      // Time at which the merge came into effect.
      // Maximum of the "payment_timestamp" and the
      // "merge_timestamp".
      exchange_timestamp: Timestamp;

      // EdDSA signature of the exchange affirming the merge of
      // purpose ``TALER_SIGNATURE_PURSE_MERGE_SUCCESS``
      // over `TALER_PurseMergeSuccessSignaturePS`.
      // Signs over the above and the account public key.
      exchange_sig: EddsaSignature;

      // public key used to create the signature.
      exchange_pub: EddsaPublicKey;

    }

  .. ts:def:: MergeConflict

    interface MergeConflict {

      // Client-side timestamp of when the merge request was made.
      merge_timestamp: Timestamp;

      // EdDSA signature of the purse private key affirming the merge
      // over a `TALER_PurseMergeSignaturePS`.
      // Must be of purpose ``TALER_SIGNATURE_PURSE_MERGE``.
      merge_sig: EddsaSignature;

      // Base URL of the exchange receiving the payment, only present
      // if the exchange hosting the reserve is not this exchange.
      partner_url?: string;

      // Public key of the reserve that the purse was merged into.
      reserve_pub: EddsaPublicKey;
    }



.. http:post:: /reserves/$RESERVE_PUB/purse

  Create purse for an account.  First step of a PULL payment.

  **Request:**

  The request body must be a `ReservePurseRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The operation succeeded, the exchange confirms that the
    purse was allocated.
    The response will include a `PurseDepositSuccess` object.
  :http:statuscode:`402 Payment Required`:
    The account needs to contain more funding to create more purses.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`403 Forbidden`:
    Account or contract signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The purse creation operation failed as we could not find the reserve.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`409 Conflict`:
    The purse creation failed because a purse with
    the same public key but different meta data was
    created previously.  Which specific conflict it is
    can be decided by looking at the error code
    (``TALER_EC_EXCHANGE_PURSE_CREATE_CONFLICTING_META_DATA`` or
    ``TALER_EC_EXCHANGE_PURSE_DEPOSIT_CONFLICTING_META_DATA`` or
    ``TALER_EC_EXCHANGE_PURSE_ECONTRACT_CONFLICTING_META_DATA``).
    The specific fields of the response depend on the error code
    and include the signatures (and what was signed over) proving the
    conflict.
    The response will be a `PurseConflict` response
    (but not a `DepositDoubleSpendError`).
  :http:statuscode:`451 Unavailable For Legal Reasons`:
    This account has not yet passed the KYC checks.
    The client must pass KYC checks before proceeding with the merge.
    The response will be an `KycNeededRedirect` object.

  **Details:**

  .. ts:def:: ReservePurseRequest

    interface ReservePurseRequest {

      // Minimum amount that must be credited to the reserve, that is
      // the total value of the purse minus the deposit fees.
      // If the deposit fees are lower, the contribution to the
      // reserve can be higher!
      purse_value: Amount;

      // Minimum age required for all coins deposited into the purse.
      min_age: Integer;

      // Purse fee the reserve owner is willing to pay
      // for the purse creation. Optional, if not present
      // the purse is to be created from the purse quota
      // of the reserve.
      purse_fee: Amount;

      // Optional encrypted contract, in case the buyer is
      // proposing the contract and thus establishing the
      // purse with the payment.
      econtract?: EncryptedContract;

      // EdDSA public key used to approve merges of this purse.
      merge_pub: EddsaPublicKey;

      // EdDSA signature of the purse private key affirming the merge
      // over a `TALER_PurseMergeSignaturePS`.
      // Must be of purpose ``TALER_SIGNATURE_PURSE_MERGE``.
      merge_sig: EddsaSignature;

      // EdDSA signature of the account/reserve affirming the merge.
      // Must be of purpose ``TALER_SIGNATURE_WALLET_ACCOUNT_MERGE``
      reserve_sig: EddsaSignature;

      // Purse public key.
      purse_pub: EddsaPublicKey;

      // EdDSA signature of the purse over
      // `TALER_PurseRequestSignaturePS` of
      // purpose ``TALER_SIGNATURE_PURSE_REQUEST``
      // confirming that the
      // above details hold for this purse.
      purse_sig: EddsaSignature;

      // SHA-512 hash of the contact of the purse.
      h_contract_terms: HashCode;

      // Client-side timestamp of when the merge request was made.
      merge_timestamp: Timestamp;

      // Indicative time by which the purse should expire
      // if it has not been paid.
      purse_expiration: Timestamp;

    }


.. http:post:: /purses/$PURSE_PUB/deposit

  Deposit money into a purse. Used by the buyer for a PULL payment.

  **Request:**

  The request body must be a `PurseDeposits` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The operation succeeded, the exchange confirms that all
    coins were deposited into the purse.
    The response will include a `PurseDepositSuccess` object.
  :http:statuscode:`403 Forbidden`:
    A coin or denomination signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The purse is unknown.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`409 Conflict`:
    The deposit operation has either failed because a coin has insufficient
    residual value, or because the same public key of the coin has been
    previously used with a different denomination.  Which case it is
    can be decided by looking at the error code
    (``TALER_EC_EXCHANGE_GENERIC_INSUFFICIENT_FUNDS`` or
    ``TALER_EC_EXCHANGE_GENERIC_COIN_CONFLICTING_DENOMINATION_KEY`` or
    ``TALER_EC_EXCHANGE_PURSE_DEPOSIT_CONFLICTING_META_DATA``).
    This response comes with a standard `PurseConflict` response
    (alas some cases are impossible).
  :http:statuscode:`410 Gone`:
    The purse has expired.


  **Details:**

   .. ts:def:: PurseDeposits

    interface PurseDeposits {

      // Array of coins to deposit into the purse.
      deposits: PurseDeposit[];
    }

  .. ts:def:: PurseDeposit

    interface PurseDeposit {

      // Amount to be deposited, can be a fraction of the
      // coin's total value.
      amount: Amount;

      // Hash of denomination RSA key with which the coin is signed.
      denom_pub_hash: HashCode;

      // Exchange's unblinded RSA signature of the coin.
      ub_sig: DenominationSignature;

      // Age commitment for the coin, if the denomination is age-restricted.
      age_commitment?: AgeCommitment;

      // Attestation for the minimum age, if the denomination is age-restricted.
      attest?: Attestation;

      // Signature over `TALER_PurseDepositSignaturePS`
      // of purpose ``TALER_SIGNATURE_WALLET_PURSE_DEPOSIT``
      // made by the customer with the
      // `coin's private key <coin-priv>`.
      coin_sig: EddsaSignature;

      // Public key of the coin being deposited into the purse.
      coin_pub: EddsaPublicKey;

    }

  .. ts:def:: PurseDepositSuccess

     interface PurseDepositSuccess {

      // Total amount paid into the purse.
      total_deposited: Amount;

      // Total amount expected in the purse.
      purse_value_after_fees: Amount;

      // Time at which the deposit came into effect.
      exchange_timestamp: Timestamp;

      // Indicative time by which the purse should expire
      // if it has not been merged into an account. At this
      // point, all of the deposits made will be auto-refunded.
      purse_expiration: Timestamp;

      // SHA-512 hash of the contact of the purse.
      h_contract_terms: HashCode;

      // EdDSA signature of the exchange affirming the payment,
      // of purpose ``TALER_SIGNATURE_PURSE_DEPOSIT_CONFIRMED``
      // over a `TALER_PurseDepositConfirmedSignaturePS`.
      // Signs over the above and the purse public key and
      // the hash of the contract terms.
      exchange_sig: EddsaSignature;

      // public key used to create the signature.
      exchange_pub: EddsaPublicKey;

    }

  .. ts:def:: AgeCommitment

     // AgeCommitment is an array of public keys, one for each age group of the
     // age-restricted denomination.
     type AgeCommitment = Edx25519PublicKey[];

  .. ts:def:: Attestation

     // An attestation for a minimum age is an Edx25519 signature of the age
     // with purpose ``TALER_SIGNATURE_WALLET_AGE_ATTESTATION``.
     type Attestation = string;

.. _exchange_wads:


----
Wads
----

  .. note::

     This is a draft API that is not yet implemented.


These endpoints are used to manage exchange-to-exchange payments in support of
wallet-to-wallet payments.  Only another exchange should access this endpoint.


.. http:get:: /wads/$WAD_ID

  Obtain information about a wad.

  **Request:**

  **Response:**

  :http:statuscode:`200 OK`:
    The operation succeeded, the exchange provides details
    about the wad.
    The response will include a `WadDetails` object.
  :http:statuscode:`404 Not found`:
    The wad is unknown to the exchange.

  **Details:**

  .. ts:def:: WadDetails

     interface WadDetails {

      // Total transfer amount claimed by the exchange.
      total: Amount;

      // Indicative time by which the wad was given to the
      // bank to execute the wire transfer.
      wad_execution_time: Timestamp;

      // Transfers aggregated in the wad.
      items: WadItem[];

      // EdDSA signature of the exchange affirming the wad
      // data is correct, must be over `TALER_WadDataSignaturePS`
      // and of purpose ``TALER_SIGNATURE_WAD_DATA``.
      exchange_sig: EddsaSignature;

      // public key used to create the signature.
      exchange_pub: EddsaPublicKey;
     }

  Objects in the wad item list have the following format:

  .. ts:def:: WadItem

    interface WadItem {

      // Amount in the purse.
      amount: Amount;

      // payto://-URI of the account the purse is to be merged into.
      // Must be of the form: 'payto://taler/EXCHANGE_URL/RESERVE_PUB'.
      payto_uri: string;

      // Purse public key.
      purse_pub: EddsaPublicKey;

      // Hash of the contract.
      h_contract: HashCode;

      // Indicative time by which the purse should expire
      // if it has not been paid.
      purse_expiration: Timestamp;

      // Client-side timestamp of when the merge request was made.
      merge_timestamp: Timestamp;

      // Signature created with the reserve's private key.
      // Must be of purpose ``TALER_SIGNATURE_ACCOUNT_MERGE``
      // and over `TALER_AccountMergeSignaturePS`.
      reserve_sig: EddsaSignature;

      // Signature created with the purse's private key.
      // Must be of purpose ``TALER_SIGNATURE_PURSE_MERGE``
      // and over `TALER_PurseMergeSignaturePS`.
      purse_sig: EddsaSignature;

      // Deposit fees that were charged to the purse.
      deposit_fees: Amount;

      // Wad fees that was charged to the purse.
      wad_fees: Amount;
    }


------------------
KYC status updates
------------------

.. http:post:: /kyc-wallet

  Setup KYC identification for a wallet.  Returns the KYC UUID.

  **Request:**

  The request body must be a `WalletKycRequest` object.

  **Response:**

  :http:statuscode:`204 No Content`:
    KYC is disabled at this exchange, or the balance
    is below the threshold that requires KYC, or this
    wallet already satisfied the KYC check for the
    given balance.
  :http:statuscode:`403 Forbidden`:
    The provided signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`451 Unavailable for Legal Reasons`:
    The wallet must undergo a KYC check. A KYC ID was created.
    The response will be a `WalletKycUuid` object.

  **Details:**

  .. ts:def:: WalletKycRequest

     interface WalletKycRequest {

      // Balance threshold (not necessarily exact balance)
      // to be crossed by the wallet that (may) trigger
      // additional KYC requirements.
      balance: Amount;

      // EdDSA signature of the wallet affirming the
      // request, must be of purpose
      // ``TALER_SIGNATURE_WALLET_ACCOUNT_SETUP``
      reserve_sig: EddsaSignature;

      // long-term wallet reserve-account
      // public key used to create the signature.
      reserve_pub: EddsaPublicKey;
    }

  .. ts:def:: WalletKycUuid

     interface WalletKycUuid {

       // UUID that the wallet should use when initiating
       // the KYC check.
       requirement_row: number;

       // Hash of the payto:// account URI for the wallet.
       h_payto: PaytoHash;

     }


.. http:get:: /kyc-check/$REQUIREMENT_ROW/$H_PAYTO/$USERTYPE

  Check or update KYC status of a particular payment target.
  Returns the current KYC status of the account and, if
  negative, returns the URL where the KYC process can be
  initiated.  The ``$REQUIREMENT_ROW`` must have been
  returned previously from an exchange API endpoint that
  determined that KYC was needed.  The ``$H_PATYO`` must be
  the hash of the payto:// URI of the payment target.
  The ``$USERTYPE`` states whether the entity to perform
  the KYC is an "individual" or "business".

  **Request:**

  :query timeout_ms=NUMBER: *Optional.*  If specified, the exchange will
    wait up to ``timeout_ms`` milliseconds if the payment target
    is currently not legitimized.  Ignored if the payment target
    is already legitimized. Note that the legitimization would be
    triggered by another request to the same endpoint with a valid
    ``token``.

  **Response:**

  :http:statuscode:`200 Ok`:
    The KYC operation succeeded, the exchange confirms that the
    payment target already authorized to transact.
    The response will be an `AccountKycStatus` object.
  :http:statuscode:`202 Accepted`:
    The user should be redirected to the provided location to perform
    the required KYC checks to open the account. Afterwards, the
    ``/kyc-check/`` request should be repeated.
    The response will be an `AccountKycRedirect` object.
  :http:statuscode:`204 No content`:
    The exchange is not configured to perform KYC and thus
    generally all accounts are simply considered legitimate.
  :http:statuscode:`402 Payment Required`:
    The client must pay the KYC fee for the KYC process.
    **This is currently not implemented, see #7365.**
  :http:statuscode:`403 Forbidden`:
    The provided hash does not match the payment target.
  :http:statuscode:`404 Not found`:
    The payment target is unknown.
  :http:statuscode:`451 Unavailable for Legal Reasons`:
    The transaction cannot be completed due to AML rules.
    The response will be a `AccountAmlBlocked` object.

  **Details:**

  .. ts:def:: AccountKycStatus

    interface AccountKycStatus {

      // Details about the KYC check that the user
      // passed.
      kyc_details: KycDetails;

      // Current time of the exchange, used as part of
      // what the exchange signs over.
      now: Timestamp;

      // EdDSA signature of the exchange affirming the account
      // is KYC'ed, must be of purpose
      // ``TALER_SIGNATURE_EXCHANGE_ACCOUNT_SETUP_SUCCESS``
      // and over ``TALER_AccountSetupStatusSignaturePS``.
      exchange_sig: EddsaSignature;

      // public key used to create the signature.
      exchange_pub: EddsaPublicKey;

      // Current AML state for the target account. Non-zero
      // values indicate that the transfer is blocked due to
      // AML enforcement.
      aml_status: Integer;

    }

  .. ts:def:: AccountKycRedirect

    interface AccountKycRedirect {

      // URL that the user should open in a browser to
      // proceed with the KYC process.
      kyc_url: string;

      // Current AML state for the target account. Non-zero
      // values indicate that the transfer is blocked due to
      // AML enforcement.
      aml_status: Integer;

    }

  .. ts:def:: AccountAmlBlocked

    interface AccountAmlBlocked {

      // Current AML state for the target account. Non-zero
      // values indicate that the transfer is blocked due to
      // AML enforcement.
      aml_status: Integer;

    }

  .. ts:def:: KycDetails

    // Object that specifies which KYC checks are satisfied.
    interface KycDetails {

      // Keys are the names of the check(s).
      // The values are for now always empty objects.

    }

.. http:get:: /kyc-proof/$PROVIDER_SECTION?state=$H_PAYTO

  Update KYC status of a particular payment target. Provides
  information to the exchange that allows it to verify that the
  user has completed the KYC process. The details depend on
  the logic, which is selected by the $PROVIDER_SECTION.

  **Request:**

  Details on the request depend on the specific KYC logic
  that was used.

  If the KYC plugin logic is OAuth 2.0, the query parameters are:

  :query code=CODE:  OAuth 2.0 code argument.
  :query state=STATE:  OAuth 2.0 state argument with the H_PAYTO.

  .. note::

    Depending on the OAuth variant used, additional
    query parameters may need to be passed here.

  **Response:**

  :http:statuscode:`302 Found`:
    The KYC operation succeeded and the
    payment target is now authorized to transact.
    The browser is redirected to a human-readable
    page configured by the exchange operator.
  :http:statuscode:`401 Unauthorized`:
    The provided authorization token is invalid.
  :http:statuscode:`404 Not found`:
    The payment target is unknown.
  :http:statuscode:`502 Bad Gateway`:
    The exchange received an invalid reply from the
    legitimization service.
  :http:statuscode:`504 Gateway Timeout`:
    The exchange did not receive a reply from the legitimization
    service within a reasonable time period.


.. http:get:: /kyc-webhook/$PROVIDER_SECTION/*
.. http:post:: /kyc-webhook/$PROVIDER_SECTION/*
.. http:get:: /kyc-webhook/$LOGIC/*
.. http:post:: /kyc-webhook/$LOGIC/*

  Update KYC status of a particular payment target. Provides
  information to the KYC logic of the exchange that allows
  it to verify that the user has completed the KYC process.
  May be a GET or a POST request, depending on $LOGIC or
  $PROVIDER_SECTION.

  **Request:**

  Details on the request depend on the specific KYC logic
  that was used.

  **Response:**

  :http:statuscode:`204 No content`:
    The webhook succeeded.
  :http:statuscode:`404 Not found`:
    The specified logic is unknown.


---------------
Reserve control
---------------

This section describes the reserve control API which can be used to (1)
prevent a reserve from expiring (which is useful if the reserve is used for
rewards), to (2) pay an annual fee to allow a number of purses to be created
for the respective reserve without paying a purse fee each time, to (3) obtain
KYC information associated with a reserve to prove the identity of the person
sending an invoice to the payer, and to (4) close a reserve before it would
naturally expire and possibly (5) wire the funds to a designated account.

  .. note::

     This section is about a proposed API. It is not implemented. See also DD 31.

.. http:post:: /reserves/$RESERVE_PUB/open

  Request keeping a reserve open for rewards or invoicing.

  **Request:**

  The request body must be a `ReserveOpenRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The exchange responds with a `ReserveOpenResponse` object.
  :http:statuscode:`402 Payment Required`:
    The exchange responds with a `ReserveOpenFailure` object when
    the payment offered is insufficient for the requested operation.
  :http:statuscode:`403 Forbidden`:
    The *TALER_SIGNATURE_WALLET_RESERVE_OPEN* signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The reserve key does not belong to a reserve known to the exchange.
  :http:statuscode:`409 Conflict`:
    The balance of the reserve or of a coin was insufficient.
    Which case it is can be decided by looking at the error code
    (``TALER_EC_EXCHANGE_GENERIC_INSUFFICIENT_FUNDS`` or
    ``TALER_EC_EXCHANGE_GENERIC_COIN_CONFLICTING_DENOMINATION_KEY`` or
    ``TALER_EC_EXCHANGE_OPEN_INSUFFICIENT_FUNDS``).
    The specific fields of the response depend on the error code
    and include the signatures (and what was signed over) proving the
    conflict.
    The response is `WithdrawError` object or a `DepositDoubleSpendError`
    depending on the error type.
  :http:statuscode:`451 Unavailable For Legal Reasons`:
    This account has not yet passed the KYC checks.
    The client must pass KYC checks before the reserve can be opened.
    The response will be an `KycNeededRedirect` object.

  **Details:**

  .. ts:def:: ReserveOpenRequest

    interface ReserveOpenRequest {
      // Signature of purpose
      // ``TALER_SIGNATURE_WALLET_RESERVE_OPEN`` over
      // a `TALER_ReserveOpenPS`.
      reserve_sig: EddsaSignature;

      // Array of payments made towards the cost of the
      // operation.
      payments: OpenPaymentDetail[];

      // Amount to be paid from the reserve for this
      // operation.
      reserve_payment: Amount;

      // Time when the client made the request.
      // Timestamp must be reasonably close to the time of
      // the exchange, otherwise the exchange may reject
      // the request (with a status code of 400).
      request_timestamp: Timestamp;

      // Desired new expiration time for the reserve.
      // If the reserve would expire before this time,
      // the exchange will charge account fees (and
      // possibly KYC fees) until the expiration time
      // exceeds this timestamp. Note that the exchange
      // will refuse requests (with a status code of 400)
      // if the time is so far in the future that the
      // fees are not yet known (see /keys).
      reserve_expiration: Timestamp;

      // Desired open purse limit. Can be used to pay the
      // annual account fee more than once to get a larger
      // purse limit.
      purse_limit: Integer;

    }

  .. ts:def:: ReserveOpenResponse

    interface ReserveOpenResponse {
      // Transaction cost for extending the expiration time.
      // Excludes KYC fees.
      open_cost: Amount;

      // Current expiration time for the reserve.
      reserve_expiration: Timestamp;
    }

  .. ts:def:: ReserveOpenFailure

    interface ReserveOpenFailure {
      // Transaction cost that should have been paid
      // to extending the reserve as requested.
      // Excludes KYC fees.
      open_cost: Amount;

      // Remaining expiration time for the reserve.
      reserve_expiration: Timestamp;
    }

  .. ts:def:: OpenPaymentDetail

    interface OpenPaymentDetail {

      // Contribution of this coin to the overall amount.
      // Can be a fraciton of the coin's total value.
      amount: Amount;

      // Hash of denomination RSA key with which the coin is signed.
      denom_pub_hash: HashCode;

      // Exchange's unblinded RSA signature of the coin.
      ub_sig: DenominationSignature;

      // Age commitment for the coin, if the denomination is age-restricted.
      age_commitment?: AgeCommitment;

      // Signature over `TALER_ReserveOpenDepositSignaturePS`
      // of purpose ``TALER_SIGNATURE_WALLET_RESERVE_OPEN_DEPOSIT``
      // made by the customer with the
      // `coin's private key <coin-priv>`.
      coin_sig: EddsaSignature;

      // Public key of the coin being used to pay for
      // opening the reserve.
      coin_pub: EddsaPublicKey;

    }


.. http:get:: /reserves-attest/$RESERVE_PUB

  Request list of available KYC attributes about the owner of a reserve.
  Used as a preliminary step to find out which subsets of attributes the
  exchange could provide signatures over.

  **Response:**

  :http:statuscode:`200 OK`:
    The exchange responds with a `ReserveKycAttributes` object.
  :http:statuscode:`404 Not found`:
    The reserve key does not belong to a reserve known to the exchange.
  :http:statuscode:`409 Conflict`:
    The exchange does not have the requested KYC information.

  **Details:**

  .. ts:def:: ReserveKycAttributes

    interface ReserveKycAttributes {
      // Array of KYC attributes available.  The attribute names
      // listed are expected to be from the respective GANA
      // registry.
      details: string[];
    }


.. http:post:: /reserves-attest/$RESERVE_PUB

  Request signed KYC information about the owner of a reserve.
  This can be used by a reserve owner to include a proof
  of their identity in invoices.

  **Request:**

  The request body must be a `ReserveAttestRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The exchange responds with a `ReserveAttestResponse` object.
  :http:statuscode:`403 Forbidden`:
    The *TALER_SIGNATURE_WALLET_KYC_DETAILS* signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The reserve key does not belong to a reserve known to the exchange.
  :http:statuscode:`409 Conflict`:
    The exchange does not have the requested KYC information.

  **Details:**

  .. ts:def:: ReserveAttestRequest

    interface ReserveAttestRequest {
      // Signature of purpose
      // ``TALER_SIGNATURE_WALLET_ATTEST_DETAILS`` over
      // a `TALER_WalletReserveAttestRequestSignaturePS`.
      reserve_sig: EddsaSignature;

      // Client's time for the request.
      request_timestamp: Timestamp;

      // Array of KYC attributes requested.
      details: string[];
    }

  .. ts:def:: ReserveAttestResponse

    interface ReserveAttestResponse {
      // Signature of purpose
      // ``TALER_SIGNATURE_EXCHANGE_RESERVE_ATTEST_DETAILS`` over
      // a `TALER_ExchangeAttestPS`.
      exchange_sig: EddsaSignature;

      // Exchange public key used to create the
      // signature.
      exchange_pub: EddsaPublicKey;

      // Time when the exchange created the signature.
      exchange_timestamp: Timestamp;

      // Expiration time for the provided information.
      expiration_time: Timestamp;

      // KYC details (key-value pairs) as requested.
      // The keys will match the elements of the
      // ``details`` array from the request.
      attributes: Object;
    }


.. http:post:: /reserves/$RESERVE_PUB/close

  Force immediate closure of a reserve. Does not actually
  delete the reserve or the KYC data, but merely forces
  the reserve's current balance to be wired back to the
  account where it originated from, or another account of
  the user's choosing if they performed the required KYC
  check and designated another target account.

  **Request:**

  The request body must be a `ReserveCloseRequest` object.

  **Response:**

  :http:statuscode:`200 OK`:
    The exchange responds with a `ReserveCloseResponse` object.
  :http:statuscode:`403 Forbidden`:
    The *TALER_SIGNATURE_WALLET_RESERVE_CLOSE* signature is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The reserve key does not belong to a reserve known to the exchange.
  :http:statuscode:`409 Conflict`:
    No target account was given, and the exchange does not know an
    origin account for this reserve.
  :http:statuscode:`451 Unavailable For Legal Reasons`:
    This account has not yet passed the KYC checks, hence wiring
    funds to a non-origin account is not allowed.
    The client must pass KYC checks before the reserve can be opened.
    The response will be an `KycNeededRedirect` object.

  **Details:**

  .. ts:def:: ReserveCloseRequest

    interface ReserveCloseRequest {
      // Signature of purpose
      // ``TALER_SIGNATURE_WALLET_RESERVE_CLOSE`` over
      // a `TALER_ReserveCloseRequestSignaturePS`.
      reserve_sig: EddsaSignature;

      // Time when the client made the request.
      // Timestamp must be reasonably close to the time of
      // the exchange, otherwise the exchange may reject
      // the request (with a status code of 400).
      request_timestamp: Timestamp;

      // payto://-URI of the account the reserve balance is to be
      // wired to.  Must be of the form: 'payto://$METHOD' for a
      // wire method supported by this exchange (if the
      // method is not supported, this is a bad request (400)).
      // If not given, the reserve's origin account
      // will be used. If no origin account is known for the
      // reserve and not given, this is a conflict (409).
      payto_uri?: string;

    }

  .. ts:def:: ReserveCloseResponse

    interface ReserveCloseResponse {

      // Actual amount that will be wired (excludes closing fee).
      wire_amount: Amount;

    }


.. _delete-reserve:

.. http:DELETE:: /reserves/$RESERVE_PUB

  Forcefully closes a reserve.
  The request header must contain an *Account-Request-Signature*.
  Note: this endpoint is not currently implemented!

  **Request:**

  *Account-Request-Signature*: The client must provide Base-32 encoded EdDSA signature made with ``$ACCOUNT_PRIV``, affirming its authorization to delete the account.  The purpose used MUST be ``TALER_SIGNATURE_RESERVE_CLOSE``.

  :query force=BOOLEAN: *Optional.*  If set to 'true' specified, the exchange
    will delete the account even if there is a balance remaining.

  **Response:**

  :http:statuscode:`200 OK`:
    The operation succeeded, the exchange provides details
    about the account deletion.
    The response will include a `ReserveDeletedResponse` object.
  :http:statuscode:`403 Forbidden`:
    The *Account-Request-Signature* is invalid.
    This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
    The account is unknown to the exchange.
  :http:statuscode:`409 Conflict`:
    The account is still has digital cash in it, the associated
    wire method is ``void`` and the *force* option was not provided.
    This response comes with a standard `ErrorDetail` response.

  **Details:**

  .. ts:def:: ReserveDeletedResponse

     interface ReserveDeletedResponse {

      // Final balance of the account.
      closing_amount: Amount;

      // Current time of the exchange, used as part of
      // what the exchange signs over.
      close_time: Timestamp;

      // Hash of the wire account into which the remaining
      // balance will be transferred. Note: may be the
      // hash over ``payto://void/`, in which case the
      // balance is forfeit to the profit of the exchange.
      h_wire: HashCode;

      // This is a signature over a
      // struct ``TALER_AccountDeleteConfirmationPS`` with purpose
      // ``TALER_SIGNATURE_EXCHANGE_RESERVE_DELETED``.
      exchange_sig: EddsaSignature;

    }
