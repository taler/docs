..
  This file is part of GNU TALER.
  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Florian Dold
  @author Benedikt Muller
  @author Sree Harsha Totakura
  @author Marcello Stanisci
  @author Christian Grothoff

========
Overview
========

.. rubric:: Taler Exchange Public API

* **Summary**: Public API for the payment service provider component of GNU Taler.
* **Providers**: GNU Taler Exchange
* **Consumers**: Wallet, Merchant
* :doc:`Docs <api-exchange>`

.. rubric:: Taler Exchange Management API

* **Summary**: Management API for the payment service provider component of GNU Taler.
* **Providers**: GNU Taler Exchange
* **Consumers**: Exchange tooling (``taler-exchange-offline``), Auditor
* :doc:`Docs <api-exchange>`

.. rubric:: Taler Merchant Backend Public API

* **Summary**: Allows communication between merchants and users' wallets.
* **Providers**: GNU Taler Merchant backend
* **Consumers**: Wallet
* :doc:`Docs <api-merchant>`

.. rubric:: Taler Merchant Backend Private API

* **Summary**: Allows the merchant to manage Taler-based payments and related functionality.
* **Providers**: GNU Taler Merchant backend
* **Consumers**: Merchant's shop Website backend, Merchant PoS app, Merchant Backoffice UI
* :doc:`Docs <api-merchant>`

.. rubric:: Taler Wallet Core API

* **Summary**: API to access functionality of the Taler Wallet service running locally on user's devices.
* **Providers**: wallet-core
* **Consumers**: UIs for the GNU Taler wallet

.. rubric:: Core Bank API

* **Summary**: Protocol to manage a simple core bank with optional regional
  currency support.  Allows access to a bank account by the owner of the
  account. The owner can access the account balance, transaction list, and initate
  payments.
* **Providers**: LibEuFin demobank, Taler Fakebank (partial)
* **Consumers**: Cashier App, demobank-ui
* :doc:`Docs <api-corebank>`

.. rubric:: Taler Bank Integration API

* **Summary**: Offered by banks to provide the wallet/user with more information about ongoing withdrawals of Taler digital cash.
* **Providers**: Taler fakebank, LibEuFin demobank, Banks (that provide extra Taler support)
* **Consumers**: Taler Wallet
* :doc:`Docs <api-bank-integration>`

.. rubric:: Taler Wire Gateway API

*  **Summary**: Allows the Taler Exchange to query incoming transactions and initiate payments with a protocol that abstracts away details of the underlying banking system.

*  **Providers**: Taler fakebank, LibEuFin Nexus, Depoloymerization wire gateway

*  **Consumers**: GNU Taler Exchange, Wire Auditor

*  :doc:`Docs <api-bank-wire>`

.. rubric:: Taler Bank Revenue API

* **Summary**: Offered by banks to provide clients the ability to download credit transaction histories.
* **Providers**: Taler fakebank, LibEuFin demobank, Banks (that provide extra Taler support)
* **Consumers**: Taler Merchant, GNU Anastasis
* :doc:`Docs <api-bank-revenue>`


.. rubric:: Taler Sync API

* **Summary**: Encrypted Data blob storage and retrieval API with payments for storage handled by GNU Taler payments.

* **Providers**: GNU Taler Sync service

* **Consumers**: Taler Wallet

* :doc:`Docs <api-sync>`

.. rubric:: Taler Auditor API

* **Summary**: Reporting of certain transactions or potential problems directly to the auditor.
* **Providers**: GNU Taler Auditor service
* **Consumers**: GNU Taler Merchant, eventually Taler Wallet
* :doc:`Docs <api-auditor>`

.. rubric:: Taldir API

* **Summary**: Looking up of Taler mailboxes associated with particular Internet service addresses.
* **Providers**: GNU TalDir service
* **Consumers**: GNU Taler Wallet
* :doc:`Docs <api-taldir>`

.. rubric:: Taler Mailbox API

* **Summary**: Tansmission of encrypted payment messages between Taler wallets.
* **Providers**: GNU Taler Mailbox service
* **Consumers**: GNU Taler Wallet
* :doc:`Docs <api-mailbox>`

.. rubric:: Anastasis Provider Public API

* **Summary**: Backup for secret splitting backup and recovery with GNU Anastasis providers.
* **Providers**: GNU Anastasis providers
* **Consumers**: Anastasis core client implementations (C implementation, TypeScript implementation)

.. rubric:: Anastasis Reducer API

* **Summary**:  API used to step through the backup and recovery process of GNU Anastasis.
* **Providers**: Anastasis core client implementations (C implementation, TypeScript implementation)
* **Consumers**: Anastasis UIs (CLI, GTK, anastasis-webui)

.. rubric:: LibEuFin Nexus API

* **Summary**: API used to configure and access LibEuFin nexus, a generic server that supports multiple protocols to access a bank account.

* **Providers**: LibEuFin Nexus service

* **Consumers**: ``libeufin-cli``, (future) LibEuFin Web UI

.. rubric:: EBICS

* **Summary**: Allows businesses/banks/consumers to exchange data with a bank's core banking system.
* **Consumers**: LibEuFin Nexus
* **Providers**: libeufin-bank, Banks
