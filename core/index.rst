..
  This file is part of GNU TALER.
  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Florian Dold
  @author Benedikt Muller
  @author Sree Harsha Totakura
  @author Marcello Stanisci
  @author Christian Grothoff

---------------------------
Core Protocol Specification
---------------------------

This chapter describes the APIs used in the GNU Taler project.  It includes
both APIs that are pre-existing as well as APIs specific to the project.

These *protocol specifications* define the interfaces between the core
components of GNU Taler.  Most of these interfaces use HTTP-based RESTful
protocols using JSON.


.. toctree::
  :maxdepth: 2

  api-overview
  api-common
  api-exchange
  api-merchant
  ../wallet/wallet-core
  api-auditor
  api-sync
  api-challenger
  api-taldir
  api-mailbox
  index-bank-apis
  api-donau

.. toctree::
  :hidden:
