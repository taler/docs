..
  This file is part of GNU TALER.
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Christian Grothoff


=======================
The Mailbox RESTful API
=======================

This is a proposed API for the GNU Taler Mailbox service which allows Taler
wallets to securely send push and pull payment requests to other wallets
without having to interact with the respective messaging service.

The API specified here follows the :ref:`general conventions <http-common>`
for all details not specified in the individual requests.
The `glossary <https://docs.taler.net/glossary.html#glossary>`_
defines all specific terms used in this section.

.. include:: tos.rst

-------------------------
Configuration information
-------------------------

.. http:get:: /config

  Return the protocol version and currency supported by this service.

  **Response:**

  :http:statuscode:`200 OK`:
    The body is a `VersionResponse`.

  **Details:**

  .. ts:def:: VersionResponse

    interface VersionResponse {
      // libtool-style representation of the Merchant protocol version, see
      // https://www.gnu.org/software/libtool/manual/html_node/Versioning.html#Versioning
      // The format is "current:revision:age".
      version: string;

      // Name of the protocol.
      name: "taler-mailbox";

      // Fee per message.
      message_fee: Amount;

      // How long will the service store a message
      // before giving up on delivery?
      delivery_period: RelativeTime;

    }

----------------
Sending messages
----------------

.. http:post:: /$H_MAILBOX

  Puts a message into ``$H_MAILBOX``.
  ``$H_MAILBOX`` is the SHA512 of an EdDSA public key.

  **Request**

  The body of the request must be an `IdentityMessage`.

  **Response**

  :http:statuscode:`204 No Content`
     Message was stored and will be delivered.
  :http:statuscode:`402 Payment Required`
     Client needs to make a Taler payment before proceeding. See
     standard Taler payment procedure.
  :http:statuscode:`403 Forbidden`
     The specified ``order_id`` does not permit sending
     of this message. Possible reaons include the order
     being for a different message, unpaid or
     malformed.
     This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`429 Too Many Requests`:
    The system is currently experiencing a too high request
    load and is unable to accept the message for delivery.
    The response format is given by `MailboxRateLimitedResponse`_.

  **Details:**

  .. _IdentityMessage:
  .. ts:def:: IdentityMessage

    interface IdentityMessage {
      // Public DH key used to encrypt the body. Must be fresh
      // and only used once (ephemeral).
      ephemeral_key: EcdhePublicKey;

      // Encrypted message. Must be exactly 256-32 bytes long.
      body: string;

      // Order ID, if the client recently paid for this message.
      order_id?: string;
    }

  .. _MailboxRateLimitedResponse:
  .. ts:def:: MailboxRateLimitedResponse

    interface MailboxRateLimitedResponse {

      // Taler error code, TALER_EC_MAILBOX_DELIVERY_RATE_LIMITED.
      code: number;

      // When the client should retry.
      retry_delay: RelativeTime;

      // The human readable error message.
      hint: string;

    }

------------------
Receiving messages
------------------

.. http:get:: /$H_MAILBOX

  Endpoint that returns unread messages in ``$H_MAILBOX``.
  The number of messages returned by the service can be limited.
  If the request is simply repeated, the same messages will be
  returned again (or possibly more if additional messages arrived
  and the total number is below the service's current internal limit).
  To receive additional messages, the client generally has to first
  explicitly DELETE already downloaded messages from the mailbox.

  **Request:**

  :query timeout_ms=NUMBER: *Optional.*  If specified,
    the Mailbox service will wait up to ``NUMBER``
    milliseconds for the arrival of new messages
    before sending the HTTP response.  Note that if the
    mailbox is non-empty, the service will always return
    immediately with the messages in the mailbox, and not
    wait for additional messages to arrive.

  **Response**

  :http:statuscode:`200 Ok`:
    Messages are returned in binary format, 256 bytes per message,
    starting with the ephemeral key and followed by
    the encrypted body. Messages are not encapsulated in JSON!
  :http:statuscode:`204 No Content`:
    The mailbox is empty.
  :http:statuscode:`429 Too Many Requests`:
    The system is currently experiencing a too high request
    load and is unable to accept the message for delivery.
    The response format is given by `MailboxRateLimitedResponse`_.

.. http:delete:: /$ADDRESS

  Requests the deletion of already received messages from the
  mailbox. Here, ``$ADDRESS`` must be the EdDSA public key
  of the mailbox (not the hash!).

  **Request**

  The body of the request must be a ``MessageDeletionRequest``.

  **Details:**

  .. _MessageDeletionRequest:
  .. ts:def:: MessageDeletionRequest

    interface MessageDeletionRequest {

    // Number of messages to delete. (Starting from the beginning
    // of the latest GET response).
    count: Integer;

    // SHA-512 hash over all messages to delete.
    checksum: HashCode;

    // Signature by the mailbox's private key affirming
    // the deletion of the messages, of purpuse
    // ``TALER_SIGNATURE_WALLET_MAILBOX_DELETE_MESSAGES``.
    wallet_sig: EddsaSignature;

  }

  **Response**

  :http:statuscode:`204 No Content`:
     Deletion complete.
  :http:statuscode:`403 Forbidden`:
     The ``wallet_sig`` is invalid.
     This response comes with a standard `ErrorDetail` response.
  :http:statuscode:`404 Not found`:
     The checksum does not match the messages currently at the
     head of the mailbox, or ``count`` is larger
     than the number of messages currently in the mailbox.
     This response comes with a standard `ErrorDetail` response.
