--------------------
Terms of service API
--------------------

These APIs allow clients to obtain the terms of service
and the privacy policy of a service.


.. http:get:: /terms

  Get the terms of service of the service.
  The endpoint will consider the "Accept" and "Accept-Language" and
  "Accept-Encoding" headers when generating a response. Specifically,
  it will try to find a response with an acceptable mime-type, then
  pick the version in the most preferred language of the user, and
  finally apply compression if that is allowed by the client and
  deemed beneficial.

  The endpoint will set an "Etag", and subsequent requests of the same client
  should provide the tag in an "If-None-Match" header to detect if the terms
  of service have changed.  If not, a "304 Not Modified" response will be
  returned.  Note that the "304 Not Modified" will also be returned if the
  client changed the "Accept-Language" or "Accept-Encoding" header.  Thus, if
  the client would like to download the resource in a different language or
  format, the "If-None-Match" header must be omitted.

  If the "Etag" is missing, the client should not cache the response and
  instead prompt the user again at the next opportunity. This is usually only
  the case if the terms of service were not configured correctly.

  When returning a full response (not a "304 Not Modified"), the server
  should also include a "Avail-Languages" header which includes
  a comma-separated list of the languages in which the terms of service
  are available in (see `availability hints specification
  <https://datatracker.ietf.org/doc/draft-nottingham-http-availability-hints/>`_). Clients can use this to generate a language switcher
  for users that may not have expressed a proper language preference.


.. http:get:: /privacy

  Get the privacy policy of the service.  Behaves the same way as
  The "/terms" endpoint, except that it returns the privacy policy
  instead of the terms of service.
