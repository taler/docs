Guidelines for Python Packages
==============================

This document describes conventions used for Python repos in the Taler project.


Packaging
---------

* We use `poetry <https://github.com/python-poetry/poetry>`__ for managing dependencies and dev-dependencies.
* The ``poetry.lock`` file must be committed to repo.
* Entry points **must not** be defined as shell scripts.  Instead, use poetry's script facility to define entry points.  This makes the package work on different platforms properly.


GNU Compatibility Layer
-----------------------

In addition to the Python-native tooling, we provide a GNU-style interface for the build system.
The commands supported by every Python repo should be:

* ``./bootstrap``:  Only necessary when the repo is checked out via git.
  Initializes the build system and checks out git submodules if applicable.
* ``./configure``:  Should check for build-time dependencies, **including** Python tooling.
* ``make``: Invoking make without a target should create the Python wheel for the project.
* ``make install``: Installation should **only** install the Python package
  based on the wheel via ``pip``.  Note that we can't do dependency tracking
  properly, so the ``install`` target will always re-build the wheel and
  install it.
* ``make pretty``: Should invoke the pretty-printer (``black`` for Python projects).
* ``make dist``: This should create the source tarball.
* ``make clean``: Should delete generated files.

The `build-common.git <https://git.taler.net/build-common.git/>`__ repository contains helpers
to make the GNU compatibility easier.  Here are some hints for using it:

* The ``build-common.git`` repo should added as a submodule in the path ``build-system/taler-build-scripts``
  of the repository.
* The `bootstrap template <https://git.taler.net/build-common.git/tree/bootstrap.template>`__ should
  be copied as ``./bootstrap`` to the root of the repository.
* The configure script is automatically created by ``./bootstrap``.
* Dependencies for the configure file are defined in ``build-system/configure.py``.
  There is no documentation yet, but adjusting the `example file <https://git.taler.net/build-common.git/tree/testconfigure.py>`__ is a good starting point.
* The source distribution (``make dist``) should either be created via ``poetry build -f sdist``
  or using the `git-archive-all <https://git.taler.net/build-common.git/tree/archive-with-submodules/git_archive_all.py>`__.

Formatting
----------

* We follow `pep8 <https://www.python.org/dev/peps/pep-0008/>`__.
* Code should be auto-formatted with `black <https://github.com/psf/black>`__.


Distro Packaging
----------------

For Debian, we should try to use `wheel2deb <https://github.com/upciti/wheel2deb>`__.
