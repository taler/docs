Configuration format
--------------------

All GNU Taler components are designed to possibly share the same
configuration files.  When installing a GNU Taler component, the
installation deploys default values in configuration files located
at ${prefix}/share/taler/config.d/ where ${prefix} is the installation
prefix. Different components must be installed to the same prefix.

In order to override these defaults, the user can write a custom configuration
file and either pass it to the component at execution time using the *-c*
option, or name it taler.conf and place it under $HOME/.config/ which is where
components will look by default. Note that the systemd service files pass ``-c
/etc/taler.conf``, thus making ``/etc/taler.conf`` the primary location for
the configuration.

A config file is a text file containing sections, and each section
contains maps options to their values.  Configuration files follow
basically the INI syntax:

.. code-block:: ini

   [section1]
   value1 = string
   value2 = 23

   [section2]
   value21 = string
   value22 = /path22

Comments start with a hash (``#``).  Throughout the configuration, it is
possible to use ``$``-substitution for options relating to names of files or
directories. It is also possible to provide defaults values for those
variables that are unset, by using the following syntax:
``${VAR:-default}``. There are two ways a user can set the value
of ``$``-prefixable variables:

  (1) by defining them under a ``[paths]`` section:

     .. code-block:: ini

       [paths]
       TALER_DEPLOYMENT_SHARED = ${HOME}/shared-data
       ..
       [section-x]
       path-x = ${TALER_DEPLOYMENT_SHARED}/x

  (2) or by setting them in the environment:

     .. code-block:: console

        $ export VAR=/x

The configuration loader will give precedence to variables set under
``[path]`` over environment variables.

The utility ``taler-config``, which gets installed along with the exchange,
can be used get and set configuration values without directly editing the
configuration file. The option ``-f`` is particularly useful to resolve
pathnames, when they use several levels of ``$``-expanded variables. See
``taler-config --help``.

The repository ``git://git.taler.net/deployment`` contains example code
for generating configuration files under ``deployment/netzbon/``.

