The Taler payment cycle involves six parties:
(a) customer,
(b) exchange,
(c) merchant,
(d) customer's bank,
(e) exchange's bank,
(f) merchant's bank.

The exchange is the central entity that mediates the wire transfer of real
currency between (d), (e), (f) by way of *coins*, cryptographically secure
tokens passed between (a), (b), (c).

There are six steps to a Taler payment cycle.

In step 1, (a) directs (d) to make real funds available to (b).

In step 2, (d) does a wire transfer of real funds to (e), fulfilling the
request from step 1.  (b) generates coins corresponding to those real funds;
these are called the *reserve*.

In step 3, (a) *withdraws* coins, either wholly or partially, from (b).  These
coins are kept in a *wallet* under control of (a).  The coins in the wallet
are unlinkable to the identity of (a) that was revealed during the withdraw
operation.

In step 4, (a) authorizes payment of coins from the wallet to (c).  This
transfers payment coins from the wallet to (c), and change coins from (b) to
the wallet (unless the payment amount exactly matches the denomination of the
coins in the wallet).

In step 5, (c) *deposits* coins into (b).  At this point, (b) knows the
identity of (c), but not of (a).  Taler uses cryptography to validate that the
coins are unique and were issued by (b), but (b) cannot determine to whom the
coins were originally issued.

In step 6, (b) directs (e) to wire transfer real funds corresponding to the
accumulated deposited coins to (f).

NB: The Taler payment cycle is part of the Taler payment system, which
includes also an auditor component, not described here.
