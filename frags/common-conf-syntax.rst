The configuration file is line-oriented.  Blank lines and whitespace at the
beginning and end of a line are ignored.  Comments start with ``#`` or ``%``
in the first column (after any beginning-of-line whitespace) and go to the end
of the line.

The file is split into sections.  Every section begins with ``[SECTIONNAME]``
and contains a number of options of the form ``OPTION=VALUE``.  There may be
whitespace around the ``=`` (equal sign).  Section names and options are
*case-insensitive*.

The values, however, are *case-sensitive*.  In particular, boolean values are
one of ``YES`` or ``NO``.  Values can include whitespace by surrounding the
entire value with ``"`` (double quote).  Note, however, that there are no
escape characters in such strings; all characters between the double quotes
(including other double quotes) are taken verbatim.

Values that represent a time duration are represented as a series
of one or more ``NUMBER UNIT`` pairs, e.g. ``60 s``, ``4 weeks 1 day``,
``5 years 2 minutes``.

Values that represent an amount are in the usual amount syntax:
``CURRENCY:VALUE.FRACTION``, e.g. ``EUR:1.50``.
The ``FRACTION`` portion may extend up to 8 places.

Values that represent filenames can begin with a ``/bin/sh``-like variable
reference.  This can be simple, such as ``$TMPDIR/foo``, or complex, such as
``${TMPDIR:-${TMP:-/tmp}}/foo``.  The variables are expanded either using
key-values from the ``[PATHS]`` section (see below) or from the environment
(``getenv()``). The values from ``[PATHS]`` take precedence over those from
the environment.  If the variable name is found in neither ``[PATHS]`` nor the
environment, a warning is printed and the value is left unchanged.  Variables (including those from the environment) are expanded recursively, so if ``FOO=$BAR`` and ``BAR=buzz`` then the result is ``FOO=buzz``.  Recursion is bounded to at most 128 levels to avoid undefined behavior for mutually recursive expansions like if ``BAR=$FOO`` in the example above.

The ``[PATHS]`` section is special in that it contains paths that can be
referenced using ``$`` in other configuration values that specify
*filenames*. Note that configuration options that are not specifically
retrieved by the application as *filenames* will not see “$”-expressions
expanded. To expand ``$``-expressions when using ``taler-config``, you must pass
the ``-f`` command-line option.

The system automatically pre-populates the ``[PATHS]`` section with a few values
at run-time (in addition to the values that are in the actual configuration
file and automatically overwriting those values if they are present).
These automatically generated values refer to installation properties
from `GNU autoconf
<https://www.gnu.org/prep/standards/html_node/Directory-Variables.html>`_. The
values are usually dependent on an ``INSTALL_PREFIX`` which is determined by
the ``--prefix`` option given to configure.  The canonical values are:

  * LIBEXECDIR = $INSTALL_PREFIX/taler/libexec/
  * DOCDIR = $INSTALL_PREFIX/share/doc/taler/
  * ICONDIR = $INSTALL_PREFIX/share/icons/
  * LOCALEDIR = $INSTALL_PREFIX/share/locale/
  * PREFIX = $INSTALL_PREFIX/
  * BINDIR = $INSTALL_PREFIX/bin/
  * LIBDIR = $INSTALL_PREFIX/lib/taler/
  * DATADIR = $INSTALL_PREFIX/share/taler/

Note that on some platforms, the given paths may differ depending
on how the system was compiled or installed, the above are just the
canonical locations of the various resources.  These
automatically generated values are never written to disk.
