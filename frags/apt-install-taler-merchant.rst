
To install the Taler merchant backend, you can now simply run:

.. code-block:: console

   # apt install taler-merchant

Note that the package does not complete the integration of the backend with
the HTTP reverse proxy (typically with TLS certificates).  A configuration
fragment for Nginx or Apache will be placed in
``/etc/{apache,nginx}/conf-available/taler-merchant.conf``.  You must
furthermore still configure the database and the instances, and may need to
extend the fragment with access control restrictions for non-default
instances.
