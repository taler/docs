To install the GNU Taler Trisquel packages, first ensure that you have
the right Trisquel distribution. Packages are currently available for
Trisquel GNU/Linux 10.0.  Simply follow the same instructions provided
for Ubuntu.
