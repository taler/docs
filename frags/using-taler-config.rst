Using taler-config
------------------

The tool ``taler-config`` can be used to extract or manipulate configuration
values; however, the configuration use the well-known INI file format and is
generally better edited by hand to preserve comments and structure.

Run

.. code-block:: console

   $ taler-config -s $SECTION

to list all of the configuration values in section ``$SECTION``.

Run

.. code-block:: console

   $ taler-config -s $SECTION -o $OPTION

to extract the respective configuration value for option ``$OPTION`` in
section ``$SECTION``.

Finally, to change a setting, run

.. code-block:: console

   $ taler-config -s $SECTION -o $OPTION -V $VALUE

to set the respective configuration value to ``$VALUE``. Note that you
have to manually restart affected Taler components after you change the
configuration to make the new configuration go into effect.

Some default options will use $-variables, such as ``$DATADIR`` within
their value. To expand the ``$DATADIR`` or other $-variables in the
configuration, pass the ``-f`` option to ``taler-config``. For example,
compare:

.. code-block:: console

   $ taler-config --section exchange-offline --option MASTER_PRIV_FILE
   $ taler-config -f --section exchange-offline --option MASTER_PRIV_FILE

While the configuration file is typically located at
``$HOME/.config/taler.conf``, an alternative location can be specified to any
GNU Taler component using the ``-c`` option.
