Please note that unlike most packages, if you want to run the ``make check``
command, you should run it only *after* having done ``make install``.  The
latter ensures that necessary binaries are copied to the right place.

In any case, if ``make check`` fails, please consider filing a
bug report with the Taler `bug tracker <https://bugs.taler.net>`__.
