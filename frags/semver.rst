..
  This file is part of GNU TALER.

  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Christian Grothoff

GNU Taler components version numbers follow the ``MAJOR.MINOR.MICRO`` format.
The general rule for compatibility is that ``MAJOR`` and ``MINOR`` must match.
Exceptions to this general rule are documented in the release notes.
For example, Taler merchant 1.3.0 should be compatible with Taler exchange 1.4.x
as the MAJOR version matches.  A MAJOR version of 0 indicates experimental
development, and you are expected to always run all of the *latest* releases
together (no compatibility guarantees).
