To install the GNU Taler Ubuntu packages, first ensure that you have
the right Ubuntu distribution. At this time, the packages are built for
Ubuntu Lunar and Ubuntu Jammy. Make sure to have ``universe`` in your
``/etc/apt/sources.list`` (after ``main``) as we depend on some packages
from Ubuntu ``universe``.

A typical ``/etc/apt/sources.list.d/taler.list`` file for this setup
would look like this for Ubuntu Lunar:

.. code-block::

   deb [signed-by=/etc/apt/keyrings/taler-systems.gpg] https://deb.taler.net/apt/ubuntu/ lunar taler-lunar

For Ubuntu Jammy use this instead:

.. code-block::

   deb [signed-by=/etc/apt/keyrings/taler-systems.gpg] https://deb.taler.net/apt/ubuntu/ jammy taler-jammy

The last line is crucial, as it adds the GNU Taler packages.

Next, you must import the Taler Systems SA public package signing key
into your keyring and update the package lists:

.. code-block:: console

   # wget -O /etc/apt/keyrings/taler-systems.gpg \
       https://taler.net/taler-systems.gpg
   # apt update

.. note::

   You may want to verify the correctness of the Taler Systems key out-of-band.

Now your system is ready to install the official GNU Taler binary packages
using apt.
