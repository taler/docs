.. note::

   Taler may store sensitive business and customer data in the database.  Any
   operator SHOULD thus ensure that backup operations are encrypted and
   secured from unauthorized access.
