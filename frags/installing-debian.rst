To install the GNU Taler Debian packages, first ensure that you have
the right Debian distribution. At this time, the packages are built for
Debian bookworm.

You need to add a file to import the GNU Taler packages. Typically,
this is done by adding a file ``/etc/apt/sources.list.d/taler.list`` that
looks like this:

.. code-block::

   deb [signed-by=/etc/apt/keyrings/taler-systems.gpg] https://deb.taler.net/apt/debian stable main

Next, you must import the Taler Systems SA public package signing key
into your keyring and update the package lists:

.. code-block:: console

   # wget -O /etc/apt/keyrings/taler-systems.gpg \
       https://taler.net/taler-systems.gpg
   # apt update

.. note::

   You may want to verify the correctness of the Taler Systems SA key out-of-band.

Now your system is ready to install the official GNU Taler binary packages
using apt.
