-  "Sphinx RTD Theme" Python package aka ``python3-sphinx-rtd-theme``
   on Debian-based systems (for GNUnet documentation support, can be
   omitted if GNUnet is configured with ``--disable-documentation``)

-  libsqlite3 >= 3.16.2

-  GNU libunistring >= 0.9.3

-  libcurl >= 7.26 (or libgnurl >= 7.26)

-  libqrencode >= 4.0.0 (Taler merchant only)

-  GNU libgcrypt >= 1.6 (1.10 or later highly recommended)

-  libsodium >= 1.0

-  libargon2 >= 20171227

-  libjansson >= 2.7

-  PostgreSQL >= 15, including libpq

-  GNU libmicrohttpd >= 0.9.71

-  GNUnet >= 0.20 (from `source tarball <http://ftpmirror.gnu.org/gnunet/>`__)

-  Python3 with ``jinja2``


If you are on Debian stable or later, the following command may help you
install these dependencies:

.. code-block:: console

   # apt-get install \
     libqrencode-dev \
     libsqlite3-dev \
     libltdl-dev \
     libunistring-dev \
     libsodium-dev \
     libargon2-dev \
     libcurl4-gnutls-dev \
     libgcrypt20-dev \
     libjansson-dev \
     libpq-dev \
     libmicrohttpd-dev \
     python3-jinja2 \
     postgresql-15
