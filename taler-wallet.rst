GNU Taler Wallet Manual
#######################

The GNU Taler wallet allows customers to withdraw and spend digital cash.


WebExtension Wallet
===================

Building from source
--------------------

.. code-block:: console

  $ git clone https://git.taler.net/wallet-core.git
  $ cd wallet-core
  $ ./configure
  $ make webex-stable
  # Packaged extension now available as:
  # dist/taler-wallet-$VERSION.zip


Android Wallet
==============

Please see :ref:`Build-apps-from-source` in the :doc:`taler-developer-manual`.


iOS Wallet
==========

Please see :ref:`Build-iOS-from-source` in the :doc:`taler-developer-manual`.

.. _command-line-wallet:

Command-line Wallet
===================

This section describes how to use the GNU Taler wallet command line
interface (CLI).

The the wallet CLI is targeted at developers and operators, but not meant to be
used by customers.  It exposes all functionality that the more user-friendly
interfaces (Android app, browser extension) offer.  However, it provides more
diagnostics and advanced features as well.

Building from source
--------------------

The easiest way to install the wallet is via NPM.  Note that a recent version of
Node.JS (``>=12.20.1``) is required.

We recommend to install the wallet package on a per-user basis,
thus setting ``$INSTALL_PREFIX`` to a directory in ``$HOME``.

.. code-block:: console

  $ git clone https://git.taler.net/wallet-core.git
  $ cd wallet-core
  $ ./bootstrap
  $ ./configure --prefix=$INSTALL_PREFIX
  $ make && make install

The wallet command-line interface should then be available as ``taler-wallet-cli`` under ``$INSTALL_PREFIX/bin``.

Installation via NPM
--------------------

The wallet can also obtained via NPM, the Node Package Manager.

To install the wallet as a global package, run:

.. code-block:: console

  $ npm install -g taler-wallet
  # check if installation was successful
  $ taler-wallet-cli --version

To install the wallet only for your user, run:

.. code-block:: console

  $ npm install -g --prefix=$HOME/local taler-wallet
  # check if installation was successful
  $ taler-wallet-cli --version
  # If this fails, make sure that $HOME/local/bin is in your $PATH

To use the wallet as a library in your own project, run:

.. code-block:: console

  $ npm install taler-wallet


Getting Help
------------

The wallet CLI comes with built-in help.  Invoke the wallet CLI (or any subcommand) with the ``--help`` flag to get help:

.. code-block:: console

  $ taler-wallet-cli --help
  Usage: taler-wallet-cli COMMAND

  Command line interface for the GNU Taler wallet.

  Options:
    -h, --help             Show this message and exit.
    --wallet-db=VALUE      location of the wallet database file
    --timetravel=VALUE     modify system time by given offset in microseconds
    --inhibit=VALUE        Inhibit running certain operations, useful for debugging and testing.
    --no-throttle          Don't do any request throttling.
    -v, --version
    -V, --verbose          Enable verbose output.

  Commands:
    advanced               Subcommands for advanced operations (only use if you know what you're doing!).
    api                    Call the wallet-core API directly.
    backup                 Subcommands for backups
    balance                Show wallet balance.
    deposit                Subcommands for depositing money to payto:// accounts
    exchanges              Manage exchanges.
    handle-uri             Handle a taler:// URI.
    pending                Show pending operations.
    run-pending            Run pending operations.
    run-until-done         Run until no more work is left.
    testing                Subcommands for testing GNU Taler deployments.
    transactions           Show transactions.

Completing operations
---------------------

Note that the CLI does not run as a background daemon.  When starting
operations that don't immediately finish, the wallet needs to be run explicitly
to finish any pending tasks:


.. code-block:: console

  # Do one attempt to finish all pending operations
  $ taler-wallet-cli run-pending

  # Run until all work is done
  $ taler-wallet-cli run-until-done

Resetting the wallet
--------------------

The wallet can be reset by deleting its database file.  By default, the database file
is ``$HOME/.talerwalletdb.json``.


Handling taler:// URIs
----------------------

Many interactions with the Taler wallet happen by scanning QR codes or special
headers on Websites.  To emulate this with the command line interface, run the following
command:

.. code-block:: console

  $ taler-wallet-cli handle-uri $URI


Manual withdrawing
------------------

.. code-block:: console

  $ taler-wallet-cli advanced withdraw-manually \
      --exchange https://exchange.eurint.taler.net/ \
      --amount EUR:5


Testing an exchange deployment
------------------------------

The following series of commands can be used to check that an exchange deployment
is functional:

.. code-block:: console

  # This will now output a payto URI that money needs to be sent to in order to allow withdrawal
  # of taler coins
  $ taler-wallet-cli advanced withdraw-manually --exchange $EXCHANGE_URL --amount EUR:10.50

  # Show the status of the manual withdrawal operation
  $ taler-wallet-cli transactions

  # Once the transfer has been made, try completing the withdrawal
  $ taler-wallet-cli run-pending

  # Check status of transactions and show balance
  $ taler-wallet-cli transactions
  $ taler-wallet-cli balance

  # Now, directly deposit coins with the exchange into a target account
  # (Usually, a payment is made via a merchant.  The wallet provides
  # this functionality for testing.)
  $ taler-wallet-cli deposit create EUR:5 payto://iban/$IBAN

  # Check if transaction was successful.
  # (If not, fix issue with exchange and run "run-pending" command again)
  $ taler-wallet-cli transactions

  # The wallet can also track if the exchange wired the money to the merchant account.
  # The "deposit group id" can be found in the output of the transactions list.
  $ taler-wallet-cli deposit track $DEPOSIT_GROUP_ID




APIs and Data Formats
=====================

Envelope Format
---------------

All API responses and notifications are returned in the
following envelope:

.. ts:def:: WalletResponseEnvelope

   type WalletResponseEnvelope =
    | WalletSuccess
    | WalletError
    | WalletNotification

.. ts:def:: WalletSuccess

   export interface WalletSuccess {
     type: "response";
     operation: string;
     // ID to correlate success response to request
     id: string;
     // Result type depends on operation
     result: unknown;
   }

.. ts:def:: WalletError

   export interface WalletError {
     type: "error";
     operation: string;
     // ID to correlate error response to request
     id: string;
     error: WalletErrorInfo;
   }

.. ts:def:: WalletNotification

   export interface WalletSuccess {
     type: "notification";

     // actual type is WalletNotification,
     // to be documented here
     payload: any;
   }

.. ts:def:: WalletErrorInfo

   export interface WalletErrorInfo {
     // Numeric error code defined defined in the
     // GANA gnu-taler-error-codes registry.
     talerErrorCode: number;

     // English description of the error code.
     talerErrorHint: string;

     // English diagnostic message that can give details
     // for the instance of the error.
     message: string;

     // Error details, type depends
     // on talerErrorCode
     details: unknown;
   }

Withdrawal
----------

A typical API sequence for *bank-integrated* withdrawals can for example look like this:

#. ``"getWithdrawalDetailsForUri"`` returns an amount and default exchange
#. ``"getWithdrawalDetailsForAmount"`` returns fee information and that ToS are not accepted

   #. ``"getExchangeTos"`` are shown to the user and return currentEtag
   #. ``"setExchangeTosAccepted"`` called with currentEtag after user accepted

#. ``"acceptWithdrawal"`` after the user confirmed withdrawal with associated fees

A typical API sequence for *manual* withdrawals can for example look like this:

#. ``"listExchanges"`` shows a list of exchanges to the user who picks one and an amount
#. ``"getWithdrawalDetailsForAmount"`` returns fee information and that ToS are not accepted

   #. ``"getExchangeTos"`` are shown to the user and return currentEtag
   #. ``"setExchangeTosAccepted"`` called with currentEtag after user accepted

#. ``"acceptManualWithdrawal"`` after the user confirmed withdrawal with associated fees

Integration Tests
=================

Integration Test Example
------------------------

Integration tests can be done with the low-level wallet commands.  To select which coins and denominations
to use, the wallet can dump the coins in an easy-to-process format (`CoinDumpJson <https://git.taler.net/wallet-core.git/tree/src/types/talerTypes.ts#n734>`__).

The database file for the wallet can be selected with the ``--wallet-db``
option.  This option must be passed to the ``taler-wallet-cli`` command and not
the subcommands.  If the database file doesn't exist, it will be created.

The following example does a simple withdrawal recoup:

.. code-block:: console

  # Withdraw digital cash
  $ taler-wallet-cli --wallet-db=mydb.json testing withdraw \
      -b https://bank.int.taler.net/ \
      -e https://exchange.int.taler.net/ \
      -a INTKUDOS:10

  $ coins=$(taler-wallet-cli --wallet-db=mydb.json advanced dump-coins)

  # Find coin we want to revoke
  $ rc=$(echo "$coins" | \
         jq -r '[.coins[] | select((.denom_value == "INTKUDOS:5"))][0] | .coin_pub')

  # Find the denom
  $ rd=$(echo "$coins" | \
         jq -r '[.coins[] | select((.denom_value == "INTKUDOS:5"))][0] | .denom_pub_hash')

  # Find all other coins, which will be suspended
  $ susp=$(echo "$coins" | \
           jq --arg rc "$rc" '[.coins[] | select(.coin_pub != $rc) | .coin_pub]')

  # The exchange revokes the denom
  $ taler-exchange-keyup -r $rd
  $ taler-deployment-restart

  # Now we suspend the other coins, so later we will pay with the recouped coin
  $ taler-wallet-cli --wallet-db=mydb.json advanced suspend-coins "$susp"

  # Update exchange /keys so recoup gets scheduled
  $ taler-wallet-cli --wallet-db=mydb.json exchanges update -f https://exchange.int.taler.net/

  # Block until scheduled operations are done
  $ taler-wallet-cli --wallet-db=mydb.json run-until-done

  # Now we buy something, only the coins resulting from recouped will be
  # used, as other ones are suspended
  $ taler-wallet-cli --wallet-db=mydb.json testing test-pay \
      -m https://backend.int.taler.net/ \
      -k sandbox \
      -a "INTKUDOS:1" \
      -s "foo"
  $ taler-wallet-cli --wallet-db=mydb.json run-until-done


To test refreshing, force a refresh:

.. code-block:: console

  $ taler-wallet-cli --wallet-db=mydb.json advanced force-refresh "$coin_pub"


To test zombie coins, use the timetravel option. It **must** be passed to the
top-level command and not the subcommand:

.. code-block:: console

  # Update exchange /keys with time travel, value in microseconds
  $ taler-wallet-cli --timetravel=1000000 --wallet-db=mydb.json \
      exchanges update -f https://exchange.int.taler.net/

Test Cases
----------

Things we already have tests for:

* Can the wallet recoup coins and spend them?
  [`link <https://git.taler.net/wallet-core.git/tree/integrationtests/test-recoup.sh>`__]

Things we still need tests for:

* Does the wallet do retries correctly when the exchange is not reachable?
  Or when the merchant is not reachable?  Or the bank?
  This can be tested by temporarily killing those services.
* How does the wallet deal with processing the same ``taler://(pay|withdraw)`` URI twice?
* Test rewards (accepting/refusing a reward)
* Test refunds
* Test for :ref:`session-based payments <repurchase>`
* Test case for auto-refunds
  (scenario where the vending machine finds out that its motor is broken,
  so it automatically gives a refund)
* Does the wallet report "insufficient balance" correctly
  (as opposed to, say, crashing)?
* Perf tests:  How does the wallet handle withdrawing a *LOT* of coins?
* Are the transaction history and pending operations reported correctly?

Tests for things the wallet doesn't handle correctly yet:

* What happens if the wallet double-spends a coin?
  (Easy to test by copying the wallet DB before spending
  and then running a spend again with the old DB).
* What happens when a reserve is changed between accepting withdrawal
  and actually withdrawing coins?
  (This is harder to test.  Might not be possible with the current CLI.
  The idea would be be to have some ``--inhibit=withdraw`` flag
  that tells the wallet to not actually withdraw,
  so we can change the reserve state and then resume the wallet.)
* What happens if the exchange suddenly has a completely new list of denominations on offer?
* What happens if the exchange changes its master public key?
  The wallet *should* handle this gracefully
  even if we have coins with that exchange,
  provided that the old denominations can be recouped.
  (That one is pretty difficult!)
* Does the wallet handle :ref:`payment aborts <order-abort>` correctly?

There are test cases that require us to modify the communication between the wallet and exchange.

* What does the wallet do when the exchange/merchant announce an incompatible protocol version?
* What happens if some signature made by the exchange/merchant is garbage?
* What if the exchange reports a double-spend and the proof it gives us is invalid?



Integration Test and Fault Injection Framework
----------------------------------------------

This section describes the current approach to integration testing in the wallet.

It's all based on a TypeScript harness process, which itself implements
the fault injection proxy (async and in-process)!

The new approach consists of the following parts:

1. A strongly typed, convenient helper library to easily set up and run
arbitrary Taler deployments and run test cases.  These components plug
together as easily as lego bricks, even with multiple
exchanges/merchants/banks/etc.  Logs and clean shutdown (even on SIGINT
or errors) are handled properly.  (Support for auditors is still pending
but needed to fully test the wallet.)

This is how a simple withdrawal and payment test case looks like:
`<https://git.taler.net/wallet-core.git/tree/packages/taler-integrationtests/src/test-payment.ts>`__

(What's particularly nice is that all our docs contain TypeScript
definitions for all API request bodies.  So just copying them into the
test harness gives us auto-completion and compile-time checks to avoid
typos.  The wallet's JSON validation machinery is also re-used.)

2. A fault injection proxy that can be plugged between the services
and/or the wallet.  It runs alongside the test harness, and can thus can
use arbitrary custom logic.  There's no dependency for it other than
built-in Node.JS libraries.  Simple fault injections are just as easy to
set up as with the twister.

The following test case (a) logs all requests and responses to the test
harness stdout and (b) at a certain point, starts dropping the next 10
requests to the exchange (testing the wallet's retry logic):

`<https://git.taler.net/wallet-core.git/tree/packages/taler-integrationtests/src/test-payment-fault.ts#n165>`__

3. All util functionality from JS wallet-core, such as the Taler crypto,
amount/date/etc. handling and JSON parsing/validation (the wallet is now
more modular and easier to use as a library) can be used in the
integration tests, even if a different wallet (Kotlin, whatever) is
tested via the CLI.

4. A bunch of test cases that use (1)-(3).  These are *significantly*
more readable and hackable than other test approaches we had, while
allowing for more complex scenarios.  There are still way too few tests
though!

5. A test runner (written in bash) that runs test cases based on a glob
pattern and reports the results.

Injecting a fault is as easy as:

.. code:: ts

  // Set up test case
  [...]

  exchangeProxy.addFault({
    beforeResponse(ctx: FaultInjectionResponseContext) {
       if (cond1) { // Drop some responses
         ctx.dropResponse = true;
         return;
       } else if (cond2) { // modify some others
         ctx.responseBody = Buffer.from(`{"oops": true}`, "utf-8");
         return;
       }
       // Other things that can be modified:
       // - drop/modify the request, not just the response
       // - modify headers
       // - modify status codes
    }
  });

  await doSomethingWithTheWallet();

  exchangeProxy.clearFault();

  await doMoreWithTheWallet();


To make the configuration easy, an ``ExchangeService`` (or ``MerchantService``,
``BankService`` etc.) can be wrapped in a ``FaultInjectedExchangeService``,
which implements the ``ExchangeServiceInterface``:

.. code:: ts

  // create exchange and two merchants
  const exchange = await setupExchange(...);
  const merchant1 = ...;
  const merchant2 = ...;

  // Add exchange to merchant-accepted exchanges.
  // This will adjust the config.
  merchant1.addExchange(exchange);

  // Wrap exchange in fault injection proxy
  const faultInjectedExchange: ExchangeServiceInterface
    = new FaultInjectedExchangeService(t, exchange1, 8085);

  // Merchant 2 talks to the exchange over fault injection,
  // and thus must use the "twisted" base URL.
  merchant2.addExchange(faultInjectedExchange);


The package for the integration tests is here:

`<https://git.taler.net/wallet-core.git/tree/packages/taler-integrationtests>`__

The shortcut to run all integration tests is

.. code:: sh

  ./bootstrap && ./configure --prefix=... \
     && make install integrationtests

(From the root of the whole repo.  If you're developing tests, it's way
faster to just run "make compile install" once and then use
"./testrunner" from the ``taler-integrationtests`` package.)
