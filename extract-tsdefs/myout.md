# Wallet API Documentation
```{note}
This file is auto-generated from [wallet-core](https://git.taler.net/wallet-core.git/tree/packages/taler-wallet-core/src/wallet-api-types.ts)
```
## Overview
###  Initialization
* [InitWalletOp](#initwalletop)
###  Basic Wallet Information
* [GetBalancesOp](#getbalancesop)
###  Managing Transactions
* [GetTransactionsOp](#gettransactionsop)
* [DeleteTransactionOp](#deletetransactionop)
* [RetryTransactionOp](#retrytransactionop)
###  Withdrawals
* [GetWithdrawalDetailsForAmountOp](#getwithdrawaldetailsforamountop)
* [GetWithdrawalDetailsForUriOp](#getwithdrawaldetailsforuriop)
* [AcceptBankIntegratedWithdrawalOp](#acceptbankintegratedwithdrawalop)
* [AcceptManualWithdrawalOp](#acceptmanualwithdrawalop)
###  Merchant Payments
* [PreparePayForUriOp](#preparepayforuriop)
* [ConfirmPayOp](#confirmpayop)
* [AbortPayWithRefundOp](#abortpaywithrefundop)
* [ApplyRefundOp](#applyrefundop)
###  Tipping
* [PrepareTipOp](#preparetipop)
* [AcceptTipOp](#accepttipop)
###  Exchange Management
* [ListExchangesOp](#listexchangesop)
* [AddExchangeOp](#addexchangeop)
* [SetExchangeTosAcceptedOp](#setexchangetosacceptedop)
* [GetExchangeTosOp](#getexchangetosop)
* [ListCurrenciesOp](#listcurrenciesop)
###  Deposits
* [CreateDepositGroupOp](#createdepositgroupop)
* [TrackDepositGroupOp](#trackdepositgroupop)
###  Backups
* [ExportBackupRecoveryOp](#exportbackuprecoveryop)
* [ImportBackupRecoveryOp](#importbackuprecoveryop)
* [RunBackupCycleOp](#runbackupcycleop)
* [AddBackupProviderOp](#addbackupproviderop)
* [GetBackupInfoOp](#getbackupinfoop)
* [SetWalletDeviceIdOp](#setwalletdeviceidop)
* [ExportBackupPlainOp](#exportbackupplainop)
###  Peer Payments
* [InitiatePeerPushPaymentOp](#initiatepeerpushpaymentop)
* [CheckPeerPushPaymentOp](#checkpeerpushpaymentop)
* [AcceptPeerPushPaymentOp](#acceptpeerpushpaymentop)
* [InitiatePeerPullPaymentOp](#initiatepeerpullpaymentop)
* [CheckPeerPullPaymentOp](#checkpeerpullpaymentop)
* [AcceptPeerPullPaymentOp](#acceptpeerpullpaymentop)
###  Database Management
* [ExportDbOp](#exportdbop)
* [ClearDbOp](#cleardbop)
* [RecycleOp](#recycleop)
###  Testing and Debugging
* [RunIntegrationTestOp](#runintegrationtestop)
* [WithdrawTestBalanceOp](#withdrawtestbalanceop)
* [WithdrawTestkudosOp](#withdrawtestkudosop)
* [TestPayOp](#testpayop)
* [WithdrawFakebankOp](#withdrawfakebankop)
* [GetPendingTasksOp](#getpendingtasksop)
* [DumpCoinsOp](#dumpcoinsop)
* [SetCoinSuspendedOp](#setcoinsuspendedop)
* [ForceRefreshOp](#forcerefreshop)
## Operation Reference
(initwalletop)=
### InitWalletOp
```typescript
// group: Initialization
/**
 * Initialize wallet-core.
 *
 * Must be the request before any other operations.
 */
export type InitWalletOp = {
  op: WalletApiOperation.InitWallet;
  request: {};
  response: {};
};

```
```typescript
// Enum value:
// WalletApiOperation.InitWallet = "initWallet"

```

(getbalancesop)=
### GetBalancesOp
```typescript
// group: Basic Wallet Information
/**
 * Get current wallet balance.
 */
export type GetBalancesOp = {
  request: {};
  response: BalancesResponse;
};

```
```typescript
export interface BalancesResponse {
  balances: Balance[];
}

```
```typescript
export interface Balance {
  available: AmountString;
  pendingIncoming: AmountString;
  pendingOutgoing: AmountString;
  hasPendingTransactions: boolean;
  requiresUserInput: boolean;
}

```

(gettransactionsop)=
### GetTransactionsOp
```typescript
// group: Managing Transactions
/**
 * Get transactions.
 */
export type GetTransactionsOp = {
  request: TransactionsRequest;
  response: TransactionsResponse;
};

```
```typescript
export interface TransactionsRequest {
  /**
   * return only transactions in the given currency
   */
  currency?: string;
  /**
   * if present, results will be limited to transactions related to the given search string
   */
  search?: string;
}

```
```typescript
export interface TransactionsResponse {
  transactions: Transaction[];
}

```
```typescript
export declare type Transaction =
  | TransactionWithdrawal
  | TransactionPayment
  | TransactionRefund
  | TransactionTip
  | TransactionRefresh
  | TransactionDeposit
  | TransactionPeerPullCredit
  | TransactionPeerPullDebit
  | TransactionPeerPushCredit
  | TransactionPeerPushDebit;

```
```typescript
export interface TransactionWithdrawal extends TransactionCommon {
  type: TransactionType.Withdrawal;
  /**
   * Exchange of the withdrawal.
   */
  exchangeBaseUrl: string;
  /**
   * Amount that got subtracted from the reserve balance.
   */
  amountRaw: AmountString;
  /**
   * Amount that actually was (or will be) added to the wallet's balance.
   */
  amountEffective: AmountString;
  withdrawalDetails: WithdrawalDetails;
}

```
```typescript
// Enum value:
// TransactionType.Withdrawal = "withdrawal"

```
```typescript
export declare type WithdrawalDetails =
  | WithdrawalDetailsForManualTransfer
  | WithdrawalDetailsForTalerBankIntegrationApi;

```
```typescript
interface WithdrawalDetailsForManualTransfer {
  type: WithdrawalType.ManualTransfer;
  /**
   * Payto URIs that the exchange supports.
   *
   * Already contains the amount and message.
   */
  exchangePaytoUris: string[];
  reservePub: string;
}

```
```typescript
// Enum value:
// WithdrawalType.ManualTransfer = "manual-transfer"

```
```typescript
interface WithdrawalDetailsForTalerBankIntegrationApi {
  type: WithdrawalType.TalerBankIntegrationApi;
  /**
   * Set to true if the bank has confirmed the withdrawal, false if not.
   * An unconfirmed withdrawal usually requires user-input and should be highlighted in the UI.
   * See also bankConfirmationUrl below.
   */
  confirmed: boolean;
  /**
   * If the withdrawal is unconfirmed, this can include a URL for user
   * initiated confirmation.
   */
  bankConfirmationUrl?: string;
  reservePub: string;
}

```
```typescript
// Enum value:
// WithdrawalType.TalerBankIntegrationApi = "taler-bank-integration-api"

```
```typescript
export interface TransactionPayment extends TransactionCommon {
  type: TransactionType.Payment;
  /**
   * Additional information about the payment.
   */
  info: OrderShortInfo;
  /**
   * Wallet-internal end-to-end identifier for the payment.
   */
  proposalId: string;
  /**
   * How far did the wallet get with processing the payment?
   */
  status: PaymentStatus;
  /**
   * Amount that must be paid for the contract
   */
  amountRaw: AmountString;
  /**
   * Amount that was paid, including deposit, wire and refresh fees.
   */
  amountEffective: AmountString;
  /**
   * Amount that has been refunded by the merchant
   */
  totalRefundRaw: AmountString;
  /**
   * Amount will be added to the wallet's balance after fees and refreshing
   */
  totalRefundEffective: AmountString;
  /**
   * Amount pending to be picked up
   */
  refundPending: AmountString | undefined;
  /**
   * Reference to applied refunds
   */
  refunds: RefundInfoShort[];
}

```
```typescript
// Enum value:
// TransactionType.Payment = "payment"

```
```typescript
export declare enum PaymentStatus {
  /**
   * Explicitly aborted after timeout / failure
   */
  Aborted = "aborted",
  /**
   * Payment failed, wallet will auto-retry.
   * User should be given the option to retry now / abort.
   */
  Failed = "failed",
  /**
   * Paid successfully
   */
  Paid = "paid",
  /**
   * User accepted, payment is processing.
   */
  Accepted = "accepted",
}

```
```typescript
export interface RefundInfoShort {
  transactionId: string;
  timestamp: TalerProtocolTimestamp;
  amountEffective: AmountString;
  amountRaw: AmountString;
}

```
```typescript
export interface TransactionRefund extends TransactionCommon {
  type: TransactionType.Refund;
  refundedTransactionId: string;
  info: OrderShortInfo;
  /**
   * Amount pending to be picked up
   */
  refundPending: AmountString | undefined;
  amountRaw: AmountString;
  amountEffective: AmountString;
}

```
```typescript
// Enum value:
// TransactionType.Refund = "refund"

```
```typescript
export interface TransactionTip extends TransactionCommon {
  type: TransactionType.Tip;
  amountRaw: AmountString;
  /**
   * More information about the merchant
   */
  amountEffective: AmountString;
  merchantBaseUrl: string;
}

```
```typescript
// Enum value:
// TransactionType.Tip = "tip"

```
```typescript
export interface TransactionRefresh extends TransactionCommon {
  type: TransactionType.Refresh;
  exchangeBaseUrl: string;
  amountRaw: AmountString;
  amountEffective: AmountString;
}

```
```typescript
/**
 * Deposit transaction, which effectively sends
 * money from this wallet somewhere else.
 */
export interface TransactionDeposit extends TransactionCommon {
  type: TransactionType.Deposit;
  depositGroupId: string;
  /**
   * Target for the deposit.
   */
  targetPaytoUri: string;
  /**
   * Raw amount that is being deposited
   */
  amountRaw: AmountString;
  /**
   * Effective amount that is being deposited
   */
  amountEffective: AmountString;
}

```
```typescript
/**
 * Credit because we were paid for a P2P invoice we created.
 */
export interface TransactionPeerPullCredit extends TransactionCommon {
  type: TransactionType.PeerPullCredit;
  info: PeerInfoShort;
  /**
   * Exchange used.
   */
  exchangeBaseUrl: string;
  /**
   * Amount that got subtracted from the reserve balance.
   */
  amountRaw: AmountString;
  /**
   * Amount that actually was (or will be) added to the wallet's balance.
   */
  amountEffective: AmountString;
  /**
   * URI to send to the other party.
   */
  talerUri: string;
}

```
```typescript
// Enum value:
// TransactionType.PeerPullCredit = "peer-pull-credit"

```
```typescript
export interface PeerInfoShort {
  expiration: TalerProtocolTimestamp | undefined;
  summary: string | undefined;
}

```
```typescript
/**
 * Debit because we paid someone's invoice.
 */
export interface TransactionPeerPullDebit extends TransactionCommon {
  type: TransactionType.PeerPullDebit;
  info: PeerInfoShort;
  /**
   * Exchange used.
   */
  exchangeBaseUrl: string;
  amountRaw: AmountString;
  amountEffective: AmountString;
}

```
```typescript
// Enum value:
// TransactionType.PeerPullDebit = "peer-pull-debit"

```
```typescript
/**
 * We received money via a P2P payment.
 */
export interface TransactionPeerPushCredit extends TransactionCommon {
  type: TransactionType.PeerPushCredit;
  info: PeerInfoShort;
  /**
   * Exchange used.
   */
  exchangeBaseUrl: string;
  /**
   * Amount that got subtracted from the reserve balance.
   */
  amountRaw: AmountString;
  /**
   * Amount that actually was (or will be) added to the wallet's balance.
   */
  amountEffective: AmountString;
}

```
```typescript
// Enum value:
// TransactionType.PeerPushCredit = "peer-push-credit"

```
```typescript
/**
 * We sent money via a P2P payment.
 */
export interface TransactionPeerPushDebit extends TransactionCommon {
  type: TransactionType.PeerPushDebit;
  info: PeerInfoShort;
  /**
   * Exchange used.
   */
  exchangeBaseUrl: string;
  /**
   * Amount that got subtracted from the reserve balance.
   */
  amountRaw: AmountString;
  /**
   * Amount that actually was (or will be) added to the wallet's balance.
   */
  amountEffective: AmountString;
  /**
   * URI to accept the payment.
   */
  talerUri: string;
}

```
```typescript
// Enum value:
// TransactionType.PeerPushDebit = "peer-push-debit"

```

(deletetransactionop)=
### DeleteTransactionOp
```typescript
/**
 * Delete a transaction locally in the wallet.
 */
export type DeleteTransactionOp = {
  request: DeleteTransactionRequest;
  response: {};
};

```
```typescript
export interface DeleteTransactionRequest {
  transactionId: string;
}

```

(retrytransactionop)=
### RetryTransactionOp
```typescript
/**
 * Immediately retry a transaction.
 */
export type RetryTransactionOp = {
  request: RetryTransactionRequest;
  response: {};
};

```
```typescript
export interface RetryTransactionRequest {
  transactionId: string;
}

```

(getwithdrawaldetailsforamountop)=
### GetWithdrawalDetailsForAmountOp
```typescript
// group: Withdrawals
/**
 * Get details for withdrawing a particular amount (manual withdrawal).
 */
export type GetWithdrawalDetailsForAmountOp = {
  request: GetWithdrawalDetailsForAmountRequest;
  response: ManualWithdrawalDetails;
};

```
```typescript
export interface GetWithdrawalDetailsForAmountRequest {
  exchangeBaseUrl: string;
  amount: string;
  restrictAge?: number;
}

```
```typescript
export interface ManualWithdrawalDetails {
  /**
   * Did the user accept the current version of the exchange's
   * terms of service?
   */
  tosAccepted: boolean;
  /**
   * Amount that the user will transfer to the exchange.
   */
  amountRaw: AmountString;
  /**
   * Amount that will be added to the user's wallet balance.
   */
  amountEffective: AmountString;
  /**
   * Ways to pay the exchange.
   */
  paytoUris: string[];
}

```

(getwithdrawaldetailsforuriop)=
### GetWithdrawalDetailsForUriOp
```typescript
/**
 * Get details for withdrawing via a particular taler:// URI.
 */
export type GetWithdrawalDetailsForUriOp = {
  request: GetWithdrawalDetailsForUriRequest;
  response: WithdrawUriInfoResponse;
};

```
```typescript
export interface GetWithdrawalDetailsForUriRequest {
  talerWithdrawUri: string;
  restrictAge?: number;
}

```
```typescript
export interface WithdrawUriInfoResponse {
  amount: AmountString;
  defaultExchangeBaseUrl?: string;
  possibleExchanges: ExchangeListItem[];
}

```

(acceptbankintegratedwithdrawalop)=
### AcceptBankIntegratedWithdrawalOp
```typescript
/**
 * Accept a bank-integrated withdrawal.
 */
export type AcceptBankIntegratedWithdrawalOp = {
  request: AcceptBankIntegratedWithdrawalRequest;
  response: AcceptWithdrawalResponse;
};

```
```typescript
export interface AcceptBankIntegratedWithdrawalRequest {
  talerWithdrawUri: string;
  exchangeBaseUrl: string;
  forcedDenomSel?: ForcedDenomSel;
  restrictAge?: number;
}

```
```typescript
export interface AcceptWithdrawalResponse {
  reservePub: string;
  confirmTransferUrl?: string;
  transactionId: string;
}

```

(acceptmanualwithdrawalop)=
### AcceptManualWithdrawalOp
```typescript
/**
 * Create a manual withdrawal.
 */
export type AcceptManualWithdrawalOp = {
  request: AcceptManualWithdrawalRequest;
  response: AcceptManualWithdrawalResult;
};

```
```typescript
export interface AcceptManualWithdrawalRequest {
  exchangeBaseUrl: string;
  amount: string;
  restrictAge?: number;
}

```
```typescript
export interface AcceptManualWithdrawalResult {
  /**
   * Payto URIs that can be used to fund the withdrawal.
   */
  exchangePaytoUris: string[];
  /**
   * Public key of the newly created reserve.
   */
  reservePub: string;
  transactionId: string;
}

```

(preparepayforuriop)=
### PreparePayForUriOp
```typescript
// group: Merchant Payments
/**
 * Prepare to make a payment
 */
export type PreparePayForUriOp = {
  op: WalletApiOperation.PreparePayForUri;
  request: PreparePayRequest;
  response: PreparePayResult;
};

```
```typescript
// Enum value:
// WalletApiOperation.PreparePayForUri = "preparePayForUri"

```
```typescript
export interface PreparePayRequest {
  talerPayUri: string;
}

```
```typescript
/**
 * Result of a prepare pay operation.
 */
export declare type PreparePayResult =
  | PreparePayResultInsufficientBalance
  | PreparePayResultAlreadyConfirmed
  | PreparePayResultPaymentPossible;

```
```typescript
export interface PreparePayResultInsufficientBalance {
  status: PreparePayResultType.InsufficientBalance;
  proposalId: string;
  contractTerms: ContractTerms;
  amountRaw: string;
  noncePriv: string;
}

```
```typescript
export interface PreparePayResultAlreadyConfirmed {
  status: PreparePayResultType.AlreadyConfirmed;
  contractTerms: ContractTerms;
  paid: boolean;
  amountRaw: string;
  amountEffective: string;
  contractTermsHash: string;
  proposalId: string;
}

```
```typescript
// Enum value:
// PreparePayResultType.AlreadyConfirmed = "already-confirmed"

```
```typescript
/**
 * Payment is possible.
 */
export interface PreparePayResultPaymentPossible {
  status: PreparePayResultType.PaymentPossible;
  proposalId: string;
  contractTerms: ContractTerms;
  contractTermsHash: string;
  amountRaw: string;
  amountEffective: string;
  noncePriv: string;
}

```
```typescript
// Enum value:
// PreparePayResultType.PaymentPossible = "payment-possible"

```

(confirmpayop)=
### ConfirmPayOp
```typescript
/**
 * Confirm a payment that was previously prepared with
 * {@link PreparePayForUriOp}
 */
export type ConfirmPayOp = {
  op: WalletApiOperation.ConfirmPay;
  request: ConfirmPayRequest;
  response: ConfirmPayResult;
};

```
```typescript
// Enum value:
// WalletApiOperation.ConfirmPay = "confirmPay"

```
```typescript
export interface ConfirmPayRequest {
  proposalId: string;
  sessionId?: string;
  forcedCoinSel?: ForcedCoinSel;
}

```
```typescript
export declare type ConfirmPayResult =
  | ConfirmPayResultDone
  | ConfirmPayResultPending;

```
```typescript
/**
 * Result for confirmPay
 */
export interface ConfirmPayResultDone {
  type: ConfirmPayResultType.Done;
  contractTerms: ContractTerms;
  transactionId: string;
}

```
```typescript
// Enum value:
// ConfirmPayResultType.Done = "done"

```
```typescript
export interface ConfirmPayResultPending {
  type: ConfirmPayResultType.Pending;
  transactionId: string;
  lastError: TalerErrorDetail | undefined;
}

```

(abortpaywithrefundop)=
### AbortPayWithRefundOp
```typescript
/**
 * Abort a pending payment with a refund.
 */
export type AbortPayWithRefundOp = {
  request: AbortPayWithRefundRequest;
  response: {};
};

```
```typescript
export interface AbortPayWithRefundRequest {
  proposalId: string;
}

```

(applyrefundop)=
### ApplyRefundOp
```typescript
/**
 * Check for a refund based on a taler://refund URI.
 */
export type ApplyRefundOp = {
  request: ApplyRefundRequest;
  response: ApplyRefundResponse;
};

```
```typescript
export interface ApplyRefundRequest {
  talerRefundUri: string;
}

```
```typescript
export interface ApplyRefundResponse {
  contractTermsHash: string;
  transactionId: string;
  proposalId: string;
  amountEffectivePaid: AmountString;
  amountRefundGranted: AmountString;
  amountRefundGone: AmountString;
  pendingAtExchange: boolean;
  info: OrderShortInfo;
}

```

(preparetipop)=
### PrepareTipOp
```typescript
// group: Tipping
/**
 * Query and store information about a tip.
 */
export type PrepareTipOp = {
  request: PrepareTipRequest;
  response: PrepareTipResult;
};

```
```typescript
export interface PrepareTipRequest {
  talerTipUri: string;
}

```
```typescript
export interface PrepareTipResult {
  /**
   * Unique ID for the tip assigned by the wallet.
   * Typically different from the merchant-generated tip ID.
   */
  walletTipId: string;
  /**
   * Has the tip already been accepted?
   */
  accepted: boolean;
  /**
   * Amount that the merchant gave.
   */
  tipAmountRaw: AmountString;
  /**
   * Amount that arrived at the wallet.
   * Might be lower than the raw amount due to fees.
   */
  tipAmountEffective: AmountString;
  /**
   * Base URL of the merchant backend giving then tip.
   */
  merchantBaseUrl: string;
  /**
   * Base URL of the exchange that is used to withdraw the tip.
   * Determined by the merchant, the wallet/user has no choice here.
   */
  exchangeBaseUrl: string;
  /**
   * Time when the tip will expire.  After it expired, it can't be picked
   * up anymore.
   */
  expirationTimestamp: TalerProtocolTimestamp;
}

```

(accepttipop)=
### AcceptTipOp
```typescript
/**
 * Accept a tip.
 */
export type AcceptTipOp = {
  request: AcceptTipRequest;
  response: {};
};

```
```typescript
export interface AcceptTipRequest {
  walletTipId: string;
}

```

(listexchangesop)=
### ListExchangesOp
```typescript
// group: Exchange Management
/**
 * List exchanges known to the wallet.
 */
export type ListExchangesOp = {
  request: {};
  response: ExchangesListResponse;
};

```
```typescript
export interface ExchangesListResponse {
  exchanges: ExchangeListItem[];
}

```

(addexchangeop)=
### AddExchangeOp
```typescript
/**
 * Add / force-update an exchange.
 */
export type AddExchangeOp = {
  request: AddExchangeRequest;
  response: {};
};

```
```typescript
export interface AddExchangeRequest {
  exchangeBaseUrl: string;
  forceUpdate?: boolean;
}

```

(setexchangetosacceptedop)=
### SetExchangeTosAcceptedOp
```typescript
/**
 * Accept a particular version of the exchange terms of service.
 */
export type SetExchangeTosAcceptedOp = {
  request: AcceptExchangeTosRequest;
  response: {};
};

```
```typescript
export interface AcceptExchangeTosRequest {
  exchangeBaseUrl: string;
  etag: string | undefined;
}

```

(getexchangetosop)=
### GetExchangeTosOp
```typescript
/**
 * Get the current terms of a service of an exchange.
 */
export type GetExchangeTosOp = {
  request: GetExchangeTosRequest;
  response: GetExchangeTosResult;
};

```
```typescript
export interface GetExchangeTosRequest {
  exchangeBaseUrl: string;
  acceptedFormat?: string[];
}

```
```typescript
export interface GetExchangeTosResult {
  /**
   * Markdown version of the current ToS.
   */
  content: string;
  /**
   * Version tag of the current ToS.
   */
  currentEtag: string;
  /**
   * Version tag of the last ToS that the user has accepted,
   * if any.
   */
  acceptedEtag: string | undefined;
  /**
   * Accepted content type
   */
  contentType: string;
}

```

(listcurrenciesop)=
### ListCurrenciesOp
```typescript
/**
 * List currencies known to the wallet.
 */
export type ListCurrenciesOp = {
  request: {};
  response: WalletCurrencyInfo;
};

```
```typescript
export interface WalletCurrencyInfo {
  trustedAuditors: {
    currency: string;
    auditorPub: string;
    auditorBaseUrl: string;
  }[];
  trustedExchanges: {
    currency: string;
    exchangeMasterPub: string;
    exchangeBaseUrl: string;
  }[];
}

```

(createdepositgroupop)=
### CreateDepositGroupOp
```typescript
// group: Deposits
/**
 * Create a new deposit group.
 *
 * Deposit groups are used to deposit multiple coins to a bank
 * account, usually the wallet user's own bank account.
 */
export type CreateDepositGroupOp = {
  request: CreateDepositGroupRequest;
  response: CreateDepositGroupResponse;
};

```
```typescript
export interface CreateDepositGroupRequest {
  depositPaytoUri: string;
  amount: AmountString;
}

```
```typescript
export interface CreateDepositGroupResponse {
  depositGroupId: string;
  transactionId: string;
}

```

(trackdepositgroupop)=
### TrackDepositGroupOp
```typescript
/**
 * Track the status of a deposit group by querying the exchange.
 */
export type TrackDepositGroupOp = {
  request: TrackDepositGroupRequest;
  response: TrackDepositGroupResponse;
};

```
```typescript
export interface TrackDepositGroupRequest {
  depositGroupId: string;
}

```
```typescript
export interface TrackDepositGroupResponse {
  responses: {
    status: number;
    body: any;
  }[];
}

```

(exportbackuprecoveryop)=
### ExportBackupRecoveryOp
```typescript
// group: Backups
/**
 * Export the recovery information for the wallet.
 */
export type ExportBackupRecoveryOp = {
  request: {};
  response: BackupRecovery;
};

```

(importbackuprecoveryop)=
### ImportBackupRecoveryOp
```typescript
/**
 * Import recovery information into the wallet.
 */
export type ImportBackupRecoveryOp = {
  request: RecoveryLoadRequest;
  response: {};
};

```
```typescript
/**
 * Load recovery information into the wallet.
 */
export interface RecoveryLoadRequest {
  recovery: BackupRecovery;
  strategy?: RecoveryMergeStrategy;
}

```
```typescript
/**
 * Strategy for loading recovery information.
 */
export declare enum RecoveryMergeStrategy {
  /**
   * Keep the local wallet root key, import and take over providers.
   */
  Ours = "ours",
  /**
   * Migrate to the wallet root key from the recovery information.
   */
  Theirs = "theirs",
}

```

(runbackupcycleop)=
### RunBackupCycleOp
```typescript
/**
 * Manually make and upload a backup.
 */
export type RunBackupCycleOp = {
  request: {};
  response: {};
};

```

(addbackupproviderop)=
### AddBackupProviderOp
```typescript
/**
 * Add a new backup provider.
 */
export type AddBackupProviderOp = {
  request: AddBackupProviderRequest;
  response: {};
};

```
```typescript
export interface AddBackupProviderRequest {
  backupProviderBaseUrl: string;
  name: string;
  /**
   * Activate the provider.  Should only be done after
   * the user has reviewed the provider.
   */
  activate?: boolean;
}

```

(getbackupinfoop)=
### GetBackupInfoOp
```typescript
/**
 * Get some useful stats about the backup state.
 */
export type GetBackupInfoOp = {
  request: {};
  response: BackupInfo;
};

```
```typescript
export interface BackupInfo {
  walletRootPub: string;
  deviceId: string;
  providers: ProviderInfo[];
}

```
```typescript
/**
 * Information about one provider.
 *
 * We don't store the account key here,
 * as that's derived from the wallet root key.
 */
export interface ProviderInfo {
  active: boolean;
  syncProviderBaseUrl: string;
  name: string;
  terms?: BackupProviderTerms;
  /**
   * Last communication issue with the provider.
   */
  lastError?: TalerErrorDetail;
  lastSuccessfulBackupTimestamp?: TalerProtocolTimestamp;
  lastAttemptedBackupTimestamp?: TalerProtocolTimestamp;
  paymentProposalIds: string[];
  backupProblem?: BackupProblem;
  paymentStatus: ProviderPaymentStatus;
}

```
```typescript
export interface BackupProviderTerms {
  supportedProtocolVersion: string;
  annualFee: AmountString;
  storageLimitInMegabytes: number;
}

```
```typescript
export type BackupProblem =
  | BackupUnreadableProblem
  | BackupConflictingDeviceProblem;

```
```typescript
export interface BackupUnreadableProblem {
  type: "backup-unreadable";
}

```
```typescript
export interface BackupConflictingDeviceProblem {
  type: "backup-conflicting-device";
  otherDeviceId: string;
  myDeviceId: string;
  backupTimestamp: AbsoluteTime;
}

```
```typescript
export type ProviderPaymentStatus =
  | ProviderPaymentTermsChanged
  | ProviderPaymentPaid
  | ProviderPaymentInsufficientBalance
  | ProviderPaymentUnpaid
  | ProviderPaymentPending;

```
```typescript
export interface ProviderPaymentTermsChanged {
  type: ProviderPaymentType.TermsChanged;
  paidUntil: AbsoluteTime;
  oldTerms: BackupProviderTerms;
  newTerms: BackupProviderTerms;
}

```
```typescript
// Enum value:
// ProviderPaymentType.TermsChanged = "terms-changed"

```
```typescript
export interface ProviderPaymentPaid {
  type: ProviderPaymentType.Paid;
  paidUntil: AbsoluteTime;
}

```
```typescript
// Enum value:
// ProviderPaymentType.Paid = "paid"

```
```typescript
export interface ProviderPaymentInsufficientBalance {
  type: ProviderPaymentType.InsufficientBalance;
}

```
```typescript
export interface ProviderPaymentUnpaid {
  type: ProviderPaymentType.Unpaid;
}

```
```typescript
// Enum value:
// ProviderPaymentType.Unpaid = "unpaid"

```
```typescript
export interface ProviderPaymentPending {
  type: ProviderPaymentType.Pending;
}

```

(setwalletdeviceidop)=
### SetWalletDeviceIdOp
```typescript
/**
 * Set the internal device ID of the wallet, used to
 * identify whether a different/new wallet is accessing
 * the backup of another wallet.
 */
export type SetWalletDeviceIdOp = {
  request: SetWalletDeviceIdRequest;
  response: {};
};

```
```typescript
export interface SetWalletDeviceIdRequest {
  /**
   * New wallet device ID to set.
   */
  walletDeviceId: string;
}

```

(exportbackupplainop)=
### ExportBackupPlainOp
```typescript
/**
 * Export a backup JSON, mostly useful for testing.
 */
export type ExportBackupPlainOp = {
  request: {};
  response: WalletBackupContentV1;
};

```

(initiatepeerpushpaymentop)=
### InitiatePeerPushPaymentOp
```typescript
// group: Peer Payments
/**
 * Initiate an outgoing peer push payment.
 */
export type InitiatePeerPushPaymentOp = {
  request: InitiatePeerPushPaymentRequest;
  response: InitiatePeerPushPaymentResponse;
};

```
```typescript
export interface InitiatePeerPushPaymentRequest {
  amount: AmountString;
  partialContractTerms: any;
}

```
```typescript
export interface InitiatePeerPushPaymentResponse {
  exchangeBaseUrl: string;
  pursePub: string;
  mergePriv: string;
  contractPriv: string;
  talerUri: string;
  transactionId: string;
}

```

(checkpeerpushpaymentop)=
### CheckPeerPushPaymentOp
```typescript
/**
 * Check an incoming peer push payment.
 */
export type CheckPeerPushPaymentOp = {
  request: CheckPeerPushPaymentRequest;
  response: CheckPeerPushPaymentResponse;
};

```
```typescript
export interface CheckPeerPushPaymentRequest {
  talerUri: string;
}

```
```typescript
export interface CheckPeerPushPaymentResponse {
  contractTerms: any;
  amount: AmountString;
  peerPushPaymentIncomingId: string;
}

```

(acceptpeerpushpaymentop)=
### AcceptPeerPushPaymentOp
```typescript
/**
 * Accept an incoming peer push payment.
 */
export type AcceptPeerPushPaymentOp = {
  request: AcceptPeerPushPaymentRequest;
  response: {};
};

```
```typescript
export interface AcceptPeerPushPaymentRequest {
  /**
   * Transparent identifier of the incoming peer push payment.
   */
  peerPushPaymentIncomingId: string;
}

```

(initiatepeerpullpaymentop)=
### InitiatePeerPullPaymentOp
```typescript
/**
 * Initiate an outgoing peer pull payment.
 */
export type InitiatePeerPullPaymentOp = {
  request: InitiatePeerPullPaymentRequest;
  response: InitiatePeerPullPaymentResponse;
};

```
```typescript
export interface InitiatePeerPullPaymentRequest {
  /**
   * FIXME: Make this optional?
   */
  exchangeBaseUrl: string;
  amount: AmountString;
  partialContractTerms: any;
}

```
```typescript
export interface InitiatePeerPullPaymentResponse {
  /**
   * Taler URI for the other party to make the payment
   * that was requested.
   */
  talerUri: string;
  transactionId: string;
}

```

(checkpeerpullpaymentop)=
### CheckPeerPullPaymentOp
```typescript
/**
 * Prepare for an incoming peer pull payment.
 */
export type CheckPeerPullPaymentOp = {
  request: CheckPeerPullPaymentRequest;
  response: CheckPeerPullPaymentResponse;
};

```
```typescript
export interface CheckPeerPullPaymentRequest {
  talerUri: string;
}

```
```typescript
export interface CheckPeerPullPaymentResponse {
  contractTerms: any;
  amount: AmountString;
  peerPullPaymentIncomingId: string;
}

```

(acceptpeerpullpaymentop)=
### AcceptPeerPullPaymentOp
```typescript
/**
 * Accept an incoming peer pull payment.
 */
export type AcceptPeerPullPaymentOp = {
  request: AcceptPeerPullPaymentRequest;
  response: {};
};

```
```typescript
export interface AcceptPeerPullPaymentRequest {
  /**
   * Transparent identifier of the incoming peer pull payment.
   */
  peerPullPaymentIncomingId: string;
}

```

(exportdbop)=
### ExportDbOp
```typescript
// group: Database Management
/**
 * Exoport the wallet database's contents to JSON.
 */
export type ExportDbOp = {
  request: {};
  response: any;
};

```

(cleardbop)=
### ClearDbOp
```typescript
/**
 * Dangerously clear the whole wallet database.
 */
export type ClearDbOp = {
  request: {};
  response: {};
};

```

(recycleop)=
### RecycleOp
```typescript
/**
 * Export a backup, clear the database and re-import it.
 */
export type RecycleOp = {
  request: {};
  response: {};
};

```

(runintegrationtestop)=
### RunIntegrationTestOp
```typescript
// group: Testing and Debugging
/**
 * Run a simple integration test on a test deployment
 * of the exchange and merchant.
 */
export type RunIntegrationTestOp = {
  request: IntegrationTestArgs;
  response: {};
};

```
```typescript
export interface IntegrationTestArgs {
  exchangeBaseUrl: string;
  bankBaseUrl: string;
  bankAccessApiBaseUrl?: string;
  merchantBaseUrl: string;
  merchantAuthToken?: string;
  amountToWithdraw: string;
  amountToSpend: string;
}

```

(withdrawtestbalanceop)=
### WithdrawTestBalanceOp
```typescript
/**
 * Make withdrawal on a test deployment of the exchange
 * and merchant.
 */
export type WithdrawTestBalanceOp = {
  request: WithdrawTestBalanceRequest;
  response: {};
};

```
```typescript
export interface WithdrawTestBalanceRequest {
  amount: string;
  bankBaseUrl: string;
  /**
   * Bank access API base URL.  Defaults to the bankBaseUrl.
   */
  bankAccessApiBaseUrl?: string;
  exchangeBaseUrl: string;
  forcedDenomSel?: ForcedDenomSel;
}

```

(withdrawtestkudosop)=
### WithdrawTestkudosOp
```typescript
/**
 * Make a withdrawal of testkudos on test.taler.net.
 */
export type WithdrawTestkudosOp = {
  op: WalletApiOperation.WithdrawTestkudos;
  request: {};
  response: {};
};

```
```typescript
// Enum value:
// WalletApiOperation.WithdrawTestkudos = "withdrawTestkudos"

```

(testpayop)=
### TestPayOp
```typescript
/**
 * Make a test payment using a test deployment of
 * the exchange and merchant.
 */
export type TestPayOp = {
  request: TestPayArgs;
  response: TestPayResult;
};

```
```typescript
export interface TestPayArgs {
  merchantBaseUrl: string;
  merchantAuthToken?: string;
  amount: string;
  summary: string;
  forcedCoinSel?: ForcedCoinSel;
}

```
```typescript
export interface TestPayResult {
  payCoinSelection: PayCoinSelection;
}

```
```typescript
/**
 * Result of selecting coins, contains the exchange, and selected
 * coins with their denomination.
 */
export interface PayCoinSelection {
  /**
   * Amount requested by the merchant.
   */
  paymentAmount: AmountJson;
  /**
   * Public keys of the coins that were selected.
   */
  coinPubs: string[];
  /**
   * Amount that each coin contributes.
   */
  coinContributions: AmountJson[];
  /**
   * How much of the wire fees is the customer paying?
   */
  customerWireFees: AmountJson;
  /**
   * How much of the deposit fees is the customer paying?
   */
  customerDepositFees: AmountJson;
}

```
```typescript
/**
 * Non-negative financial amount.  Fractional values are expressed as multiples
 * of 1e-8.
 */
export interface AmountJson {
  /**
   * Value, must be an integer.
   */
  readonly value: number;
  /**
   * Fraction, must be an integer.  Represent 1/1e8 of a unit.
   */
  readonly fraction: number;
  /**
   * Currency of the amount.
   */
  readonly currency: string;
}

```

(withdrawfakebankop)=
### WithdrawFakebankOp
```typescript
/**
 * Make a withdrawal from a fakebank, i.e.
 * a bank where test users can be registered freely
 * and testing APIs are available.
 */
export type WithdrawFakebankOp = {
  op: WalletApiOperation.WithdrawFakebank;
  request: WithdrawFakebankRequest;
  response: {};
};

```
```typescript
// Enum value:
// WalletApiOperation.WithdrawFakebank = "withdrawFakebank"

```
```typescript
export interface WithdrawFakebankRequest {
  amount: AmountString;
  exchange: string;
  bank: string;
}

```

(getpendingtasksop)=
### GetPendingTasksOp
```typescript
/**
 * Get wallet-internal pending tasks.
 */
export type GetPendingTasksOp = {
  request: {};
  response: PendingTasksResponse;
};

```
```typescript
/**
 * Response returned from the pending operations API.
 */
export interface PendingOperationsResponse {
  /**
   * List of pending operations.
   */
  pendingOperations: PendingTaskInfo[];
}

```
```typescript
/**
 * Information about a pending operation.
 */
export type PendingTaskInfo = PendingTaskInfoCommon &
  (
    | PendingExchangeUpdateTask
    | PendingExchangeCheckRefreshTask
    | PendingPayTask
    | PendingProposalDownloadTask
    | PendingRefreshTask
    | PendingRefundQueryTask
    | PendingTipPickupTask
    | PendingWithdrawTask
    | PendingRecoupTask
    | PendingDepositTask
    | PendingBackupTask
  );

```
```typescript
/**
 * Fields that are present in every pending operation.
 */
export interface PendingTaskInfoCommon {
  /**
   * Type of the pending operation.
   */
  type: PendingTaskType;
  /**
   * Unique identifier for the pending task.
   */
  id: string;
  /**
   * Set to true if the operation indicates that something is really in progress,
   * as opposed to some regular scheduled operation that can be tried later.
   */
  givesLifeness: boolean;
  /**
   * Timestamp when the pending operation should be executed next.
   */
  timestampDue: AbsoluteTime;
  /**
   * Retry info.  Currently used to stop the wallet after any operation
   * exceeds a number of retries.
   */
  retryInfo?: RetryInfo;
}

```
```typescript
export enum PendingTaskType {
  ExchangeUpdate = "exchange-update",
  ExchangeCheckRefresh = "exchange-check-refresh",
  Pay = "pay",
  ProposalDownload = "proposal-download",
  Refresh = "refresh",
  Recoup = "recoup",
  RefundQuery = "refund-query",
  TipPickup = "tip-pickup",
  Withdraw = "withdraw",
  Deposit = "deposit",
  Backup = "backup",
}

```
```typescript
export interface RetryInfo {
  firstTry: AbsoluteTime;
  nextRetry: AbsoluteTime;
  retryCounter: number;
}

```
```typescript
export interface RetryPolicy {
  readonly backoffDelta: Duration;
  readonly backoffBase: number;
  readonly maxTimeout: Duration;
}

```
```typescript
/**
 * The wallet is currently updating information about an exchange.
 */
export interface PendingExchangeUpdateTask {
  type: PendingTaskType.ExchangeUpdate;
  exchangeBaseUrl: string;
  lastError: TalerErrorDetail | undefined;
}

```
```typescript
// Enum value:
// PendingTaskType.ExchangeUpdate = "exchange-update"

```
```typescript
/**
 * The wallet should check whether coins from this exchange
 * need to be auto-refreshed.
 */
export interface PendingExchangeCheckRefreshTask {
  type: PendingTaskType.ExchangeCheckRefresh;
  exchangeBaseUrl: string;
}

```
```typescript
// Enum value:
// PendingTaskType.ExchangeCheckRefresh = "exchange-check-refresh"

```
```typescript
/**
 * The wallet is signing coins and then sending them to
 * the merchant.
 */
export interface PendingPayTask {
  type: PendingTaskType.Pay;
  proposalId: string;
  isReplay: boolean;
  retryInfo?: RetryInfo;
  lastError: TalerErrorDetail | undefined;
}

```
```typescript
// Enum value:
// PendingTaskType.Pay = "pay"

```
```typescript
/**
 * Status of downloading signed contract terms from a merchant.
 */
export interface PendingProposalDownloadTask {
  type: PendingTaskType.ProposalDownload;
  merchantBaseUrl: string;
  proposalTimestamp: TalerProtocolTimestamp;
  proposalId: string;
  orderId: string;
  lastError?: TalerErrorDetail;
  retryInfo?: RetryInfo;
}

```
```typescript
// Enum value:
// PendingTaskType.ProposalDownload = "proposal-download"

```
```typescript
/**
 * Status of an ongoing withdrawal operation.
 */
export interface PendingRefreshTask {
  type: PendingTaskType.Refresh;
  lastError?: TalerErrorDetail;
  refreshGroupId: string;
  finishedPerCoin: boolean[];
  retryInfo?: RetryInfo;
}

```
```typescript
/**
 * The wallet is querying the merchant about whether any refund
 * permissions are available for a purchase.
 */
export interface PendingRefundQueryTask {
  type: PendingTaskType.RefundQuery;
  proposalId: string;
  retryInfo?: RetryInfo;
  lastError: TalerErrorDetail | undefined;
}

```
```typescript
// Enum value:
// PendingTaskType.RefundQuery = "refund-query"

```
```typescript
/**
 * The wallet is picking up a tip that the user has accepted.
 */
export interface PendingTipPickupTask {
  type: PendingTaskType.TipPickup;
  tipId: string;
  merchantBaseUrl: string;
  merchantTipId: string;
}

```
```typescript
// Enum value:
// PendingTaskType.TipPickup = "tip-pickup"

```
```typescript
/**
 * Status of an ongoing withdrawal operation.
 */
export interface PendingWithdrawTask {
  type: PendingTaskType.Withdraw;
  lastError: TalerErrorDetail | undefined;
  retryInfo?: RetryInfo;
  withdrawalGroupId: string;
}

```
```typescript
// Enum value:
// PendingTaskType.Withdraw = "withdraw"

```
```typescript
export interface PendingRecoupTask {
  type: PendingTaskType.Recoup;
  recoupGroupId: string;
  retryInfo?: RetryInfo;
  lastError: TalerErrorDetail | undefined;
}

```
```typescript
// Enum value:
// PendingTaskType.Recoup = "recoup"

```
```typescript
/**
 * Status of an ongoing deposit operation.
 */
export interface PendingDepositTask {
  type: PendingTaskType.Deposit;
  lastError: TalerErrorDetail | undefined;
  retryInfo: RetryInfo | undefined;
  depositGroupId: string;
}

```
```typescript
export interface PendingBackupTask {
  type: PendingTaskType.Backup;
  backupProviderBaseUrl: string;
  lastError: TalerErrorDetail | undefined;
}

```
```typescript
// Enum value:
// PendingTaskType.Backup = "backup"

```

(dumpcoinsop)=
### DumpCoinsOp
```typescript
/**
 * Dump all coins of the wallet in a simple JSON format.
 */
export type DumpCoinsOp = {
  request: {};
  response: CoinDumpJson;
};

```
```typescript
/**
 * Easy to process format for the public data of coins
 * managed by the wallet.
 */
export interface CoinDumpJson {
  coins: Array<{
    /**
     * The coin's denomination's public key.
     */
    denom_pub: DenominationPubKey;
    /**
     * Hash of denom_pub.
     */
    denom_pub_hash: string;
    /**
     * Value of the denomination (without any fees).
     */
    denom_value: string;
    /**
     * Public key of the coin.
     */
    coin_pub: string;
    /**
     * Base URL of the exchange for the coin.
     */
    exchange_base_url: string;
    /**
     * Remaining value on the coin, to the knowledge of
     * the wallet.
     */
    remaining_value: string;
    /**
     * Public key of the parent coin.
     * Only present if this coin was obtained via refreshing.
     */
    refresh_parent_coin_pub: string | undefined;
    /**
     * Public key of the reserve for this coin.
     * Only present if this coin was obtained via refreshing.
     */
    withdrawal_reserve_pub: string | undefined;
    /**
     * Is the coin suspended?
     * Suspended coins are not considered for payments.
     */
    coin_suspended: boolean;
    /**
     * Information about the age restriction
     */
    ageCommitmentProof: AgeCommitmentProof | undefined;
  }>;
}

```
```typescript
interface Array<T> extends RelativeIndexable<T> {}

```

(setcoinsuspendedop)=
### SetCoinSuspendedOp
```typescript
/**
 * Set a coin as (un-)suspended.
 * Suspended coins won't be used for payments.
 */
export type SetCoinSuspendedOp = {
  request: SetCoinSuspendedRequest;
  response: {};
};

```
```typescript
export interface SetCoinSuspendedRequest {
  coinPub: string;
  suspended: boolean;
}

```

(forcerefreshop)=
### ForceRefreshOp
```typescript
/**
 * Force a refresh on coins where it would not
 * be necessary.
 */
export type ForceRefreshOp = {
  request: ForceRefreshRequest;
  response: {};
};

```
```typescript
export interface ForceRefreshRequest {
  coinPubList: string[];
}

```

## Common Declarations
```typescript
export interface OrderShortInfo {
  /**
   * Order ID, uniquely identifies the order within a merchant instance
   */
  orderId: string;
  /**
   * Hash of the contract terms.
   */
  contractTermsHash: string;
  /**
   * More information about the merchant
   */
  merchant: MerchantInfo;
  /**
   * Summary of the order, given by the merchant
   */
  summary: string;
  /**
   * Map from IETF BCP 47 language tags to localized summaries
   */
  summary_i18n?: InternationalizedString;
  /**
   * List of products that are part of the order
   */
  products: Product[] | undefined;
  /**
   * Time indicating when the order should be delivered.
   * May be overwritten by individual products.
   */
  delivery_date?: TalerProtocolTimestamp;
  /**
   * Delivery location for (all!) products.
   */
  delivery_location?: Location;
  /**
   * URL of the fulfillment, given by the merchant
   */
  fulfillmentUrl?: string;
  /**
   * Plain text message that should be shown to the user
   * when the payment is complete.
   */
  fulfillmentMessage?: string;
  /**
   * Translations of fulfillmentMessage.
   */
  fulfillmentMessage_i18n?: InternationalizedString;
}
```
```typescript
export interface MerchantInfo {
  name: string;
  jurisdiction?: Location;
  address?: Location;
  logo?: string;
  website?: string;
  email?: string;
}
```
```typescript
export interface Location {
  country?: string;
  country_subdivision?: string;
  district?: string;
  town?: string;
  town_location?: string;
  post_code?: string;
  street?: string;
  building_name?: string;
  building_number?: string;
  address_lines?: string[];
}
```
```typescript
export interface InternationalizedString {
  [lang_tag: string]: string;
}
```
```typescript
export interface Product {
  product_id?: string;
  description: string;
  description_i18n?: {
    [lang_tag: string]: string;
  };
  quantity?: number;
  unit?: string;
  price?: AmountString;
  image?: string;
  taxes?: Tax[];
  delivery_date?: TalerProtocolTimestamp;
}
```
```typescript
export interface Tax {
  name: string;
  tax: AmountString;
}
```
```typescript
export interface TalerProtocolTimestamp {
  readonly t_s: number | "never";
}
```
```typescript
// Enum value:
// PendingTaskType.Refresh = "refresh"
```
```typescript
// Enum value:
// PendingTaskType.Deposit = "deposit"
```
```typescript
export interface ExchangeListItem {
  exchangeBaseUrl: string;
  currency: string;
  paytoUris: string[];
  tos: ExchangeTos;
}
```
```typescript
export interface ExchangeTos {
  acceptedVersion?: string;
  currentVersion?: string;
  contentType?: string;
  content?: string;
}
```
```typescript
export interface ForcedDenomSel {
  denoms: {
    value: AmountString;
    count: number;
  }[];
}
```
```typescript
// Enum value:
// ProviderPaymentType.InsufficientBalance = "insufficient-balance"
```
```typescript
/**
 * Contract terms from a merchant.
 */
export interface ContractTerms {
  /**
   * Hash of the merchant's wire details.
   */
  h_wire: string;
  /**
   * Hash of the merchant's wire details.
   */
  auto_refund?: TalerProtocolDuration;
  /**
   * Wire method the merchant wants to use.
   */
  wire_method: string;
  /**
   * Human-readable short summary of the contract.
   */
  summary: string;
  summary_i18n?: InternationalizedString;
  /**
   * Nonce used to ensure freshness.
   */
  nonce: string;
  /**
   * Total amount payable.
   */
  amount: string;
  /**
   * Auditors accepted by the merchant.
   */
  auditors: AuditorHandle[];
  /**
   * Deadline to pay for the contract.
   */
  pay_deadline: TalerProtocolTimestamp;
  /**
   * Maximum deposit fee covered by the merchant.
   */
  max_fee: string;
  /**
   * Information about the merchant.
   */
  merchant: MerchantInfo;
  /**
   * Public key of the merchant.
   */
  merchant_pub: string;
  /**
   * Time indicating when the order should be delivered.
   * May be overwritten by individual products.
   */
  delivery_date?: TalerProtocolTimestamp;
  /**
   * Delivery location for (all!) products.
   */
  delivery_location?: Location;
  /**
   * List of accepted exchanges.
   */
  exchanges: ExchangeHandle[];
  /**
   * Products that are sold in this contract.
   */
  products?: Product[];
  /**
   * Deadline for refunds.
   */
  refund_deadline: TalerProtocolTimestamp;
  /**
   * Deadline for the wire transfer.
   */
  wire_transfer_deadline: TalerProtocolTimestamp;
  /**
   * Time when the contract was generated by the merchant.
   */
  timestamp: TalerProtocolTimestamp;
  /**
   * Order id to uniquely identify the purchase within
   * one merchant instance.
   */
  order_id: string;
  /**
   * Base URL of the merchant's backend.
   */
  merchant_base_url: string;
  /**
   * Fulfillment URL to view the product or
   * delivery status.
   */
  fulfillment_url?: string;
  /**
   * URL meant to share the shopping cart.
   */
  public_reorder_url?: string;
  /**
   * Plain text fulfillment message in the merchant's default language.
   */
  fulfillment_message?: string;
  /**
   * Internationalized fulfillment messages.
   */
  fulfillment_message_i18n?: InternationalizedString;
  /**
   * Share of the wire fee that must be settled with one payment.
   */
  wire_fee_amortization?: number;
  /**
   * Maximum wire fee that the merchant agrees to pay for.
   */
  max_wire_fee?: string;
  minimum_age?: number;
  /**
   * Extra data, interpreted by the mechant only.
   */
  extra?: any;
}
```
```typescript
export interface TalerProtocolDuration {
  readonly d_us: number | "forever";
}
```
```typescript
export interface AuditorHandle {
  /**
   * Official name of the auditor.
   */
  name: string;
  /**
   * Master public signing key of the auditor.
   */
  auditor_pub: string;
  /**
   * Base URL of the auditor.
   */
  url: string;
}
```
```typescript
/**
 * Information about an exchange as stored inside a
 * merchant's contract terms.
 */
export interface ExchangeHandle {
  /**
   * Master public signing key of the exchange.
   */
  master_pub: string;
  /**
   * Base URL of the exchange.
   */
  url: string;
}
```
```typescript
/**
 * Forced coin selection for deposits/payments.
 */
export interface ForcedCoinSel {
  coins: {
    value: AmountString;
    contribution: AmountString;
  }[];
}
```
```typescript
// Enum value:
// ProviderPaymentType.Pending = "pending"
```
```typescript
export interface TalerErrorDetail {
  code: TalerErrorCode;
  hint?: string;
  [x: string]: unknown;
}
```
```typescript
export interface BackupRecovery {
  walletRootPriv: string;
  providers: {
    url: string;
  }[];
}
```
```typescript
export interface AbsoluteTime {
  /**
   * Timestamp in milliseconds.
   */
  readonly t_ms: number | "never";
}
```
```typescript
export interface Duration {
  /**
   * Duration in milliseconds.
   */
  readonly d_ms: number | "forever";
}
```
