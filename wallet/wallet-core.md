# Wallet-Core API Documentation
This file is auto-generated from [wallet-core](https://git.taler.net/wallet-core.git/tree/packages/taler-wallet-core/src/wallet-api-types.ts).
## Overview
### Unknown Group
* [InitWalletOp](#initwalletop)
* [GetVersionOp](#getversionop)
###  Basic Wallet Information
* [GetBalancesOp](#getbalancesop)
* [GetBalancesDetailOp](#getbalancesdetailop)
* [GetPlanForOperationOp](#getplanforoperationop)
* [ConvertDepositAmountOp](#convertdepositamountop)
* [GetMaxDepositAmountOp](#getmaxdepositamountop)
* [ConvertPeerPushAmountOp](#convertpeerpushamountop)
* [GetMaxPeerPushAmountOp](#getmaxpeerpushamountop)
* [ConvertWithdrawalAmountOp](#convertwithdrawalamountop)
###  Managing Transactions
* [GetTransactionsOp](#gettransactionsop)
* [TestingGetSampleTransactionsOp](#testinggetsampletransactionsop)
* [GetTransactionByIdOp](#gettransactionbyidop)
* [RetryPendingNowOp](#retrypendingnowop)
* [DeleteTransactionOp](#deletetransactionop)
* [RetryTransactionOp](#retrytransactionop)
* [AbortTransactionOp](#aborttransactionop)
* [FailTransactionOp](#failtransactionop)
* [SuspendTransactionOp](#suspendtransactionop)
* [ResumeTransactionOp](#resumetransactionop)
###  Withdrawals
* [GetWithdrawalDetailsForAmountOp](#getwithdrawaldetailsforamountop)
* [GetWithdrawalDetailsForUriOp](#getwithdrawaldetailsforuriop)
* [AcceptBankIntegratedWithdrawalOp](#acceptbankintegratedwithdrawalop)
* [AcceptManualWithdrawalOp](#acceptmanualwithdrawalop)
###  Merchant Payments
* [PreparePayForUriOp](#preparepayforuriop)
* [SharePaymentOp](#sharepaymentop)
* [PreparePayForTemplateOp](#preparepayfortemplateop)
* [GetContractTermsDetailsOp](#getcontracttermsdetailsop)
* [ConfirmPayOp](#confirmpayop)
* [StartRefundQueryForUriOp](#startrefundqueryforuriop)
* [StartRefundQueryOp](#startrefundqueryop)
###  Rewards
* [PrepareTipOp](#preparetipop)
* [AcceptTipOp](#accepttipop)
###  Exchange Management
* [ListExchangesOp](#listexchangesop)
* [ListExchangesForScopedCurrencyOp](#listexchangesforscopedcurrencyop)
* [AddExchangeOp](#addexchangeop)
* [UpdateExchangeEntryOp](#updateexchangeentryop)
* [ListKnownBankAccountsOp](#listknownbankaccountsop)
* [AddKnownBankAccountsOp](#addknownbankaccountsop)
* [ForgetKnownBankAccountsOp](#forgetknownbankaccountsop)
* [SetExchangeTosAcceptedOp](#setexchangetosacceptedop)
* [GetExchangeTosOp](#getexchangetosop)
* [GetExchangeDetailedInfoOp](#getexchangedetailedinfoop)
* [ListCurrenciesOp](#listcurrenciesop)
* [GetCurrencySpecificationOp](#getcurrencyspecificationop)
###  Deposits
* [GenerateDepositGroupTxIdOp](#generatedepositgrouptxidop)
* [CreateDepositGroupOp](#createdepositgroupop)
* [PrepareDepositOp](#preparedepositop)
###  Backups
* [ExportBackupRecoveryOp](#exportbackuprecoveryop)
* [ImportBackupRecoveryOp](#importbackuprecoveryop)
* [RunBackupCycleOp](#runbackupcycleop)
* [ExportBackupOp](#exportbackupop)
* [AddBackupProviderOp](#addbackupproviderop)
* [RemoveBackupProviderOp](#removebackupproviderop)
* [GetBackupInfoOp](#getbackupinfoop)
* [SetWalletDeviceIdOp](#setwalletdeviceidop)
* [ListStoredBackupsOp](#liststoredbackupsop)
* [CreateStoredBackupsOp](#createstoredbackupsop)
* [RecoverStoredBackupsOp](#recoverstoredbackupsop)
* [DeleteStoredBackupOp](#deletestoredbackupop)
###  Peer Payments
* [CheckPeerPushDebitOp](#checkpeerpushdebitop)
* [InitiatePeerPushDebitOp](#initiatepeerpushdebitop)
* [PreparePeerPushCreditOp](#preparepeerpushcreditop)
* [ConfirmPeerPushCreditOp](#confirmpeerpushcreditop)
* [CheckPeerPullCreditOp](#checkpeerpullcreditop)
* [InitiatePeerPullCreditOp](#initiatepeerpullcreditop)
* [PreparePeerPullDebitOp](#preparepeerpulldebitop)
* [ConfirmPeerPullDebitOp](#confirmpeerpulldebitop)
###  Data Validation
* [ValidateIbanOp](#validateibanop)
###  Database Management
* [ExportDbOp](#exportdbop)
* [ImportDbOp](#importdbop)
* [ClearDbOp](#cleardbop)
* [RecycleOp](#recycleop)
###  Testing and Debugging
* [ApplyDevExperimentOp](#applydevexperimentop)
* [RunIntegrationTestOp](#runintegrationtestop)
* [RunIntegrationTestV2Op](#runintegrationtestv2op)
* [TestCryptoOp](#testcryptoop)
* [WithdrawTestBalanceOp](#withdrawtestbalanceop)
* [WithdrawTestkudosOp](#withdrawtestkudosop)
* [TestPayOp](#testpayop)
* [GetPendingTasksOp](#getpendingtasksop)
* [DumpCoinsOp](#dumpcoinsop)
* [TestingSetTimetravelOp](#testingsettimetravelop)
* [TestingWaitTransactionsFinalOp](#testingwaittransactionsfinalop)
* [TestingWaitRefreshesFinalOp](#testingwaitrefreshesfinalop)
* [TestingWaitTasksProcessedOp](#testingwaittasksprocessedop)
* [TestingWaitTransactionStateOp](#testingwaittransactionstateop)
* [SetCoinSuspendedOp](#setcoinsuspendedop)
* [ForceRefreshOp](#forcerefreshop)
## Operation Reference
### InitWalletOp
```typescript
/**
 * Initialize wallet-core.
 *
 * Must be the request before any other operations.
 */
export type InitWalletOp = {
  op: WalletApiOperation.InitWallet;
  request: InitRequest;
  response: InitResponse;
};
// InitWallet = "initWallet"

```
```typescript
export interface InitRequest {
  skipDefaults?: boolean;
}

```
```typescript
export interface InitResponse {
  versionInfo: WalletCoreVersion;
}

```

### GetVersionOp
```typescript
export type GetVersionOp = {
  op: WalletApiOperation.GetVersion;
  request: EmptyObject;
  response: WalletCoreVersion;
};
// GetVersion = "getVersion"

```

### GetBalancesOp
```typescript
/**
 * Get current wallet balance.
 */
export type GetBalancesOp = {
  op: WalletApiOperation.GetBalances;
  request: EmptyObject;
  response: BalancesResponse;
};
// GetBalances = "getBalances"

```
```typescript
export interface BalancesResponse {
  balances: WalletBalance[];
}

```
```typescript
export interface WalletBalance {
  scopeInfo: ScopeInfo;
  available: AmountString;
  pendingIncoming: AmountString;
  pendingOutgoing: AmountString;
  /**
   * Does the balance for this currency have a pending
   * transaction?
   *
   * @deprecated use flags and pendingIncoming/pendingOutgoing instead
   */
  hasPendingTransactions: boolean;
  /**
   * Is there a transaction that requires user input?
   *
   * @deprecated use flags instead
   */
  requiresUserInput: boolean;
  flags: BalanceFlag[];
}

```
```typescript
export declare enum BalanceFlag {
  IncomingKyc = "incoming-kyc",
  IncomingAml = "incoming-aml",
  IncomingConfirmation = "incoming-confirmation",
  OutgoingKyc = "outgoing-kyc",
}

```

### GetBalancesDetailOp
```typescript
export type GetBalancesDetailOp = {
  op: WalletApiOperation.GetBalanceDetail;
  request: GetBalanceDetailRequest;
  response: MerchantPaymentBalanceDetails;
};
// GetBalanceDetail = "getBalanceDetail"

```
```typescript
export interface GetBalanceDetailRequest {
  currency: string;
}

```
```typescript
export interface MerchantPaymentBalanceDetails {
  /**
   * Balance of type "available" (see balance.ts for definition).
   */
  balanceAvailable: AmountJson;
  /**
   * Balance of type "material" (see balance.ts for definition).
   */
  balanceMaterial: AmountJson;
  /**
   * Balance of type "age-acceptable" (see balance.ts for definition).
   */
  balanceAgeAcceptable: AmountJson;
  /**
   * Balance of type "merchant-acceptable" (see balance.ts for definition).
   */
  balanceMerchantAcceptable: AmountJson;
  /**
   * Balance of type "merchant-depositable" (see balance.ts for definition).
   */
  balanceMerchantDepositable: AmountJson;
}

```
```typescript
/**
 * Non-negative financial amount.  Fractional values are expressed as multiples
 * of 1e-8.
 */
export interface AmountJson {
  /**
   * Value, must be an integer.
   */
  readonly value: number;
  /**
   * Fraction, must be an integer.  Represent 1/1e8 of a unit.
   */
  readonly fraction: number;
  /**
   * Currency of the amount.
   */
  readonly currency: string;
}

```

### GetPlanForOperationOp
```typescript
export type GetPlanForOperationOp = {
  op: WalletApiOperation.GetPlanForOperation;
  request: GetPlanForOperationRequest;
  response: GetPlanForOperationResponse;
};
// GetPlanForOperation = "getPlanForOperation"

```
```typescript
export type GetPlanForOperationRequest =
  | GetPlanForWithdrawRequest
  | GetPlanForDepositRequest;

```
```typescript
interface GetPlanForWithdrawRequest extends GetPlanForWalletInitiatedOperation {
  type: TransactionType.Withdrawal;
  exchangeUrl?: string;
}

```
```typescript
interface GetPlanForWalletInitiatedOperation {
  instructedAmount: AmountString;
  mode: TransactionAmountMode;
}

```
```typescript
interface GetPlanForDepositRequest extends GetPlanForWalletInitiatedOperation {
  type: TransactionType.Deposit;
  account: string;
}

```
```typescript
export interface GetPlanForOperationResponse {
  effectiveAmount: AmountString;
  rawAmount: AmountString;
  counterPartyAmount?: AmountString;
  details: any;
}

```

### ConvertDepositAmountOp
```typescript
export type ConvertDepositAmountOp = {
  op: WalletApiOperation.ConvertDepositAmount;
  request: ConvertAmountRequest;
  response: AmountResponse;
};
// ConvertDepositAmount = "ConvertDepositAmount"

```

### GetMaxDepositAmountOp
```typescript
export type GetMaxDepositAmountOp = {
  op: WalletApiOperation.GetMaxDepositAmount;
  request: GetAmountRequest;
  response: AmountResponse;
};
// GetMaxDepositAmount = "GetMaxDepositAmount"

```

### ConvertPeerPushAmountOp
```typescript
export type ConvertPeerPushAmountOp = {
  op: WalletApiOperation.ConvertPeerPushAmount;
  request: ConvertAmountRequest;
  response: AmountResponse;
};
// ConvertPeerPushAmount = "ConvertPeerPushAmount"

```

### GetMaxPeerPushAmountOp
```typescript
export type GetMaxPeerPushAmountOp = {
  op: WalletApiOperation.GetMaxPeerPushAmount;
  request: GetAmountRequest;
  response: AmountResponse;
};
// GetMaxPeerPushAmount = "GetMaxPeerPushAmount"

```

### ConvertWithdrawalAmountOp
```typescript
export type ConvertWithdrawalAmountOp = {
  op: WalletApiOperation.ConvertWithdrawalAmount;
  request: ConvertAmountRequest;
  response: AmountResponse;
};
// ConvertWithdrawalAmount = "ConvertWithdrawalAmount"

```

### GetTransactionsOp
```typescript
/**
 * Get transactions.
 */
export type GetTransactionsOp = {
  op: WalletApiOperation.GetTransactions;
  request: TransactionsRequest;
  response: TransactionsResponse;
};
// GetTransactions = "getTransactions"

```
```typescript
export interface TransactionsRequest {
  /**
   * return only transactions in the given currency
   */
  currency?: string;
  /**
   * if present, results will be limited to transactions related to the given search string
   */
  search?: string;
  /**
   * Sort order of the transaction items.
   * By default, items are sorted ascending by their
   * main timestamp.
   */
  sort?: "ascending" | "descending";
  /**
   * If true, include all refreshes in the transactions list.
   */
  includeRefreshes?: boolean;
  filterByState?: TransactionStateFilter;
}

```

### TestingGetSampleTransactionsOp
```typescript
/**
 * Get sample transactions.
 */
export type TestingGetSampleTransactionsOp = {
  op: WalletApiOperation.TestingGetSampleTransactions;
  request: EmptyObject;
  response: TransactionsResponse;
};
// TestingGetSampleTransactions = "testingGetSampleTransactions"

```

### GetTransactionByIdOp
```typescript
export type GetTransactionByIdOp = {
  op: WalletApiOperation.GetTransactionById;
  request: TransactionByIdRequest;
  response: Transaction;
};
// GetTransactionById = "getTransactionById"

```
```typescript
export interface TransactionByIdRequest {
  transactionId: string;
}

```

### RetryPendingNowOp
```typescript
export type RetryPendingNowOp = {
  op: WalletApiOperation.RetryPendingNow;
  request: EmptyObject;
  response: EmptyObject;
};
// RetryPendingNow = "retryPendingNow"

```

### DeleteTransactionOp
```typescript
/**
 * Delete a transaction locally in the wallet.
 */
export type DeleteTransactionOp = {
  op: WalletApiOperation.DeleteTransaction;
  request: DeleteTransactionRequest;
  response: EmptyObject;
};
// DeleteTransaction = "deleteTransaction"

```
```typescript
export interface DeleteTransactionRequest {
  transactionId: TransactionIdStr;
}

```

### RetryTransactionOp
```typescript
/**
 * Immediately retry a transaction.
 */
export type RetryTransactionOp = {
  op: WalletApiOperation.RetryTransaction;
  request: RetryTransactionRequest;
  response: EmptyObject;
};
// RetryTransaction = "retryTransaction"

```
```typescript
export interface RetryTransactionRequest {
  transactionId: TransactionIdStr;
}

```

### AbortTransactionOp
```typescript
/**
 * Abort a transaction
 *
 * For payment transactions, it puts the payment into an "aborting" state.
 */
export type AbortTransactionOp = {
  op: WalletApiOperation.AbortTransaction;
  request: AbortTransactionRequest;
  response: EmptyObject;
};
// AbortTransaction = "abortTransaction"

```

### FailTransactionOp
```typescript
/**
 * Cancel aborting a transaction
 *
 * For payment transactions, it puts the payment into an "aborting" state.
 */
export type FailTransactionOp = {
  op: WalletApiOperation.FailTransaction;
  request: FailTransactionRequest;
  response: EmptyObject;
};
// FailTransaction = "failTransaction"

```
```typescript
export interface FailTransactionRequest {
  transactionId: TransactionIdStr;
}

```

### SuspendTransactionOp
```typescript
/**
 * Suspend a transaction
 */
export type SuspendTransactionOp = {
  op: WalletApiOperation.SuspendTransaction;
  request: AbortTransactionRequest;
  response: EmptyObject;
};
// SuspendTransaction = "suspendTransaction"

```

### ResumeTransactionOp
```typescript
/**
 * Resume a transaction
 */
export type ResumeTransactionOp = {
  op: WalletApiOperation.ResumeTransaction;
  request: AbortTransactionRequest;
  response: EmptyObject;
};
// ResumeTransaction = "resumeTransaction"

```

### GetWithdrawalDetailsForAmountOp
```typescript
/**
 * Get details for withdrawing a particular amount (manual withdrawal).
 */
export type GetWithdrawalDetailsForAmountOp = {
  op: WalletApiOperation.GetWithdrawalDetailsForAmount;
  request: GetWithdrawalDetailsForAmountRequest;
  response: ManualWithdrawalDetails;
};
// GetWithdrawalDetailsForAmount = "getWithdrawalDetailsForAmount"

```
```typescript
export interface GetWithdrawalDetailsForAmountRequest {
  exchangeBaseUrl: string;
  amount: AmountString;
  restrictAge?: number;
}

```
```typescript
export interface ManualWithdrawalDetails {
  /**
   * Did the user accept the current version of the exchange's
   * terms of service?
   */
  tosAccepted: boolean;
  /**
   * Amount that the user will transfer to the exchange.
   */
  amountRaw: AmountString;
  /**
   * Amount that will be added to the user's wallet balance.
   */
  amountEffective: AmountString;
  /**
   * Number of coins that would be used for withdrawal.
   *
   * The UIs should warn if this number is too high (roughly at >100).
   */
  numCoins: number;
  /**
   * Ways to pay the exchange.
   *
   * @deprecated in favor of withdrawalAccountList
   */
  paytoUris: string[];
  /**
   * Ways to pay the exchange, including accounts that require currency conversion.
   */
  withdrawalAccountList: WithdrawalExchangeAccountDetails[];
  /**
   * If the exchange supports age-restricted coins it will return
   * the array of ages.
   */
  ageRestrictionOptions?: number[];
}

```

### GetWithdrawalDetailsForUriOp
```typescript
/**
 * Get details for withdrawing via a particular taler:// URI.
 */
export type GetWithdrawalDetailsForUriOp = {
  op: WalletApiOperation.GetWithdrawalDetailsForUri;
  request: GetWithdrawalDetailsForUriRequest;
  response: WithdrawUriInfoResponse;
};
// GetWithdrawalDetailsForUri = "getWithdrawalDetailsForUri"

```
```typescript
export interface GetWithdrawalDetailsForUriRequest {
  talerWithdrawUri: string;
  restrictAge?: number;
}

```
```typescript
export interface WithdrawUriInfoResponse {
  amount: AmountString;
  defaultExchangeBaseUrl?: string;
  possibleExchanges: ExchangeListItem[];
}

```

### AcceptBankIntegratedWithdrawalOp
```typescript
/**
 * Accept a bank-integrated withdrawal.
 */
export type AcceptBankIntegratedWithdrawalOp = {
  op: WalletApiOperation.AcceptBankIntegratedWithdrawal;
  request: AcceptBankIntegratedWithdrawalRequest;
  response: AcceptWithdrawalResponse;
};
// AcceptBankIntegratedWithdrawal = "acceptBankIntegratedWithdrawal"

```
```typescript
export interface AcceptBankIntegratedWithdrawalRequest {
  talerWithdrawUri: string;
  exchangeBaseUrl: string;
  forcedDenomSel?: ForcedDenomSel;
  restrictAge?: number;
}

```
```typescript
export interface AcceptWithdrawalResponse {
  reservePub: string;
  confirmTransferUrl?: string;
  transactionId: TransactionIdStr;
}

```

### AcceptManualWithdrawalOp
```typescript
/**
 * Create a manual withdrawal.
 */
export type AcceptManualWithdrawalOp = {
  op: WalletApiOperation.AcceptManualWithdrawal;
  request: AcceptManualWithdrawalRequest;
  response: AcceptManualWithdrawalResult;
};
// AcceptManualWithdrawal = "acceptManualWithdrawal"

```
```typescript
export interface AcceptManualWithdrawalRequest {
  exchangeBaseUrl: string;
  amount: AmountString;
  restrictAge?: number;
}

```
```typescript
export interface AcceptManualWithdrawalResult {
  /**
   * Payto URIs that can be used to fund the withdrawal.
   */
  exchangePaytoUris: string[];
  /**
   * Public key of the newly created reserve.
   */
  reservePub: string;
  withdrawalAccountsList: WithdrawalExchangeAccountDetails[];
  transactionId: TransactionIdStr;
}

```

### PreparePayForUriOp
```typescript
/**
 * Prepare to make a payment based on a taler://pay/ URI.
 */
export type PreparePayForUriOp = {
  op: WalletApiOperation.PreparePayForUri;
  request: PreparePayRequest;
  response: PreparePayResult;
};
// PreparePayForUri = "preparePayForUri"

```
```typescript
export interface PreparePayRequest {
  talerPayUri: string;
}

```

### SharePaymentOp
```typescript
export type SharePaymentOp = {
  op: WalletApiOperation.SharePayment;
  request: SharePaymentRequest;
  response: SharePaymentResult;
};
// SharePayment = "sharePayment"

```
```typescript
export interface SharePaymentRequest {
  merchantBaseUrl: string;
  orderId: string;
}

```
```typescript
export interface SharePaymentResult {
  privatePayUri: string;
}

```

### PreparePayForTemplateOp
```typescript
/**
 * Prepare to make a payment based on a taler://pay-template/ URI.
 */
export type PreparePayForTemplateOp = {
  op: WalletApiOperation.PreparePayForTemplate;
  request: PreparePayTemplateRequest;
  response: PreparePayResult;
};
// PreparePayForTemplate = "preparePayForTemplate"

```
```typescript
export interface PreparePayTemplateRequest {
  talerPayTemplateUri: string;
  templateParams: Record<string, string>;
}

```

### GetContractTermsDetailsOp
```typescript
export type GetContractTermsDetailsOp = {
  op: WalletApiOperation.GetContractTermsDetails;
  request: GetContractTermsDetailsRequest;
  response: WalletContractData;
};
// GetContractTermsDetails = "getContractTermsDetails"

```
```typescript
export interface GetContractTermsDetailsRequest {
  proposalId: string;
}

```
```typescript
/**
 * Data extracted from the contract terms that is relevant for payment
 * processing in the wallet.
 */
export interface WalletContractData {
  /**
   * Fulfillment URL, or the empty string if the order has no fulfillment URL.
   *
   * Stored as a non-nullable string as we use this field for IndexedDB indexing.
   */
  fulfillmentUrl: string;
  contractTermsHash: string;
  fulfillmentMessage?: string;
  fulfillmentMessageI18n?: InternationalizedString;
  merchantSig: string;
  merchantPub: string;
  merchant: MerchantInfo;
  amount: AmountString;
  orderId: string;
  merchantBaseUrl: string;
  summary: string;
  summaryI18n:
    | {
        [lang_tag: string]: string;
      }
    | undefined;
  autoRefund: TalerProtocolDuration | undefined;
  maxWireFee: AmountString;
  wireFeeAmortization: number;
  payDeadline: TalerProtocolTimestamp;
  refundDeadline: TalerProtocolTimestamp;
  allowedExchanges: AllowedExchangeInfo[];
  timestamp: TalerProtocolTimestamp;
  wireMethod: string;
  wireInfoHash: string;
  maxDepositFee: AmountString;
  minimumAge?: number;
}

```
```typescript
export interface AllowedExchangeInfo {
  exchangeBaseUrl: string;
  exchangePub: string;
}

```

### ConfirmPayOp
```typescript
/**
 * Confirm a payment that was previously prepared with
 * {@link PreparePayForUriOp}
 */
export type ConfirmPayOp = {
  op: WalletApiOperation.ConfirmPay;
  request: ConfirmPayRequest;
  response: ConfirmPayResult;
};
// ConfirmPay = "confirmPay"

```
```typescript
export interface ConfirmPayRequest {
  /**
   * @deprecated use transactionId instead
   */
  proposalId?: string;
  transactionId?: string;
  sessionId?: string;
  forcedCoinSel?: ForcedCoinSel;
}

```
```typescript
export type ConfirmPayResult = ConfirmPayResultDone | ConfirmPayResultPending;

```
```typescript
/**
 * Result for confirmPay
 */
export interface ConfirmPayResultDone {
  type: ConfirmPayResultType.Done;
  contractTerms: MerchantContractTerms;
  transactionId: TransactionIdStr;
}

```
```typescript
export interface ConfirmPayResultPending {
  type: ConfirmPayResultType.Pending;
  transactionId: TransactionIdStr;
  lastError: TalerErrorDetail | undefined;
}

```

### StartRefundQueryForUriOp
```typescript
/**
 * Check for a refund based on a taler://refund URI.
 */
export type StartRefundQueryForUriOp = {
  op: WalletApiOperation.StartRefundQueryForUri;
  request: PrepareRefundRequest;
  response: StartRefundQueryForUriResponse;
};
// StartRefundQueryForUri = "startRefundQueryForUri"

```
```typescript
export interface PrepareRefundRequest {
  talerRefundUri: string;
}

```
```typescript
export interface StartRefundQueryForUriResponse {
  transactionId: TransactionIdStr;
}

```

### StartRefundQueryOp
```typescript
export type StartRefundQueryOp = {
  op: WalletApiOperation.StartRefundQuery;
  request: StartRefundQueryRequest;
  response: EmptyObject;
};
// StartRefundQuery = "startRefundQuery"

```
```typescript
export interface StartRefundQueryRequest {
  transactionId: TransactionIdStr;
}

```

### PrepareTipOp
```typescript
/**
 * Query and store information about a reward.
 */
export type PrepareTipOp = {
  op: WalletApiOperation.PrepareReward;
  request: PrepareRewardRequest;
  response: PrepareRewardResult;
};
// PrepareReward = "prepareReward"

```
```typescript
export interface PrepareRewardRequest {
  talerRewardUri: string;
}

```
```typescript
export interface PrepareTipResult {
  /**
   * Unique ID for the tip assigned by the wallet.
   * Typically different from the merchant-generated tip ID.
   *
   * @deprecated use transactionId instead
   */
  walletRewardId: string;
  /**
   * Tip transaction ID.
   */
  transactionId: string;
  /**
   * Has the tip already been accepted?
   */
  accepted: boolean;
  /**
   * Amount that the merchant gave.
   */
  rewardAmountRaw: AmountString;
  /**
   * Amount that arrived at the wallet.
   * Might be lower than the raw amount due to fees.
   */
  rewardAmountEffective: AmountString;
  /**
   * Base URL of the merchant backend giving then tip.
   */
  merchantBaseUrl: string;
  /**
   * Base URL of the exchange that is used to withdraw the tip.
   * Determined by the merchant, the wallet/user has no choice here.
   */
  exchangeBaseUrl: string;
  /**
   * Time when the tip will expire.  After it expired, it can't be picked
   * up anymore.
   */
  expirationTimestamp: TalerProtocolTimestamp;
}

```

### AcceptTipOp
```typescript
/**
 * Accept a reward.
 */
export type AcceptTipOp = {
  op: WalletApiOperation.AcceptReward;
  request: AcceptRewardRequest;
  response: AcceptTipResponse;
};
// AcceptReward = "acceptReward"

```
```typescript
export interface AcceptRewardRequest {
  walletRewardId: string;
}

```
```typescript
export interface AcceptTipResponse {
  transactionId: TransactionIdStr;
  next_url?: string;
}

```

### ListExchangesOp
```typescript
/**
 * List exchanges known to the wallet.
 */
export type ListExchangesOp = {
  op: WalletApiOperation.ListExchanges;
  request: EmptyObject;
  response: ExchangesListResponse;
};
// ListExchanges = "listExchanges"

```
```typescript
export interface ExchangesListResponse {
  exchanges: ExchangeListItem[];
}

```

### ListExchangesForScopedCurrencyOp
```typescript
/**
 * List exchanges that are available for withdrawing a particular
 * scoped currency.
 */
export type ListExchangesForScopedCurrencyOp = {
  op: WalletApiOperation.ListExchangesForScopedCurrency;
  request: ListExchangesForScopedCurrencyRequest;
  response: ExchangesShortListResponse;
};
// ListExchangesForScopedCurrency = "listExchangesForScopedCurrency"

```
```typescript
export interface ListExchangesForScopedCurrencyRequest {
  scope: ScopeInfo;
}

```
```typescript
export interface ExchangesShortListResponse {
  exchanges: ShortExchangeListItem[];
}

```
```typescript
export interface ShortExchangeListItem {
  exchangeBaseUrl: string;
}

```

### AddExchangeOp
```typescript
/**
 * Add / force-update an exchange.
 */
export type AddExchangeOp = {
  op: WalletApiOperation.AddExchange;
  request: AddExchangeRequest;
  response: EmptyObject;
};
// AddExchange = "addExchange"

```

### UpdateExchangeEntryOp
```typescript
/**
 * Update an exchange entry.
 */
export type UpdateExchangeEntryOp = {
  op: WalletApiOperation.UpdateExchangeEntry;
  request: UpdateExchangeEntryRequest;
  response: EmptyObject;
};
// UpdateExchangeEntry = "updateExchangeEntry"

```
```typescript
export interface UpdateExchangeEntryRequest {
  exchangeBaseUrl: string;
}

```

### ListKnownBankAccountsOp
```typescript
export type ListKnownBankAccountsOp = {
  op: WalletApiOperation.ListKnownBankAccounts;
  request: ListKnownBankAccountsRequest;
  response: KnownBankAccounts;
};
// ListKnownBankAccounts = "listKnownBankAccounts"

```
```typescript
export interface ListKnownBankAccountsRequest {
  currency?: string;
}

```
```typescript
export interface KnownBankAccounts {
  accounts: KnownBankAccountsInfo[];
}

```
```typescript
export interface KnownBankAccountsInfo {
  uri: PaytoUri;
  kyc_completed: boolean;
  currency: string;
  alias: string;
}

```
```typescript
export type PaytoUri =
  | PaytoUriUnknown
  | PaytoUriIBAN
  | PaytoUriTalerBank
  | PaytoUriBitcoin;

```
```typescript
export interface PaytoUriUnknown extends PaytoUriGeneric {
  isKnown: false;
}

```
```typescript
export interface PaytoUriGeneric {
  targetType: PaytoType | string;
  targetPath: string;
  params: {
    [name: string]: string;
  };
}

```
```typescript
export type PaytoType = "iban" | "bitcoin" | "x-taler-bank";

```
```typescript
export interface PaytoUriIBAN extends PaytoUriGeneric {
  isKnown: true;
  targetType: "iban";
  iban: string;
  bic?: string;
}

```
```typescript
export interface PaytoUriTalerBank extends PaytoUriGeneric {
  isKnown: true;
  targetType: "x-taler-bank";
  host: string;
  account: string;
}

```
```typescript
export interface PaytoUriBitcoin extends PaytoUriGeneric {
  isKnown: true;
  targetType: "bitcoin";
  segwitAddrs: Array<string>;
}

```

### AddKnownBankAccountsOp
```typescript
export type AddKnownBankAccountsOp = {
  op: WalletApiOperation.AddKnownBankAccounts;
  request: AddKnownBankAccountsRequest;
  response: EmptyObject;
};
// AddKnownBankAccounts = "addKnownBankAccounts"

```
```typescript
export interface AddKnownBankAccountsRequest {
  payto: string;
  alias: string;
  currency: string;
}

```

### ForgetKnownBankAccountsOp
```typescript
export type ForgetKnownBankAccountsOp = {
  op: WalletApiOperation.ForgetKnownBankAccounts;
  request: ForgetKnownBankAccountsRequest;
  response: EmptyObject;
};
// ForgetKnownBankAccounts = "forgetKnownBankAccounts"

```
```typescript
export interface ForgetKnownBankAccountsRequest {
  payto: string;
}

```

### SetExchangeTosAcceptedOp
```typescript
/**
 * Accept a particular version of the exchange terms of service.
 */
export type SetExchangeTosAcceptedOp = {
  op: WalletApiOperation.SetExchangeTosAccepted;
  request: AcceptExchangeTosRequest;
  response: EmptyObject;
};
// SetExchangeTosAccepted = "setExchangeTosAccepted"

```
```typescript
export interface AcceptExchangeTosRequest {
  exchangeBaseUrl: string;
  etag: string | undefined;
}

```

### GetExchangeTosOp
```typescript
/**
 * Get the current terms of a service of an exchange.
 */
export type GetExchangeTosOp = {
  op: WalletApiOperation.GetExchangeTos;
  request: GetExchangeTosRequest;
  response: GetExchangeTosResult;
};
// GetExchangeTos = "getExchangeTos"

```
```typescript
export interface GetExchangeTosRequest {
  exchangeBaseUrl: string;
  acceptedFormat?: string[];
}

```
```typescript
export interface GetExchangeTosResult {
  /**
   * Markdown version of the current ToS.
   */
  content: string;
  /**
   * Version tag of the current ToS.
   */
  currentEtag: string;
  /**
   * Version tag of the last ToS that the user has accepted,
   * if any.
   */
  acceptedEtag: string | undefined;
  /**
   * Accepted content type
   */
  contentType: string;
  tosStatus: ExchangeTosStatus;
}

```

### GetExchangeDetailedInfoOp
```typescript
/**
 * Get the current terms of a service of an exchange.
 */
export type GetExchangeDetailedInfoOp = {
  op: WalletApiOperation.GetExchangeDetailedInfo;
  request: AddExchangeRequest;
  response: ExchangeDetailedResponse;
};
// GetExchangeDetailedInfo = "getExchangeDetailedInfo"

```
```typescript
export interface ExchangeDetailedResponse {
  exchange: ExchangeFullDetails;
}

```
```typescript
export interface ExchangeFullDetails {
  exchangeBaseUrl: string;
  currency: string;
  paytoUris: string[];
  auditors: ExchangeAuditor[];
  wireInfo: WireInfo;
  denomFees: DenomOperationMap<FeeDescription[]>;
  transferFees: Record<string, FeeDescription[]>;
  globalFees: FeeDescription[];
}

```
```typescript
export interface WireInfo {
  feesForType: WireFeeMap;
  accounts: ExchangeWireAccount[];
}

```
```typescript
export interface ExchangeWireAccount {
  payto_uri: string;
  conversion_url?: string;
  credit_restrictions: AccountRestriction[];
  debit_restrictions: AccountRestriction[];
  master_sig: EddsaSignatureString;
}

```
```typescript
export interface FeeDescription {
  group: string;
  from: AbsoluteTime;
  until: AbsoluteTime;
  fee?: AmountString;
}

```

### ListCurrenciesOp
```typescript
/**
 * List currencies known to the wallet.
 */
export type ListCurrenciesOp = {
  op: WalletApiOperation.ListCurrencies;
  request: EmptyObject;
  response: WalletCurrencyInfo;
};
// ListCurrencies = "listCurrencies"

```
```typescript
export interface WalletCurrencyInfo {
  trustedAuditors: {
    currency: string;
    auditorPub: string;
    auditorBaseUrl: string;
  }[];
  trustedExchanges: {
    currency: string;
    exchangeMasterPub: string;
    exchangeBaseUrl: string;
  }[];
}

```

### GetCurrencySpecificationOp
```typescript
export type GetCurrencySpecificationOp = {
  op: WalletApiOperation.GetCurrencySpecification;
  request: GetCurrencySpecificationRequest;
  response: GetCurrencySpecificationResponse;
};
// GetCurrencySpecification = "getCurrencySpecification"

```
```typescript
export interface GetCurrencySpecificationRequest {
  scope: ScopeInfo;
}

```
```typescript
export interface GetCurrencySpecificationResponse {
  currencySpecification: CurrencySpecification;
}

```
```typescript
export interface CurrencySpecification {
  name: string;
  num_fractional_input_digits: Integer;
  num_fractional_normal_digits: Integer;
  num_fractional_trailing_zero_digits: Integer;
  alt_unit_names: {
    [log10: string]: string;
  };
}

```

### GenerateDepositGroupTxIdOp
```typescript
/**
 * Generate a fresh transaction ID for a deposit group.
 *
 * The resulting transaction ID can be specified when creating
 * a deposit group, so that the client can already start waiting for notifications
 * on that specific deposit group before the GreateDepositGroup request returns.
 */
export type GenerateDepositGroupTxIdOp = {
  op: WalletApiOperation.GenerateDepositGroupTxId;
  request: EmptyObject;
  response: TxIdResponse;
};
// GenerateDepositGroupTxId = "generateDepositGroupTxId"

```
```typescript
export interface TxIdResponse {
  transactionId: TransactionIdStr;
}

```

### CreateDepositGroupOp
```typescript
/**
 * Create a new deposit group.
 *
 * Deposit groups are used to deposit multiple coins to a bank
 * account, usually the wallet user's own bank account.
 */
export type CreateDepositGroupOp = {
  op: WalletApiOperation.CreateDepositGroup;
  request: CreateDepositGroupRequest;
  response: CreateDepositGroupResponse;
};
// CreateDepositGroup = "createDepositGroup"

```
```typescript
export interface CreateDepositGroupRequest {
  /**
   * Pre-allocated transaction ID.
   * Allows clients to easily handle notifications
   * that occur while the operation has been created but
   * before the creation request has returned.
   */
  transactionId?: string;
  depositPaytoUri: string;
  amount: AmountString;
}

```
```typescript
export interface CreateDepositGroupResponse {
  depositGroupId: string;
  transactionId: TransactionIdStr;
}

```

### PrepareDepositOp
```typescript
export type PrepareDepositOp = {
  op: WalletApiOperation.PrepareDeposit;
  request: PrepareDepositRequest;
  response: PrepareDepositResponse;
};
// PrepareDeposit = "prepareDeposit"

```
```typescript
export interface PrepareDepositRequest {
  depositPaytoUri: string;
  amount: AmountString;
}

```
```typescript
export interface PrepareDepositResponse {
  totalDepositCost: AmountString;
  effectiveDepositAmount: AmountString;
  fees: DepositGroupFees;
}

```
```typescript
export interface DepositGroupFees {
  coin: AmountString;
  wire: AmountString;
  refresh: AmountString;
}

```

### ExportBackupRecoveryOp
```typescript
/**
 * Export the recovery information for the wallet.
 */
export type ExportBackupRecoveryOp = {
  op: WalletApiOperation.ExportBackupRecovery;
  request: EmptyObject;
  response: BackupRecovery;
};
// ExportBackupRecovery = "exportBackupRecovery"

```

### ImportBackupRecoveryOp
```typescript
/**
 * Import recovery information into the wallet.
 */
export type ImportBackupRecoveryOp = {
  op: WalletApiOperation.ImportBackupRecovery;
  request: RecoveryLoadRequest;
  response: EmptyObject;
};
// ImportBackupRecovery = "importBackupRecovery"

```
```typescript
/**
 * Load recovery information into the wallet.
 */
export interface RecoveryLoadRequest {
  recovery: BackupRecovery;
  strategy?: RecoveryMergeStrategy;
}

```
```typescript
/**
 * Strategy for loading recovery information.
 */
export declare enum RecoveryMergeStrategy {
  /**
   * Keep the local wallet root key, import and take over providers.
   */
  Ours = "ours",
  /**
   * Migrate to the wallet root key from the recovery information.
   */
  Theirs = "theirs",
}

```

### RunBackupCycleOp
```typescript
/**
 * Manually make and upload a backup.
 */
export type RunBackupCycleOp = {
  op: WalletApiOperation.RunBackupCycle;
  request: RunBackupCycleRequest;
  response: EmptyObject;
};
// RunBackupCycle = "runBackupCycle"

```
```typescript
export interface RunBackupCycleRequest {
  /**
   * List of providers to backup or empty for all known providers.
   */
  providers?: Array<string>;
}

```

### ExportBackupOp
```typescript
export type ExportBackupOp = {
  op: WalletApiOperation.ExportBackup;
  request: EmptyObject;
  response: EmptyObject;
};
// ExportBackup = "exportBackup"

```

### AddBackupProviderOp
```typescript
/**
 * Add a new backup provider.
 */
export type AddBackupProviderOp = {
  op: WalletApiOperation.AddBackupProvider;
  request: AddBackupProviderRequest;
  response: AddBackupProviderResponse;
};
// AddBackupProvider = "addBackupProvider"

```
```typescript
export interface AddBackupProviderRequest {
  backupProviderBaseUrl: string;
  name: string;
  /**
   * Activate the provider.  Should only be done after
   * the user has reviewed the provider.
   */
  activate?: boolean;
}

```
```typescript
export type AddBackupProviderResponse =
  | AddBackupProviderOk
  | AddBackupProviderPaymentRequired;

```
```typescript
interface AddBackupProviderOk {
  status: "ok";
}

```
```typescript
interface AddBackupProviderPaymentRequired {
  status: "payment-required";
  talerUri?: string;
}

```

### RemoveBackupProviderOp
```typescript
export type RemoveBackupProviderOp = {
  op: WalletApiOperation.RemoveBackupProvider;
  request: RemoveBackupProviderRequest;
  response: EmptyObject;
};
// RemoveBackupProvider = "removeBackupProvider"

```
```typescript
export interface RemoveBackupProviderRequest {
  provider: string;
}

```

### GetBackupInfoOp
```typescript
/**
 * Get some useful stats about the backup state.
 */
export type GetBackupInfoOp = {
  op: WalletApiOperation.GetBackupInfo;
  request: EmptyObject;
  response: BackupInfo;
};
// GetBackupInfo = "getBackupInfo"

```
```typescript
export interface BackupInfo {
  walletRootPub: string;
  deviceId: string;
  providers: ProviderInfo[];
}

```
```typescript
/**
 * Information about one provider.
 *
 * We don't store the account key here,
 * as that's derived from the wallet root key.
 */
export interface ProviderInfo {
  active: boolean;
  syncProviderBaseUrl: string;
  name: string;
  terms?: BackupProviderTerms;
  /**
   * Last communication issue with the provider.
   */
  lastError?: TalerErrorDetail;
  lastSuccessfulBackupTimestamp?: TalerPreciseTimestamp;
  lastAttemptedBackupTimestamp?: TalerPreciseTimestamp;
  paymentProposalIds: string[];
  backupProblem?: BackupProblem;
  paymentStatus: ProviderPaymentStatus;
}

```
```typescript
export interface BackupProviderTerms {
  supportedProtocolVersion: string;
  annualFee: AmountString;
  storageLimitInMegabytes: number;
}

```
```typescript
export type BackupProblem =
  | BackupUnreadableProblem
  | BackupConflictingDeviceProblem;

```
```typescript
export interface BackupUnreadableProblem {
  type: "backup-unreadable";
}

```
```typescript
export interface BackupConflictingDeviceProblem {
  type: "backup-conflicting-device";
  otherDeviceId: string;
  myDeviceId: string;
  backupTimestamp: AbsoluteTime;
}

```
```typescript
export type ProviderPaymentStatus =
  | ProviderPaymentTermsChanged
  | ProviderPaymentPaid
  | ProviderPaymentInsufficientBalance
  | ProviderPaymentUnpaid
  | ProviderPaymentPending;

```
```typescript
export interface ProviderPaymentTermsChanged {
  type: ProviderPaymentType.TermsChanged;
  paidUntil: AbsoluteTime;
  oldTerms: BackupProviderTerms;
  newTerms: BackupProviderTerms;
}

```
```typescript
export interface ProviderPaymentPaid {
  type: ProviderPaymentType.Paid;
  paidUntil: AbsoluteTime;
}

```
```typescript
export interface ProviderPaymentInsufficientBalance {
  type: ProviderPaymentType.InsufficientBalance;
  amount: AmountString;
}

```
```typescript
export interface ProviderPaymentUnpaid {
  type: ProviderPaymentType.Unpaid;
}

```
```typescript
export interface ProviderPaymentPending {
  type: ProviderPaymentType.Pending;
  talerUri?: string;
}

```

### SetWalletDeviceIdOp
```typescript
/**
 * Set the internal device ID of the wallet, used to
 * identify whether a different/new wallet is accessing
 * the backup of another wallet.
 */
export type SetWalletDeviceIdOp = {
  op: WalletApiOperation.SetWalletDeviceId;
  request: SetWalletDeviceIdRequest;
  response: EmptyObject;
};
// SetWalletDeviceId = "setWalletDeviceId"

```
```typescript
export interface SetWalletDeviceIdRequest {
  /**
   * New wallet device ID to set.
   */
  walletDeviceId: string;
}

```

### ListStoredBackupsOp
```typescript
export type ListStoredBackupsOp = {
  op: WalletApiOperation.ListStoredBackups;
  request: EmptyObject;
  response: StoredBackupList;
};
// ListStoredBackups = "listStoredBackups"

```
```typescript
export interface StoredBackupList {
  storedBackups: {
    name: string;
  }[];
}

```

### CreateStoredBackupsOp
```typescript
export type CreateStoredBackupsOp = {
  op: WalletApiOperation.CreateStoredBackup;
  request: EmptyObject;
  response: CreateStoredBackupResponse;
};
// CreateStoredBackup = "createStoredBackup"

```
```typescript
export interface CreateStoredBackupResponse {
  name: string;
}

```

### RecoverStoredBackupsOp
```typescript
export type RecoverStoredBackupsOp = {
  op: WalletApiOperation.RecoverStoredBackup;
  request: RecoverStoredBackupRequest;
  response: EmptyObject;
};
// RecoverStoredBackup = "recoverStoredBackup"

```
```typescript
export interface RecoverStoredBackupRequest {
  name: string;
}

```

### DeleteStoredBackupOp
```typescript
export type DeleteStoredBackupOp = {
  op: WalletApiOperation.DeleteStoredBackup;
  request: DeleteStoredBackupRequest;
  response: EmptyObject;
};
// DeleteStoredBackup = "deleteStoredBackup"

```
```typescript
export interface DeleteStoredBackupRequest {
  name: string;
}

```

### CheckPeerPushDebitOp
```typescript
/**
 * Check if initiating a peer push payment is possible
 * based on the funds in the wallet.
 */
export type CheckPeerPushDebitOp = {
  op: WalletApiOperation.CheckPeerPushDebit;
  request: CheckPeerPushDebitRequest;
  response: CheckPeerPushDebitResponse;
};
// CheckPeerPushDebit = "checkPeerPushDebit"

```
```typescript
export interface CheckPeerPushDebitRequest {
  /**
   * Preferred exchange to use for the p2p payment.
   */
  exchangeBaseUrl?: string;
  /**
   * Instructed amount.
   *
   * FIXME: Allow specifying the instructed amount type.
   */
  amount: AmountString;
}

```
```typescript
export interface CheckPeerPushDebitResponse {
  amountRaw: AmountString;
  amountEffective: AmountString;
}

```

### InitiatePeerPushDebitOp
```typescript
/**
 * Initiate an outgoing peer push payment.
 */
export type InitiatePeerPushDebitOp = {
  op: WalletApiOperation.InitiatePeerPushDebit;
  request: InitiatePeerPushDebitRequest;
  response: InitiatePeerPushDebitResponse;
};
// InitiatePeerPushDebit = "initiatePeerPushDebit"

```
```typescript
export interface InitiatePeerPushDebitRequest {
  exchangeBaseUrl?: string;
  partialContractTerms: PeerContractTerms;
}

```
```typescript
export interface InitiatePeerPushDebitResponse {
  exchangeBaseUrl: string;
  pursePub: string;
  mergePriv: string;
  contractPriv: string;
  transactionId: TransactionIdStr;
}

```

### PreparePeerPushCreditOp
```typescript
/**
 * Check an incoming peer push payment.
 */
export type PreparePeerPushCreditOp = {
  op: WalletApiOperation.PreparePeerPushCredit;
  request: PreparePeerPushCreditRequest;
  response: PreparePeerPushCreditResponse;
};
// PreparePeerPushCredit = "preparePeerPushCredit"

```
```typescript
export interface PreparePeerPushCreditRequest {
  talerUri: string;
}

```
```typescript
export interface PreparePeerPushCreditResponse {
  contractTerms: PeerContractTerms;
  /**
   * @deprecated
   */
  amount: AmountString;
  amountRaw: AmountString;
  amountEffective: AmountString;
  peerPushCreditId: string;
  transactionId: string;
}

```

### ConfirmPeerPushCreditOp
```typescript
/**
 * Accept an incoming peer push payment.
 */
export type ConfirmPeerPushCreditOp = {
  op: WalletApiOperation.ConfirmPeerPushCredit;
  request: ConfirmPeerPushCreditRequest;
  response: EmptyObject;
};
// ConfirmPeerPushCredit = "confirmPeerPushCredit"

```
```typescript
export interface ConfirmPeerPushCreditRequest {
  /**
   * Transparent identifier of the incoming peer push payment.
   *
   * @deprecated specify transactionId instead!
   */
  peerPushCreditId?: string;
  transactionId?: string;
}

```

### CheckPeerPullCreditOp
```typescript
/**
 * Check fees for an outgoing peer pull payment.
 */
export type CheckPeerPullCreditOp = {
  op: WalletApiOperation.CheckPeerPullCredit;
  request: CheckPeerPullCreditRequest;
  response: CheckPeerPullCreditResponse;
};
// CheckPeerPullCredit = "checkPeerPullCredit"

```
```typescript
export interface CheckPeerPullCreditRequest {
  exchangeBaseUrl?: string;
  amount: AmountString;
}

```
```typescript
export interface CheckPeerPullCreditResponse {
  exchangeBaseUrl: string;
  amountRaw: AmountString;
  amountEffective: AmountString;
  /**
   * Number of coins that will be used,
   * can be used by the UI to warn if excessively large.
   */
  numCoins: number;
}

```

### InitiatePeerPullCreditOp
```typescript
/**
 * Initiate an outgoing peer pull payment.
 */
export type InitiatePeerPullCreditOp = {
  op: WalletApiOperation.InitiatePeerPullCredit;
  request: InitiatePeerPullCreditRequest;
  response: InitiatePeerPullCreditResponse;
};
// InitiatePeerPullCredit = "initiatePeerPullCredit"

```
```typescript
export interface InitiatePeerPullCreditRequest {
  exchangeBaseUrl?: string;
  partialContractTerms: PeerContractTerms;
}

```
```typescript
export interface InitiatePeerPullCreditResponse {
  /**
   * Taler URI for the other party to make the payment
   * that was requested.
   *
   * @deprecated since it's not necessarily valid yet until the tx is in the right state
   */
  talerUri: string;
  transactionId: TransactionIdStr;
}

```

### PreparePeerPullDebitOp
```typescript
/**
 * Prepare for an incoming peer pull payment.
 */
export type PreparePeerPullDebitOp = {
  op: WalletApiOperation.PreparePeerPullDebit;
  request: PreparePeerPullDebitRequest;
  response: PreparePeerPullDebitResponse;
};
// PreparePeerPullDebit = "preparePeerPullDebit"

```
```typescript
export interface PreparePeerPullDebitRequest {
  talerUri: string;
}

```
```typescript
export interface PreparePeerPullDebitResponse {
  contractTerms: PeerContractTerms;
  /**
   * @deprecated Redundant field with bad name, will be removed soon.
   */
  amount: AmountString;
  amountRaw: AmountString;
  amountEffective: AmountString;
  peerPullDebitId: string;
  transactionId: string;
}

```

### ConfirmPeerPullDebitOp
```typescript
/**
 * Accept an incoming peer pull payment (i.e. pay the other party).
 */
export type ConfirmPeerPullDebitOp = {
  op: WalletApiOperation.ConfirmPeerPullDebit;
  request: ConfirmPeerPullDebitRequest;
  response: EmptyObject;
};
// ConfirmPeerPullDebit = "confirmPeerPullDebit"

```
```typescript
export interface ConfirmPeerPullDebitRequest {
  /**
   * Transparent identifier of the incoming peer pull payment.
   *
   * @deprecated use transactionId instead
   */
  peerPullDebitId?: string;
  transactionId?: string;
}

```

### ValidateIbanOp
```typescript
export type ValidateIbanOp = {
  op: WalletApiOperation.ValidateIban;
  request: ValidateIbanRequest;
  response: ValidateIbanResponse;
};
// ValidateIban = "validateIban"

```
```typescript
export interface ValidateIbanRequest {
  iban: string;
}

```
```typescript
export interface ValidateIbanResponse {
  valid: boolean;
}

```

### ExportDbOp
```typescript
/**
 * Export the wallet database's contents to JSON.
 */
export type ExportDbOp = {
  op: WalletApiOperation.ExportDb;
  request: EmptyObject;
  response: any;
};
// ExportDb = "exportDb"

```

### ImportDbOp
```typescript
export type ImportDbOp = {
  op: WalletApiOperation.ImportDb;
  request: any;
  response: any;
};
// ImportDb = "importDb"

```

### ClearDbOp
```typescript
/**
 * Dangerously clear the whole wallet database.
 */
export type ClearDbOp = {
  op: WalletApiOperation.ClearDb;
  request: EmptyObject;
  response: EmptyObject;
};
// ClearDb = "clearDb"

```

### RecycleOp
```typescript
/**
 * Export a backup, clear the database and re-import it.
 */
export type RecycleOp = {
  op: WalletApiOperation.Recycle;
  request: EmptyObject;
  response: EmptyObject;
};
// Recycle = "recycle"

```

### ApplyDevExperimentOp
```typescript
/**
 * Apply a developer experiment to the current wallet state.
 *
 * This allows UI developers / testers to play around without
 * an elaborate test environment.
 */
export type ApplyDevExperimentOp = {
  op: WalletApiOperation.ApplyDevExperiment;
  request: ApplyDevExperimentRequest;
  response: EmptyObject;
};
// ApplyDevExperiment = "applyDevExperiment"

```
```typescript
export interface ApplyDevExperimentRequest {
  devExperimentUri: string;
}

```

### RunIntegrationTestOp
```typescript
/**
 * Run a simple integration test on a test deployment
 * of the exchange and merchant.
 */
export type RunIntegrationTestOp = {
  op: WalletApiOperation.RunIntegrationTest;
  request: IntegrationTestArgs;
  response: EmptyObject;
};
// RunIntegrationTest = "runIntegrationTest"

```

### RunIntegrationTestV2Op
```typescript
/**
 * Run a simple integration test on a test deployment
 * of the exchange and merchant.
 */
export type RunIntegrationTestV2Op = {
  op: WalletApiOperation.RunIntegrationTestV2;
  request: IntegrationTestArgs;
  response: EmptyObject;
};
// RunIntegrationTestV2 = "runIntegrationTestV2"

```

### TestCryptoOp
```typescript
/**
 * Test crypto worker.
 */
export type TestCryptoOp = {
  op: WalletApiOperation.TestCrypto;
  request: EmptyObject;
  response: any;
};
// TestCrypto = "testCrypto"

```

### WithdrawTestBalanceOp
```typescript
/**
 * Make withdrawal on a test deployment of the exchange
 * and merchant.
 */
export type WithdrawTestBalanceOp = {
  op: WalletApiOperation.WithdrawTestBalance;
  request: WithdrawTestBalanceRequest;
  response: EmptyObject;
};
// WithdrawTestBalance = "withdrawTestBalance"

```
```typescript
export interface WithdrawTestBalanceRequest {
  amount: AmountString;
  /**
   * Corebank API base URL.
   */
  corebankApiBaseUrl: string;
  exchangeBaseUrl: string;
  forcedDenomSel?: ForcedDenomSel;
}

```

### WithdrawTestkudosOp
```typescript
/**
 * Make a withdrawal of testkudos on test.taler.net.
 */
export type WithdrawTestkudosOp = {
  op: WalletApiOperation.WithdrawTestkudos;
  request: EmptyObject;
  response: EmptyObject;
};
// WithdrawTestkudos = "withdrawTestkudos"

```

### TestPayOp
```typescript
/**
 * Make a test payment using a test deployment of
 * the exchange and merchant.
 */
export type TestPayOp = {
  op: WalletApiOperation.TestPay;
  request: TestPayArgs;
  response: TestPayResult;
};
// TestPay = "testPay"

```
```typescript
export interface TestPayArgs {
  merchantBaseUrl: string;
  merchantAuthToken?: string;
  amount: AmountString;
  summary: string;
  forcedCoinSel?: ForcedCoinSel;
}

```
```typescript
export interface TestPayResult {
  payCoinSelection: PayCoinSelection;
}

```
```typescript
/**
 * Result of selecting coins, contains the exchange, and selected
 * coins with their denomination.
 */
export interface PayCoinSelection {
  /**
   * Amount requested by the merchant.
   */
  paymentAmount: AmountString;
  /**
   * Public keys of the coins that were selected.
   */
  coinPubs: string[];
  /**
   * Amount that each coin contributes.
   */
  coinContributions: AmountString[];
  /**
   * How much of the wire fees is the customer paying?
   */
  customerWireFees: AmountString;
  /**
   * How much of the deposit fees is the customer paying?
   */
  customerDepositFees: AmountString;
}

```

### GetPendingTasksOp
```typescript
/**
 * Get wallet-internal pending tasks.
 */
export type GetPendingTasksOp = {
  op: WalletApiOperation.GetPendingOperations;
  request: EmptyObject;
  response: PendingTasksResponse;
};
// GetPendingOperations = "getPendingOperations"

```
```typescript
/**
 * Response returned from the pending operations API.
 */
export interface PendingOperationsResponse {
  /**
   * List of pending operations.
   */
  pendingOperations: PendingTaskInfo[];
}

```
```typescript
/**
 * Information about a pending operation.
 */
export type PendingTaskInfo = PendingTaskInfoCommon &
  (
    | PendingExchangeUpdateTask
    | PendingExchangeCheckRefreshTask
    | PendingPurchaseTask
    | PendingRefreshTask
    | PendingTipPickupTask
    | PendingWithdrawTask
    | PendingRecoupTask
    | PendingDepositTask
    | PendingBackupTask
    | PendingPeerPushInitiationTask
    | PendingPeerPullInitiationTask
    | PendingPeerPullDebitTask
    | PendingPeerPushCreditTask
  );

```
```typescript
/**
 * Fields that are present in every pending operation.
 */
export interface PendingTaskInfoCommon {
  /**
   * Type of the pending operation.
   */
  type: PendingTaskType;
  /**
   * Unique identifier for the pending task.
   */
  id: TaskId;
  /**
   * Set to true if the operation indicates that something is really in progress,
   * as opposed to some regular scheduled operation that can be tried later.
   */
  givesLifeness: boolean;
  /**
   * Operation is active and waiting for a longpoll result.
   */
  isLongpolling: boolean;
  /**
   * Operation is waiting to be executed.
   */
  isDue: boolean;
  /**
   * Timestamp when the pending operation should be executed next.
   */
  timestampDue: AbsoluteTime;
  /**
   * Retry info.  Currently used to stop the wallet after any operation
   * exceeds a number of retries.
   */
  retryInfo?: DbRetryInfo;
  /**
   * Internal operation status for debugging.
   */
  internalOperationStatus?: string;
}

```
```typescript
export enum PendingTaskType {
  ExchangeUpdate = "exchange-update",
  ExchangeCheckRefresh = "exchange-check-refresh",
  Purchase = "purchase",
  Refresh = "refresh",
  Recoup = "recoup",
  RewardPickup = "reward-pickup",
  Withdraw = "withdraw",
  Deposit = "deposit",
  Backup = "backup",
  PeerPushDebit = "peer-push-debit",
  PeerPullCredit = "peer-pull-credit",
  PeerPushCredit = "peer-push-credit",
  PeerPullDebit = "peer-pull-debit",
}

```
```typescript
export type TaskId = string & {
  [__taskId]: true;
};

```
```typescript
export interface DbRetryInfo {
  firstTry: DbPreciseTimestamp;
  nextRetry: DbPreciseTimestamp;
  retryCounter: number;
}

```
```typescript
/**
 * Timestamp, stored as microseconds.
 */
export type DbPreciseTimestamp = number & {
  [symDbPreciseTimestamp]: true;
};

```
```typescript
export interface RetryPolicy {
  readonly backoffDelta: Duration;
  readonly backoffBase: number;
  readonly maxTimeout: Duration;
}

```
```typescript
// Declare "static" methods in Error
interface ErrorConstructor {
  /** Create .stack property on a target object */
  captureStackTrace(targetObject: object, constructorOpt?: Function): void;
  /**
   * Optional override for formatting stack traces
   *
   * @see https://v8.dev/docs/stack-trace-api#customizing-stack-traces
   */
  prepareStackTrace?:
    | ((err: Error, stackTraces: NodeJS.CallSite[]) => any)
    | undefined;
  stackTraceLimit: number;
}

```
```typescript
interface CallSite {
  /**
   * Value of "this"
   */
  getThis(): unknown;
  /**
   * Type of "this" as a string.
   * This is the name of the function stored in the constructor field of
   * "this", if available.  Otherwise the object's [[Class]] internal
   * property.
   */
  getTypeName(): string | null;
  /**
   * Current function
   */
  getFunction(): Function | undefined;
  /**
   * Name of the current function, typically its name property.
   * If a name property is not available an attempt will be made to try
   * to infer a name from the function's context.
   */
  getFunctionName(): string | null;
  /**
   * Name of the property [of "this" or one of its prototypes] that holds
   * the current function
   */
  getMethodName(): string | null;
  /**
   * Name of the script [if this function was defined in a script]
   */
  getFileName(): string | null;
  /**
   * Current line number [if this function was defined in a script]
   */
  getLineNumber(): number | null;
  /**
   * Current column number [if this function was defined in a script]
   */
  getColumnNumber(): number | null;
  /**
   * A call site object representing the location where eval was called
   * [if this function was created using a call to eval]
   */
  getEvalOrigin(): string | undefined;
  /**
   * Is this a toplevel invocation, that is, is "this" the global object?
   */
  isToplevel(): boolean;
  /**
   * Does this call take place in code defined by a call to eval?
   */
  isEval(): boolean;
  /**
   * Is this call in native V8 code?
   */
  isNative(): boolean;
  /**
   * Is this a constructor call?
   */
  isConstructor(): boolean;
}

```
```typescript
/**
 * The wallet is currently updating information about an exchange.
 */
export interface PendingExchangeUpdateTask {
  type: PendingTaskType.ExchangeUpdate;
  exchangeBaseUrl: string;
  lastError: TalerErrorDetail | undefined;
}

```
```typescript
/**
 * The wallet should check whether coins from this exchange
 * need to be auto-refreshed.
 */
export interface PendingExchangeCheckRefreshTask {
  type: PendingTaskType.ExchangeCheckRefresh;
  exchangeBaseUrl: string;
}

```
```typescript
/**
 * A purchase needs to be processed (i.e. for download / payment / refund).
 */
export interface PendingPurchaseTask {
  type: PendingTaskType.Purchase;
  proposalId: string;
  retryInfo?: DbRetryInfo;
  /**
   * Status of the payment as string, used only for debugging.
   */
  statusStr: string;
  lastError: TalerErrorDetail | undefined;
}

```
```typescript
/**
 * Status of an ongoing withdrawal operation.
 */
export interface PendingRefreshTask {
  type: PendingTaskType.Refresh;
  lastError?: TalerErrorDetail;
  refreshGroupId: string;
  finishedPerCoin: boolean[];
  retryInfo?: DbRetryInfo;
}

```
```typescript
/**
 * The wallet is picking up a tip that the user has accepted.
 */
export interface PendingTipPickupTask {
  type: PendingTaskType.RewardPickup;
  tipId: string;
  merchantBaseUrl: string;
  merchantTipId: string;
}

```
```typescript
/**
 * Status of an ongoing withdrawal operation.
 */
export interface PendingWithdrawTask {
  type: PendingTaskType.Withdraw;
  lastError: TalerErrorDetail | undefined;
  retryInfo?: DbRetryInfo;
  withdrawalGroupId: string;
}

```
```typescript
export interface PendingRecoupTask {
  type: PendingTaskType.Recoup;
  recoupGroupId: string;
  retryInfo?: DbRetryInfo;
  lastError: TalerErrorDetail | undefined;
}

```
```typescript
/**
 * Status of an ongoing deposit operation.
 */
export interface PendingDepositTask {
  type: PendingTaskType.Deposit;
  lastError: TalerErrorDetail | undefined;
  retryInfo: DbRetryInfo | undefined;
  depositGroupId: string;
}

```
```typescript
export interface PendingBackupTask {
  type: PendingTaskType.Backup;
  backupProviderBaseUrl: string;
  lastError: TalerErrorDetail | undefined;
}

```
```typescript
/**
 * The wallet wants to send a peer push payment.
 */
export interface PendingPeerPushInitiationTask {
  type: PendingTaskType.PeerPushDebit;
  pursePub: string;
}

```
```typescript
/**
 * The wallet wants to send a peer pull payment.
 */
export interface PendingPeerPullInitiationTask {
  type: PendingTaskType.PeerPullCredit;
  pursePub: string;
}

```
```typescript
/**
 * The wallet wants to send a peer pull payment.
 */
export interface PendingPeerPullDebitTask {
  type: PendingTaskType.PeerPullDebit;
  peerPullDebitId: string;
}

```
```typescript
/**
 */
export interface PendingPeerPushCreditTask {
  type: PendingTaskType.PeerPushCredit;
  peerPushCreditId: string;
}

```

### DumpCoinsOp
```typescript
/**
 * Dump all coins of the wallet in a simple JSON format.
 */
export type DumpCoinsOp = {
  op: WalletApiOperation.DumpCoins;
  request: EmptyObject;
  response: CoinDumpJson;
};
// DumpCoins = "dumpCoins"

```
```typescript
/**
 * Easy to process format for the public data of coins
 * managed by the wallet.
 */
export interface CoinDumpJson {
  coins: Array<{
    /**
     * The coin's denomination's public key.
     */
    denom_pub: DenominationPubKey;
    /**
     * Hash of denom_pub.
     */
    denom_pub_hash: string;
    /**
     * Value of the denomination (without any fees).
     */
    denom_value: string;
    /**
     * Public key of the coin.
     */
    coin_pub: string;
    /**
     * Base URL of the exchange for the coin.
     */
    exchange_base_url: string;
    /**
     * Public key of the parent coin.
     * Only present if this coin was obtained via refreshing.
     */
    refresh_parent_coin_pub: string | undefined;
    /**
     * Public key of the reserve for this coin.
     * Only present if this coin was obtained via refreshing.
     */
    withdrawal_reserve_pub: string | undefined;
    coin_status: CoinStatus;
    spend_allocation:
      | {
          id: string;
          amount: AmountString;
        }
      | undefined;
    /**
     * Information about the age restriction
     */
    ageCommitmentProof: AgeCommitmentProof | undefined;
  }>;
}

```
```typescript
export type DenominationPubKey = RsaDenominationPubKey | CsDenominationPubKey;

```
```typescript
export interface RsaDenominationPubKey {
  readonly cipher: DenomKeyType.Rsa;
  readonly rsa_public_key: string;
  readonly age_mask: number;
}

```
```typescript
export interface CsDenominationPubKey {
  readonly cipher: DenomKeyType.ClauseSchnorr;
  readonly age_mask: number;
  readonly cs_public_key: string;
}

```
```typescript
/**
 * Status of a coin.
 */
export declare enum CoinStatus {
  /**
   * Withdrawn and never shown to anybody.
   */
  Fresh = "fresh",
  /**
   * Fresh, but currently marked as "suspended", thus won't be used
   * for spending.  Used for testing.
   */
  FreshSuspended = "fresh-suspended",
  /**
   * A coin that has been spent and refreshed.
   */
  Dormant = "dormant",
}

```
```typescript
export interface AgeCommitmentProof {
  commitment: AgeCommitment;
  proof: AgeProof;
}

```
```typescript
export interface AgeCommitment {
  mask: number;
  /**
   * Public keys, one for each age group specified in the age mask.
   */
  publicKeys: Edx25519PublicKeyEnc[];
}

```
```typescript
export type Edx25519PublicKeyEnc = FlavorP<string, "Edx25519PublicKeyEnc", 32>;

```
```typescript
export type FlavorP<T, FlavorT extends string, S extends number> = T & {
  _flavor?: `taler.${FlavorT}`;
  _size?: S;
};

```
```typescript
export interface AgeProof {
  /**
   * Private keys.  Typically smaller than the number of public keys,
   * because we drop private keys from age groups that are restricted.
   */
  privateKeys: Edx25519PrivateKeyEnc[];
}

```
```typescript
export type Edx25519PrivateKeyEnc = FlavorP<
  string,
  "Edx25519PrivateKeyEnc",
  64
>;

```

### TestingSetTimetravelOp
```typescript
/**
 * Add an offset to the wallet's internal time.
 */
export type TestingSetTimetravelOp = {
  op: WalletApiOperation.TestingSetTimetravel;
  request: TestingSetTimetravelRequest;
  response: EmptyObject;
};
// TestingSetTimetravel = "testingSetTimetravel"

```
```typescript
export interface TestingSetTimetravelRequest {
  offsetMs: number;
}

```

### TestingWaitTransactionsFinalOp
```typescript
/**
 * Wait until all transactions are in a final state.
 */
export type TestingWaitTransactionsFinalOp = {
  op: WalletApiOperation.TestingWaitTransactionsFinal;
  request: EmptyObject;
  response: EmptyObject;
};
// TestingWaitTransactionsFinal = "testingWaitTransactionsFinal"

```

### TestingWaitRefreshesFinalOp
```typescript
/**
 * Wait until all refresh transactions are in a final state.
 */
export type TestingWaitRefreshesFinalOp = {
  op: WalletApiOperation.TestingWaitRefreshesFinal;
  request: EmptyObject;
  response: EmptyObject;
};
// TestingWaitRefreshesFinal = "testingWaitRefreshesFinal"

```

### TestingWaitTasksProcessedOp
```typescript
/**
 * Wait until all tasks have been processed and the wallet is idle.
 */
export type TestingWaitTasksProcessedOp = {
  op: WalletApiOperation.TestingWaitTasksProcessed;
  request: EmptyObject;
  response: EmptyObject;
};
// TestingWaitTasksProcessed = "testingWaitTasksProcessed"

```

### TestingWaitTransactionStateOp
```typescript
/**
 * Wait until a transaction is in a particular state.
 */
export type TestingWaitTransactionStateOp = {
  op: WalletApiOperation.TestingWaitTransactionState;
  request: TestingWaitTransactionRequest;
  response: EmptyObject;
};
// TestingWaitTransactionState = "testingWaitTransactionState"

```
```typescript
export interface TestingWaitTransactionRequest {
  transactionId: string;
  txState: TransactionState;
}

```

### SetCoinSuspendedOp
```typescript
/**
 * Set a coin as (un-)suspended.
 * Suspended coins won't be used for payments.
 */
export type SetCoinSuspendedOp = {
  op: WalletApiOperation.SetCoinSuspended;
  request: SetCoinSuspendedRequest;
  response: EmptyObject;
};
// SetCoinSuspended = "setCoinSuspended"

```
```typescript
export interface SetCoinSuspendedRequest {
  coinPub: string;
  suspended: boolean;
}

```

### ForceRefreshOp
```typescript
/**
 * Force a refresh on coins where it would not
 * be necessary.
 */
export type ForceRefreshOp = {
  op: WalletApiOperation.ForceRefresh;
  request: ForceRefreshRequest;
  response: EmptyObject;
};
// ForceRefresh = "forceRefresh"

```
```typescript
export interface ForceRefreshRequest {
  coinPubList: string[];
}

```

## Common Declarations
```typescript
export interface WalletCoreVersion {
  /**
   * @deprecated
   */
  hash: string | undefined;
  version: string;
  exchange: string;
  merchant: string;
  bank: string;
  /**
   * @deprecated will be removed
   */
  devMode: boolean;
}
```
```typescript
export type ScopeInfo = ScopeInfoGlobal | ScopeInfoExchange | ScopeInfoAuditor;
```
```typescript
export type AmountString = string & {
  [__amount_str]: true;
};
```
```typescript
/**
 * How the amount should be interpreted in a transaction
 * Effective = how the balance is change
 * Raw = effective amount without fee
 *
 * Depending on the transaction, raw can be higher than effective
 */
export declare enum TransactionAmountMode {
  Effective = "effective",
  Raw = "raw",
}
```
```typescript
export interface ConvertAmountRequest {
  amount: AmountString;
  type: TransactionAmountMode;
}
```
```typescript
export interface AmountResponse {
  effectiveAmount: AmountString;
  rawAmount: AmountString;
}
```
```typescript
export interface GetAmountRequest {
  currency: string;
}
```
```typescript
export interface TransactionsResponse {
  transactions: Transaction[];
}
```
```typescript
export type Transaction =
  | TransactionWithdrawal
  | TransactionPayment
  | TransactionRefund
  | TransactionReward
  | TransactionRefresh
  | TransactionDeposit
  | TransactionPeerPullCredit
  | TransactionPeerPullDebit
  | TransactionPeerPushCredit
  | TransactionPeerPushDebit
  | TransactionInternalWithdrawal;
```
```typescript
/**
 * A withdrawal transaction (either bank-integrated or manual).
 */
export interface TransactionWithdrawal extends TransactionCommon {
  type: TransactionType.Withdrawal;
  /**
   * Exchange of the withdrawal.
   */
  exchangeBaseUrl: string;
  /**
   * Amount that got subtracted from the reserve balance.
   */
  amountRaw: AmountString;
  /**
   * Amount that actually was (or will be) added to the wallet's balance.
   */
  amountEffective: AmountString;
  withdrawalDetails: WithdrawalDetails;
}
```
```typescript
export interface TransactionCommon {
  transactionId: TransactionIdStr;
  type: TransactionType;
  timestamp: TalerPreciseTimestamp;
  /**
   * Transaction state, as per DD37.
   */
  txState: TransactionState;
  /**
   * Possible transitions based on the current state.
   */
  txActions: TransactionAction[];
  /**
   * Raw amount of the transaction (exclusive of fees or other extra costs).
   */
  amountRaw: AmountString;
  /**
   * Amount added or removed from the wallet's balance (including all fees and other costs).
   */
  amountEffective: AmountString;
  error?: TalerErrorDetail;
  /**
   * If the transaction minor state is in KycRequired this field is going to
   * have the location where the user need to go to complete KYC information.
   */
  kycUrl?: string;
}
```
```typescript
export type TransactionIdStr = `txn:${string}:${string}` & {
  [__txId]: true;
};
```
```typescript
export declare enum TransactionType {
  Withdrawal = "withdrawal",
  InternalWithdrawal = "internal-withdrawal",
  Payment = "payment",
  Refund = "refund",
  Refresh = "refresh",
  Reward = "reward",
  Deposit = "deposit",
  PeerPushDebit = "peer-push-debit",
  PeerPushCredit = "peer-push-credit",
  PeerPullDebit = "peer-pull-debit",
  PeerPullCredit = "peer-pull-credit",
}
```
```typescript
/**
 * Precise timestamp, typically used in the wallet-core
 * API but not in other Taler APIs so far.
 */
export interface TalerPreciseTimestamp {
  /**
   * Seconds (as integer) since epoch.
   */
  readonly t_s: number | "never";
  /**
   * Optional microsecond offset (non-negative integer).
   */
  readonly off_us?: number;
  readonly _flavor?: typeof flavor_TalerPreciseTimestamp;
}
```
```typescript
export interface TalerProtocolTimestamp {
  /**
   * Seconds (as integer) since epoch.
   */
  readonly t_s: number | "never";
  readonly _flavor?: typeof flavor_TalerProtocolTimestamp;
}
```
```typescript
export interface TransactionState {
  major: TransactionMajorState;
  minor?: TransactionMinorState;
}
```
```typescript
export declare enum TransactionMajorState {
  None = "none",
  Pending = "pending",
  Done = "done",
  Aborting = "aborting",
  Aborted = "aborted",
  Suspended = "suspended",
  Dialog = "dialog",
  SuspendedAborting = "suspended-aborting",
  Failed = "failed",
  Expired = "expired",
  Deleted = "deleted",
}
```
```typescript
export declare enum TransactionMinorState {
  Unknown = "unknown",
  Deposit = "deposit",
  KycRequired = "kyc",
  AmlRequired = "aml",
  MergeKycRequired = "merge-kyc",
  Track = "track",
  SubmitPayment = "submit-payment",
  RebindSession = "rebind-session",
  Refresh = "refresh",
  Pickup = "pickup",
  AutoRefund = "auto-refund",
  User = "user",
  Bank = "bank",
  Exchange = "exchange",
  ClaimProposal = "claim-proposal",
  CheckRefund = "check-refund",
  CreatePurse = "create-purse",
  DeletePurse = "delete-purse",
  Ready = "ready",
  Merge = "merge",
  Repurchase = "repurchase",
  BankRegisterReserve = "bank-register-reserve",
  BankConfirmTransfer = "bank-confirm-transfer",
  WithdrawCoins = "withdraw-coins",
  ExchangeWaitReserve = "exchange-wait-reserve",
  AbortingBank = "aborting-bank",
  Aborting = "aborting",
  Refused = "refused",
  Withdraw = "withdraw",
  MerchantOrderProposed = "merchant-order-proposed",
  Proposed = "proposed",
  RefundAvailable = "refund-available",
  AcceptRefund = "accept-refund",
}
```
```typescript
export declare enum TransactionAction {
  Delete = "delete",
  Suspend = "suspend",
  Resume = "resume",
  Abort = "abort",
  Fail = "fail",
  Retry = "retry",
}
```
```typescript
export interface TalerErrorDetail {
  code: TalerErrorCode;
  when?: AbsoluteTime;
  hint?: string;
  [x: string]: unknown;
}
```
```typescript
export interface AbsoluteTime {
  /**
   * Timestamp in milliseconds.
   */
  readonly t_ms: number | "never";
  readonly _flavor?: typeof flavor_AbsoluteTime;
  [opaque_AbsoluteTime]: true;
}
```
```typescript
export interface Duration {
  /**
   * Duration in milliseconds.
   */
  readonly d_ms: number | "forever";
}
```
```typescript
export interface TalerProtocolDuration {
  readonly d_us: number | "forever";
}
```
```typescript
export type WithdrawalDetails =
  | WithdrawalDetailsForManualTransfer
  | WithdrawalDetailsForTalerBankIntegrationApi;
```
```typescript
interface WithdrawalDetailsForManualTransfer {
  type: WithdrawalType.ManualTransfer;
  /**
   * Payto URIs that the exchange supports.
   *
   * Already contains the amount and message.
   *
   * @deprecated in favor of exchangeCreditAccounts
   */
  exchangePaytoUris: string[];
  exchangeCreditAccountDetails?: WithdrawalExchangeAccountDetails[];
  reservePub: string;
  /**
   * Is the reserve ready for withdrawal?
   */
  reserveIsReady: boolean;
}
```
```typescript
export interface WithdrawalExchangeAccountDetails {
  /**
   * Payto URI to credit the exchange.
   *
   * Depending on whether the (manual!) withdrawal is accepted or just
   * being checked, this already includes the subject with the
   * reserve public key.
   */
  paytoUri: string;
  /**
   * Transfer amount. Might be in a different currency than the requested
   * amount for withdrawal.
   *
   * Redundant with the amount in paytoUri, just included to avoid parsing.
   */
  transferAmount: AmountString;
  /**
   * Further restrictions for sending money to the
   * exchange.
   */
  creditRestrictions?: AccountRestriction[];
}
```
```typescript
export type AccountRestriction =
  | RegexAccountRestriction
  | DenyAllAccountRestriction;
```
```typescript
export interface RegexAccountRestriction {
  type: "regex";
  payto_regex: string;
  human_hint: string;
  human_hint_i18n?: InternationalizedString;
}
```
```typescript
export interface InternationalizedString {
  [lang_tag: string]: string;
}
```
```typescript
export interface DenyAllAccountRestriction {
  type: "deny";
}
```
```typescript
interface WithdrawalDetailsForTalerBankIntegrationApi {
  type: WithdrawalType.TalerBankIntegrationApi;
  /**
   * Set to true if the bank has confirmed the withdrawal, false if not.
   * An unconfirmed withdrawal usually requires user-input and should be highlighted in the UI.
   * See also bankConfirmationUrl below.
   */
  confirmed: boolean;
  /**
   * If the withdrawal is unconfirmed, this can include a URL for user
   * initiated confirmation.
   */
  bankConfirmationUrl?: string;
  reservePub: string;
  /**
   * Is the reserve ready for withdrawal?
   */
  reserveIsReady: boolean;
}
```
```typescript
export interface TransactionPayment extends TransactionCommon {
  type: TransactionType.Payment;
  /**
   * Additional information about the payment.
   */
  info: OrderShortInfo;
  /**
   * Wallet-internal end-to-end identifier for the payment.
   */
  proposalId: string;
  /**
   * Amount that must be paid for the contract
   */
  amountRaw: AmountString;
  /**
   * Amount that was paid, including deposit, wire and refresh fees.
   */
  amountEffective: AmountString;
  /**
   * Amount that has been refunded by the merchant
   */
  totalRefundRaw: AmountString;
  /**
   * Amount will be added to the wallet's balance after fees and refreshing
   */
  totalRefundEffective: AmountString;
  /**
   * Amount pending to be picked up
   */
  refundPending: AmountString | undefined;
  /**
   * Reference to applied refunds
   */
  refunds: RefundInfoShort[];
  /**
   * Is the wallet currently checking for a refund?
   */
  refundQueryActive: boolean;
  /**
   * Does this purchase has an pos validation
   */
  posConfirmation: string | undefined;
}
```
```typescript
export interface OrderShortInfo {
  /**
   * Order ID, uniquely identifies the order within a merchant instance
   */
  orderId: string;
  /**
   * Hash of the contract terms.
   */
  contractTermsHash: string;
  /**
   * More information about the merchant
   */
  merchant: MerchantInfo;
  /**
   * Summary of the order, given by the merchant
   */
  summary: string;
  /**
   * Map from IETF BCP 47 language tags to localized summaries
   */
  summary_i18n?: InternationalizedString;
  /**
   * URL of the fulfillment, given by the merchant
   */
  fulfillmentUrl?: string;
  /**
   * Plain text message that should be shown to the user
   * when the payment is complete.
   */
  fulfillmentMessage?: string;
  /**
   * Translations of fulfillmentMessage.
   */
  fulfillmentMessage_i18n?: InternationalizedString;
}
```
```typescript
export interface MerchantInfo {
  name: string;
  jurisdiction?: Location;
  address?: Location;
  logo?: string;
  website?: string;
  email?: string;
}
```
```typescript
export interface Location {
  country?: string;
  country_subdivision?: string;
  district?: string;
  town?: string;
  town_location?: string;
  post_code?: string;
  street?: string;
  building_name?: string;
  building_number?: string;
  address_lines?: string[];
}
```
```typescript
export interface RefundInfoShort {
  transactionId: string;
  timestamp: TalerProtocolTimestamp;
  amountEffective: AmountString;
  amountRaw: AmountString;
}
```
```typescript
export interface TransactionRefund extends TransactionCommon {
  type: TransactionType.Refund;
  amountRaw: AmountString;
  amountEffective: AmountString;
  refundedTransactionId: string;
  paymentInfo: RefundPaymentInfo | undefined;
}
```
```typescript
/**
 * Summary information about the payment that we got a refund for.
 */
export interface RefundPaymentInfo {
  summary: string;
  summary_i18n?: InternationalizedString;
  /**
   * More information about the merchant
   */
  merchant: MerchantInfo;
}
```
```typescript
export interface TransactionReward extends TransactionCommon {
  type: TransactionType.Reward;
  amountRaw: AmountString;
  /**
   * More information about the merchant
   */
  amountEffective: AmountString;
  merchantBaseUrl: string;
}
```
```typescript
/**
 * A transaction shown for refreshes.
 * Only shown for (1) refreshes not associated with other transactions
 * and (2) refreshes in an error state.
 */
export interface TransactionRefresh extends TransactionCommon {
  type: TransactionType.Refresh;
  refreshReason: RefreshReason;
  /**
   * Transaction ID that caused this refresh.
   */
  originatingTransactionId?: string;
  /**
   * Always zero for refreshes
   */
  amountRaw: AmountString;
  /**
   * Fees, i.e. the effective, negative effect of the refresh
   * on the balance.
   *
   * Only applicable for stand-alone refreshes, and zero for
   * other refreshes where the transaction itself accounts for the
   * refresh fee.
   */
  amountEffective: AmountString;
  refreshInputAmount: AmountString;
  refreshOutputAmount: AmountString;
}
```
```typescript
/**
 * Reasons for why a coin is being refreshed.
 */
export declare enum RefreshReason {
  Manual = "manual",
  PayMerchant = "pay-merchant",
  PayDeposit = "pay-deposit",
  PayPeerPush = "pay-peer-push",
  PayPeerPull = "pay-peer-pull",
  Refund = "refund",
  AbortPay = "abort-pay",
  AbortDeposit = "abort-deposit",
  AbortPeerPushDebit = "abort-peer-push-debit",
  Recoup = "recoup",
  BackupRestored = "backup-restored",
  Scheduled = "scheduled",
}
```
```typescript
/**
 * Deposit transaction, which effectively sends
 * money from this wallet somewhere else.
 */
export interface TransactionDeposit extends TransactionCommon {
  type: TransactionType.Deposit;
  depositGroupId: string;
  /**
   * Target for the deposit.
   */
  targetPaytoUri: string;
  /**
   * Raw amount that is being deposited
   */
  amountRaw: AmountString;
  /**
   * Effective amount that is being deposited
   */
  amountEffective: AmountString;
  wireTransferDeadline: TalerProtocolTimestamp;
  wireTransferProgress: number;
  /**
   * Did all the deposit requests succeed?
   */
  deposited: boolean;
  trackingState: Array<DepositTransactionTrackingState>;
}
```
```typescript
export interface DepositTransactionTrackingState {
  wireTransferId: string;
  timestampExecuted: TalerProtocolTimestamp;
  amountRaw: AmountString;
  wireFee: AmountString;
}
```
```typescript
/**
 * Credit because we were paid for a P2P invoice we created.
 */
export interface TransactionPeerPullCredit extends TransactionCommon {
  type: TransactionType.PeerPullCredit;
  info: PeerInfoShort;
  /**
   * Exchange used.
   */
  exchangeBaseUrl: string;
  /**
   * Amount that got subtracted from the reserve balance.
   */
  amountRaw: AmountString;
  /**
   * Amount that actually was (or will be) added to the wallet's balance.
   */
  amountEffective: AmountString;
  /**
   * URI to send to the other party.
   */
  talerUri: string;
}
```
```typescript
export interface PeerInfoShort {
  expiration: TalerProtocolTimestamp | undefined;
  summary: string | undefined;
}
```
```typescript
/**
 * Debit because we paid someone's invoice.
 */
export interface TransactionPeerPullDebit extends TransactionCommon {
  type: TransactionType.PeerPullDebit;
  info: PeerInfoShort;
  /**
   * Exchange used.
   */
  exchangeBaseUrl: string;
  amountRaw: AmountString;
  amountEffective: AmountString;
}
```
```typescript
/**
 * We received money via a P2P payment.
 */
export interface TransactionPeerPushCredit extends TransactionCommon {
  type: TransactionType.PeerPushCredit;
  info: PeerInfoShort;
  /**
   * Exchange used.
   */
  exchangeBaseUrl: string;
  /**
   * Amount that got subtracted from the reserve balance.
   */
  amountRaw: AmountString;
  /**
   * Amount that actually was (or will be) added to the wallet's balance.
   */
  amountEffective: AmountString;
}
```
```typescript
/**
 * We sent money via a P2P payment.
 */
export interface TransactionPeerPushDebit extends TransactionCommon {
  type: TransactionType.PeerPushDebit;
  info: PeerInfoShort;
  /**
   * Exchange used.
   */
  exchangeBaseUrl: string;
  /**
   * Amount that got subtracted from the reserve balance.
   */
  amountRaw: AmountString;
  /**
   * Amount that actually was (or will be) added to the wallet's balance.
   */
  amountEffective: AmountString;
  /**
   * URI to accept the payment.
   *
   * Only present if the transaction is in a state where the other party can
   * accept the payment.
   */
  talerUri?: string;
}
```
```typescript
/**
 * Internal withdrawal operation, only reported on request.
 *
 * Some transactions (peer-*-credit) internally do a withdrawal,
 * but only the peer-*-credit transaction is reported.
 *
 * The internal withdrawal transaction allows to access the details of
 * the underlying withdrawal for testing/debugging.
 *
 * It is usually not reported, so that amounts of transactions properly
 * add up, since the amountEffecive of the withdrawal is already reported
 * in the peer-*-credit transaction.
 */
export interface TransactionInternalWithdrawal extends TransactionCommon {
  type: TransactionType.InternalWithdrawal;
  /**
   * Exchange of the withdrawal.
   */
  exchangeBaseUrl: string;
  /**
   * Amount that got subtracted from the reserve balance.
   */
  amountRaw: AmountString;
  /**
   * Amount that actually was (or will be) added to the wallet's balance.
   */
  amountEffective: AmountString;
  withdrawalDetails: WithdrawalDetails;
}
```
```typescript
export interface AbortTransactionRequest {
  transactionId: TransactionIdStr;
}
```
```typescript
export interface ExchangeListItem {
  exchangeBaseUrl: string;
  currency: string | undefined;
  paytoUris: string[];
  tosStatus: ExchangeTosStatus;
  exchangeEntryStatus: ExchangeEntryStatus;
  exchangeUpdateStatus: ExchangeUpdateStatus;
  ageRestrictionOptions: number[];
  /**
   * Information about the last error that occurred when trying
   * to update the exchange info.
   */
  lastUpdateErrorInfo?: OperationErrorInfo;
}
```
```typescript
export declare enum ExchangeTosStatus {
  Pending = "pending",
  Proposed = "proposed",
  Accepted = "accepted",
}
```
```typescript
export declare enum ExchangeEntryStatus {
  Preset = "preset",
  Ephemeral = "ephemeral",
  Used = "used",
}
```
```typescript
export declare enum ExchangeUpdateStatus {
  Initial = "initial",
  InitialUpdate = "initial(update)",
  Suspended = "suspended",
  Failed = "failed",
  OutdatedUpdate = "outdated(update)",
  Ready = "ready",
  ReadyUpdate = "ready(update)",
}
```
```typescript
export interface OperationErrorInfo {
  error: TalerErrorDetail;
}
```
```typescript
export interface ForcedDenomSel {
  denoms: {
    value: AmountString;
    count: number;
  }[];
}
```
```typescript
/**
 * Result of a prepare pay operation.
 */
export type PreparePayResult =
  | PreparePayResultInsufficientBalance
  | PreparePayResultAlreadyConfirmed
  | PreparePayResultPaymentPossible;
```
```typescript
export interface PreparePayResultInsufficientBalance {
  status: PreparePayResultType.InsufficientBalance;
  transactionId: TransactionIdStr;
  /**
   * @deprecated use transactionId
   */
  proposalId: string;
  contractTerms: MerchantContractTerms;
  amountRaw: AmountString;
  talerUri: string;
  balanceDetails: PayMerchantInsufficientBalanceDetails;
}
```
```typescript
/**
 * Contract terms from a merchant.
 * FIXME: Add type field!
 */
export interface MerchantContractTerms {
  /**
   * Hash of the merchant's wire details.
   */
  h_wire: string;
  /**
   * Hash of the merchant's wire details.
   */
  auto_refund?: TalerProtocolDuration;
  /**
   * Wire method the merchant wants to use.
   */
  wire_method: string;
  /**
   * Human-readable short summary of the contract.
   */
  summary: string;
  summary_i18n?: InternationalizedString;
  /**
   * Nonce used to ensure freshness.
   */
  nonce: string;
  /**
   * Total amount payable.
   */
  amount: string;
  /**
   * Deadline to pay for the contract.
   */
  pay_deadline: TalerProtocolTimestamp;
  /**
   * Maximum deposit fee covered by the merchant.
   */
  max_fee: string;
  /**
   * Information about the merchant.
   */
  merchant: MerchantInfo;
  /**
   * Public key of the merchant.
   */
  merchant_pub: string;
  /**
   * Time indicating when the order should be delivered.
   * May be overwritten by individual products.
   */
  delivery_date?: TalerProtocolTimestamp;
  /**
   * Delivery location for (all!) products.
   */
  delivery_location?: Location;
  /**
   * List of accepted exchanges.
   */
  exchanges: ExchangeHandle[];
  /**
   * Products that are sold in this contract.
   */
  products?: Product[];
  /**
   * Deadline for refunds.
   */
  refund_deadline: TalerProtocolTimestamp;
  /**
   * Deadline for the wire transfer.
   */
  wire_transfer_deadline: TalerProtocolTimestamp;
  /**
   * Time when the contract was generated by the merchant.
   */
  timestamp: TalerProtocolTimestamp;
  /**
   * Order id to uniquely identify the purchase within
   * one merchant instance.
   */
  order_id: string;
  /**
   * Base URL of the merchant's backend.
   */
  merchant_base_url: string;
  /**
   * Fulfillment URL to view the product or
   * delivery status.
   */
  fulfillment_url?: string;
  /**
   * URL meant to share the shopping cart.
   */
  public_reorder_url?: string;
  /**
   * Plain text fulfillment message in the merchant's default language.
   */
  fulfillment_message?: string;
  /**
   * Internationalized fulfillment messages.
   */
  fulfillment_message_i18n?: InternationalizedString;
  /**
   * Share of the wire fee that must be settled with one payment.
   */
  wire_fee_amortization?: number;
  /**
   * Maximum wire fee that the merchant agrees to pay for.
   */
  max_wire_fee?: string;
  minimum_age?: number;
  /**
   * Extra data, interpreted by the mechant only.
   */
  extra?: any;
}
```
```typescript
/**
 * Information about an exchange as stored inside a
 * merchant's contract terms.
 */
export interface ExchangeHandle {
  /**
   * Master public signing key of the exchange.
   */
  master_pub: string;
  /**
   * Base URL of the exchange.
   */
  url: string;
}
```
```typescript
export interface Product {
  product_id?: string;
  description: string;
  description_i18n?: {
    [lang_tag: string]: string;
  };
  quantity?: number;
  unit?: string;
  price?: AmountString;
  image?: string;
  taxes?: Tax[];
  delivery_date?: TalerProtocolTimestamp;
}
```
```typescript
export interface Tax {
  name: string;
  tax: AmountString;
}
```
```typescript
/**
 * Detailed reason for why the wallet's balance is insufficient.
 */
export interface PayMerchantInsufficientBalanceDetails {
  /**
   * Amount requested by the merchant.
   */
  amountRequested: AmountString;
  /**
   * Balance of type "available" (see balance.ts for definition).
   */
  balanceAvailable: AmountString;
  /**
   * Balance of type "material" (see balance.ts for definition).
   */
  balanceMaterial: AmountString;
  /**
   * Balance of type "age-acceptable" (see balance.ts for definition).
   */
  balanceAgeAcceptable: AmountString;
  /**
   * Balance of type "merchant-acceptable" (see balance.ts for definition).
   */
  balanceMerchantAcceptable: AmountString;
  /**
   * Balance of type "merchant-depositable" (see balance.ts for definition).
   */
  balanceMerchantDepositable: AmountString;
  /**
   * If the payment would succeed without fees
   * (i.e. balanceMerchantDepositable >= amountRequested),
   * this field contains an estimate of the amount that would additionally
   * be required to cover the fees.
   *
   * It is not possible to give an exact value here, since it depends
   * on the coin selection for the amount that would be additionally withdrawn.
   */
  feeGapEstimate: AmountString;
}
```
```typescript
export interface PreparePayResultAlreadyConfirmed {
  status: PreparePayResultType.AlreadyConfirmed;
  transactionId: TransactionIdStr;
  contractTerms: MerchantContractTerms;
  paid: boolean;
  amountRaw: AmountString;
  amountEffective: AmountString | undefined;
  contractTermsHash: string;
  /**
   * @deprecated use transactionId
   */
  proposalId: string;
  talerUri: string;
}
```
```typescript
/**
 * Payment is possible.
 */
export interface PreparePayResultPaymentPossible {
  status: PreparePayResultType.PaymentPossible;
  transactionId: TransactionIdStr;
  /**
   * @deprecated use transactionId instead
   */
  proposalId: string;
  contractTerms: MerchantContractTerms;
  contractTermsHash: string;
  amountRaw: AmountString;
  amountEffective: AmountString;
  talerUri: string;
}
```
```typescript
/**
 * Forced coin selection for deposits/payments.
 */
export interface ForcedCoinSel {
  coins: {
    value: AmountString;
    contribution: AmountString;
  }[];
}
```
```typescript
export interface AddExchangeRequest {
  exchangeBaseUrl: string;
  masterPub?: string;
  forceUpdate?: boolean;
}
```
```typescript
export interface BackupRecovery {
  walletRootPriv: string;
  providers: {
    name: string;
    url: string;
  }[];
}
```
```typescript
/**
 * Contract terms between two wallets (as opposed to a merchant and wallet).
 */
export interface PeerContractTerms {
  amount: AmountString;
  summary: string;
  purse_expiration: TalerProtocolTimestamp;
}
```
```typescript
export interface IntegrationTestArgs {
  exchangeBaseUrl: string;
  corebankApiBaseUrl: string;
  merchantBaseUrl: string;
  merchantAuthToken?: string;
  amountToWithdraw: AmountString;
  amountToSpend: AmountString;
}
```
