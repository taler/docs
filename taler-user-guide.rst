..
  This file is part of GNU TALER.
  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Christian Grothoff


GNU Taler User Guide
####################

Introduction
============

About GNU Taler
---------------

.. include:: frags/about-taler.rst


About this guide
----------------

This guide explains various ways how users can interact with a GNU Taler
installation. It assumes that a bank, exchange and merchant backend are
running and that the user has a GNU Taler wallet installed is able to create
an account at the bank.  Some operations also require access to the merchant
backend.


Withdrawing
===========

Withdrawing is the step where money is moved from a bank account into
a GNU Taler wallet.  There are two main ways to do this.

Bank integrated withdrawal
--------------------------

If the bank supports it, you can withdraw money into your GNU Taler wallet
directly from your banking app or online banking website. If supported, there
should be an option in your online banking to withdraw money to a GNU Taler
wallet. The bank should ask for the amount and then generate a QR code that
you need to scan with your GNU Taler wallet. Alternatively, if the
WebExtension wallet is installed, the bank website may directly switch to the
GNU Taler wallet.  In the GNU Taler wallet, you may have to first confirm the
terms of service of the selected GNU Taler exchange. Afterwards, applicable
fees will be shown and you will be given the option to accept the withdrawal.
Next, you need to authorize the withdraw operation in the bank.  If possible,
the GNU Taler wallet may automatically switch to the bank's website, it is
also possible that you have to go back to the banking app explicitly.  After
authorizing the withdraw operation, you will have to wait a bit for the money
to be wired to the exchange. Depending on the banking system, this can take
anywhere from a few seconds to many hours.  Afterwards, the money will show up
in your wallet.

Wallet initiated withdrawal
---------------------------

In this case, you will start the withdraw process from the GNU Taler wallet.
Under "Settings", you will find a list of exchanges. If the list is empty or
does not contain the desired exchange, you may have to first add the exchange
by providing the respective URL.

Next to the exchange, there is a drop-down menu with an option to "withdraw".
(If you already have money in your wallet, you will also find the same button
when viewing the transaction history of the respective currency.)  The wallet
will ask you to enter the amount to withdraw and accept the terms of service
and to pay the applicable fees (if any).  Afterwards, the wallet will give you
wire instructions, telling you which amount to wire to which bank account.
Most importantly, the wallet will give you a wire transfer subject that must
be specified for the wire transfer. If you make a typo in the subject, the
wallet will not be topped up and the exchange will send the money back to your
bank account eventually (possibly minus a fee).  Simply make the wire transfer
as instructed by the wallet.  Once the money has arrived at the exchange, the
wallet will automatically withdraw the funds.


Depositing
==========

If you have money in your wallet, you can use the "deposit" button to deposit
the funds into a bank account. The wallet will ask you to specify the amount
and the target bank account.


Sending cash
============

Once you have digital cash, you can send it to another GNU Taler
wallet. Simply specify the amount and a human-readable reason for the
transfer. The wallet will then show a QR code (and give the option to export
the payment as a taler://-URL).  Send the image of the QR code to the
receiving wallet (or send the taler://-URL securely to the target wallet).

The target wallet should scan the QR code (or enter the text of the
taler://-URL into the URL import dialog which is available by holding or
clicking the QR code scan button).  Afterwards, review the reason text and
accept the funds to complete the transaction.


Invoicing
=========

To receive funds from another user, you can send an invoice to another GNU
Taler wallet. Simply specify the amount and a human-readable reason for the
invoice. The wallet will then show a QR code (and give the option to export
the invoice as a taler://-URL).  Send the image of the QR code to the payer
wallet (or send the taler://-URL to the target wallet).

The target wallet should scan the QR code (or enter the text of the
taler://-URL into the URL import dialog which is available by holding or
clicking the QR code scan button).  Afterwards, review the reason for
the invoice and decide whether or not to pay the invoice.  Selecting
"pay" will complete the transaction.

Depending on the configuration of the exchange, the receiving wallet may have
to undergo some KYC check before the funds are actually released to the
receiver.


.. index:: instance
.. _Instance-account-configuration:

Configuring Accounts at a Merchant Instance
===========================================

Before you can setup a merchant instance, you need somebody to operate a
`Taler Merchant Backend <taler-merchant-backend-operator-manual>`_ and `grant
you access to an instance <Instance-setup>`_ at that backend.  For this, you
should receive the base URL of the instance and an access token.

The main configuration data that must be provided for each instance
is the bank account information.

In order to receive payments, the merchant backend needs to
communicate bank account details to the exchange.

The bank account information is provided in the form of a ``payto://``-URI.
See `RFC 8905 <https://tools.ietf.org/html/rfc8905>`_
for the format of ``payto://``-URIs.  Note that the "receiver-name" is
optional in RFC 8905 but mandatory in GNU Taler.

For first tests, you may want to sign up for a KUDOS bank account at
`https://bank.demo.taler.net/ <https://bank.demo.taler.net/>`_.  In this case,
the ``payto://``-URI will be of the form
``payto://iban/$IBAN?receiver-name=$NAME`` where ``$IBAN`` must be replaced
with the IBAN shown on the main page of the account shown at
`https://bank.demo.taler.net/ <https://bank.demo.taler.net/>`_ after logging
in.

When deploying Taler with the real banking system, you primarily need to
change the currency of the configuration from KUDOS to the actual currency
(such as EUR, USD, CHF) and provide a ``payto://``-URI of your actual bank
account. In Europe, this will involve knowing your IBAN number. If you have an
IBAN, the corresponding ``payto://``-URI is simply
``payto://iban/$IBAN?receiver-name=$NAME`` where ``$IBAN`` must be replaced
with the actual IBAN number and ``$NAME`` with your actual name. Make sure to
URI-encode your name.  The merchant SPA will do this automatically when you
use it to configure the bank account.



Using the Point-of-Sale App
===========================

A simple way for merchants to accept GNU Taler payments is the use of the
point-of-sale app. The app can be installed on an Android phone or tablet
and is configured via a simple JSON file on a Web site:

  * In the app settings you need to specify the URL of the Web site where
    the app can download the categories, products and prices from which
    orders are to be compiled. You can optionally specify a username and
    password to authenticate to the Web server.

  * The syntax of the JSON file is described in the point-of-sale app
    manual. However, you may simply want to download the sample JSON
    file from our documentation and use it as a starting point.

  * A key option is the merchant backend with the authorization key
    which must be included in this JSON configuration. You may point
    the point-of-sale app to any instance of a merchant backend.

Once configured, the point-of-sale app allows the user to select a product
category and then to quickly add products from that category to an order.  You
can easily edit the order, and finally use the "complete" button to generate a
QR code. The QR code must then be scanned by the GNU Taler wallet to initiate
the payment.  Multiple orders can be entered concurrently, for example in a
restaurant where multiple tables are waited on at the same time.


Setting up an order in the merchant backoffice SPA
==================================================

Arbitrary orders can also be created manually using the Web interface of
the GNU Taler merchant backend. For this, log into the single page app (SPA)
of the merchant backend using the authorization token of the respective
instance you want to use.

You can then set up orders by providing all of the required fields of an
order, in particular an order summary and a price.  You can also set various
optional fields or override instance default settings.

When the order has been setup, you can follow a link to the payment page
which will show the QR code (and/or URL) that a GNU Taler wallet would need
to receive to initiate the payment process.  The order status page also
shows you the progress of the order, including when a wallet has made the
payment. You can also use the backend to approve refunds.


Paying an order
===============

The payer simply scans the (dynamic) QR code to initiate the payment.  If a
website is interacting with a WebExtension wallet, it may also directly
trigger the GNU Taler wallet without requiring the user to explicitly scan the
QR code.  The payer should now review the contract terms and applicable fees.
Selecting "pay" will complete the transaction.  Typically, the wallet will
then redirect the user to the fulfillment page where they can track the order
or directly view the digital product that they purchased.


Setting up a template
=====================

A template provides all or part of the information needed to setup an order
and allows GNU Taler wallets to create an order.  Usually, the creation of
orders is a privileged process that requires knowledge of the authorization
code for the respective instance. With templates, a customer's wallet can
directly create an order on-demand.  The information of a template can be
partial, in which case the customer is expected to provide the remaining
details, typically the summary and/or amount of the order.

When setting up a template you need to specify all of the fixed inputs that
the customer cannot change.  You can then generate a template QR code where
you may additionally specify editable defaults for the order, such as a
default summary or a default amount which may still be changed by the wallet.
The resulting template QR code encodes the specific merchant backend, instance
and template ID as well as the (editable) default values.  The resulting
static QR code can then be printed and put on display.

Customers can scan the QR code with their GNU Taler wallet, complete the
missing details or edit the defaults (if any), and pay the resulting order.

To secure template-based payments, you may specify a TOTP secret as part of
the template. In this case, the merchant backend will send a set of TOTP
payment confirmation codes to the GNU Taler wallet upon receiving a payment
for an order created based on the template.  If the point-of-sale has a TOTP
generator with the same secret, they can compare their TOTP code with the
codes shown by the customer on their wallet. This provides additional
assurance that the customer actually made the payment instead of just showing
a fake confirmation screen.


Paying with static QR codes
===========================

The payer simply scans the (static) QR code to initiate the payment.  If the
template does not specify a fixed amount, the payer will be prompted to enter
the amount to be paid (and possibly given the opportunity to specify or alter
the summary).  Selecting "pay" will complete the transaction.  If payment
confirmations are configured by the merchant backend, the wallet will then
display a confirmation code that can be shown to the merchant as a proof of
payment.




Setting up a webhook
====================

To receive notifications when a purchase has been made or a refund was given
to a wallet, you can set up webhooks in the GNU Taler merchant backend.
Webhooks allow you to trigger HTTP(S) requests based on certain events.  A
webhook is thus simply an HTTP request that the GNU Taler merchant backend
will make when a certain event (such as a payment) happens.

There are various providers that can send an SMS to a phone number based on an
HTTP request.  Thus, by configuring such a provider in a webhook you can
receive an SMS notification whenever a customer makes a payment.

Webhooks are configured per instance.  In the Webhook configuration,
you can specify which URL, which HTTP headers, which HTTP method and what HTTP
body to send to the Webhook.  Webhooks are automatically retried (with
increasing delays) when the target server returns a temporary error.

`Mustach templates <https://mustache.github.io/mustache.5.html>`__ are used
when defining the contents of Webhooks.  Depending on the triggering event,
the templates will be expanded with event-specific data.


Pay events
----------

For "pay" events, the backend will provide the following
information to the Mustach templating engine:

  * :ref:`contract_terms <contract-terms>`: the contract terms of the paid order
  * order_id: the ID of the order that received the refund


Refund events
-------------

For "refund" events, the backend will provide the following information to the
Mustach templating engine:

  * timestamp: time of the refund (in nanoseconds since 1970)
  * order_id: the ID of the order that received the refund
  * :ref:`contract_terms <contract-terms>`: the full JSON of the contract terms of the refunded order
  * refund_amout: the amount that was being refunded
  * reason: the reason entered by the merchant staff for granting the refund;
    be careful, you probably want to inform your staff if a webhook may expose
    this information to the consumer


.. _Rewarding-visitors:

Rewarding visitors
==================

.. index:: rewards

Taler can also be used to reward digital cash to Web site visitors. For
example, you may be running an online survey, and you want to reward those
people that have dutifully completed the survey. If they have installed a
Taler wallet, you can provide them with a reward for their deeds. This section
describes how to setup the Taler merchant backend for rewarding.

There are three basic steps that must happen to reward a visitor.

.. _Fund-the-reserve:

Fund the reserve
----------------

.. index:: reserve

First, the reserve must be setup in the merchant backend. A reserve
is always tied to a particular instance. To create a reserve with
10 KUDOS at instance ``default`` using the demo exchange, use:

.. code-block:: console

   $ taler-merchant-setup-reserve \
       -a KUDOS:10 \
       -e https://exchange.demo.taler.net/ \
       -m http://localhost:8888/instances/default

The above command assumes that the merchant runs on localhost on
port 8888.
For more information, including how to transmit authentication information
to the backend, see :doc:`manpages/taler-merchant-setup-reserve.1`.

The command will output a ``payto://`` URI which specifies where to
wire the funds and which wire transfer subject to use.

  .. note::

     FIXME: add full example output.

In our example, the output for the wire transfer subject is:

.. code-block:: none

   QPE24X8PBX3BZ6E7GQ5VAVHV32FWTTCADR0TRQ183MSSJD2CHNEG

You now need to make a wire transfer to the exchange’s bank account
using the given wire transfer subject.

Make your wire transfer and (optionally) check at
“https://exchange/reserves/QPE24X...” whether your transfer has arrived at the
exchange.

Once the funds have arrived, you can start to use the reserve for
rewarding.

Note that an exchange will typically close a reserve after four weeks, wiring
all remaining funds back to the sender’s account. Thus, you should plan to
wire funds corresponding to a campaign of about two weeks to the exchange
initially. If your campaign runs longer, you should setup another reserve
every other week to ensure one is always ready.

.. _Authorize-a-reward:

Authorize a reward
------------------

When your frontend has reached the point where a client is supposed to receive
a reward, it needs to first authorize the reward. For this, the frontend must use
a POST to ``/private/reserves/$RESERVE_PUB/authorize-reward``. To authorize a
reward, the frontend has to provide the following information in the body of the
POST request:

-  The amount of the reward

-  The justification (only used internally for the back-office)

-  The URL where the wallet should navigate next after the reward was
   processed

-  The reward-pickup URL (see next section)

In response to this request, the backend will return a reward token, an
expiration time and the exchange URL. The expiration time will indicate
how long the reward is valid (when the reserve expires). The reward token is
an opaque string that contains all the information needed by the wallet
to process the reward. The frontend must send this reward token to the browser
in a special “402 Payment Required” response inside the ``Taler``
header.

The frontend should handle errors returned by the backend, such as
misconfigured instances or a lack of remaining funds for rewarding.

.. _Picking-up-of-the-reward:

Picking up of the reward
------------------------

The wallet will POST a JSON object to the shop’s
``/rewards/$REWARD_ID/pickup`` handler.
The frontend must then forward this request to the backend. The response
generated by the backend can then be forwarded directly to the wallet.
