taler-aggregator-benchmark(1)
#############################


.. only:: html

   Name
   ====

   **taler-aggregator-benchmark** - setup database to measure aggregator performance


Synopsis
========

**taler-aggregator-benchmark**
[**-c** *CONFIG_FILENAME* | **--config=**\ ‌\ *CONFIG_FILENAME*]
[**-d** *DN* | **--deposits=**\ \ *DN*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--log-level=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-m** *DM* | **--merchants=**\ ‌\ *DM*]
[**-r** *RATE* | **--refunds=**\ \ *RATE*]
[**-v** | **--version**]

Description
===========

**taler-aggregator-benchmark** is a command-line tool to fill an exchange
database with records suitable for benchmarking the
**taler-exchange-aggregator**.  The **taler-aggregator-benchmark** tool does
not run the actual workload for the benchmark (which usually consists of
starting multiple **taler-exchange-aggregator** processes) and instead only
prepares the database with synthetic work.

**-c** *CONFIG_FILENAME* \| **--config=**\ ‌\ *CONFIG_FILENAME*
   (Mandatory) Use CONFIG_FILENAME as the name for the configuration file.

**-d** *DN* \| **--deposits=**\ ‌\ *DN*
   How many deposits should be instantiated *per merchant*.
   Defaults to 1.

**-h** \| **--help**
   Prints a compiled-in help text.

**-L** *LOGLEVEL* \| **--log-level=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-m** *DM* \| **--merchants=**\ ‌\ *DM*
   How many different merchants should we create. Defaults to 1.

**-r** *RATE* \| **--refunds=**\ \ *RATE*
   Probability of a deposit having a refund (as an integer between 0-100).

**-v** \| **--version**
   Print version information.

See Also
========

taler-exchange-dbinit(1), taler-merchant-benchmark(1),
taler-exchange-aggregator(1), taler-unified-setup(1), taler.conf(5)

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
