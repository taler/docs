sync.conf(5)
############

.. only:: html

   Name
   ====

   **sync.conf** - Sync configuration file


Description
===========

.. include:: ../frags/common-conf-syntax.rst

Files containing default values for many of the options described below
are installed under ``$PREFIX/share/sync/config.d/``.
The configuration file given with **-c** to Sync binaries
overrides these defaults.

A configuration file may include another, by using the ``@INLINE@`` directive,
for example, in ``main.conf``, you could write ``@INLINE@ sub.conf`` to
include the entirety of ``sub.conf`` at that point in ``main.conf``.

Be extra careful when using ``sync-config -V VALUE`` to change configuration
values: it will destroy all uses of ``@INLINE@`` and furthermore remove all
comments from the configuration file!


GLOBAL OPTIONS
--------------

The following options are from the “[sync]” section.
This is normally the only section in a sync.conf file.

SERVE
  This can either be ``tcp`` or ``unix``.

PORT
  Port on which the HTTP server listens, e.g. 9967.
  Only used if ``SERVE`` is ``tcp``.

BIND_TO
  Which IP address should we bind to?  E.g. ``127.0.0.1`` or ``::1``
  for loopback.  Can also be given as a hostname.  We will bind to
  the wildcard (dual-stack) if left empty.
  Only used if ``SERVE`` is ``tcp``.

UNIXPATH
  Which unix domain path should we bind to?
  Only used if ``SERVE`` is ``unix``.

UNIXPATH_MODE = 660
  What should be the file access permissions for ``UNIXPATH``?
  Only used if ``SERVE`` is ``unix``.

DB
  Plugin to use for the database, e.g. “postgres”.

ANNUAL_FEE
  Annual fee for an account.
  This is in the usual amount syntax, e.g. ``TESTKUDOS:0.1``.

INSURANCE
  Insurance provided against loss, e.g. ``TESTKUDOS:0.0``.

UPLOAD_LIMIT_MB
  Upload limit per backup, in megabytes, e.g. ``16``.

FULFILLMENT_URL
  Fulfillment URL of the SYNC service itself.

PAYMENT_BACKEND_URL
  Base URL of our payment backend.

API_KEY
  API key to pass when accessing the merchant backend.
  This is a secret value.


SEE ALSO
========

sync-dbinit(1), sync-httpd(1), sync-config(1).


BUGS
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
