taler-helper-auditor-purses(1)
##############################

.. only:: html

   Name
   ====

   **taler-helper-auditor-purses** - Audit Taler exchange purse handling


Synopsis
========

**taler-helper-auditor-purses**
[**-c** *FILENAME* | **--config=**\ \ *FILENAME*]
[**-h** | **--help**]
[**i** | **--internal**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-m** *KEY* | **--exchange-key=**\ \ *KEY*]
[**-T** *USEC* | **--timetravel=**\ \ *USEC*]
[**-v** | **--version**]


Description
===========

**taler-helper-auditor-purses** is a command-line tool to
audit Taler exchange purse handling.

FIXME: More detail.

Its options are as follows:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the auditor to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-i** \| **--internal**
   Perform checks only applicable for exchange-internal audits.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-m** *KEY* \| **--exchange-key=**\ \ *KEY*
   Use *KEY* (Crockford base32 encoded) as the public key of the exchange.

**-T** *USEC* \| **--timetravel=**\ \ *USEC*
   Modify the system time by *USEC* microseconds.
   *USEC* may be prefixed with ``+`` or ``-`` (e.g. ``-T +300``).
   This option is intended for debugging/testing only.

**-v** \| **--version**
   Print version information.


See Also
========

taler-auditor(1), taler.conf(5).


Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
