taler-exchange-kyc-tester(1)
############################

.. only:: html

   Name
   ====

   **taler-exchange-kyc-tester** - tool to test interaction with KYC provider

Synopsis
========

**taler-exchange-kyc-tester**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-i** *SECTION_NAME* | **--initiate=**\ ‌\ *SECTION_NAME*]
[**-u** *ID* | **--user=**\ ‌\ *ID*]
[**-U** *ID* | **--legitimization=**\ ‌\ *ID*]
[**-P** | **--print-payto-hash**]
[**-p** *HASH* | **--payto-hash=**\ ‌\ *HASH*]
[**-r** *NUMBER* | **--rowid=**\ ‌\ *NUMBER*]
[**-v** | **--version**]
[**-w** | **--run-webservice**]


Description
===========

**taler-exchange-kyc-tester** is used to test the interaction between a Taler exchange and a KYC service. The tool can be used to manually trigger the various steps of a KYC process and to observe the interaction with the respective KYC service. It is supposted to help test the configuration of the integration, and *not* required at all during production.

To use it, you must first provide a configuration file with at least one KYC service configured. Some other exchange-specific options, like the PORT for the HTTP service and the BASE_URL under which the Taler exchange will run are also required. You should be able to use exactly the same configuration file that one would usually give to a Taler exchange.  Starting with this, the tool allows the simulation of a KYC process. Note that it will not write any information to the database.

Begin with a first invocation of taler-exchange-kyc-tester using the options **-i** for an individual or business and use **-R** to specify a list of checks required from the process. The output will be an URL to visit with the browser, as well as **-p**, **-u**, **-U** options to use in future invocations of the tool.

Next, run taler-exchange-kyc-tester again, but this time using **-w** (to run the Webserver) and using the **-u** and **-U** options output by the previous call, as well as the **-p** option with the payto hash.  Then visit the Web site from the link output by the previous invocation and "pass" (or "fail") the KYC check.


**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the exchange to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-i** *USERTYPE* \| **--initiate=**\ ‌\ *USERTYPE*
   Specifies the type of user for which we are starting a fresh KYC process. USERTYPE must be either "individual" or "business".

**-u** *ID* \| **--user=**\ ‌\ *ID*
   Run the process with ID for the user identifier at the KYC provider. Not useful in conjunction with **-i** and **-R** as that option will override whatever value is provided here.

**-U** *ID* \| **--legitimization=**\ ‌\ *ID*
   Run the process with ID for the legitimization process identifier at the KYC provider. Not useful in conjunction with **-R** / **-i** as that option will override whatever value is provided here.

**-p** *HASH* \| **--payto-hash=**\ ‌\ *HASH*
   Run the process with HASH as the hash of the payto://-URI that identifies the account or wallet triggering the KYC requirement. If not given, a fresh random value is used. Rarely useful.

**-P** \| **--print-payto-hash**
   Print the HASH of the payto://-URI used for the KYC simulation this time. Useful if the hash is needed for a subsequent use in conjunction with **-p**.

**-r** *NUMBER* \| **--rowid=**\ ‌\ *NUMBER*
   Run the process with NUMBER as the database row for the legitimization operation. Rarely useful, except maybe for debugging. Defaults to 42.

**-R** *CHECKS* \| **--requirements=**\ ‌\ *CHECKS*
   Start a fresh KYC process for the given list of CHECKS. CHECKS must be a space-separated list of checks that must be in the configuration under *PROVIDED_CHECKS* for some of the providers. The exchange will determine which provider to use for KYC based on the CHECKS given.  The tool will output the HTTP URL where the user has to begin the KYC process to the command-line. This is usually the first thing to do when using this tool. Outputs the KYC-logic specific user and legitimization IDs, or NULL if not used by the KYC-logic at the initiation stage.  You may want to use the **-P** option to also obtain the Payto-Hash for use with **p** later.


**-v** \| **--version**
   Print version information.

**-w** \| **--run-webservice**
   Run a simulated Taler exchange HTTP service on the configured port with the ``/kyc-proof/`` and ``/kyc-webhook/`` endpoints.


See Also
========

taler-exchange-httpd(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
