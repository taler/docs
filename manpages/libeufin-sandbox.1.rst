libeufin-sandbox(1)
###################

.. only:: html

   Name
   ====

   **libeufin-sandbox** - Simulate a banking system core
   with EBICS access to bank accounts


Synopsis
========

**libeufin-sandbox**
[**-h** | **--help**]
[**--version**]
COMMAND [ARGS...]

Commands: serve, reset-tables, config, make-transaction, camt053tick
default-exchange


Description
===========

**libeufin-sandbox** is a program to simulate a banking system core
with EBICS access to bank accounts.
It maintains state in its own private database.
You interact with it through HTTP
requests either over the network or via a Unix domain socket.
Related program **libeufin-cli** is the preferred front end
for that mode of operation.
There is also a mode where **libeufin-sandbox** accepts commands directly,
useful for configuring one or more "demobank" (simulated bank) instances.

Its options are as follows:

**-h** \| **--help**
   Print short help on options.

**–version**
   Print version information.

The interaction model is as follows:

.. @MS Is the order of the first two steps correct?
   Or are some of the commands to be used AFTER ‘serve’ starts?
   Or is it a mix?  (I believe it is a mix, but am not sure.)

- Configure the sandbox with commands ``config``, ``default-exchange``,
  ``make-transaction``, and ``camt053tick``.

- Start the HTTP server with command ``serve``.
  Let this run in a shell, writing logs to stderr.

- Point program **libeufin-nexus** at the sandbox.

- Interact with **libeufin-nexus**.

- When finished, interrupt the ``serve`` process and clean up with command
  ``reset-tables``.

The following sections describe each command in detail.


config
------

This command takes argument ``NAME`` and creates a demobank with that name.

Option ``--currency CUR`` (default: ``EUR``) specifes another currency.
Option ``--captcha-url $URL`` specifies where the wallet user is going
to be redirected to confirm the withdrawal operation. This $URL should 
point to the bank frontend. More precisely to the UI that let the user
finish a withdrawal operation that needs to be confirmed. Example of
this value may be "https://bank.domain/#/operation/{wopid}" where 
"https://bank.domain" returns the demobank SPA and the demobank view under
the route "/operation/{wopid}" will show the status of the operation id {wopid}.
Note that "{wopid}" is literally in the --captcha-url config and replaced at
runtime by the sandbox server. 
Option ``--bank-debt-limit N`` (default: 1000000) specifies that
the bank debt limit should be N (units of currency).
Similarly, option ``--users-debt-limit N`` (default: 1000) specifies
that the users debt limit should be N (units of currency).

For example:

.. code-block:: console

  $ libeufin-sandbox config default

This creates the demobank ``default`` with currency ``EUR``,
bank debt limit 1000000, users debt limit 1000,
and allows registrations.


default-exchange
----------------

This command sets the exchange that a demobank will suggest to wallets.
(Wallets are of course free to disregard the suggestion and choose
another exchange.)
It requires two arguments, ``EXCHANGE-BASEURL`` and ``EXCHANGE-PAYTO``.
The option ``--demobank NAME`` (default: ``default``) specifies
which demobank this setting affects.

For example:

.. code-block:: console

  $ libeufin-sandbox default-exchange \
      --demobank bank01 \
      https://exchange.example.com/ \
      payto://iban/CH9300762011623852957

This sets the default exchange for demobank ``bank01``.
It is an error if the demobank does not exist.


make-transaction
----------------

This is a "legacy" command, useful in a previous iteration of LibEuFin
and for internal testing.
It creates and records a wire transfer transaction in the database.

It takes two arguments and several required options.
The arguments are ``AMOUNT``, in ``CUR:X.Y`` format;
and ``SUBJECT``, a short textual description of the transaction.
The options are: ``--credit-account LABEL`` and ``--debit-account LABEL``,
where each LABEL names a bank account for receiving and issuing,
respectively, the wire transfer.
The option ``--demobank NAME`` (default: ``default``) specifies
in which demobank the wire transfer occurs.

.. note::

   If you have not yet called ``config``, this command creates
   a demobank named ``default`` on its first use.  The currency,
   and bank debt limit have the same defaults as for the ``config``
   command.  The users debt limit, however, defaults to 10000.

FIXME: How to achieve the same result with **libeufin-cli**?


camt053tick
-----------

This command advances the internal time step that the demobank
uses to group transactions for reporting.
(Successive transactions will be inserted in a new Camt.053 report.)

For example:

.. code-block:: console

  $ libeufin-sandbox camt053tick

FIXME: How to achieve the same result with **libeufin-cli**?


serve
-----

This command starts the HTTP server, listening on port 5000.
To use a different port, use option ``--port INT``.
To listen, instead, on a Unix domain socket,
use option ``--with-unix-socket PATH``.
When both ``--port`` and ``--with-unix-socket`` are given,
``--with-unix-socket`` takes precedence.

.. note::

   If you have not yet called ``config``, this command creates
   a demobank named ``default`` on its first use.  The currency,
   and bank debt limit have the same defaults as for the ``config``
   command.  The users debt limit, however, defaults to 10000.

The process runs in the foreground, writing its logs to standard error.
The normal log level is ``DEBUG``.
To change it, use ``--log-level LEVEL``, where ``LEVEL`` is one of:
``ERROR``, ``WARN``, ``INFO``, ``DEBUG``, ``TRACE``.

Before invoking ``serve``, the following environment variables need to be set:

``LIBEUFIN_SANDBOX_ADMIN_PASSWORD``
   The password required for later use by **libeufin-cli**.
   For testing purposes, you can use option ``--no-auth`` to disable
   this requirement.
   (In that case, this environment variable need not be set.)

``LIBEUFIN_SANDBOX_DB_CONNECTION``
   This specifies the database **libeufin-sandbox** uses to maintain state.
   Currently, both Sqlite and PostgreSQL are supported.
   (Only one needs to be specified.)
   Examples:

   - ``jdbc:sqlite:/tmp/libeufin-sandbox.db``
   - ``jdbc:postgresql://localhost:5432/libeufindb?user=Foo&password=secret``

Normally, the ``serve`` command runs until interrupted.
When run in a shell, you can use ``Control-C`` for that.


reset-tables
------------

This command drops all the tables in the internal database.
(The next time the tables are needed, **libeufin-sandbox** creates them
again, automatically.)

It should only be used when the sandbox is quiescent.


See Also
========

.. TODO: libeufin-nexus(1), libeufin-cli(1).


Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
