challenger.conf(5)
##################

.. only:: html

   Name
   ====

   **challenger.conf** - Challenger configuration file


Description
===========

.. include:: ../frags/common-conf-syntax.rst

Files containing default values for many of the options described below
are installed under ``$PREFIX/share/challenger/config.d/``.
The configuration file given with **-c** to Challenger binaries
overrides these defaults.

A configuration file may include another, by using the ``@INLINE@`` directive,
for example, in ``main.conf``, you could write ``@INLINE@ sub.conf`` to
include the entirety of ``sub.conf`` at that point in ``main.conf``.

Be extra careful when using ``challenger-config -V VALUE`` to change configuration
values: it will destroy all uses of ``@INLINE@`` and furthermore remove all
comments from the configuration file!


GLOBAL OPTIONS
--------------

The following options are from the “[challenger]” section.
This is normally the only section in a challenger.conf file.

SERVE
  This can either be ``tcp`` or ``unix``.

PORT
  Port on which the HTTP server listens, e.g. 9967.
  Only used if ``SERVE`` is ``tcp``.

BIND_TO
  Which IP address should we bind to?  E.g. ``127.0.0.1`` or ``::1``
  for loopback.  Can also be given as a hostname.  We will bind to
  the wildcard (dual-stack) if left empty.
  Only used if ``SERVE`` is ``tcp``.

UNIXPATH
  Which unix domain path should we bind to?
  Only used if ``SERVE`` is ``unix``.

UNIXPATH_MODE = 660
  What should be the file access permissions for ``UNIXPATH``?
  Only used if ``SERVE`` is ``unix``.

DB
  Plugin to use for the database, e.g. “postgres”.

VALIDATION_DURATION
  How long is a validation challenge valid. After this time period, a fresh random challenge code will be generated and the retry limit counter (against guessing attacks) will be reset (to 3).

VALIDATION_EXPIRATION
  How long is a validation allowed to take (time from
  ``/setup`` to ``/token``). After this time, the garbage collection process can delete all associated data. (Note that tokens will always allow access to 1h after they have been issued, regardless of when the validation expires).

AUTH_COMMAND
  Which command should we execute to transmit the challenge code to the address. The address is given as the first argument, while the message to send is provided on stdin. Templates (possibly without the necessary credentials) for such commands are provided as challenger-send-email.sh, challenger-send-post.sh and challenger-send-sms.sh.

ADDRESS_TYPE
  Type of the address that is being collected, returned as part of the ``address_type`` in the ``/info`` endpoint. Examples include ``email`` or ``phone``.



SEE ALSO
========

challenger-dbinit(1), challenger-httpd(1), challenger-config(1).


BUGS
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
