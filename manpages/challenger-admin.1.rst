challenger-admin(1)
###################

.. only:: html

   Name
   ====

   **challenger-admin** - manipulate clients registered in Challenger database


Synopsis
========

**challenger-admin**
[**-a** *CLIENT_SECRET* | **--add=**\ \ *CLIENT_SECRET*]
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-d** | **--delete**]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--log=**\ \ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ \ *FILENAME*]
[**-q** | **--quiet**]
[**-v** | **--version**]
CLIENT_URL


Description
===========

**challenger-admin** is a command-line tool to add or delete clients from the Challenger database.

Its options are as follows:

**-a** *SECRET* \| **--add=**\ ‌\ *SECRET*
   Add the client with the given *CLIENT_URL setting the client secret to *SECRET*.  Prints the CLIENT_ID of the added client.

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the Challenger commands
   to operate from *FILENAME*.

**-d** \| **--delete**
   Delete the client with the given *CLIENT_URL*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--log=**\ \ *LOGLEVEL*
   Configure logging to use *LOGLEVEL*.

**-l** *FILENAME* \| **--logfile=**\ \ *FILENAME*
   Configure logging to write logs to *FILENAME*.

**-q** \| **–-quiet**
   Be less verbose in the output. Useful in shell scripts.

**-v** \| **–-version**
   Print version information.


See Also
========

challenger-config(1), challenger-httpd(1), challenger.conf(5).


Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
