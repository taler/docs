taler-exchange-expire(1)
#########################

.. only:: html

   Name
   ====

   **taler-exchange-expire** - refund expired purses

Synopsis
========

**taler-exchange-expire**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-T** *USEC* | **--timetravel=**\ \ *USEC*]
[**-t** | **--test**]
[**-v** | **--version**]

Description
===========

**taler-exchange-expire** is a command-line tool to run refund
money in purses that were not merged before their expiration time.
This allows the wallet to recover the funds deposited into the
purse using a refresh operation.


**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the exchange to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-T** *USEC* \| **--timetravel=**\ \ *USEC*
   Modify the system time by *USEC* microseconds.
   *USEC* may be prefixed with ``+`` or ``-`` (e.g. ``-T +300``).
   This option is intended for debugging/testing only.

**-t** \| **--test**
   Run in test mode and exit when idle.

**-v** \| **--version**
   Print version information.

See Also
========

taler-exchange-router(1), taler-exchange-httpd(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
