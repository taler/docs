challenger-httpd(1)
###################

.. only:: html

   Name
   ====

   **challenger-httpd** - provide the Challenger HTTP interface


Synopsis
========

**challenger-httpd**
[**-C** | **--connection-close**]
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--log=**\ \ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ \ *FILENAME*]
[**-v** | **--version**]


Description
===========

**challenger-httpd** is a command-line tool to provide the Challenger HTTP interface.

Its options are as follows:

**-C** \| **--connection-close**
   Force HTTP connections to be closed after each request.

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the Challenger commands
   to operate from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--log=**\ \ *LOGLEVEL*
   Configure logging to use *LOGLEVEL*.

**-l** *FILENAME* \| **--logfile=**\ \ *FILENAME*
   Configure logging to write logs to *FILENAME*.

**-v** \| **–version**
   Print version information.


See Also
========

challenger-config(1), challenger-dbinit(1), challenger.conf(5).


Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
