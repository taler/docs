libeufin-nexus(1)
#################

.. only:: html

   Name
   ====

   **libeufin-nexus** - EBICS client.


Synopsis
========

**libeufin-nexus**
[**-h** | **--help**]
[**--version**]
COMMAND [ARGS...]

Subcommands: **dbinit**, **ebics-setup**, **ebics-submit**, **ebics-fetch**


Description
===========

**libeufin-nexus** is a program that provides a service to interface to
various bank access APIs

Its options are as follows:

**-h** \| **--help**
   Print short help on options.

**–version**
   Print version information.

The interaction model is as follows:

In order to operate any EBICS communication with ``libeufin-nexus``, it is necessary to setup EBICS access via the ``ebics-setup`` subcommand.  Setting the access means to share the client keys with the bank and downloading the bank keys.  After a successful setup, the subcommands ``ebics-submit`` and ``ebics-fetch`` can be run to respectively send payments and download the bank account history.

The following sections describe each command in detail.

ebics-setup
-----------

This command creates the client keys, if they aren't found already on the disk, and sends them to the bank if they were not sent yet.  In case of sending, it ejects the PDF document that contains the keys fingerprints, so that the user can send it to the bank to confirm their keys.  The process continues by checking if the bank keys exist already on disk, and proceeds with downloading them in case they are not.  It checks then if the bank keys were accepted by the user; if yes, the setup terminates, otherwise it interactively asks the user to mark the keys as accepted.  By accepting the bank keys, the setup terminates successfully.

Its options are as follows:

**-h** \| **--help**
   Prints short help on options.
**-c** \| **--config** \ ‌\ *FILENAME*
   Specifies the configuration file.
**--check-full-config**
  Checks the configuration of all the three subcommands and returns.
**--force-keys-resubmission**
  Resubmits the client keys.  If no keys were found, it creates and submits them.
**--auto-accept-keys**
  Accepts the bank keys without interactively asking the user.
**--generate-registration-pdf**
  Generates the PDF with the client keys fingerprints, if the keys have the submitted state.  That's useful in case the PDF went lost after the first submission and the user needs a new PDF.


dbinit
------

This subcommand defines the database schema for Nexus.  It is mandatory to run this command before invoking the ``ebics-submit`` or ``ebics-fetch`` subcommands.

Its options are as follows:

**-h** \| **--help**
   Print short help on options.
**-c** \| **--config** \ ‌\ *FILENAME*
   Specifies the configuration file.
**-r** \| **--reset**
   If present, deletes any database table (WARNING: potential data loss)


ebics-submit
------------

This subcommand submits any initiated payment that was not already sent to the bank.  In the current version, initiated payments may come from a cash-out operation or from a bounced incoming payment.  ebics-submit is Taler friendly, therefore bounced payments are those that do not contain a valid subject to start a Taler withdrawal.  Cash-out operations come from a tightly integrated bank that offers their customers to convert their currency to the currency whose the EBICS subscriber bank account is tied to.

Its options are as follows:

**-h** \| **--help**
   Print short help on options.
**-c** \| **--config** \ ‌\ *FILENAME*
   Specifies the configuration file.
**--transient**
   This flag, enabled by default, causes the command to check the database and submit only once, and then return.
**--debug**
  With this flag, it is possible to pass via STDIN a raw pain.001 document to be submitted to the bank.  This method turns ebics-submit to run in transient mode and it does NOT affect the database.
**--ebics-extra-log**
  Enables (verbose) logging of EBICS 3 messages to STDERR.  It logs only EBICS 3 messages from the initialisation phase of an upload order.


ebics-fetch
-----------

This subcommand downloads banking records via EBICS and stores them into the database.  By default, it downloads ``camt.054`` notifications.  Along the download, ebics-fetch would bounce incoming payments that do not have a valid Taler subject, or as well those with an already existing valid subject.  Valid incoming payments are then stored in the database so that they can trigger Taler withdrawals.  Along this process, ebics-submit would as well reconcile initiated outgoing payments with any outgoing transactions that shows up in the downloaded records.  Any option starting with ``--only`` would only be useful with a defined log directory (STATEMENTS_LOG_DIRECTORY) in the configuration.

**-h** \| **--help**
   Print short help on options.
**-c** \| **--config** \ ‌\ *FILENAME*
   Specifies the configuration file.
**--transient**
   This flag, enabled by default, causes the command to perform one download and return.
**--only-statements**
   It downloads statements (instead of notifications) in the form of camt.053 documents.  It does NOT affect the database.
**--only-ack**
   It downloads payment submissions acknowledgements (instead of notifications) in the form of pain.002 documents.  It MAY affect the database by setting the state of submitted payments.
**--only-reports**
   It downloads only intraday reports (instead of notifications) in the form of camt.052 documents.  It does NOT affect the database.
**--only-logs**
   It downloads the subscriber activity via EBICS HAC in the form of pain.002 documents.  It does NOT affect the database.
**--pinned-start**
  Only supported in --transient mode, this option lets specify the earliest timestamp of the downloaded documents.  The latest timestamp is always the current time.
**--parse**
  Reads one ISO20022 document from STDIN.  It prints on the console the result and exits without affecting the database
**--import**
  Reads one ISO20022 document from STDIN.  It prints the result on the console **and** also ingests the input content into the database.  NOTE: if used in a debug scenario, consider to destroy the database before going to production for the following reasons; (1) the database likely ends up having phony transactions that do not belong to the bank account associated with the EBICS subscriber, and (2) whose timestamps will alter the way ebics-fetch will request new records to the bank.  (3) This flags skips the check on the bank keys, so the database could receive records even though hte bank keys were not accepted / downloaded.

SEE ALSO
========

libeufin-nexus.conf(5)

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic mail to <taler@gnu.org>.
