taler-exchange-httpd(1)
#######################

.. only:: html

   Name
   ====

   **taler-exchange-httpd** - run Taler exchange (with RESTful API)

Synopsis
========

**taler-exchange-httpd**
[**-a** | **--allow-timetravel**]
[**-C** | **--connection-close**]
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-f** *FILENAME* | **--file-input=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-n** *N* | **--num-threads=**\ \ *N*]
[**-r**|**--allow-reuse-address**]
[**-T** *USEC* | **--timetravel=**\ \ *USEC*]
[**-t** *SECONDS* | **--timeout=**\ ‌\ *SECONDS*]
[**-v** | **--version**]

Description
===========

**taler-exchange-httpd** is a command-line tool to run the Taler
exchange (HTTP server). The required configuration, keys and database
must exist before running this command.

Its options are as follows:

**-a** \| **--allow-timetravel**
   Allow clients to request /keys for arbitrary timestamps.
   This should only be enabled for testing and development,
   as clients could abuse this in denial of service attacks,
   as it makes the /keys response generation much more expensive.

**-C** \| **--connection-close**
   Force each HTTP connection to be closed after each request (useful in
   combination with **-f** to avoid having to wait for
   netcat (nc) to time out).

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the merchant to operate
   from FILENAME.

**-f** *FILENAME* \| **--file-input=**\ ‌\ *FILENAME*
   This option is only available if the exchange was compiled with the
   configure option --enable-developer-mode. It is used for generating
   test cases against the exchange using AFL. When this option is
   present, the HTTP server will

   1. terminate after the first client’s HTTP connection is completed,
      and
   2. automatically start such a client using a helper process based on
      the nc(1) or ncat(1) binary using FILENAME as the standard input
      to the helper process.

   As a result, the process will effectively run with *FILENAME* as the
   input from an HTTP client and then immediately exit. This is useful
   to test taler-exchange-httpd against many different possible inputs
   in a controlled way.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-r** \| **--allow-reuse-address**
   Allow the exchange to re-use the listen port even if another service
   is already using it. Useful if multiple processes are used to increase
   processing capacity.

**-T** *USEC* \| **--timetravel=**\ \ *USEC*
   Modify the system time by *USEC* microseconds.
   *USEC* may be prefixed with ``+`` or ``-`` (e.g. ``-T +300``).
   This option is intended for debugging/testing only.

**-t** *SECONDS* \| **--timeout=**\ ‌\ *SECONDS*
   Specifies the number of SECONDS after which the HTTPD should close
   (idle) HTTP connections.

**-v** \| **--version**
   Print version information.


Signals
=======

**taler-exchange-httpd** responds to the following signals:

``SIGTERM``
   Sending a SIGTERM to the process will cause it to shutdown cleanly.

``SIGHUP``
   Sending a SIGHUP to the process will cause it to re-execute the
   taler-exchange-httpd binary in the PATH, passing it the existing
   listen socket. Then the old server process will automatically exit
   after it is done handling existing client connections; the new server
   process will accept and handle new client connections.

See Also
========

taler-exchange-dbinit(1), taler-exchange-offline(1),
taler-exchange-reservemod(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
