challenger-dbinit(1)
####################

.. only:: html

   Name
   ====

   **challenger-dbinit** - initialize the Challenger database


Synopsis
========

**challenger-dbinit**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-g** | **--garbagecollect**]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--log=**\ \ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ \ *FILENAME*]
[**-r** | **--reset**]
[**-v** | **--version**]


Description
===========

**challenger-dbinit** is a command-line tool to initialize the Challenger database.

Its options are as follows:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the Challenger commands
   to operate from *FILENAME*.

**-g** \| **--garbagecollect**
   Remove state data from database.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--log=**\ \ *LOGLEVEL*
   Configure logging to use *LOGLEVEL*.

**-l** *FILENAME* \| **--logfile=**\ \ *FILENAME*
   Configure logging to write logs to *FILENAME*.

**-r** \| **--reset**
   Reset database.  (**DANGEROUS**: All existing data is lost!)

**-v** \| **–version**
   Print version information.


See Also
========

challenger-config(1), challenger-httpd(1), challenger.conf(5).


Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
