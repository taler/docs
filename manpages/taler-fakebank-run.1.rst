taler-fakebank-run(1)
#####################

.. only:: html

   Name
   ====

   **taler-fakebank-run** - run Taler "in memory" bank (with RESTful API)

Synopsis
========

**taler-fakebank-run**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-n** *N* | **--num-threads=**\ \ *N*]
[**-s** *AMOUNT* | **--signup-bonus=**\ \ *AMOUNT*]
[**-v** | **--version**]

Description
===========

**taler-fakebank-run** is a command-line tool to run a Taler "bank" API (HTTP REST service). The program is useful to provide the bank functionality for benchmarking or testing when LibEuFin is unavailable or otherwise unsuitable.

It should be noted that the fakebank will keep a configured number of transactions in memory. If the number of transactions exceeds the configured memory limit, the fakebank will simply discard and overwrite the old entries. At that point, any requests involving such overwritten transactions will fail. Users of the fakebank are responsible for ensuring that the transaction history kept in memory is long enough for the purpose the bank is used for. The default limit is 128k entries.

Its options are as follows:

**-C** \| **--connection-close**
   Force each HTTP connection to be closed after each request (useful in
   combination with **-f** to avoid having to wait for nc to time out).

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the merchant to operate
   from FILENAME.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-n** *N* \| **--num-threads=**\ \ *N*
   Use *N* threads in the thread pool.

**-s** *AMOUNT* \| **--signup-bonus=**\ \ *AMOUNT*
   Credit newly registered accounts with a balance of *AMOUNT*. Unlike other banks, this initial balance will be created out of thin air and not via a wire transfer from some bank-internal account.

**-v** \| **--version**
   Print version information.

See Also
========

taler-exchange-httpd(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
