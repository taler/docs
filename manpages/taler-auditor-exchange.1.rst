taler-auditor-exchange(1)
##########################

.. only:: html

   Name
   ====

   **taler-auditor-exchange** - add or remove exchange from auditor’s list

Synopsis
========

**taler-auditor-exchange**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-m** *MASTERKEY* | **--exchange-key=**\ ‌\ *MASTERKEY*]
[**-r** | **--remove**]
[**-u** *EXCHANGE_URL* | **--auditor-url=**\ ‌\ *EXCHANGE_URL*]
[**-v** | **--version**]

Description
===========

**taler-auditor-exchange** is a command-line tool to be used by an
auditor to add or remove an exchange from the list of exchanges audited
by the auditor. You must add an exchange to that list before signing
denomination keys with taler-auditor-offline or trying to audit it with
taler-auditor or taler-wire-auditor. Afterwards the exchange will be
visible via the /exchanges API of the taler-auditor-httpd.

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the exchange to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-m** *MASTERKEY* \| **--exchange-key=**\ ‌\ *MASTERKEY*
   Public key of the exchange in Crockford base32 encoding, for example
   as generated by ``taler-auditor-offline setup``.

**-r** \| **--remove**
   Instead of adding the exchange, remove it. Note that this will drop
   ALL data associated with that exchange, including existing auditing
   information. So use with extreme care!

**-u** *EXCHANGE_URL* \| **--auditor-url=**\ ‌\ *EXCHANGE_URL*
   URL of the exchange. The exchange’s HTTP API must be available at
   this address.

**-v** \| **--version**
   Print version information.

Diagnostics
===========

**taler-auditor-exchange** will return 0 on success, 1 on usage errors, 3 on problems interacting with the database backend, 4 if exchange entry to be added is already in the database (or already missing when used with **-r**).


See Also
========

taler-auditor-offline(1), taler.conf(5)

Bugs
====

We should optionally verify the correctness of this exchange’s base URL
and that it matches the master public key (note that the exchange may
still be offline, so it should be possible to bypass such a verification
step). Furthermore, if we do verification, as a (less secure)
convenience option, we should make **-** m optional and obtain it from
the base URL.

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
