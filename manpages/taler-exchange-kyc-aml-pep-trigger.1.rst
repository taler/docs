taler-exchange-kyc-aml-pep-trigger(1)
#####################################

.. only:: html

   Name
   ====

   **taler-exchange-kyc-aml-pep-trigger** - helper script to trigger AML if KYC attributes indicate a politically exposed person

Synopsis
========

**taler-exchange-kyc-aml-pep-trigger**


Description
===========

**taler-exchange-kyc-aml-pep-trigger** is a trivial shell script to illustrate how to trigger an AML process when the KYC process sets the "PEP" flag in the attribute data.

The script is mostly an example (or starting point) for
writing programs for the KYC_AML_TRIGGER option of the
exchange.

See Also
========

taler.conf(5)

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
