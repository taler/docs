libeufin-bank(1)
#################

.. only:: html

   Name
   ====

   **libeufin-bank** - LibEuFin Bank


Synopsis
========

**libeufin-bank**
[**-h** | **--help**]
[**--version**]
COMMAND [ARGS...]

Subcommands: **serve**, **dbinit**, **create-account**, **passwd**, **config**


Description
===========

**libeufin-bank** is a program that implements a simple core banking system with
account and REST APIs, including REST APIs for a Web interface
and REST APIs to interact with GNU Taler components.

Its options are as follows:

**-h** \| **--help**
   Print short help on options.

**–version**
   Print version information.

The interaction model is as follows:

- Configure the database with commands ``dbinit``.

- Set admin account password with commands ``passwd``.

- Start the HTTP server with command ``serve``.
  Let this run in a shell, writing logs to stderr.

The following sections describe each command in detail.

dbinit
------

This command defines the database schema for LibEuFin Bank.  It is mandatory to run this command before invoking the ``serve`` command.

Its options are as follows:

**-h** \| **--help**
   Print short help on options.
**-c** \| **--config** \ ‌\ *FILENAME*
   Specifies the configuration file.
**-r** \| **--reset**
   If present, deletes any database table (WARNING: potential data loss)


serve
-----

This command starts the HTTP server.

Its options are as follows:

**-h** \| **--help**
   Print short help on options.
**-c** \| **--config** \ ‌\ *FILENAME*
   Specifies the configuration file.

create-account
--------------

This command create a bank account.

It takes one argument, the JSON body of an account creation request via the Rest API.

Its options are as follows:

**-h** \| **--help**
   Print short help on options.
**-c** \| **--config** \ ‌\ *FILENAME*
   Specifies the configuration file.

passwd
------

This command change any account password.

It takes two arguments, the account login and the account new password.

Its options are as follows:

**-h** \| **--help**
   Print short help on options.
**-c** \| **--config** \ ‌\ *FILENAME*
   Specifies the configuration file.

config
------

TODO

SEE ALSO
========

libeufin-bank.conf(5)

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic mail to <taler@gnu.org>.
