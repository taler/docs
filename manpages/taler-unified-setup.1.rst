taler-unified-setup.sh(1)
#########################


.. only:: html

   Name
   ====

   **taler-unified-setup.sh** - conveniently launch GNU Taler services


Synopsis
========

**taler-unified-setup**
[**-a**]
[**-b**]
[**-c** *CONFIG_FILENAME*]
[**-d** *METHOD*]
[**-e**]
[**-f**]
[**-h**]
[**-k**]
[**-l** *FILENAME*]
[**-m**]
[**-n**]
[**-r** *MEX*]
[**-s**]
[**-t**]
[**-u** *SECTION*]
[**-v**]
[**-w**]


Description
===========

**taler-unified-setup** is a command-line tool to launch and stop
GNU Taler services in bulk. It is usually used in test cases or
for development. For production, services should be launched via
systemd and not via this tool.

**-a**
   Start auditor

**-b**
   Start backup/sync service

**-c** *CONFIG_FILENAME*
   (Mandatory) Use CONFIG_FILENAME.

**-d** *METHOD*
   use the given wire method. Default is 'x-taler-bank'.

**-e**
   Start exchange

**-f**
   Start fakebank

**-g**
   Start aggregator

**-h** \| **--help**
   Prints a compiled-in help text.

**-k**
   Start challenger (KYC service)

**-L** *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-m**
   Start merchant

**-n**
   Start libeufin nexus

**-r** *MEX*
   Which exchange to use at the merchant

**-s**
   Start libeufin sandbox

**-t**
   Start taler-exchange-transfer

**-u** *SECTION*
   Specifies configuration section with the exchange account to use

**-v**
   Use valgrind

**-w**
   Start taler-exchange-wirewatch


See Also
========

taler-exchange-dbinit(1), taler-exchange-offline(1), taler-merchant-benchmark(1),
taler-exchange-httpd(1), taler.conf(5)

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
