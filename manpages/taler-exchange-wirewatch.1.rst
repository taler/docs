taler-exchange-wirewatch(1)
###########################

.. only:: html

   Name
   ====

   **taler-exchange-wirewatch** - watch for incoming wire transfers

Synopsis
========

**taler-exchange-wirewatch**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-I**_|_**--ignore-not-found**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-r** | **--reset**]
[**-T** | **--test**]
[**-t** *PLUGINNAME* | **--type=**\ ‌\ *PLUGINNAME*]
[**-v** | **--version**]

Description
===========

**taler-exchange-wirewatch** is a command-line tool to import wire
transactions into the Taler exchange database.

Its options are as follows:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the exchange to operate
   from *FILENAME*.

**-f** *DELAY*\| **--longpoll-timeout=**\ \ *DELAY*
   How long do we wait for a response for bank transactions from the bank. This is both the timeout for the long polling as well as the maximum frequency at which we would query the bank. Specified with unit (e.g. 30s, 1d, 2w), if no unit is given the number is interpreted in microseconds. Default is 60s.

**-h** \| **--help**
   Print short help on options.

**-I** \| **--ignore-not-found**
   Do not fail if the bank says that the exchange bank account does not (yet) exist.
   Keep trying.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-r** \| **--reset**
   Ignore our own database and start with transactions from the
   beginning of time.

**-T** *USEC* \| **--timetravel=**\ \ *USEC*
   Modify the system time by *USEC* microseconds.
   *USEC* may be prefixed with ``+`` or ``-`` (e.g. ``-T +300``).
   This option is intended for debugging/testing only.

**-t** \| **--test**
   Run in test mode and exit when idle.

**-v** \| **--version**
   Print version information.

See Also
========

taler-exchange-aggregator(1), taler-exchange-httpd(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
