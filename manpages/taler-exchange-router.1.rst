taler-exchange-router(1)
#########################

.. only:: html

   Name
   ====

   **taler-exchange-router** - route purse payments to partner exchanges

Synopsis
========

**taler-exchange-router**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-T** *USEC* | **--timetravel=**\ \ *USEC*]
[**-t** | **--test**]
[**-v** | **--version**]

Description
===========

**taler-exchange-router** is a NOT YET IMPLEMENTED command-line
tool to route P2P payments to partner exchanges via wad transfers.
This will be needed if wallet-to-wallet payments are made between
wallets where the recipient has performed the KYC at a different
exchange than the sender.  This is currently not supported.


**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the exchange to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-T** *USEC* \| **--timetravel=**\ \ *USEC*
   Modify the system time by *USEC* microseconds.
   *USEC* may be prefixed with ``+`` or ``-`` (e.g. ``-T +300``).
   This option is intended for debugging/testing only.

**-t** \| **--test**
   Run in test mode and exit when idle.

**-v** \| **--version**
   Print version information.

See Also
========

taler-exchange-expire(1), taler-exchange-httpd(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
