taler-auditor-offline(1)
########################

.. only:: html

   Name
   ====

   **taler-auditor-offline** - operations using the auditor's private key

Synopsis
========

**taler-auditor-offline**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-v** | **--version**]
[subcommand ...]


Description
===========

**taler-auditor-offline** is a command-line tool to be used by an auditor to
sign that he is aware of certain keys being used by an exchange. Using this
signature, the auditor affirms that he will verify that the exchange is
properly accounting for coins of those denominations.  The tool takes a list
of subcommands as arguments which are then processed sequentially.

The tool includes two subcommands to interact *online* with the exchange's
REST APIs.  The ``download`` subcommand downloads current public keys from the
running exchange.  Note that this only includes keys that the exchange
operator has previously validated using the **taler-exchange-offline** tool.
The resulting data serves as input to the ``sign`` and ``show`` subcommands.

The ``upload`` subcommand uploads the signatures created with the private key to
the exchange.  It handles the output of all subcommands (except ``download``).
The ``download`` and ``upload`` subcommands must naturally be run "online" and do not
require access to the auditor's private key, which should be kept offline.

All other subcommands are intended to be run "offline". However, especially
when testing, it is of course possible to run the subcommands online as well.
Generally, subcommands read inputs (beyond command-line arguments)
from ``stdin``. However, they may also consume outputs of previous
subcommands.  The outputs of multiple subcommands are automatically combined,
and if not consumed the final output is printed to ``stdout``.


The general options for **taler-auditor-offline** are:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the merchant to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-v** \| **--version**
   Print version information.




Configuration
=============

The exchange and the **taler-auditor-httpd** must both be provided with
the auditor's public key, such that they can validate messages signed
by the auditor. To obtain the auditor's public key, use:

.. code-block:: console

   $ taler-auditor-offline setup

Note that if the private key file already exists, the above will simply output
the existing key.  Passing additional arguments after setup (including "-")
will cause the output to be encapsulated in JSON.

Relevant configuration options for **taler-auditor-offline** are:

* ``[auditor/AUDITOR_PRIV_FILE]`` --- where to store the private key



Subcommands
===========

setup
-----

When run the first time, this subcommand sets up the offline private key and
outputs the resulting public key. Subsequent invocations will simply again
output the (same) public key (in the format usable for the exchange
configuration).

download
--------

This subcommand must be run online. It downloads future signing and denomination
keys with the associated meta data from the exchange and outputs the resulting
JSON (for consumption by subsequent subcommands, or to ``stdout``).


show
----

This subcommand outputs information about future signing and denomination keys for
manual checking against the business-approved fee structure, lifetimes and
other parameters.

It consumes the output of the ``download`` subcommand, either from ``stdin`` or
directly.

Its output always goes to ``stdout`` for human consumption (not in JSON).  It
is usually a bad idea (but possible) to combine ``show`` with other subcommands,
except maybe for testing.


sign
----

This subcommand signs information about future signing and denomination keys.

It consumes the output of the ``download`` subcommand, either from ``stdin`` or
directly.

It outputs the signatures over *all* denomination and signing keys
present in the input, in a format suitable for the ``upload`` subcommand.


upload
------

This subcommand uploads outputs from other subcommands (except ``download`` and ``show``)
to the exchange.  Note that it is possible that some uploads succeed, while others
fail, as the operation is not atomic.

The subcommand takes no arguments and has no output.


help
----

This subcommand shows a summary of all available subcommands with the
required arguments.



Examples
========

Download public keys from an exchange (online)
----------------------------------------------

.. code-block:: console

   $ taler-auditor-offline download > keys.json

Show information about public keys (offline or online)
------------------------------------------------------

.. code-block:: console

   $ taler-auditor-offline show < keys.json

Sign public keys (offline)
--------------------------

.. code-block:: console

   $ taler-auditor-offline sign < keys.json > sigs.json

Upload auditor signatures (online)
----------------------------------

.. code-block:: console

   $ taler-auditor-offline upload < sigs.json

Download, sign and upload, all in one (online)
----------------------------------------------

Note that doing this is only recommended in non-production deployments.

.. code-block:: console

   $ taler-auditor-offline download sign upload




Security considerations
=======================

The **taler-auditor-offline** tool assumes that it is run on a high-security
system, especially for the ``sign`` subcommand.

The auditor should first use the ``show`` subcommand on the offline system to
check that the keys being signed are acceptable. This process requires
manual work: the auditor should check with the exchange operator that
the keys (and meta data) matches that previously seen by the
exchange operator when they used the **taler-exchange-offline** tool.


See Also
========

taler-auditor-exchange(1), taler-exchange-offline(1),
taler.conf(5)

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
