libeufin-nexus.conf(5)
######################

.. only:: html

   Name
   ====

   **libeufin-nexus.conf** - LibEuFin Nexus configuration file


Description
===========

The configuration file is line-oriented.  Blank lines and whitespace at the
beginning and end of a line are ignored.  Comments start with ``#`` or ``%``
in the first column (after any beginning-of-line whitespace) and go to the end
of the line.

The file is split into sections.  Every section begins with ``[SECTIONNAME]``
and contains a number of options of the form ``OPTION=VALUE``.  There may be
whitespace around the ``=`` (equal sign).  Section names and options are
*case-insensitive*.

The values, however, are *case-sensitive*.  In particular, boolean values are
one of ``YES`` or ``NO``.  Values can include whitespace by surrounding the
entire value with ``"`` (double quote).  Note, however, that there are no
escape characters in such strings; all characters between the double quotes
(including other double quotes) are taken verbatim.

Durations must be expressed with a number followed by the time unit.  The following
time units are supported: 's' (seconds), 'm' (minutes), 'h' (hours).  For example,
the value *5m* denotes a duration of *five minutes*.

Values that represent filenames can begin with a ``/bin/sh``-like variable
reference.  This can be simple, such as ``$TMPDIR/foo``, or complex, such as
``${TMPDIR:-${TMP:-/tmp}}/foo``.  The variables are expanded either using
key-values from the ``[PATHS]`` section (see below) or from the environment
(``getenv()``). The values from ``[PATHS]`` take precedence over those from
the environment.  If the variable name is found in neither ``[PATHS]`` nor the
environment, a warning is printed and the value is left unchanged.  Variables (including those from the environment) are expanded recursively, so if ``FOO=$BAR`` and ``BAR=buzz`` then the result is ``FOO=buzz``.  Recursion is bounded to at most 128 levels to avoid undefined behavior for mutually recursive expansions like if ``BAR=$FOO`` in the example above.

The ``[PATHS]`` section is special in that it contains paths that can be
referenced using ``$`` in other configuration values that specify
*filenames*. Note that configuration options that are not specifically
retrieved by the application as *filenames* will not see “$”-expressions
expanded. To expand ``$``-expressions when using ``taler-config``, you must pass
the ``-f`` command-line option.

The system automatically pre-populates the ``[PATHS]`` section with a few values
at run-time (in addition to the values that are in the actual configuration
file and automatically overwriting those values if they are present).
These automatically generated values refer to installation properties
from `GNU autoconf
<https://www.gnu.org/prep/standards/html_node/Directory-Variables.html>`_. The
values are usually dependent on an ``INSTALL_PREFIX`` which is determined by
the ``--prefix`` option given to configure.  The canonical values are:

  * LIBEXECDIR = $INSTALL_PREFIX/taler/libexec/
  * DOCDIR = $INSTALL_PREFIX/share/doc/taler/
  * ICONDIR = $INSTALL_PREFIX/share/icons/
  * LOCALEDIR = $INSTALL_PREFIX/share/locale/
  * PREFIX = $INSTALL_PREFIX/
  * BINDIR = $INSTALL_PREFIX/bin/
  * LIBDIR = $INSTALL_PREFIX/lib/taler/
  * DATADIR = $INSTALL_PREFIX/share/taler/

Note that on some platforms, the given paths may differ depending
on how the system was compiled or installed, the above are just the
canonical locations of the various resources.  These
automatically generated values are never written to disk.

Files containing default values for many of the options described below
are installed under ``$TALER_PREFIX/share/libeufin-nexus/config.d/``.
The configuration file given with **-c** to Taler binaries
overrides these defaults.

A configuration file may include another, by using the ``@INLINE@`` directive,
for example, in ``main.conf``, you could write ``@INLINE@ sub.conf`` to
include the entirety of ``sub.conf`` at that point in ``main.conf``.

Be extra careful when using ``taler-config -V VALUE`` to change configuration
values: it will destroy all uses of ``@INLINE@`` and furthermore remove all
comments from the configuration file!

GLOBAL OPTIONS
--------------

Setting the database belongs to the “[nexus-postgres]” section and the
following value.

CONFIG
  PostgreSQL connection string.  Note: this option is NOT used by the
  ebics-setup subcommand, as it stores the key files directly on the
  filesystem.

The “[paths]” section is special in that it contains paths that can be
referenced using “$” in other configuration values that specify
filenames. For Taler, it commonly contains the following paths:

LIBEUFIN_HOME
  Home directory of the user, usually “${HOME}”. Can be overwritten by
  testcases by setting ${LIBEUFIN_TEST_HOME}.

EBICS SETUP OPTIONS
-------------------

The following options are from the “[nexus-ebics]” section and used by
the ``libeufin-nexus ebics-setup`` command.

CURRENCY
  Name of the currency, e.g. “EUR” for Euro.

HOST_BASE_URL = http://bank.example.com/
  URL of the EBICS server

BANK_DIALECT
  Name of the following combination: EBICS version and ISO20022 recommendations
  that Nexus would honor in the communication with the bank.  Currently only the
  'postfinance' value is supported.

HOST_ID
  EBICS specific: name of the EBICS host

USER_ID
  EBICS specific: user ID of the EBICS subscriber.  This value must be assigned
  by the bank after having activated a new EBICS subscriber.

PARTNER_ID
  EBICS specific: partner ID of the EBICS subscriber.  This value must be assigned
  by the bank after having activated a new EBICS subscriber.


BANK_PUBLIC_KEYS_FILE
  Filesystem location where Nexus should store the bank public keys.
  
CLIENT_PRIVATE_KEYS_FILE
  Filesystem location where Nexus should store the subscriber private keys.

IBAN
  IBAN of the bank account that is associated with the EBICS subscriber.

BIC
  BIC of the bank account that is associated with the EBICS subscriber.

NAME
  Legal entity that is associated with the EBICS subscriber.


EBICS SUBMIT OPTIONS
--------------------

The following configuration value(s) belong to the “[nexus-submit]” section.

FREQUENCY
  Duration value to instruct the ``ebics-submit`` subcommand how much to wait
  before checking the database again to find new unsubmitted payments.

SUBMISSIONS_LOG_DIRECTORY
  Optional value to define the path where the pain.001 documents gets stored *before*
  being submitted to the bank.  This directory would contain several directories,
  each named after the YYYY-MM-DD format.  The pain.001 file would then be named in
  the following schema: ``$submissionTimestampMicroseconds_requestUid_$requestUid_pain.001.xml``.
  ``$requestUid`` is the same value that constitutes the pain.001 ``MsgIg`` element.
  In the event that one log file would be overridden, ``ebics-submit`` fails without
  overriding it.


EBICS FETCH OPTIONS
-------------------

The following configuration value(s) belong to the “[nexus-fetch]” section.

FREQUENCY
  Duration value to instruct the ``ebics-fetch`` subcommand how often it should
  download from the bank.

STATEMENT_LOG_DIRECTORY
  Optional value to define the path where the downloaded documents would be stored
  *before* being ingested in the database.  This directory would contain several
  directories, each named after the YYYY-MM-DD format.  The stored files would then
  be named after the following schema:
  ``$downloadTimestampMicroseconds_$filenameAsGivenByTheBank``.  In case one log file
  would be overridden, ``ebics-fetch`` fails without overriding it.
  Exception to this naming scheme are the HAC responses, since they do not get any
  filename assigned by the ZIP archive (they are sent unzipped).  Their naming scheme
  is: ``$downloadTimestampMicroseconds_HAC_response.pain.002.xml``

MINIMUM_AMOUNT
  Optional value to indicate the minumum amount that users should wire to the watched
  bank account.  It takes only the number part *without the currency*, where at most two
  fractional digits are allowed.

SEE ALSO
========

libeufin-nexus(1)

BUGS
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
