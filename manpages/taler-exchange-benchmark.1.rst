taler-exchange-benchmark(1)
###########################


.. only:: html

   Name
   ====

   **taler-exchange-benchmark** - measure exchange performance


Synopsis
========

**taler-exchange-benchmark**
[**-c** *CONFIG_FILENAME* | **--config=**\ ‌\ *CONFIG_FILENAME*]
[**-F** | **--reserves-first**]
[**-f** | **--fakebank**]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--log-level=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-n** *HOWMANY_COINS* | **--coins-number=**\ ‌\ *HOWMANY_COINS*]
[**-p** *NPROCS* | **--parallelism=**\ \ *NPROCS*]
[**-R** *RATE* | **--refresh-rate=**\ \ *RATE*]
[**-r** *N* | **--reserves=**\ \ *N*]
[**-u** *SECTION* | **--exchange-account-section=**\ \ *SECTION*]
[**-v** | **--version**]

Description
===========

**taler-exchange-benchmark** is a command-line tool to measure the time
spent to serve withdrawals/deposits/refreshes.  Before running the benchmark,
the GNU Taler services must already be running at the configured addresses.

**-c** *CONFIG_FILENAME* \| **--config=**\ ‌\ *CONFIG_FILENAME*
   (Mandatory) Use CONFIG_FILENAME.

**-F** \| **--reserves-first**
   Create all reserves first, before starting normal operations.

**-f** \| **--fakebank**
   Expect to interact with a fakebank instead of libeufin.

**-h** \| **--help**
   Prints a compiled-in help text.

**-L** *LOGLEVEL* \| **--log-level=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-n** *HOWMANY_COINS* \| **--coins-number=**\ ‌\ *HOWMANY_COINS*
   Defaults to 1. Specifies how many coins this benchmark should
   withdraw and spend. After being spent, each coin will be refreshed
   with a probability RATE (see option ``--refresh-rate``).

**-p** *NPROCS* \| **--parallelism=**\ \ *NPROCS*
   Run with *NPROCS* client processes.

**-R** *RATE* \| **--refresh-rate=**\ \ *RATE*
   Defaults to 10.  Probability of refresh per coin (0-100).

**-r** *N* \| **--reserves=**\ \ *N*
   Create *N* reserves per client.

**-u** *SECTION* \| **--exchange-account-section=**\ \ *SECTION*
   Which configuration section should be used for the bank account
   of the exchange.

**-v** \| **--version**
   Print version information.

See Also
========

taler-exchange-dbinit(1), taler-exchange-offline(1), taler-merchant-benchmark(1),
taler-exchange-httpd(1), taler-unified-setup(1), taler.conf(5)

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
