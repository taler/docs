taler-exchange-dbinit(1)
########################

.. only:: html

   Name
   ====

   **taler-exchange-dbinit** - initialize Taler exchange database


Synopsis
========

**taler-exchange-dbinit**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-g** | **--gc**]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-r** | **--reset**]
[**-s** | **--shardunlock**]
[**-v** | **--version**]

Description
===========

**taler-exchange-dbinit** is a command-line tool to initialize the Taler
exchange database. It creates the necessary tables and indices for the
Taler exchange to operate.

Its options are as follows:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the exchange to operate
   from *FILENAME*.

**-g** \| **--gc**
   Garbage collect database. Deletes all unnecessary data in the
   database.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-r** \| **--reset**
   Drop tables. Dangerous, will delete all existing data in the database
   before creating the tables.

**-s** \| **--shardunlock**
   Clears the (revolving) shards table. Needed to clear locks that may be held after a crash (of taler-exchange-aggregator or the operating system, say due to power outage) or if the AGGREGATOR_SHARD_SIZE option is changed in the configuration file.

**-v** \| **–version**
   Print version information.

See Also
========

taler-exchange-httpd(1), taler-exchange-offline(1),
taler-exchange-reservemod(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
