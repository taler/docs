taler-merchant-benchmark(1)
###########################


.. only:: html

  Name
  ====

  **taler-merchant-benchmark** - generate Taler-style benchmarking payments


Synopsis
========

**taler-merchant-benchmark** [*subcommand*] [*options*]


Description
===========

**taler-merchant-benchmark** is a command-line tool to populate your
merchant database with payments for benchmarking.


Subcommands
===========

ordinary
       Generate normal payments: all the payments are performed (by the
       default instance) and aggregated by the exchange.  Takes the following
       option:

       **-p** *PN* \| **--payments-number=**\ \ *PN*
              Perform PN many payments, defaults to 1.


corner
       Drive the generator to create unusual situations, like for example
       leaving payments unaggregated, or using a non-default merchant
       instance.  Takes the following options:


       **-t** *TC* \| **--two-coins=**\ \ *TC*
              Perform TC many payments that use two coins (normally, all the
              payments use only one coin).  TC defaults to 1.


       **-u** *UN* \| **--unaggregated-number=**\ \ *UN*
              Generate UN payments that will be left unaggregated.  Note that
              subsequent invocations of the generator may pick those
              unaggregated payments and actually aggregated them.



Common Options
==============

**-a** *APIKEY* \| **--apikey=**\ \ *APIKEY*
   HTTP 'Authorization' header to send to the merchant.

**-c** *FILENAME* \| **--config=**\ \ *FILENAME*
   Use the configuration and other resources for the merchant to
   operate from FILENAME.

**-u** *SECTION* \| **--exchange-account-section=**\ \ *SECTION*
   Configuration *SECTION* specifying the exchange account to use.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-v** \| **--version**
   Print version information.


See Also
========

taler-merchant-dbinit(1), taler.conf(5)


Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
