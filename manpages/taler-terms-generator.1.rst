taler-terms-generator(1)
########################


.. only:: html

   Name
   ====

   **taler-terms-generator** - generate legal terms for GNU Taler services


Synopsis
========

**taler-terms-generator**
[**-a** *AUTHOR*]
[**-C** *COPYRIGHT*]
[**-h**]
[**-i** *INPUT*]
[**-l** *LANGUAGE*]
[**-o** *OUTPUT*]
[**-p** *PAPER*]
[**-t** *TITLE*]


Description
===========

**taler-terms-generator** is a command-line tool to create terms of service
 and privacy policy files in various file formats and languages from a
 reStructuredText (".rst") input. It can be used to generate the responses
 various GNU Taler services serve under the ``/terms`` and ``/pp`` endpoints.

**-a** *AUTHOR*
   set the author information to the given AUTHOR in the meta-data of various generated outputs.

**-C** *COPYRIGHT*
   set the copyright information to the given COPYRIGHT in the meta-data of various generated outputs.

**-h** \| **--help**
   Prints a compiled-in help text.

**-i** *INPUT*
   Name of the input (.rst) file with the terms of service or privacy policy to convert.

**-l** *LANGUAGE*
   Add the given *LANGUAGE* to the list of translations for the current *INPUT*. *LANGUAGE* must be a two-letter language code (like "de" or "it"). This will generate or update the respective ".po" files to translate the *INPUT* terms to this *LANGUAGE*.

**-L** *LOCALE_DIR*
   Specify locale/ directory where GNU gettext resources for translating the input are located. If "-l" is given, this directory is where fresh or updated ".po" files will be placed, and otherwise this directory will be scanned for translations of the ".rst" input file.

**-o** *OUTPUT*
   Specifies where to write the output. This should be the directory where the service expects to find the generated resources.  Unless you changed the default configuration, you probably do not have to specify this value.

**-p** *PAPER*
   Specifies the paper format for generated PDF documents. Can be "a4" or "letter".

**-t** *TITLE*
   Overrides the document title. By default, the title will be set to the contents of the first line of the *INPUT* ".rst" file.


See Also
========

taler-config(1), taler.conf(5)

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
