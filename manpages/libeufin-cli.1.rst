libeufin-cli(1)
###############

.. only:: html

   Name
   ====

   **libeufin-cli** - Interact with LibEuFin Sandbox and Nexus


Synopsis
========

**libeufin-cli**
[**-h** | **--help**]
[**--version**]
COMMAND [ARGS...]

Commands: accounts, connections, facades, permissions, sandbox, users


Description
===========

**libeufin-cli** is the user interface program to interact
with **libeufin-sandbox** and **libeufin-nexus** when they are
operating as HTTP servers, listening on localhost ports.
Normally, you invoke them with their respective ``serve`` commands,
and in a separate shell, use **libeufin-cli** to send requests
and receive responses from them.

The interaction model is as follows:

- (Optionally) Start the sandbox.

- Start the nexus.

- Use **libeufin-cli** to interact with them.
  You can manage users and permissions, bank accounts, "facades",
  transactions, and connections between the various systems.

For **libeufin-cli** to be able to communicate with **libeufin-sandbox**,
the following environment variables need to be set:

``LIBEUFIN_SANDBOX_USERNAME``
  This should normally be ``admin``.

``LIBEUFIN_SANDBOX_PASSWORD``
  This is the same password chosen when the sandbox was started.

``LIBEUFIN_SANDBOX_URL``
  This is ``http://localhost:PORT``, where ``PORT`` is the
  same port chosen when the sandbox was started.
  This URL can also be specified with the ``--sandbox-url URL``
  option to the ``sandbox`` command (see below).
  If both are given, the ``--sandbox-url`` option takes precedence.

For **libeufin-cli** to be able to communicate with **libeufin-nexus**,
the following environment variables need to be set:

``LIBEUFIN_NEXUS_USERNAME``
  For some operations (such as ``users create``), this must be the
  same username chosen by the ``libeufin-nexus superuser`` command.

``LIBEUFIN_NEXUS_PASSWORD``
  This is the password associated with the username.

``LIBEUFIN_NEXUS_URL``
  This is ``http://localhost:PORT``, where ``PORT`` is the
  same port chosen when the nexus was started.

Of the six commands, the ``sandbox`` command talks to the sandbox,
while the other five commands talk to the nexus.
The following sections describe each command and their subcommands in detail.


sandbox
-------

The ``libeufin-cli sandbox`` command is for managing the sandbox process
started by ``libeufin-sandbox serve``.
It takes one option, ``--sandbox-url URL``, to be specified before
the subcommands and their arguments.
This URL can also be specified with the ``LIBEUFIN_SANDBOX_URL``
environment variable (see above).
If both are given, the ``--sandbox-url`` option takes precedence.

The following subcommands are available: bankaccount, check, demobank,
ebicsbankaccount, ebicshost, ebicssubscriber.

The first command to use is ``check``,
followed by ``ebicshost``, ``ebicssubscriber``,
and ``ebicsbankaccount``, to configure the basic system.

You can then use the ``bankaccount`` command to generate (simulated)
transaction traffic.

After that, the ``demobank`` command and its subcommands
provide the same access to the API that Nexus itself uses.
You normally do not need to use those commands directly.


sandbox check
-------------

The ``check`` command attempts a simple aliveness check by
contacting the sandbox and displaying its returned message.
If all goes well, you should see something like:

.. code-block:: console

  $ libeufin-cli sandbox check
  Hello, this is the Sandbox


sandbox ebicshost
-----------------

The ``ebicshost`` command manages EBICS hosts.
It has two subcommands: create and list.

sandbox ebicshost create
^^^^^^^^^^^^^^^^^^^^^^^^

The ``ebicshost create`` command creates a EBICS host.
It takes one option, ``--host-id ID``, where ``ID`` is used to
identify that host for all future commands.

sandbox ebicshost list
^^^^^^^^^^^^^^^^^^^^^^

The ``ebicshost list`` command lists the hosts in the system.

For example:

.. code-block:: console

  $ libeufin-cli sandbox ebicshost create --host-id testhost
  $ libeufin-cli sandbox ebicshost list
  {
    "ebicsHosts" : [ "testhost" ]
  }

Here, the ``ID`` is ``testhost``.


sandbox ebicssubscriber
-----------------------

The ``ebicssubscriber`` command manages EBICS subscribers.
It has two subcommands: create and list.

sandbox ebicssubscriber create
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

NB: This command is deprecated and will be removed at some point.
See ``sandbox demobank new-ebicssubscriber`` (below) for its replacement.

The ``ebicssubscriber create`` command creates a EBICS subscriber.
It takes three options, all of which are required:

::

  --host-id TEXT     Ebics host ID
  --partner-id TEXT  Ebics partner ID
  --user-id TEXT     Ebics user ID

The host ID should be the same one specified in ``ebicshost create``
(see above).

For example:

.. code-block:: console

  $ libeufin-cli sandbox ebicssubscriber create \
      --host-id testhost \
      --partner-id partner01 \
      --user-id user01

Note that ``testhost`` is the same as in the ``ebicshost create``
example (see above).

sandbox ebicssubscriber list
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``ebicssubscriber list`` command lists the EBICS subscribers
in the system.

For example:

.. code-block:: console

  $ libeufin-cli sandbox ebicssubscriber list
  {
    "subscribers" : [ {
      "hostID" : "testhost",
      "partnerID" : "partner01",
      "userID" : "user01",
      "systemID" : null,
      "demobankAccountLabel" : "not associated yet"
    } ]
  }

Note that the output reflects the subscriber created in
the ``ebicssubscriber create`` example (see above).


sandbox ebicsbankaccount
------------------------

The ``ebicsbankaccount`` command manages EBICS bank accounts.
It has one subcommand: create.
(To list accounts, see ``sandbox bankaccount`` below.)

sandbox ebicsbankaccount
^^^^^^^^^^^^^^^^^^^^^^^^

The ``ebicsbankaccount create`` command takes several options, all required:

::

  --iban TEXT              IBAN
  --bic TEXT               BIC
  --person-name TEXT       bank account owner name
  --account-name TEXT      label of this bank account
  --ebics-user-id TEXT     user ID of the Ebics subscriber
  --ebics-host-id TEXT     host ID of the Ebics subscriber
  --ebics-partner-id TEXT  partner ID of the Ebics subscriber

At this time, although the ``--person-name`` option is required,
the sandbox does not remember the option value.
(When queried, it displays "Bank account owner's name" instead.
See ``sandbox bankaccount`` below.)
For the sandbox, the important value is the ``--account-name`` option.

For example:

.. code-block:: console

  $ libeufin-cli sandbox ebicsbankaccount create \
      --iban DE18500105172929531888 \
      --bic INGDDEFFXXX \
      --person-name "Jane Normal" \
      --account-name testacct01 \
      --ebics-host-id testhost \
      --ebics-user-id user01 \
      --ebics-partner-id partner01

Note that ``testhost`` is the same as in the ``ebicshost create``
example, and that ``user01`` and ``partner01`` are the same as in the
``ebicssubscriber create`` example (see above).


sandbox bankaccount
-------------------

The ``bankaccount`` command manages bank accounts.
It has several subcommands: list, generate-transactions,
simulate-incoming-transaction, transactions.

sandbox bankaccount list
^^^^^^^^^^^^^^^^^^^^^^^^

The ``bankaccount list`` command lists the bank accounts in the system.

For example:

.. code-block:: console

  $ libeufin-cli sandbox bankaccount list
  [ {
    "label" : "bank",
    "name" : "Bank account owner's name",
    "iban" : "DE370758",
    "bic" : "SANDBOXX"
  }, {
    "label" : "testacct01",
    "name" : "Bank account owner's name",
    "iban" : "DE18500105172929531888",
    "bic" : "INGDDEFFXXX"
  } ]

Note that ``testacct01``, ``DE18500105172929531888``, and ``INGDDEFFXXX``
are the same as specified in the ``ebicsbankaccount create`` example
(see above).

sandbox bankaccount generate-transactions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The remaining ``bankaccount`` commands deal with transactions
to and from the bank accounts.

The ``bankaccount generate-transactions`` command generates
test transactions.
It takes one arg, the account label.

For example:

.. code-block:: console

  $ libeufin-cli sandbox bankaccount generate-transactions testacct01

Note that ``testacct01`` is the account label shown in the ``bankaccount
list`` example (see above).

sandbox bankaccount simulate-incoming-transaction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``bankaccount simulate-incoming-transaction`` books an
incoming payment in the sandbox.
It takes one arg, the account label, and several options.

::

  --debtor-iban TEXT  IBAN sending the payment
  --debtor-bic TEXT   BIC sending the payment
  --debtor-name TEXT  name of the person who is sending the payment
  --amount TEXT       amount, no currency
  --subject TEXT      payment subject

For example:

.. code-block:: console

  $ libeufin-cli sandbox bankaccount simulate-incoming-transaction
      testacct01 \
      --debtor-iban DE06500105174526623718 \
      --debtor-bic INGDDEFFXXX \
      --debtor-name "Joe Foo" \
      --subject "Hello World" \
      --amount 10.50

Note that ``testacct01`` is the same as in previous examples (see above),
and that ``10.50`` is in ``X.Y`` format.

sandbox bankaccount transactions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``bankaccount transactions`` command lists transactions.
It takes one arg, the account label.

For example:

.. code-block:: console

  $ libeufin-cli sandbox bankaccount transactions testacct01
  {
    "payments" : [ {
      "account_label" : "testacct01",
      "creditor_iban" : "DE18500105172929531888",
      "creditor_bic" : "INGDDEFFXXX",
      "creditor_name" : "Creditor Name",
      "debtor_iban" : "DE64500105178797276788",
      "debtor_bic" : "DEUTDEBB101",
      "debtor_name" : "Max Mustermann",
      "amount" : "5",
      "currency" : "EUR",
      "subject" : "sample transaction DILWBJHL",
      "date" : "Wed, 26 Jan 2022 09:03:44 GMT",
      "credit_debit_indicator" : "credit",
      "account_servicer_reference" : "DILWBJHL",
      "paymentInformationId" : null
    }, {
      "account_label" : "testacct01",
      "creditor_iban" : "DE64500105178797276788",
      "creditor_bic" : "DEUTDEBB101",
      "creditor_name" : "Max Mustermann",
      "debtor_iban" : "DE18500105172929531888",
      "debtor_bic" : "INGDDEFFXXX",
      "debtor_name" : "Debitor Name",
      "amount" : "12",
      "currency" : "EUR",
      "subject" : "sample transaction N7JSY17B",
      "date" : "Wed, 26 Jan 2022 09:03:44 GMT",
      "credit_debit_indicator" : "debit",
      "account_servicer_reference" : "N7JSY17B",
      "paymentInformationId" : null
    }, {
      "account_label" : "testacct01",
      "creditor_iban" : "DE18500105172929531888",
      "creditor_bic" : "INGDDEFFXXX",
      "creditor_name" : "Creditor Name",
      "debtor_iban" : "DE06500105174526623718",
      "debtor_bic" : "INGDDEFFXXX",
      "debtor_name" : "Joe Foo",
      "amount" : "10.50",
      "currency" : "EUR",
      "subject" : "Hello World",
      "date" : "Wed, 26 Jan 2022 09:04:31 GMT",
      "credit_debit_indicator" : "credit",
      "account_servicer_reference" : "sandbox-6UI2J3636J9EESXO",
      "paymentInformationId" : null
    } ]
  }

Note that ``testacct01`` is the same as in previous examples (see above),
and that the generated transactions from previous examples are listed,
as well.


sandbox demobank
----------------

The ``demobank`` command provides an interface to the Access API,
which includes three commands: register, info, new-transaction.
There is also a fourth ``demobank`` command, new-ebicssubscriber,
that does not use the Access API.

For all ``demobank`` commands, the sandbox URL *must* specify which
*one* bank the command applies to in the base URL.
Note that this URL cannot be used with other sandbox commands.
In other words:

``--sandbox-url http://localhost:5016/demobanks/default``
  This base URL can be used for commands:

  - sandbox demobank register
  - sandbox demobank info
  - sandbox demobank new-ebicssubscriber
  - sandbox demobank new-transaction

  It specifies the ``default`` demobank.

``--sandbox-url http://localhost:5016``
  This base URL can be used for all other sandbox commands.

In the following examples, the base URL will be explicitly shown with
the ``--sandbox-url`` option for the ``demobank`` commands.

sandbox demobank register
^^^^^^^^^^^^^^^^^^^^^^^^^

The ``demobank register`` command registers a new bank account.
Note that the username will be both the username to login at
the bank and the bank account label.
It takes the username and password from the
``LIBEUFIN_SANDBOX_USERNAME`` and ``LIBEUFIN_SANDBOX_PASSWORD``
environment variables.
The username *need not be* ``admin``.

For example:

.. code-block:: console

  $ export LIBEUFIN_SANDBOX_USERNAME=jrluser
  $ export LIBEUFIN_SANDBOX_PASSWORD=easy
  $ libeufin-cli sandbox \
      --sandbox-url http://localhost:5016/demobanks/default \
      demobank register

sandbox demobank info
^^^^^^^^^^^^^^^^^^^^^

The ``demobank info`` command returns basic information on a bank account.
It takes option ``--bank-account NAME``.

For example:

.. code-block:: console

  $ libeufin-cli sandbox \
      --sandbox-url http://localhost:5016/demobanks/default \
      demobank info --bank-account jrluser
  {
    "balance" : {
      "amount" : "EUR:100",
      "credit_debit_indicator" : "credit"
    },
    "paytoUri" : "payto://iban/SANDBOXX/DE948559?receiver-name=admin"
  }

Note that ``jrluser`` is the same username / bank account name
as in the ``register`` example (see above).

sandbox demobank new-ebicssubscriber
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``demobank new-ebicssubscriber`` command associates a new Ebics
subscriber to an existing bank account.
It takes several options, all required:

::

  --host-id TEXT       Ebics host ID
  --partner-id TEXT    Ebics partner ID
  --user-id TEXT       Ebics user ID
  --bank-account TEXT  Label of the bank account to associate
                       with this Ebics subscriber

For example:

.. code-block: console

  $ libeufin-cli sandbox \
      --sandbox-url http://localhost:5016/demobanks/default \
      demobank new-ebicssubscriber \
      --host-id testhost \
      --partner-id partner01 \
      --user-id user02 \
      --bank-account jrluser

Note that ``testhost`` is the same as in the ``ebicshost create``
example, and that ``partner01`` is the same as in the
``ebicssubscriber create`` example (see above).
The ``user02`` is new.
The ``--bank-account jrluser`` is the same as in the
``info`` example (see above).
You can see the effect of the ``new-ebicssubscriber`` command
with the ``sandbox ebicssubscriber list``
and ``sandbox bankaccount list`` commands.

sandbox demobank new-transaction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``demobank new-transaction`` command initiates a new transaction.
It takes several options, all required:

::

  --bank-account TEXT        Label of the bank account to be
                             debited for the transaction
  --payto-with-subject TEXT  Payto address including the
                             subject as a query parameter
  --amount CUR:X.Y           Amount to transfer

For example:

.. code-block:: console

  $ libeufin-cli sandbox \
      --sandbox-url http://localhost:5016/demobanks/default \
      demobank new-transaction \
      --bank-account jrluser \
      --payto-with-subject 'payto://FIXME/?message=1kg+coffee' \
      --amount EUR:10.50
  FIXME: Any output?

Note that ``--bank-account jrluser`` is the same as in the
``info`` and ``new-ebicssubscriber`` command examples (see above).


connections
-----------

The ``connections`` set of commands handle connections between
the bank(s) and Nexus.
It has several subcommands:
new-ebics-connection,
list-connections,
show-connection,
delete-connection,
export-backup,
restore-backup,
get-key-letter,
connect,
list-offered-bank-accounts,
download-bank-accounts,
import-bank-account.

Generally, you will create a connection, save its critical key information
to a safe location, then use the connection to find and import a bank
account, for further use.

Several commands take a ``CONNECTION_NAME`` argument.
In the following examples, we use ``conn01`` for that.
Also, for demonstration purposes, we use the sandbox EBICS services,
so the ``EBICS_URL`` follows the previous examples in the ``sandbox``
command, as the value of environment variable ``LIBEUFIN_SANDBOX_URL``
suffixed with ``/ebicsweb``, i.e., ``http://localhost:5016/ebicsweb``.

connections new-ebics-connection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``connections new-ebics-connection`` command creates a new connection
between an EBICS service and the Nexus.
It takes one arg, the ``CONNECTION_NAME``, and four required options:

::

  --ebics-url TEXT      EBICS URL
  --host-id TEXT        Host ID
  --partner-id TEXT     Partner ID
  --ebics-user-id TEXT  Ebics user ID

For example:

.. code-block:: console

  $ libeufin-cli connections new-ebics-connection \
      --ebics-url http://localhost:5016/ebicsweb \
      --host-id testhost \
      --partner-id partner01 \
      --ebics-user-id user02 \
      conn01

Note that the ``testhost``, ``partner01``, and ``user02`` are the same as
in the ``sandbox demobank new-ebicssubscriber`` command (see above).

connections list-connections
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``connections list-connections`` command lists connections in Nexus.
For example:

.. code-block:: console

  $ libeufin-cli connections list-connections
  {
    "bankConnections" : [ {
      "name" : "conn01",
      "type" : "ebics"
    } ]
  }

The name of the connection is ``conn01`` as in the
``connections new-ebics-connection`` example (see above).

connections show-connection
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``connections show-connection`` command displays information
about a specific connection.
It takes one argument, the ``CONNECTION_NAME``.

For example:

.. code-block:: console

  $ libeufin-cli connections show-connection conn01
  {
    "type" : "ebics",
    "owner" : "foo",
    "ready" : true,
    "details" : {
      "ebicsUrl" : "http://localhost:5016/ebicsweb",
      "ebicsHostId" : "testhost",
      "partnerId" : "partner01",
      "userId" : "user02"
    }
  }

The details are the same as in the ``connections new-ebics-connections``
example (see above).

connections delete-connection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``connections delete-connection`` command deletes a bank connection.
It takes one argument, the ``CONNECTION_NAME``.

For example:

.. code-block:: console

  $ libeufin-cli connections delete-connection conn01

connections export-backup
^^^^^^^^^^^^^^^^^^^^^^^^^

The ``connections export-backup`` command writes key and signature
information to a backup file (which you should take care to store
in a secure location).
It takes one argument, the ``CONNECTION_NAME`` and two required options:

::

  --passphrase TEXT   Passphrase for locking the backup
  --output-file TEXT  Where to store the backup

For example:

.. code-block:: console

  $ libeufin-cli connections export-backup \
      --passphrase secret \
      --output-file sig-and-key-info \
      conn01
  FIXME: Output?

See:  https://bugs.gnunet.org/view.php?id=7180

connections restore-backup
^^^^^^^^^^^^^^^^^^^^^^^^^^

WRITEME

connections get-key-letter
^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``connections get-key-letter`` command creates a PDF file
that contains key information.
It takes two arguments, ``CONNECTION_NAME`` and ``OUTPUT_FILE``.

For example:

.. code-block:: console

  $ libeufin-cli connections get-key-letter conn01 key-letter.pdf

This creates file ``key-letter.pdf`` in the current working directory.

connections connect
^^^^^^^^^^^^^^^^^^^

WRITEME

connections list-offered-bank-accounts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

WRITEME

connections download-bank-accounts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

WRITEME

connections import-bank-account
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

WRITEME


users
-----

The ``libeufin-cli users`` command manages users authorized to
operate Nexus.
It has several subcommands: self, list, create, change-password.
The ``create`` and ``change-password`` commands can only be issued
by the superuser
(as configured by the ``libeufin-nexus superuser`` command),
while the ``self`` and ``list`` commands can be issued by any user.

In the following ``users`` examples, we assume that previously
the command ``libeufin-nexus superuser foo`` was issued, and
that the current username and password are for that user
(via environment variables ``LIBEUFIN_NEXUS_USERNAME`` and
``LIBEUFIN_NEXUS_PASSWORD``, see above).

users self
^^^^^^^^^^

The ``users self`` command displays information for the current user.

For example:

.. code-block:: console

  $ libeufin-cli users self
  {
    "username" : "foo",
    "superuser" : true
  }

users list
^^^^^^^^^^

The ``users list`` command displays information for all the
users authorized to operate Nexus.

For example:

.. code-block:: console

  $ libeufin-cli users list
  {
    "users" : [ {
      "username" : "foo",
      "superuser" : true
    } ]
  }

In this example, there is only one user, the superuser ``foo``.

users create
^^^^^^^^^^^^

The ``users create`` command creates a normal (non-superuser) user.
It takes one argument, the ``USERNAME`` and one option,
``--password TEXT``.
If you omit the option, **libeufin-cli** will prompt you for the password.

For example:

.. code-block:: console

  $ libeufin-cli users create --password easy jrluser
  {
    "message" : "New user 'jrluser' registered"
  }
  $ libeufin-cli users list
  {
    "users" : [ {
      "username" : "foo",
      "superuser" : true
    }, {
      "username" : "jrluser",
      "superuser" : false
    } ]
  }

In this example, we create the same user as for the sandbox examples
(see above).
Note that the system now includes two users.

users change-password
^^^^^^^^^^^^^^^^^^^^^

The ``users change-password`` command changes the password for a user.
It takes one argument, the ``USERNAME`` and one option,
``--new-password TEXT``.
If you omit the option, **libeufin-cli** will prompt you for the password.

For example:

.. code-block:: console

  $ libeufin-cli users change-password --new-password hard jrluser
  {
    "message" : "Password successfully changed"
  }


permissions
-----------

The ``libeufin-cli permissions`` command manages permissions
for operations on Nexus.
It has three subcommands: list, grant, revoke.
All three commands can only be issued by the superuser.

permissions list
^^^^^^^^^^^^^^^^

The ``permissions list`` command lists the granted permissions.
At the beginning of a session, there are none:

.. code-block:: console

  $ libeufin-cli permissions list
  {
    "permissions" : [ ]
  }


permissions grant
^^^^^^^^^^^^^^^^^

The ``permissions grant`` command adds a permission to the list
of granted permissions.
It takes five arguments: ``SUBJECT_TYPE``, ``SUBJECT_ID``,
``RESOURCE_TYPE``, ``RESOURCE_ID``, ``PERMISSION_NAME``.

FIXME: The subject type and id, resource type and id, are ...

The ``PERMISSION_NAME`` is one of the following:

- ``facade.talerwiregateway.history``
- ``facade.talerwiregateway.transfer``
- ``facade.anastasis.history``

For example:

.. code-block:: console

  $ libeufin-cli permissions grant \
      some-subject-type some-subject-id \
      some-resource-type some-resource-id \
      facade.anastasis.history
  { }
  $ libeufin-cli permissions list
  {
    "permissions" : [ {
      "subject_type" : "some-subject-type",
      "subject_id" : "some-subject-id",
      "resource_type" : "some-resource-type",
      "resource_id" : "some-resource-id",
      "permission_name" : "facade.anastasis.history"
    } ]
  }

permissions revoke
^^^^^^^^^^^^^^^^^^

The ``permissions revoke`` command does the opposite of the
``permissions grant`` command.
It takes the same arguments as the ``permissions grant`` command:
``SUBJECT_TYPE``, ``SUBJECT_ID``, ``RESOURCE_TYPE``, ``RESOURCE_ID``,
``PERMISSION_NAME``.

For example:

.. code-block:: console

  $ libeufin-cli permissions revoke \
      some-subject-type some-subject-id \
      some-resource-type some-resource-id \
      facade.anastasis.history
  { }
  $ libeufin-cli permissions list
  {
    "permissions" : [ ]
  }

This example undoes the effect of the previous (``permissions grant``) example.


accounts
--------

WRITEME

accounts transactions
^^^^^^^^^^^^^^^^^^^^^

WRITEME

accounts task-status
^^^^^^^^^^^^^^^^^^^^

WRITEME

accounts task-schedule
^^^^^^^^^^^^^^^^^^^^^^

WRITEME

accounts task-delete
^^^^^^^^^^^^^^^^^^^^

WRITEME

accounts submit-payments
^^^^^^^^^^^^^^^^^^^^^^^^

WRITEME

accounts show-payment
^^^^^^^^^^^^^^^^^^^^^

WRITEME

accounts show
^^^^^^^^^^^^^

WRITEME

accounts prepare-payment
^^^^^^^^^^^^^^^^^^^^^^^^

WRITEME

accounts list-tasks
^^^^^^^^^^^^^^^^^^^

WRITEME

accounts list-payments
^^^^^^^^^^^^^^^^^^^^^^

WRITEME

accounts fetch-transactions
^^^^^^^^^^^^^^^^^^^^^^^^^^^

WRITEME

accounts delete-payment
^^^^^^^^^^^^^^^^^^^^^^^

WRITEME


facades
-------

WRITEME

facades new-taler-wire-gateway-facade
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

WRITEME

facades new-anastasis-facade
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

WRITEME

facades list
^^^^^^^^^^^^

WRITEME


See Also
========

.. TODO: libeufin-sandbox(1), libeufin-cli(1).


Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
