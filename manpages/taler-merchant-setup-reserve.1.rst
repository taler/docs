taler-merchant-setup-reserve(1)
###############################


.. only:: html

  Name
  ====

  **taler-merchant-setup-reserve** - setup reserve for rewards


Synopsis
========

**taler-merchant-setup-reserve**
[**-A** *USERNAME:PASSWORD* | **--auth=**\ \ *USERNAME:PASSWORD*]
[**-a** *VALUE* | **--amount=**\ \ *VALUE*]
[**-C** *CERTFILE* | **--cert=**\ \ *CERTFILE*]
[**-c** *FILENAME* | **--config=**\ \ *FILENAME*]
[**-e** *URL* | **--exchange-url=**\ \ *URL*]
[**-h** | **--help**]
[**-k** *KEYFILE* | **--key=**\ \ *KEYFILE*]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-m** *URL* | **--merchant-url=**\ \ *URL*]
[**-p** *KEYFILEPASSPHRASE* | **--pass=**\ \ *KEYFILEPASSPHRASE*]
[**-t** *CERTTYPE* | **--type=**\ \ *CERTTYPE*]
[**-w** *METHOD* | **--wire-method=**\ \ *METHOD*]
[**-v** | **--version**]


Description
===========

**taler-merchant-setup-reserve** is a command-line tool to setup a reserve
(creating the private reserve key) and obtaining the wire transfer information
from the exchange needed to fill the reserve.


Options
=======

**-A** *USERNAME:PASSWORD* \| **--auth=**\ \ *USERNAME:PASSWORD*
   Use ``USERNAME`` and ``PASSWORD`` for HTTP client authentication.
   The ":" must be present as a separator.
   Note that this form of authentication has nothing to do with the TLS client
   certificate authentication supported with the ``-C``, ``-k`` and ``-p`` options.
   The ``PASSWORD`` given to this option is given to the server!

**-a** *VALUE* \| **--amount=**\ \ *VALUE*
   Mandatory.
   Amount to be transferred to the reserve.

**-C** *CERTFILE* \| **--cert=**\ \ *CERTFILE*
   The specified ``CERTFILE`` contains a TLS client certificate to be used to
   authenticate the client. See also ``-t``.

**-c** *FILENAME* \| **--config=**\ \ *FILENAME*
   Use the configuration and other resources for the merchant to
   operate from ``FILENAME``.

**-e** *URL* \| **--exchange-url=**\ \ *URL*
   Mandatory.
   Use ``URL`` for the exchange base URL.
   This is the exchange where the reserve will be created.
   The currency used in the amount specification must be offered by this exchange.

**-h** \| **--help**
   Print short help on options.

**-k** *KEYFILE* \| **--key=**\ \ *KEYFILE*
   The specified ``KEYFILE`` contains a TLS client private key to be used to
   authenticate the client. See also ``-p`` and ``-C``.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-m** *URL* \| **--merchant-url=**\ \ *URL*
   Mandatory.
   Use ``URL`` as the merchant base URL.
   Should include the path to the instance if the reserve is to be
   created for a non-default instance.

**-p** *KEYFILEPASSPHRASE* \| **--pass=**\ \ *KEYFILEPASSPHRASE*
   The specified ``KEYFILEPASSPHRASE`` is to be used to decrypt the KEYFILE.
   See also ``-k``. Not to be confused with ``-A``.
   The ``KEYFILEPASSPHRASE`` given here is only used locally to decrypt the KEYFILE.

**-t** *CERTTYPE* \| **--type=**\ \ *CERTTYPE*
   The specified CERTFILE contains a TLS client certificate of ``CERTTYPE``.
   Default is ``PEM``. See also ``-C``.

**-w** *METHOD* \| **--wire-method=**\ \ *METHOD*
   Mandatory.
   Which wire method should be used.
   Needed to select the wire transfer method of the exchange.
   The method must be supported by the exchange.
   Typical values would be ``iban`` or ``x-taler-bank``.

**-v** \| **--version**
   Print version information.


See Also
========

taler-merchant-dbinit(1), taler.conf(5)


Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
