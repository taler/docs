taler-auditor-sync(1)
#####################

.. only:: html

   Name
   ====

   **taler-auditor-sync** - create a "safe" synchronized copy of a Taler exchange database for an audit


Synopsis
========

**taler-auditor-sync**
[**-s** *FILENAME* | **--source-configuration=**\ ‌\ *FILENAME*]
[**-d** *FILENAME* | **--destination-configuration=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-b** *SIZE* | **--batch=**\ ‌\ *SIZE*]
[**-t** | **--terminate-when-synchronized**]
[**-v** | **--version**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]

Description
===========

**taler-auditor-sync** is a command-line tool to synchronize the
Taler auditor's database in a safe way from a Taler exchange
database. If the exchange database violates the assumed database
invariants (as expressed by database constraints) or attempts to
DELETE or UPDATE tables (at least those that the auditor relies
upon), **taler-auditor-sync** will not replicate those changes
and instead halt with an error.

Its options are as follows:

**-s** *FILENAME* \| **--source-configuration=**\ ‌\ *FILENAME*
   Use the configuration in *FILENAME* to access the original (source) exchange
   database to copy records from.

**-d** *FILENAME* \| **--destination-configuration=**\ ‌\ *FILENAME*
   Use the configuration in *FILENAME* to access the target (destination) exchange
   database to copy records to.

**-t** \| **--terminate-when-synchronized**
   The program should exit once the two databases are in sync, instead of continuously
   copying more records when the source database is updated.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-b** *SIZE* \| **--batch=**\ ‌\ *SIZE*
   Target number of records to copy in one transaction. Once the databases are
   in sync, the batch size is used to determine how long the process sleeps before
   trying to again synchronize the two databases. Not useful if **-t** is used.

**-v** \| **--version**
   Print version information.

See Also
========

taler-auditor(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
