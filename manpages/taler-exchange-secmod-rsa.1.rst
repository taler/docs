taler-exchange-secmod-rsa(1)
############################

.. only:: html

   Name
   ====

   **taler-exchange-secmod-rsa** - handle private RSA key operations for a Taler exchange


Synopsis
========

**taler-exchange-secmod-rsa**
[**-c** *FILENAME* | **--config=**\ \ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-p** *N* | ,**--parallelism=**\ \ *N*]
[**-T** *USEC* | **--timetravel=**\ \ *USEC*]
[**-t** *TIMESTAMP* | **--time=**\ \ *TIMESTAMP*]
[**-v** | **--version**]


Description
===========

**taler-exchange-secmod-rsa** is a command-line tool to
handle private RSA key operations for a Taler exchange.

FIXME: More details.

Its options are as follows:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the merchant to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**p** *N* \| **--parallelism=**\ \ *N*
   Run with *N* worker threads.

**-T** *USEC* \| **--timetravel=**\ \ *USEC*
   Modify the system time by *USEC* microseconds.
   *USEC* may be prefixed with ``+`` or ``-`` (e.g. ``-T +300``).
   This option is intended for debugging/testing only.

**-t** *TIMESTAMP* \| **--time=**\ \ *TIMESTAMP*
   Pretend it is *TIMESTAMP* for the update.
   *TIMESTAMP* is a human-readable string (e.g., ``YYYY-MM-DD HH:MM:SS``).

**-v** \| **--version**
   Print version information.


See Also
========

taler-exchange-httpd(1).


Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
