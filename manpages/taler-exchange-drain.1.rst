taler-exchange-drain(1)
#########################

.. only:: html

   Name
   ====

   **taler-exchange-drain** - drain profits from exchange escrow account

Synopsis
========

**taler-exchange-drain**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-v** | **--version**]

Description
===========

**taler-exchange-drain** is used to trigger a wire transfer from the exchange's escrow account to a normal (non-escrowed) bank account of the exchange.  The entire drain process is necessary to ensure that the auditor is aware of the
balance changes arising from an exchange making profits from fees.

To use it, you must first create an upload a 'drain' command using **taler-exchange-offline**. Afterwards this command should be run to actually queue the drain. The actual drain will then be executed by **taler-exchange-transfer**.


**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the exchange to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-v** \| **--version**
   Print version information.

See Also
========

taler-exchange-transfer(1), taler-exchange-offline(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
