taler-merchant-wirewatch(1)
###########################

.. only:: html

   Name
   ====

   **taler-merchant-wirewatch** - import bank transfers into Taler merchant backend


Synopsis
========

**taler-merchant-wirewatch**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-t** | **--test**]
[**-v** | **--version**]

Description
===========

**taler-merchant-wirewatch** is a command-line tool to import
information about incoming bank transfers into a Taler merchant
backend. This will allow the merchant backend to validate that
the exchange paid the merchant correctly.

Its options are as follows:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the merchant to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-s** *SECTION* \| **--section=**\ \ *SECTION*
   Configuration section to use. Default is taler-merchant-wirewatch. Needed
   if different processes are used to watch multiple bank accounts (for the
   same instance or different instances).

**-t** \| **--test**
   Run in test mode. Only runs until the current list of bank
   transactions are all imported.

**-v** \| **–version**
   Print version information.

See Also
========

taler-merchant-httpd(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
