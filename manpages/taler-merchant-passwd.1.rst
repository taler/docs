taler-merchant-passwd(1)
########################

.. only:: html

   Name
   ====

   **taler-merchant-passwd** - reset password of existing instance


Synopsis
========

**taler-merchant-passwd**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-i**_*NAME* | **--instance**\ \ *NAME*]
[**-v** | **--version**]
[PASSWORD]

Description
===========

**taler-merchant-passwd** is a command-line tool to reset passwords
of existing Taler instances.  The password must either be specified
as the last command-line argument or in the TALER_MERCHANT_PASSWORD
environment variable.

Its options are as follows:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the merchant to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-i** *NAME* \| **--instance**\ \ *NAME*
   Specifies the name of the instance for which the password should be
   updated. If not given, the "default" instance is modified.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-v** \| **–version**
   Print version information.

See Also
========

taler-merchant-httpd(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
