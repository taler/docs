taler-merchant-webhook(1)
#########################

.. only:: html

   Name
   ====

   **taler-merchant-webhook** - run webhooks of Taler merchant backend


Synopsis
========

**taler-merchant-webhook**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-t** | **--test**]
[**-v** | **--version**]

Description
===========

**taler-merchant-webhook** is a command-line tool to trigger webhooks
scheduled by a Taler merchant backend. It makes the necessary HTTP
requests and updates the Taler merchant database accordingly.

Its options are as follows:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the merchant to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-t** \| **--test**
   Run in test mode. Only runs until there are no more webhooks
   to be executed.

**-v** \| **–version**
   Print version information.

See Also
========

taler-merchant-httpd(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
