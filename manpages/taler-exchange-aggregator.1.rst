taler-exchange-aggregator(1)
############################

.. only:: html

   Name
   ====

   **taler-exchange-aggregator** - aggregate deposits into wire transfers

Synopsis
========

**taler-exchange-aggregator**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-T** *USEC* | **--timetravel**\ \ *USEC*]
[**-t** | **--test**]
[**-v** | **--version**]
[**-y**_|_**--kyc-off**]


Description
===========

**taler-exchange-aggregator** is a command-line tool to run aggregate deposits
to the same merchant into larger wire transfers. The actual transfers are then
done by **taler-exchange-transfer**.

The AGGREGATOR_SHARD_SIZE option can be used to allow multiple aggregator processes to run in parallel and share the load. This is only recommended if a single aggregator is insufficient for the workload.

The aggregator uses a special table to lock shards it is working on. If an aggregator process dies (say due to a power failure), these shard locks may prevent the aggregator from resuming normally. In this case, you must run "taler-exchange-dbinit -s" to release the shard locks before restarting the aggregator.

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the exchange to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-T** *USEC* \| **--timetravel=**\ \ *USEC*
   Modify the system time by *USEC* microseconds.
   *USEC* may be prefixed with ``+`` or ``-`` (e.g. ``-T +300``).
   This option is intended for debugging/testing only.

**-t** \| **--test**
   Run in test mode and exit when idle.

**-v** \| **--version**
   Print version information.

**-y** \| **--kyc-off**
   Run without KYC checks. Talk with your regulator before using this option.


See Also
========

taler-exchange-transfer(1), taler-exchange-closer(1),
taler-exchange-httpd(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
