taler-auditor-httpd(1)
######################

.. only:: html

   Name
   ====

   **taler-auditor-httpd** - HTTP server providing a RESTful API to access a Taler auditor


Synopsis
========

**taler-auditor-httpd**
[**-C** | **--connection-close**]
[**-c** *FILENAME* | **--config=**\ \ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-t** *SECONDS* | **--timeout**\ \ *SECONDS*]
[**-v** | **--version**]


Description
===========

**taler-auditor-httpd** is a command-line tool to run the Taler auditor
(HTTP backend).  The required configuration and database must exist
before running this command.


Options
=======

**-C** \| **--connection-close**
   Force each HTTP connection to be closed after each request
   (useful in combination with -f to avoid having to wait for nc to
   time out).

**-c** *FILENAME* \| **--config=**\ \ *FILENAME*
   Use the configuration and other resources for the auditor to
   operate from FILENAME.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-t** *SECONDS* \| **--timeout=**\ \ *SECONDS*
   Specifies the number of *SECONDS* after which the HTTPD should close
   (idle) HTTP connections.

**-v** \| **--version**
   Print version information.


Signals
=======

SIGTERM
       Sending a SIGTERM to the process will cause it to shutdown
       cleanly.


See Also
========

taler-auditor-dbinit(1), taler-auditor(1), taler.conf(5).


Bugs
====

Report bugs by using Mantis https://bugs.taler.net/ or by sending
electronic mail to <taler@gnu.org>
