taler-merchant-httpd(1)
#######################

.. only:: html

   Name
   ====

   **taler-merchant-httpd** - Run Taler merchant backend (with RESTful API)


Synopsis
========

**taler-merchant-httpd**
[**-a**_|_**--auth**]
[**-C** | **--connection-close**]
[**-c** *FILENAME* | **--config=**\ \ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-T** *USEC* | **--timetravel**\ \ *USEC*]
[**-v** | **--version**]

Description
===========

**taler-merchant-httpd** is a command-line tool to run the Taler merchant
(HTTP backend).  The required configuration and database must exist
before running this command.


Options
=======

**-a** *TOKEN* \| **--auth=**\ \ *TOKEN*
   Use TOKEN for initial access control to the merchant backend. TOKEN must start with the "secret-token:" prefix, as per RFC 8959.  The value
   given in TOKEN must appear in backoffice requests to the default instance
   of the merchant, i.e. "Authorization: Bearer TOKEN" to obtain
   access to the merchant backend.  Note that setting a passphrase for the
   default instance by any means will block future access via TOKEN.  This
   is basically a way to reset the passphrase protecting access.  TOKEN
   should be a "pchar" as per RFC 8959, but this is NOT checked.  Note that
   TOKEN will only grant access to the 'default' instance, not other instances.
   Instead of using the command-line, which exposes TOKEN to users on the
   system, you may want to consider setting the
   TALER_MERCHANT_TOKEN environment variable instead.

**-C** \| **--connection-close**
   Force each HTTP connection to be closed after each request
   (useful in combination with -f to avoid having to wait for nc to
   time out).

**-c** *FILENAME* \| **--config=**\ \ *FILENAME*
   Use the configuration and other resources for the merchant to
   operate from FILENAME.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-T** *USEC* \| **--timetravel=**\ \ *USEC*
   Modify the system time by *USEC* microseconds.
   *USEC* may be prefixed with ``+`` or ``-`` (e.g. ``-T +300``).
   This option is intended for debugging/testing only.

**-v** \| **--version**
   Print version information.


Signals
========

SIGTERM
       Sending a SIGTERM to the process will cause it to shutdown
       cleanly.


Environment Variables
=====================

TALER_MERCHANT_TOKEN
       Like the "-a" option, resets the access token for the default
       instance to the given value.


See Also
========

taler-merchant-dbinit(1), taler-merchant-setup-reserve(1), taler.conf(5)


Bugs
====

Report bugs by using Mantis https://bugs.taler.net/ or by sending
electronic mail to <taler@gnu.org>
