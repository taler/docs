taler-merchant-dbinit(1)
########################

.. only:: html

   Name
   ====

   **taler-merchant-dbinit** - initialize Taler merchant database


Synopsis
========

**taler-merchant-dbinit**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-r** | **--reset**]
[**-v** | **--version**]

Description
===========

**taler-merchant-dbinit** is a command-line tool to initialize the Taler
merchant database. It creates the necessary tables and indices for the
Taler merchant to operate.

Its options are as follows:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the merchant to operate
   from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-r** \| **--reset**
   Drop tables. Dangerous, will delete all existing data in the database
   before creating the tables.

**-v** \| **–version**
   Print version information.

See Also
========

taler-merchant-httpd(1), taler-merchant-setup-reserve(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
