sync-httpd(1)
#############

.. only:: html

   Name
   ====

   **sync-httpd** - provide the Sync HTTP interface


Synopsis
========

**sync-httpd**
[**-A** *USERNAME:PASSWORD* | **--auth=**\ \ *USERNAME:PASSWORD*]
[**-C** | **--connection-close**]
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-h** | **--help**]
[**-k** *KEYFILE* | **--key=**\ \ *KEYFILE*]
[**-L** *LOGLEVEL* | **--log=**\ \ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ \ *FILENAME*]
[**-p** *KEYFILEPASSPHRASE* | **--pass=**\ \ *KEYFILEPASSPHRASE*]
[**-t** *CERTTYPE* | **--type=**\ \ *CERTTYPE*]
[**-v** | **--version**]


Description
===========

**sync-httpd** is a command-line tool to provide the Sync HTTP interface.

Its options are as follows:

**-A** *USERNAME:PASSWORD* \| **--auth=**\ \ *USERNAME:PASSWORD*
   Use the given *USERNAME* and *PASSWORD* for client authentication.

**-C** \| **--connection-close**
   Force HTTP connections to be closed after each request.

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the Sync commands
   to operate from *FILENAME*.

**-h** \| **--help**
   Print short help on options.

**-k** *KEYFILE* \| **--key=**\ \ *KEYFILE*
   Consult *KEYFILE* for the private TLS key for TLS client authentication.

**-L** *LOGLEVEL* \| **--log=**\ \ *LOGLEVEL*
   Configure logging to use *LOGLEVEL*.

**-l** *FILENAME* \| **--logfile=**\ \ *FILENAME*
   Configure logging to write logs to *FILENAME*.

**-p** *KEYFILEPASSPHRASE* \| **--pass=**\ \ *KEYFILEPASSPHRASE*
   Use *KEYFILEPASSPHRASE* to decrypt the TLS client private key file.

**-t** *CERTTYPE* \| **--type=**\ \ *CERTTYPE*
   Use *CERTTYPE* as the type of the TLS client certificate.
   If unspecified, defaults to PEM.

**-v** \| **–version**
   Print version information.


See Also
========

sync-config(1), sync-dbinit(1), sync.conf(5).


Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
