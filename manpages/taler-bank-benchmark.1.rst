taler-bank-benchmark(1)
#######################

.. only:: html

   Name
   ====

   **taler-bank-benchmark** - benchmark only the 'bank' and the 'taler-exchange-wirewatch' tool

Synopsis
========

**taler-bank-benchmark**
[**-c** *FILENAME* | **--config=**\ ‌\ *FILENAME*]
[**-f** | **--fakebank**]
[**-h** | **--help**]
[**-L** *LOGLEVEL* | **--loglevel=**\ ‌\ *LOGLEVEL*]
[**-l** *FILENAME* | **--logfile=**\ ‌\ *FILENAME*]
[**-p** *NPROCS* | **--worker-parallelism=**\ \ *NPROCS*]
[**-r** *NRESERVES* | **--reserves=**\ \ *NRESERVES*]
[**-u** *SECTION* | **--exchange-account-section=**\ \ *SECTION*]
[**-V** | **--verbose**]
[**-v** | **--version**]
[**-w**_*NPROC* | **--wirewatch=**\ \ *NPROC*]


Description
===========

**taler-bank-benchmark** is a command-line tool to benchmark only the "bank"
and the ``taler-exchange-wirewatch`` tool.

The options for **taler-bank-benchmark** are:

**-c** *FILENAME* \| **--config=**\ ‌\ *FILENAME*
   Use the configuration and other resources for the merchant to operate
   from *FILENAME*.

**-f** \| **--fakebank**
   Expect to be run against a fakebank (instead of against libeufin)

**-h** \| **--help**
   Print short help on options.

**-L** *LOGLEVEL* \| **--loglevel=**\ ‌\ *LOGLEVEL*
   Specifies the log level to use. Accepted values are: ``DEBUG``, ``INFO``,
   ``WARNING``, ``ERROR``.

**-l** *FILENAME* \| **--logfile=**\ ‌\ *FILENAME*
   Send logging output to *FILENAME*.

**-m** *MODE* \| **--mode=**\ \ *MODE*
   Run as ``bank``, ``client`` or ``both``.
   If unspecified, *MODE* defaults to ``both``.

**-p** *NPROCS* \| **--worker-parallelism=**\ \ *NPROCS*
   Run with *NPROCS* client processes.

**-r** *NRESERVES* \| **--reserves=**\ \ *NRESERVES*
   Create *NRESERVES* reserves per client.

**-u** *SECTION* \| **--exchange-account-section=**\ \ *SECTION*
   Use *SECTION* as the name of the configuration section which specifies the exchange bank account.

**-V** \| **--verbose**
   Display more output than usual.

**-v** \| **--version**
   Print version information.

**-w** *NPROC* \| **--wirewatch=**\ \ *NPROC*
   Run *NPROC* processes of the ``taler-exchange-wirewatch`` tool.


See Also
========

taler-exchange-httpd(1), taler.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
