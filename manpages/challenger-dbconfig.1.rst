challenger-dbconfig(1)
######################

.. only:: html

   Name
   ====

   **challenger-dbconfig** - configure challenger database


Synopsis
========

**challenger-dbconfig**
[**-c** *FILENAME*]
[**-h**]
[**-n** *NAME*]
[**-r**]
[**-s**]
[**-u** *USER*]

Description
===========

**challenger-dbconfig** is a simple shell script that configures
a Postgresql database for use by ``challenger-httpd``.

Its options are as follows:

**-c** *FILENAME*
   Write the database configuration to FILENAME. The tool
   will append the required ``CONFIG`` option for the
   Postgresql access to the respective file.

**-h**
   Print short help on options.

**-n** *DBNAME*
   Use DBNAME for the name of the created database.

**-r**
   Reset any existing database. Looses all existing data. DANGEROUS.

**-s**
   Skip database initialization. Useful if you want to run
   ``challenger-dbinit`` manually.

**-u** *USER*
   Specifies the (main) challenger user that will access the database.

See Also
========

challenger-dbinit(1), challenger.conf(5).

Bugs
====

Report bugs by using https://bugs.taler.net or by sending electronic
mail to <taler@gnu.org>.
