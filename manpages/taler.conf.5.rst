taler.conf(5)
#############

.. only:: html

   Name
   ====

   **taler.conf** - Taler configuration file


Description
===========

.. include:: ../frags/common-conf-syntax.rst

Files containing default values for many of the options described below
are installed under ``$TALER_PREFIX/share/taler/config.d/``.
The configuration file given with **-c** to Taler binaries
overrides these defaults.

A configuration file may include another, by using the ``@INLINE@`` directive,
for example, in ``main.conf``, you could write ``@INLINE@ sub.conf`` to
include the entirety of ``sub.conf`` at that point in ``main.conf``.

Be extra careful when using ``taler-config -V VALUE`` to change configuration
values: it will destroy all uses of ``@INLINE@`` and furthermore remove all
comments from the configuration file!


GLOBAL OPTIONS
--------------

The following options are from the “[taler]” section and used by
virtually all Taler components. Note: work is ongoing to obsolete
these options, but for now they are in use.


CURRENCY
  Name of the currency, e.g. “EUR” for Euro.

CURRENCY_ROUND_UNIT
  Smallest amount in this currency that can be transferred using the
  underlying RTGS. For example: "EUR:0.01" or "JPY:1".



The “[PATHS]” section is special in that it contains paths that can be
referenced using “$” in other configuration values that specify
filenames. For Taler, it commonly contains the following paths:

TALER_HOME
  Home directory of the user, usually “${HOME}”. Can be overwritten by
  testcases by setting ${TALER_TEST_HOME}.

TALER_DATA_HOME
  Where should Taler store its long-term data.
  Usually “${TALER_HOME}/.local/share/taler/”.

TALER_CONFIG_HOME
  Where is the Taler configuration kept.
  Usually “${TALER_HOME}/.config/taler/”.

TALER_CACHE_HOME
  Where should Taler store cached data.
  Usually “${TALER_HOME}/.cache/taler/”.

TALER_RUNTIME_DIR
  Where should Taler store system runtime data (like UNIX domain
  sockets). Usually “${TMP}/taler-system-runtime”.


CURRENCY SPECIFICATIONS
-----------------------

Sections with a name of the form “[currency-$NAME]” (where "$NAME" could
be any unique string) are used to specify details about how currencies
should be handled (and in particularly rendered) by the user interface.
A detailed motivation for this section can be found in DD51.
Different components can have different rules for the same currency. For
example, a bank or merchant may decide to render Euros or Dollars with
always exactly two fractional decimals, while an Exchange for the same
currency may support additional decimals.  The required options in each
currency specification section are:

ENABLED
  Set to YES or NO. If set to NO, the currency specification
  section is ignored. Can be used to disable currencies or
  select alternative sections for the same CODE with different
  choices.

CODE
  Code name for the currency. Can be at most 11 characters,
  only the letters A-Z are allowed. Primary way to identify
  the currency in the protocol.

NAME
  Long human-readable name for the currency. No restrictions,
  but should match the official name in English.

FRACTIONAL_INPUT_DIGITS
  Number of fractional digits that users are allowed to enter
  manually in the user interface.

FRACTIONAL_NORMAL_DIGITS
  Number of fractional digits that will be rendered normally
  (in terms of size and placement).  Digits shown beyond this
  number will typically be rendered smaller and raised (if
  possible).

FRACTIONAL_TRAILING_ZERO_DIGITS
  Number of fractional digits to pad rendered amounts with
  even if these digits are all zero. For example, use 2 to
  render 1 USD as $1.00.

ALT_UNIT_NAMES
  JSON map determining how to encode very large or very tiny
  amounts in this currency. Maps a base10 logarithm to the
  respective currency symbol. Must include at least an
  entry for 0 (currency unit).  For example, use
  {"0":"€"} for Euros or "{"0":"$"} for Dollars. You could
  additionally use {"0":"€","3":"k€"} to render 3000 EUR
  as 3k€. For BTC a typical map would be
  {"0":"BTC","-3":"mBTC"}, informing the UI to render small
  amounts in milli-Bitcoin (mBTC).


EXCHANGE OPTIONS
----------------

The following options are from the “[exchange]” section and used by most
exchange tools.

DB
  Plugin to use for the database, e.g. “postgres”.

SERVE
  Should the HTTP server listen on a UNIX domain socket (set option to "unix") or on a TCP socket (set option to "tcp")?

UNIXPATH
  Path to listen on if we "SERVE" is set to "unix".

UNIXPATH_MODE
  Access permission mask to use for the "UNIXPATH".

PORT
  Port on which the HTTP server listens, e.g. 8080.

BIND_TO
  Hostname to which the exchange HTTP server should be bound to, e.g. "localhost".

MASTER_PUBLIC_KEY
  Crockford Base32-encoded master public key, public version of the
  exchange's long-time offline signing key.

AML_THRESHOLD
  Largest amount in this currency that can be transferred per month without
  an AML staff member doing a (manual) AML check. For example: "USD:1000000".

KYC_AML_TRIGGER
  Program to run on KYC attribute data to decide whether we should immediately flag an account for AML review. Program must return 0 if a manual AML review is not needed, and non-zero to trigger an AML review. The KYC attribute data of the new user will be passed on standard-input.

ENABLE_REWARDS
  This option can be used to announce that an exchange does not allow
  the use of the reserves for rewards. The default is YES which means
  that rewards are allowed.  The option merely announces that
  rewards is enabled or disabled, and protocol-compliant merchant
  backends will then enable or disable the feature accordingly.

STEFAN_ABS
  Absolte amount to add as an offset in the STEFAN fee approximation
  curve (see DD47).  Defaults to CURRENCY:0 if not specified.

STEFAN_LOG
  Amount to multiply by the base-2 logarithm of the total amount
  divided by the amount of the smallest denomination
  in the STEFAN fee approximation curve (see DD47).
  Defaults to CURRENCY:0 if not specified.

STEFAN_LIN
  Linear floating point factor to be multiplied by the total amount
  to use in the STEFAN fee approximation curve (see DD47).
  Defaults to 0.0 if not specified.

BASE_URL
  The base URL under which the exchange can be reached.
  Added to wire transfers to enable tracking by merchants.
  Used by the KYC logic when interacting with OAuth 2.0.

AGGREGATOR_IDLE_SLEEP_INTERVAL
  For how long should the taler-exchange-aggregator sleep when it is idle
  before trying to look for more work? Default is 60 seconds.

CLOSER_IDLE_SLEEP_INTERVAL
  For how long should the taler-exchange-closer sleep when it is idle
  before trying to look for more work? Default is 60 seconds.

TRANSFER_IDLE_SLEEP_INTERVAL
  For how long should the taler-exchange-transfer sleep when it is idle
  before trying to look for more work? Default is 60 seconds.

WIREWATCH_IDLE_SLEEP_INTERVAL
  For how long should the taler-exchange-wirewatch sleep when it is idle
  before trying to look for more work? Default is 60 seconds.

AGGREGATOR_SHARD_SIZE
  Which share of the range from [0,..2147483648] should be processed by one of the shards of the aggregator. Useful only for Taler exchanges with ultra high-performance needs. When changing this value, you must stop all aggregators and run "taler-exchange-dbinit -s" before resuming. Default is 2147483648 (no sharding).

SIGNKEY_LEGAL_DURATION
  For how long are signatures with signing keys legally valid?

MAX_KEYS_CACHING
  For how long should clients cache ``/keys`` responses at most?

MAX_REQUESTS
  How many requests should the HTTP server process at most before committing suicide?

TERMS_DIR
  Directory where the terms of service of the exchange operator can be fund.
  The directory must contain sub-directories for every supported language,
  using the two-character language code in lower case, e.g. "en/" or "fr/".
  Each subdirectory must then contain files with the terms of service in
  various formats.  The basename of the file of the current policy must be
  specified under ``TERMS_ETAG``.  The extension defines the mime type.
  Supported extensions include "html", "htm", "txt", "pdf", "jpg", "jpeg",
  "png" and "gif".  For example, using a ``TERMS_ETAG`` of "0", the structure
  could be the following:

  - $TERMS_DIR/en/0.pdf
  - $TERMS_DIR/en/0.html
  - $TERMS_DIR/en/0.txt
  - $TERMS_DIR/fr/0.pdf
  - $TERMS_DIR/fr/0.html
  - $TERMS_DIR/de/0.txt

TERMS_ETAG
  Basename of the file(s) in the ``TERMS_DIR`` with the current terms of service.
  The value is also used for the "Etag" in the HTTP request to control
  caching.  Whenever the terms of service change, the ``TERMS_ETAG`` MUST also
  change, and old values MUST NOT be repeated.  For example, the date or
  version number of the terms of service SHOULD be used for the Etag.  If
  there are minor (e.g. spelling) fixes to the terms of service, the
  ``TERMS_ETAG`` probably SHOULD NOT be changed. However, whenever users must
  approve the new terms, the ``TERMS_ETAG`` MUST change.

PRIVACY_DIR
  Works the same as ``TERMS_DIR``, just for the privacy policy.

PRIVACY_ETAG
  Works the same as ``TERMS_ETAG``, just for the privacy policy.


EXCHANGE KYC PROVIDER OPTIONS
-----------------------------

The following options must be in the section "[kyc-provider-XXX]" sections.

COST
  Relative cost of the KYC provider, non-negative number.

LOGIC
  API type of the KYC provider.

USER_TYPE
  Type of user this provider is for, either INDIVIDUAL or BUSINESS.

PROVIDED_CHECKS
  List of checks performed by this provider. Space-separated names of checks, must match check names in legitimization rules.


EXCHANGE KYC OAUTH2 OPTIONS
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following options must be in the section "[kyc-provider-XXX]" sections with "LOGIC = oauth2".


KYC_OAUTH2_VALIDITY
  Duration (e.g. "12 months") of the validity of the performed KYC check. Can be "forever".

KYC_OAUTH2_AUTHORIZE_URL
  URL of the OAuth2 endpoint to be used for KYC checks. The authorize URL is where the exchange will redirect the client to begin the authorization process.  Example: "http://localhost:8888/oauth/v2/authorize". To use the plugin in combination with the Challenger service's ``/setup`` step, append "#setup", thus "https://challenger.example.com/authorize#setup".  Here, "#setup" is not a fragment but merely a hint to the logic to determine the full authorization URL via the ``/setup/$CLIENT_ID`` handler.

KYC_OAUTH2_TOKEN_URL
  URL of the OAuth2 endpoint to be used for KYC checks. This is where the server will ultimately send the authorization token from the client and obtain its access token (which currently must be a "bearer" token). Example: "http://localhost:8888/oauth/v2/token" (or just "/token")

KYC_OAUTH2_INFO_URL
  URL of the OAuth2-protected resource endpoint, where the OAuth 2.0 token can be used to download information about the user that has undergone the KYC process. The exchange will use the access token obtained from the KYC_AUTH2_AUTH_URL to show that it is authorized to obtain the details. Example: "http://localhost:8888/api/user/me" or "http://localhost:8888/oauth/v2/info"

KYC_OAUTH2_CLIENT_ID
  Client ID of the exchange when it talks to the KYC OAuth2 endpoint.

KYC_OAUTH2_CLIENT_SECRET
  Client secret of the exchange to use when talking to the KYC Oauth2 endpoint.

KYC_OAUTH2_POST_URL
  URL to which the exchange will redirect the client's browser after successful authorization/login for the KYC process. Example: "http://example.com/thank-you"

KYC_OAUTH2_CONVERTER_HELPER
  Helper to convert JSON with KYC data returned by the OAuth2.0 info endpoint into GNU Taler internal format. Specific to the OAuth 2.0 provider.


EXCHANGE KYC KYCAID OPTIONS
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following options must be in the section "[kyc-provider-XXX]" sections with "LOGIC = kycaid".


KYC_KYCAID_VALIDITY
  Duration (e.g. "12 months") of the validity of the performed KYC check. Can be "forever".

KYC_KYCAID_AUTH_TOKEN
  Authentication token to access the KYC service.

KYC_KYCAID_FORM_ID
  ID that specifies the form to use for the KYC process.

KYC_KYCAID_POST_URL
  URL to which the exchange will redirect the client's browser after successful authorization/login for the KYC process.


EXCHANGE KYC PERSONA OPTIONS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following options must be in the section "[kyc-provider-XXX]" sections with "LOGIC = persona".


KYC_PERSONA_VALIDITY
  Duration (e.g. "12 months") of the validity of the performed KYC check. Can be "forever".

KYC_PERSONA_AUTH_TOKEN
  Authentication token to access the KYC service.

KYC_PERSONA_SALT
  Salt value to use for request idempotency. Optional, generated at random per process if not given.

KYC_PERSONA_SUBDOMAIN
  Subdomain to use under Persona.

KYC_PERSONA_CONVERTER_HELPER
  Helper to convert JSON with KYC data returned by Persona into GNU Taler internal format. Should probably always be set to "taler-exchange-kyc-persona-converter.sh".

KYC_PERSONA_POST_URL
  URL to which the exchange will redirect the client's browser after successful authorization/login for the KYC process.

KYC_PERSONA_TEMPLATE_ID
  ID of the Persona template to use.

EXCHANGE KYC PERSONA GLOBAL OPTIONS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following option must be in the section "[kyclogic-persona]".


WEBHOOK_AUTH_TOKEN
  Authentication token Persona must supply to our webhook. This is an optional setting.


EXCHANGE EXTENSIONS OPTIONS
---------------------------

The functionality of the exchange can be extended by extensions.  Those are
shared libraries which implement the extension-API of the exchange and are
located under ``$LIBDIR``, starting with prefix ``libtaler_extension_``. Each
extension can be enabled by adding a dedicated section
"[exchange-extension-<extensionname>]" and the following option:

ENABLED
  If set to ``YES`` the extension ``<extensionsname>`` is enabled.  Extension-specific
  options might be set in the same section.


EXCHANGE EXTENSION FOR AGE RESTRICTION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The extension for age restriction support can be enabled by adding a section
"[exchange-extension-age_restriction]" with the following options:

ENABLE
  Must be set to ``YES`` in order to activate the extension.

AGE_GROUPS
  A colon-seperated string of increasing non-negative integers, defining the
  buckets of age groups supported by the exchange.  Each integer marks the
  beginning of the next age group.  The zero'th age group implicitly starts
  with 0.  For example, the string "10:18" would define three age groups:

    1. Group 0: ages 0 through 9 (including)
    2. Group 1: ages 10 through 17 (including)
    3. Group 2: ages 18 and above

  If not provided, the default value is "8:10:12:14:16:18:21".

**Note**: Age restriction is bound to specific denominations and must be
enabled for each selected denomination in the corresponding section by adding
the option ``AGE_RESTRICTED = YES``, see `EXCHANGE COIN OPTIONS`_. However, the
age groups are defined globally for all denominations.



EXCHANGE OFFLINE SIGNING OPTIONS
--------------------------------

The following options must be in the section "[exchange-offline]".

MASTER_PRIV_FILE
  Location of the master private key on disk. Only used by tools that
  can be run offline (as the master key is for offline signing).
  Mandatory.

SECM_TOFU_FILE
  Where to store the public keys of both crypto helper modules.
  Used to persist the keys after the first invocation of the tool,
  so that if they ever change in the future, this is detected and
  the tool can abort.
  Mandatory.

SECM_DENOM_PUBKEY
  Public key of the (RSA) crypto helper module. Optional. If not given,
  we will rely on TOFU.  Note that once TOFU has been established,
  this option will also be ignored.

SECM_ESIGN_PUBKEY
  Public key of the (EdDSA) crypto helper module. Optional. If not given,
  we will rely on TOFU.  Note that once TOFU has been established,
  this option will also be ignored.


EXCHANGE RSA CRYPTO HELPER OPTIONS
----------------------------------

The following options must be in the section "[taler-exchange-secmod-rsa]".

LOOKAHEAD_SIGN
  How long do we generate denomination and signing keys ahead of time?

OVERLAP_DURATION
  How much should validity periods for coins overlap?
  Should be long enough to avoid problems with
  wallets picking one key and then due to network latency
  another key being valid.  The ``DURATION_WITHDRAW`` period
  must be longer than this value.

SM_PRIV_KEY
  Where should the security module store its long-term private key?

KEY_DIR
  Where should the security module store the private keys it manages?

UNIXPATH
  On which path should the security module listen for signing requests?

Note that the **taler-exchange-secmod-rsa** also evaluates the ``[coin_*]``
configuration sections described below.


EXCHANGE CS CRYPTO HELPER OPTIONS
---------------------------------

The following options must be in the section "[taler-exchange-secmod-cs]".

LOOKAHEAD_SIGN
  How long do we generate denomination and signing keys ahead of time?

OVERLAP_DURATION
  How much should validity periods for coins overlap?
  Should be long enough to avoid problems with
  wallets picking one key and then due to network latency
  another key being valid.  The ``DURATION_WITHDRAW`` period
  must be longer than this value.

SM_PRIV_KEY
  Where should the security module store its long-term private key?

KEY_DIR
  Where should the security module store the private keys it manages?

UNIXPATH
  On which path should the security module listen for signing requests?

Note that the **taler-exchange-secmod-cs** also evaluates the ``[coin_*]``
configuration sections described below.



EXCHANGE EDDSA CRYPTO HELPER OPTIONS
------------------------------------

The following options must be in the section "[taler-exchange-secmod-eddsa]".

LOOKAHEAD_SIGN
  How long do we generate denomination and signing keys ahead of time?

OVERLAP_DURATION
  How much should validity periods for coins overlap?
  Should be long enough to avoid problems with
  wallets picking one key and then due to network latency
  another key being valid.  The ``DURATION_WITHDRAW`` period
  must be longer than this value.

DURATION
  For how long should EdDSA keys be valid for signing?

SM_PRIV_KEY
  Where should the security module store its long-term private key?

KEY_DIR
  Where should the security module store the private keys it manages?

UNIXPATH
  On which path should the security module listen for signing requests?



EXCHANGE DATABASE OPTIONS
-------------------------

The following options must be in the section "[exchangedb]".

IDLE_RESERVE_EXPIRATION_TIME
  After which time period should reserves be closed if they are idle?

LEGAL_RESERVE_EXPIRATION_TIME
  After what time do we forget about (drained) reserves during garbage collection?

AGGREGATOR_SHIFT
  Delay between a deposit being eligible for aggregation and
  the aggregator actually triggering.

DEFAULT_PURSE_LIMIT
  Number of concurrent purses that a reserve may have active
  if it is paid to be opened for a year.


EXCHANGE POSTGRES BACKEND DATABASE OPTIONS
------------------------------------------

The following options must be in section “[exchangedb-postgres]” if the
“postgres” plugin was selected for the database.

CONFIG
  How to access the database, e.g. “postgres:///taler” to use the
  “taler” database. Testcases use “talercheck”.


EXCHANGE ACCOUNT OPTIONS
------------------------

An exchange (or merchant) can have multiple bank accounts. The following
options are for sections named “[exchange-account-SOMETHING]”. The ``SOMETHING`` is
arbitrary and should be chosen to uniquely identify the bank account for
the operator.  These options are used by the **taler-exchange-aggregator**, **taler-exchange-closer**, **taler-exchange-transfer** and **taler-exchange-wirewatch** tools.

PAYTO_URI
  Specifies the payto://-URL of the account. The general format is
  ``payto://$METHOD/$DETAILS``.  Examples:
  ``payto://x-taler-bank/localhost:8899/Exchange`` or
  ``payto://iban/GENODEF1SLR/DE67830654080004822650/`` or
  ``payto://iban/DE67830654080004822650/`` (providing the BIC is optional).
  Note: only the wire-method is actually used from the URI.

ENABLE_DEBIT
  Must be set to ``YES`` for the accounts that the
  **taler-exchange-aggregator** and **taler-exchange-closer** should debit.

ENABLE_CREDIT
  Must be set to ``YES`` for the accounts that the **taler-exchange-wirewatch**
  should check for credits. It is yet uncertain if the merchant
  implementation may check this flag as well.


Additionally, for each enabled account there MUST be another matching section named “[exchange-accountcredentials-SOMETHING]”.  This section SHOULD be in a ``secret/`` configuration file that is only readable for the **taler-exchange-wirewatch** and **taler-exchange-transfer** processes. It contains the credentials to access the bank account:

WIRE_GATEWAY_URL
  URL of the wire gateway.  Typically of the form
  ``https://$HOSTNAME[:$PORT]/taler-wire-gateway/$USERNAME/``
  where $HOSTNAME is the hostname of the system running the bank
  (such as the Taler Python bank or the Nexus) and ``$USERNAME`` is
  the username of the exchange's bank account (usually matching
  the ``USERNAME`` option used for authentication).  Example:
  ``https://bank.demo.taler.net/taler-wire-gateway/Exchange/``.

WIRE_GATEWAY_AUTH_METHOD
  This option determines how the exchange (auditor/wirewatch/aggregator)
  authenticates with the wire gateway.  Choices are ``basic`` and ``none``.

USERNAME
  User name for ``basic`` authentication with the wire gateway.

PASSWORD
  Password for ``basic`` authentication with the wire gateway.



EXCHANGE COIN OPTIONS
---------------------

The following options must be in sections starting with ``"[coin_]"`` and are
largely used by **taler-exchange-httpd** to determine the meta data for the
denomination keys.  Some of the options are used by the
**taler-exchange-secmod-rsa** to determine which RSA keys to create (and of
what key length).  Note that the section names must match, so this part of the
configuration MUST be shared between the RSA helper and the exchange.
Configuration values MUST NOT be changed in a running setup. Instead, if
parameters for a denomination type are to change, a fresh *section name* should
be introduced (and the existing section should be deleted).


VALUE
   Value of the coin, e.g. “EUR:1.50” for 1 Euro and 50 Cents (per
   coin).

DURATION_WITHDRAW
   How long should the same key be used for clients to withdraw coins of
   this value?

DURATION_SPEND
   How long do clients have to spend these coins?

DURATION_LEGAL
   How long does the exchange have to keep records for this denomination?

FEE_WITHDRAW
   What fee is charged for withdrawal?

FEE_DEPOSIT
   What fee is charged for depositing?

FEE_REFRESH
   What fee is charged for refreshing?

FEE_REFUND
   What fee is charged for refunds? When a coin is refunded, the deposit
   fee is returned. Instead, the refund fee is charged to the customer.

CIPHER
   What cryptosystem should be used? Must be set to either "CS" or "RSA".
   The respective crypto-helper will then generate the keys for this
   denomination.

RSA_KEYSIZE
   What is the RSA keysize modulos (in bits)?  Only used if "CIPHER=RSA".

AGE_RESTRICTED
   Setting this option to ``YES`` marks the denomination as age restricted
   (default is ``NO``).  For this option to be accepted the extension for age
   restriction MUST be enabled (see `EXCHANGE EXTENSION FOR AGE RESTRICTION`_).


MERCHANT OPTIONS
----------------

The following options are from the “[merchant]” section and used by the
merchant backend.

DB
   Plugin to use for the database, e.g._“postgres”.

SERVE
  Should the HTTP server listen on a UNIX domain socket (set option to "unix") or on a TCP socket (set option to "tcp")?

BASE_URL
  Which base URL should the merchant backend assume for itself in the protocol. Optional. If not given, the base URL will be constructed from X-Forwarded-Host, X-Forwarded-Port and X-Forwarded-Prefix headers that a reverse-proxy should be setting.

UNIXPATH
  Path to listen on if we "SERVE" is set to "unix".

UNIXPATH_MODE
  Access permission mask to use for the "UNIXPATH".

PORT
   Port on which the HTTP server listens, e.g. 8080.

BIND_TO
  Hostname to which the merchant HTTP server should be bound to, e.g. "localhost".

LEGAL_PRESERVATION
   How long do we keep data in the database for tax audits after the
   transaction has completed?  Default is 10 years.

FORCE_AUDIT
   Force the merchant to report every transaction to the auditor
   (if the exchange has an auditor)?  Default is ``NO``.
   Do not change except for testing.


MERCHANT POSTGRES BACKEND DATABASE OPTIONS
------------------------------------------

The following options must be in section “[merchantdb-postgres]” if the
“postgres” plugin was selected for the database.

CONFIG
   How to access the database, e.g. “postgres:///taler” to use the
   “taler” database. Testcases use “talercheck”.


KNOWN EXCHANGES (for merchants)
-------------------------------

The merchant configuration can include a list of known exchanges if the
merchant wants to specify that certain exchanges are explicitly trusted.
For each trusted exchange, a section [merchant-exchange-$NAME] must exist, where
$NAME is a merchant-given name for the exchange. The following options
must be given in each “[exchange-$NAME]” section.

EXCHANGE_BASE_URL
   Base URL of the exchange, e.g. “https://exchange.demo.taler.net/”

MASTER_KEY
   Crockford Base32 encoded master public key, public version of the
   exchange's long-time offline signing key.  Can be omitted, in that
   case the exchange will NOT be trusted unless it is audited by
   a known auditor.
   Omitting ``MASTER_KEY`` can be useful if we do not trust the exchange
   without an auditor, but should pre-load the keys of this
   particular exchange on startup instead of waiting for it to be
   required by a client.

CURRENCY
   Name of the currency for which this exchange is used, e.g. “KUDOS”.
   The entire section is ignored if the currency does not match the currency
   we use, which must be given in the ``[taler]`` section.

DISABLED
   Set to YES to disable this exchange. Optional option, defaults to NO.


AUDITOR OPTIONS
---------------

The following options must be in section “[auditor]” for the Taler
auditor.

DB
  Plugin to use for the database, e.g. “postgres”

AUDITOR_PRIV_FILE
  Name of the file containing the auditor’s private key.

PUBLIC_KEY
  Crockford Base32 encoded auditor public key.  Used by (online) auditor
  processes that do not have access to the (offline) auditor private key file.

BASE_URL
  Base URL of the auditor, e.g. “https://auditor.demo.taler.net/”

SERVE
  Should the HTTP server listen on a UNIX domain socket (set option to "unix") or on a TCP socket (set option to "tcp")?

UNIXPATH
  Path to listen on if we "SERVE" is set to "unix".

UNIXPATH_MODE
  Access permission mask to use for the "UNIXPATH".

PORT
   Port on which the HTTP server listens, e.g. 8080.

BIND_TO
  Hostname to which the merchant HTTP server should be bound to, e.g. "localhost".




AUDITOR POSTGRES BACKEND DATABASE OPTIONS
-----------------------------------------

The following options must be in section “[auditordb-postgres]” if the
“postgres” plugin was selected for the database.

CONFIG
  How to access the database, e.g. "postgres:///taler" to use the
  "taler" database. Testcases use “talercheck”.


Bank Options
------------

The following options must be in section "[bank]" for the taler-fakebank-run(1) command. They are not used by the exchange or LibEuFin!

HTTP_PORT
  On which TCP port should the (fake)bank offer its REST API.
RAM_LIMIT
  This gives the number of transactions to keep in memory. Older transactions will be overwritten and history requests for overwritten transactions will fail.



SEE ALSO
========

taler-exchange-dbinit(1), taler-exchange-httpd(1),
taler-exchange-offline(1), taler-auditor-offline(1).

BUGS
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
