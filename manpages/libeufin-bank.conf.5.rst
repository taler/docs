libeufin-bank.conf(5)
######################

.. only:: html

   Name
   ====

   **libeufin-bank.conf** - LibEuFin Bank configuration file


Description
===========

.. include:: ../frags/common-conf-syntax.rst

Files containing default values for many of the options described below
are installed under ``$TALER_PREFIX/share/libeufin-bank/config.d/``.
The configuration file given with **-c** to Taler binaries
overrides these defaults.

A configuration file may include another, by using the ``@INLINE@`` directive,
for example, in ``main.conf``, you could write ``@INLINE@ sub.conf`` to
include the entirety of ``sub.conf`` at that point in ``main.conf``.

Be extra careful when using ``taler-config -V VALUE`` to change configuration
values: it will destroy all uses of ``@INLINE@`` and furthermore remove all
comments from the configuration file!

GLOBAL OPTIONS
--------------

The following options are from the “[libeufin-bank]” section.

CURRENCY
  Internal currency of the libeufin-bank, e.g. “EUR” for Euro.

DEFAULT_CUSTOMER_DEBT_LIMIT
  Default debt limit for newly created customer accounts. Defaults to CURRENCY:0 if not specified.

DEFAULT_ADMIN_DEBT_LIMIT
  Default debt limit of the admin. Typically higher, since sign-up bonuses and cashin are deducted from the admin account. Defaults to CURRENCY:0 if not specified.

REGISTRATION_BONUS
  Value of the registration bonus for new users. Defaults to CURRENCY:0 if not specified.

ALLOW_REGISTRATION
  Whether anyone can create a new account or whether this action is reserved for the admin. Defaults to no if not specified.

ALLOW_ACCOUNT_DELETION
  Whether anyone can delete its account or whether this action is reserved for the admin. Defaults to no if not specified.

ALLOW_CONVERSION
  Whether regional currency conversion is enabled. Defaults to no if not specified.

FIAT_CURRENCY
  External currency used during cashin and cashout.
  Only used if ``ALLOW_CONVERSION`` is ``YES``.

TAN_SMS
  Path to TAN challenge transmission script via sms. If not specified, this TAN channel will not be supported.
  Only used if ``ALLOW_CONVERSION`` is ``YES``.

TAN_EMAIL
  Path to TAN challenge transmission script via email. If not specified, this TAN channel will not be supported.
  Only used if ``ALLOW_CONVERSION`` is ``YES``.

SERVE
  This can either be ``tcp`` or ``unix``.

PORT
  Port on which the HTTP server listens, e.g. 9967.
  Only used if ``SERVE`` is ``tcp``.

UNIXPATH
  Which unix domain path should we bind to?
  Only used if ``SERVE`` is ``unix``.

UNIXPATH_MODE
  What should be the file access permissions for ``UNIXPATH``?
  Only used if ``SERVE`` is ``unix``.

DATABASE OPTIONS
----------------

Setting the database belongs to the “[libeufin-bankdb-postgres]” section and the
following value.

CONFIG
  PostgreSQL connection string.

SEE ALSO
========

libeufin-bank(1).

BUGS
====

Report bugs by using https://bugs.taler.net/ or by sending electronic
mail to <taler@gnu.org>.
