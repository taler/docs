############################
GNU Taler Release Checklists
############################

.. |check| raw:: html

    <input type="checkbox">

For exchange:

- |check| no compiler warnings at "-Wall" with gcc
- |check| no compiler warnings at "-Wall" with clang
- |check| ensure Coverity static analysis passes
- |check| make check.
- |check| make dist, make check on result of 'make dist'.
- |check| Change version number in configure.ac.
- |check| update man pages / info page documentation (prebuilt branch)
- |check| make dist for release
- |check| verify dist builds from source
- |check| upgrade 'demo.taler.net'
- |check| run :doc:`demo upgrade checklist <checklist-demo-upgrade>`
- |check| tag repo.
- |check| use 'deployment.git/packaging/\*-docker/' to build Debian and Ubuntu packages
- |check| upload packages to 'deb.taler.net' (note: only Florian/Christian can sign)
- |check| change 'demo.taler.net' deployment to use new tag.
- |check| Upload triplet to ftp-upload.gnu.org/incoming/ftp or /incoming/alpha

For merchant (C backend):

- |check| no compiler warnings at "-Wall" with gcc
- |check| no compiler warnings at "-Wall" with clang
- |check| ensure Coverity static analysis passes
- |check| make check.
- |check| make dist, make check on result of 'make dist'.
- |check| update SPA (prebuilt branch)
- |check| Change version number in configure.ac.
- |check| make dist for release.
- |check| verify dist builds from source
- |check| upgrade 'demo.taler.net'
- |check| run :doc:`demo upgrade checklist <checklist-demo-upgrade>`
- |check| tag repo.
- |check| use 'deployment.git/packaging/\*-docker/' to build Debian and Ubuntu packages
- |check| upload packages to 'deb.taler.net' (note: only Florian/Christian can sign)
- |check| change 'demo.taler.net' deployment to use new tag.
- |check| Upload triplet to ftp-upload.gnu.org/incoming/ftp or /incoming/alpha

For sync:

- |check| no compiler warnings at "-Wall" with gcc
- |check| no compiler warnings at "-Wall" with clang
- |check| ensure Coverity static analysis passes
- |check| make check.
- |check| make dist, make check on result of 'make dist'.
- |check| Change version number in configure.ac.
- |check| make dist for release
- |check| verify dist builds from source
- |check| upgrade 'demo.taler.net'
- |check| run :doc:`demo upgrade checklist <checklist-demo-upgrade>`
- |check| tag repo.
- |check| use 'deployment.git/packaging/\*-docker/' to build Debian and Ubuntu packages
- |check| upload packages to 'deb.taler.net' (note: only Florian/Christian can sign)
- |check| change 'demo.taler.net' deployment to use new tag.
- |check| Upload triplet to ftp-upload.gnu.org/incoming/ftp or /incoming/alpha

For taler-mdb:

- |check| no compiler warnings at "-Wall" with gcc
- |check| ensure Coverity static analysis passes
- |check| Change version number in configure.ac.
- |check| make dist for release.
- |check| tag repo.
- |check| use 'deployment.git/packaging/\*-docker/' to build Debian and Ubuntu packages
- |check| upload packages to 'deb.taler.net' (note: only Florian/Christian can sign)
- |check| Upload triplet to ftp-upload.gnu.org/incoming/ftp or /incoming/alpha

For taler-twister:

- |check| no compiler warnings at "-Wall" with gcc
- |check| no compiler warnings at "-Wall" with clang
- |check| ensure Coverity static analysis passes
- |check| make check.
- |check| make dist, make check on result of 'make dist'.
- |check| Change version number in configure.ac.
- |check| make dist for release.
- |check| verify dist builds from source
- |check| upgrade 'demo.taler.net'
- |check| run :doc:`demo upgrade checklist <checklist-demo-upgrade>`
- |check| tag repo.
- |check| Upload triplet to ftp-upload.gnu.org/incoming/ftp or /incoming/alpha

For libeufin:

- |check| update SPA of bank
- |check| build libeufin
- |check| upgrade 'demo.taler.net'
- |check| run :doc:`demo upgrade checklist <checklist-demo-upgrade>`
- |check| make dist for release.
- |check| verify dist builds from source
- |check| tag repo.
- |check| use 'deployment.git/packaging/\*-docker/' to build Debian and Ubuntu packages
- |check| upload packages to 'deb.taler.net' (note: only Florian/Christian can sign)
- |check| change 'demo.taler.net' deployment to use new tag.
- |check| Upload triplet to ftp-upload.gnu.org/incoming/ftp or /incoming/alpha

For Python merchant frontend:

- |check| upgrade 'demo.taler.net'
- |check| run :doc:`demo upgrade checklist <checklist-demo-upgrade>`
- |check| change 'demo.taler.net' deployment to use new tag.

Wallet-core:

- |check| build wallet
- |check| run integration test
- |check| make dist for release.
- |check| verify dist builds from source
- |check| tag repo.
- |check| use 'deployment.git/packaging/\*-docker/' to build Debian and Ubuntu packages
- |check| upload packages to 'deb.taler.net' (note: only Florian/Christian can sign)
- |check| change 'demo.taler.net' deployment to use new tag.
- |check| Upload triplet to ftp-upload.gnu.org/incoming/ftp or /incoming/alpha

Android-Wallet:

- |check| build wallet
- |check| run :doc:`demo upgrade checklist <checklist-demo-upgrade>`
- |check| tag repo.
- |check| upload new wallet release to app store

Webextension-Wallet:

- |check| build wallet
- |check| run :doc:`demo upgrade checklist <checklist-demo-upgrade>`
- |check| tag repo.
- |check| upload new wallet release to app store

Release announcement:

- |check| Update bug tracker (mark release, resolved -> closed)
- |check| Send announcement to taler@gnu.org
- |check| Send announcement to info-gnu@gnu.org (major releases only)
- |check| Send announcement to coordinator@translationproject.org
